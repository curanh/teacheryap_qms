# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: rm-gs5yo2s8184aj3355.mysql.singapore.rds.aliyuncs.com (MySQL 5.7.30-log)
# Database: tyqms_main_production
# Generation Time: 2021-04-19 03:54:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table api_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api_log`;

CREATE TABLE `api_log` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `function` text COLLATE utf8_unicode_ci NOT NULL,
  `callinput` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sql_txt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_time` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assess`;

CREATE TABLE `assess` (
  `assess_id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(20) NOT NULL,
  `assess1` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assess_score1` varchar(11) NOT NULL,
  `assess2` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assess_score2` varchar(11) NOT NULL,
  `assess3` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assess_score3` varchar(11) NOT NULL,
  `assess4` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assess_score4` varchar(11) NOT NULL,
  `assess5` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assess_score5` varchar(11) NOT NULL,
  `teacher` varchar(100) NOT NULL,
  `reg_no` varchar(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  PRIMARY KEY (`assess_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table assessment_answer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assessment_answer`;

CREATE TABLE `assessment_answer` (
  `assess_id` int(100) NOT NULL,
  `criteria_id` int(100) NOT NULL,
  `score` int(1) NOT NULL,
  `zh_title` text NOT NULL,
  `en_title` text NOT NULL,
  PRIMARY KEY (`assess_id`,`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table assessment_criteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assessment_criteria`;

CREATE TABLE `assessment_criteria` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `zh_title` text NOT NULL,
  `en_title` text NOT NULL,
  `status` varchar(3) NOT NULL DEFAULT '' COMMENT 'yes=active, no=inactive',
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table centre
# ------------------------------------------------------------

DROP TABLE IF EXISTS `centre`;

CREATE TABLE `centre` (
  `fid` int(5) NOT NULL AUTO_INCREMENT,
  `referrer` int(5) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `c_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `located_in` varchar(10) NOT NULL DEFAULT 'Peninsula',
  `company_name` varchar(50) NOT NULL,
  `address_line_1` varchar(100) NOT NULL,
  `address_line_2` varchar(100) NOT NULL,
  `city` varchar(45) NOT NULL,
  `add_state` varchar(45) NOT NULL,
  `postcode` varchar(10) NOT NULL DEFAULT '0',
  `add_country` varchar(45) NOT NULL,
  `zone` varchar(25) DEFAULT NULL,
  `off_telephone` varchar(15) NOT NULL DEFAULT '0',
  `mobile_no` varchar(15) NOT NULL,
  `off_fax` varchar(15) NOT NULL DEFAULT '0',
  `email` varchar(80) NOT NULL DEFAULT '',
  `email2` varchar(80) NOT NULL,
  `sid` varchar(15) NOT NULL DEFAULT '',
  `active` varchar(3) NOT NULL DEFAULT 'Yes',
  `rid` int(8) unsigned DEFAULT NULL,
  `commence_date` date DEFAULT NULL,
  `financial_date` date DEFAULT NULL,
  `financial_day` int(2) NOT NULL,
  `financial_month` int(2) NOT NULL,
  `zid` int(8) unsigned DEFAULT NULL,
  `account_no` varchar(12) DEFAULT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `notice_date` date DEFAULT NULL,
  `agreement_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `company_code` varchar(20) NOT NULL,
  `bank_account` varchar(30) NOT NULL,
  `royalty` decimal(5,2) NOT NULL DEFAULT '10.00',
  `levy` decimal(5,2) NOT NULL DEFAULT '4.00',
  `company_name2` varchar(50) NOT NULL,
  `company_code2` varchar(20) NOT NULL,
  `bank_account2` varchar(30) NOT NULL,
  `yearly_sales_target` decimal(12,2) DEFAULT NULL,
  `yearly_sales_target_2018` decimal(12,2) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table delete_reason
# ------------------------------------------------------------

DROP TABLE IF EXISTS `delete_reason`;

CREATE TABLE `delete_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(20) NOT NULL,
  `level_id` int(11) DEFAULT NULL,
  `r_id` int(20) NOT NULL,
  `deleted_by` varchar(100) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `today_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table edit_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `edit_log`;

CREATE TABLE `edit_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `edit_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `edit_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `r_id` int(20) NOT NULL,
  `language_id` int(11) NOT NULL,
  `level_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_id` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `continue_id` varchar(100) DEFAULT NULL,
  `continue_title` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_room` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `times1` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `number_of_class` int(11) NOT NULL,
  `feedback` varchar(10000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fid` int(11) NOT NULL,
  `reg_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `g_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `insert_date` date NOT NULL,
  `complete` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `trial` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table examresult_backup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `examresult_backup`;

CREATE TABLE `examresult_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `qms_teacher` varchar(100) NOT NULL,
  `original_teacher` varchar(100) NOT NULL,
  `exam_id` int(100) NOT NULL,
  `date1` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(100) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table hqadmin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hqadmin`;

CREATE TABLE `hqadmin` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fid` int(100) NOT NULL,
  `position` varchar(10) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active',
  PRIMARY KEY (`language_id`),
  KEY `status` (`status`),
  KEY `languageid_status` (`language_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `total_class` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active	',
  `inactive` int(10) NOT NULL,
  `sorting` decimal(12,3) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`level_id`),
  KEY `language` (`language_id`),
  KEY `levelid` (`level_id`),
  KEY `lang_dele_sor` (`language_id`,`deleted`,`sorting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table level_title
# ------------------------------------------------------------

DROP TABLE IF EXISTS `level_title`;

CREATE TABLE `level_title` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active	',
  `sorting` decimal(12,2) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`title_id`),
  KEY `levelid_status_deleted` (`level_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table level_title_b
# ------------------------------------------------------------

DROP TABLE IF EXISTS `level_title_b`;

CREATE TABLE `level_title_b` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active	',
  `sorting` decimal(12,2) NOT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;



# Dump of table main_teacher
# ------------------------------------------------------------

DROP TABLE IF EXISTS `main_teacher`;

CREATE TABLE `main_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teachername` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reg_no` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_regno_levelid` (`reg_no`,`level_id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(20) DEFAULT NULL,
  `teachername` varchar(30) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `g_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(50) DEFAULT NULL,
  `title_id` int(50) NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `date1` date DEFAULT NULL,
  `teacher_read` int(11) DEFAULT NULL,
  `buddy_read` int(11) DEFAULT NULL,
  `cm_read` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `noti` (`fid`,`teachername`,`reg_no`,`level_id`,`title_id`,`date1`,`teacher_read`,`buddy_read`,`cm_read`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table personality_answer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `personality_answer`;

CREATE TABLE `personality_answer` (
  `assess_id` int(100) NOT NULL,
  `personality_id` int(100) NOT NULL,
  `zh_title` text NOT NULL,
  `en_title` text NOT NULL,
  `input_value` text,
  PRIMARY KEY (`assess_id`,`personality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table personality_criteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `personality_criteria`;

CREATE TABLE `personality_criteria` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `zh_title` text NOT NULL,
  `en_title` text NOT NULL,
  `status` varchar(3) NOT NULL DEFAULT '' COMMENT 'yes=active, no=inactive',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT 'personality or remarks',
  `input_type` varchar(30) NOT NULL DEFAULT '' COMMENT 'checkbox or textbox',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table qlms_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qlms_level`;

CREATE TABLE `qlms_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `qms_level` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `lms_level` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link1` varchar(100) NOT NULL,
  `link2` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table registration_form
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registration_form`;

CREATE TABLE `registration_form` (
  `form_id` int(20) NOT NULL AUTO_INCREMENT,
  `fid` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `englishname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chinesename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `student_ic` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `line2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` int(20) NOT NULL,
  `homephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `birthdate` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `allergy` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `emergencynumber` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `emergencyperson` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fathername` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `father_ic` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fphone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `femail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mothername` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mom_ic` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mphone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `memail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sname1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sage1` int(20) DEFAULT NULL,
  `sname2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sage2` int(20) DEFAULT NULL,
  `sname3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sage3` int(20) DEFAULT NULL,
  `transporter_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `transporter_ic` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `transporter_phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `agree_photograph` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `agree_marketing` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` int(11) NOT NULL,
  `confirmed_m` int(11) NOT NULL,
  `imported` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'No',
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` int(20) NOT NULL AUTO_INCREMENT,
  `studentname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `race` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reg_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `student_ic` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`student_id`),
  KEY `fid_active` (`fid`,`active`),
  KEY `studentname_fid_active` (`studentname`,`fid`,`active`),
  KEY `reg_no` (`reg_no`),
  KEY `reg_fid_active_age` (`reg_no`,`fid`,`active`,`age`) USING BTREE,
  KEY `active` (`active`),
  KEY `active_student` (`active`,`studentname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table student_assessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `student_assessment`;

CREATE TABLE `student_assessment` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `regno` varchar(15) NOT NULL,
  `mother_tongue` varchar(30) NOT NULL,
  `language_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table studentresult
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studentresult`;

CREATE TABLE `studentresult` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_id` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `continue_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `continue_title` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `teachername` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `class_room` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `times1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `student_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_class` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date1` date NOT NULL,
  `feedback` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `reg_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `g_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insert_date` date NOT NULL,
  `deleted` int(11) NOT NULL,
  `version` text COLLATE utf8_unicode_ci NOT NULL,
  `complete` text COLLATE utf8_unicode_ci NOT NULL,
  `trial` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `record_trial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `migrate_from_regno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`r_id`),
  KEY `gid` (`g_id`),
  KEY `fid_rid_platform` (`r_id`,`fid`,`platform`),
  KEY `level` (`level_id`),
  KEY `reg` (`reg_no`),
  KEY `teacher` (`teachername`),
  KEY `deleted` (`deleted`),
  KEY `level_reg_teacher` (`level_id`,`reg_no`,`teachername`),
  KEY `reg_deleted_fid` (`reg_no`,`deleted`,`fid`),
  KEY `fid_deleted_trial` (`fid`,`deleted`,`trial`),
  KEY `languageid_regno,_deleted` (`language_id`,`reg_no`,`deleted`),
  KEY `regno_deleted` (`reg_no`,`deleted`),
  KEY `teachername_deleted_insertdate` (`teachername`,`deleted`,`insert_date`),
  KEY `fid_rid_deleted` (`fid`,`r_id`,`deleted`),
  KEY `deleted_fid_insertdate` (`deleted`,`fid`,`insert_date`),
  KEY `idx_fid_deleted_gid` (`fid`,`deleted`,`g_id`),
  KEY `fid_deleted_trial_reg` (`fid`,`deleted`,`trial`,`level_id`,`reg_no`),
  KEY `method` (`method`),
  KEY `fid` (`fid`),
  KEY `fid_insertdate_deleted_trial` (`fid`,`insert_date`,`deleted`,`trial`),
  KEY `teachername_level_reg_deleted_trial` (`teachername`,`level_id`,`reg_no`,`deleted`,`trial`),
  KEY `teachername_deleted_trial` (`teachername`,`deleted`,`trial`),
  KEY `level_reg_deleted_trial` (`level_id`,`reg_no`,`deleted`,`trial`),
  KEY `level_reg_teachername_deleted_trial` (`level_id`,`reg_no`,`teachername`,`deleted`,`trial`),
  KEY `teachername_fid_trial_deleted` (`teachername`,`fid`,`trial`,`deleted`),
  KEY `teachername_reg_trial_deleted` (`teachername`,`reg_no`,`trial`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table studentresult_20210216
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studentresult_20210216`;

CREATE TABLE `studentresult_20210216` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_id` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `continue_title` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `teachername` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `class_room` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `times1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `student_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_class` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date1` date NOT NULL,
  `feedback` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `reg_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `g_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insert_date` date NOT NULL,
  `deleted` int(11) NOT NULL,
  `version` text COLLATE utf8_unicode_ci NOT NULL,
  `complete` text COLLATE utf8_unicode_ci NOT NULL,
  `trial` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `migrate_from_regno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`r_id`),
  KEY `gid` (`g_id`),
  KEY `fid_rid_platform` (`r_id`,`fid`,`platform`),
  KEY `level` (`level_id`),
  KEY `reg` (`reg_no`),
  KEY `teacher` (`teachername`),
  KEY `deleted` (`deleted`),
  KEY `level_reg_teacher` (`level_id`,`reg_no`,`teachername`),
  KEY `reg_deleted_fid` (`reg_no`,`deleted`,`fid`),
  KEY `fid_deleted_trial` (`fid`,`deleted`,`trial`),
  KEY `languageid_regno,_deleted` (`language_id`,`reg_no`,`deleted`),
  KEY `regno_deleted` (`reg_no`,`deleted`),
  KEY `teachername_deleted_insertdate` (`teachername`,`deleted`,`insert_date`),
  KEY `fid_rid_deleted` (`fid`,`r_id`,`deleted`),
  KEY `deleted_fid_insertdate` (`deleted`,`fid`,`insert_date`),
  KEY `idx_fid_deleted_gid` (`fid`,`deleted`,`g_id`),
  KEY `fid_deleted_trial_reg` (`fid`,`deleted`,`trial`,`level_id`,`reg_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table studentresult_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studentresult_test`;

CREATE TABLE `studentresult_test` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `teachername` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `class_room` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `times1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `student_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_class` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date1` date NOT NULL,
  `feedback` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `reg_no` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `g_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insert_date` date NOT NULL,
  `deleted` int(11) NOT NULL,
  `version` text COLLATE utf8_unicode_ci NOT NULL,
  `complete` text COLLATE utf8_unicode_ci NOT NULL,
  `migrate_from_regno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`r_id`),
  KEY `gid` (`g_id`),
  KEY `fid_rid_platform` (`r_id`,`fid`,`platform`),
  KEY `level` (`level_id`),
  KEY `reg` (`reg_no`),
  KEY `teacher` (`teachername`),
  KEY `deleted` (`deleted`),
  KEY `level_reg_teacher` (`level_id`,`reg_no`,`teachername`),
  KEY `reg_deleted_fid` (`reg_no`,`deleted`,`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table studentresult_title
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studentresult_title`;

CREATE TABLE `studentresult_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL COMMENT 'normal=title_id, continue=continue_title',
  PRIMARY KEY (`id`),
  KEY `type_gid` (`type`,`g_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table support
# ------------------------------------------------------------

DROP TABLE IF EXISTS `support`;

CREATE TABLE `support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` varchar(200) NOT NULL,
  `image_1` varchar(200) NOT NULL,
  `date_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table teacher
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fid` varchar(100) NOT NULL,
  `language1` varchar(100) DEFAULT NULL,
  `language2` varchar(100) DEFAULT NULL,
  `language3` varchar(100) DEFAULT NULL,
  `language4` varchar(100) DEFAULT NULL,
  `language5` varchar(100) DEFAULT NULL,
  `language6` varchar(100) DEFAULT NULL,
  `language7` varchar(100) DEFAULT NULL,
  `language8` varchar(20) DEFAULT NULL,
  `language9` varchar(20) DEFAULT NULL,
  `qms_kpi` int(20) NOT NULL,
  `lms_kpi` int(20) NOT NULL,
  `deleted` int(11) NOT NULL,
  `push_token` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `position` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fid` (`fid`),
  KEY `get_teacher` (`fid`,`deleted`),
  KEY `search_teacher` (`fid`,`username`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table teacher_activity_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teacher_activity_logs`;

CREATE TABLE `teacher_activity_logs` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(100) NOT NULL,
  `actions` text NOT NULL,
  `log_id` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table trial_student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trial_student`;

CREATE TABLE `trial_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentname` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `ic_number` varchar(50) NOT NULL,
  `fid` int(11) NOT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table update_table
# ------------------------------------------------------------

DROP TABLE IF EXISTS `update_table`;

CREATE TABLE `update_table` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `command` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
