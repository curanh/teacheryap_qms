INSERT INTO `student` (`student_id`, `studentname`, `age`, `race`, `dob`, `contact`, `reg_no`, `student_ic`, `fid`, `active`, `gender`, `created_date`)
VALUES
	(1, 'Shivaani Parama Sivam', 12, '', '2009-00-00', NULL, 'SP00001', NULL, 3, 'No', 'Female', '2021-01-16 14:57:59'),
	(2, 'Yap Pui Lam', 10, '', '2011-00-00', NULL, 'SP00002', NULL, 3, 'No', 'Male', '2021-01-16 14:57:59'),
	(3, 'Sea Xu', 12, '', '2009-00-00', NULL, 'SP00003', NULL, 3, 'No', 'Male', '2021-01-16 14:57:59');


	INSERT INTO `studentresult` (`r_id`, `student_id`, `level_id`, `title_id`, `continue_id`, `continue_title`, `teachername`, `class_room`, `times1`, `student_status`, `number_of_class`, `date1`, `feedback`, `platform`, `created_date`, `language_id`, `fid`, `reg_no`, `g_id`, `insert_date`, `deleted`, `version`, `complete`, `trial`, `record_trial`, `migrate_from_regno`, `method`)
	VALUES
		(1632129, '0', '223', '10604', '10604', '评审', 'honyuetorng', 'Room1', 'Afthernoon1', 'Not complete', '1', '2021-04-19', '复习 21-30', 'App', '2021-04-19 11:04:53', '1', 31, 'JMA00453', 'JMA00453_20210419110453', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632128, '0', '45', '2914', '', '', 'jklzhitong ', 'Room3', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:27', '5', 55, 'JKL00217', 'JKL00217_20210419110427', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632127, '0', '33', '1894', '', '', 'hspvenus', 'Room2', 'Morning2', 'Completed', '1', '2021-04-19', 'Not stable: d故事不稳', 'App', '2021-04-19 11:04:06', '2', 76, 'WSP00043', 'WSP00043_20210419110406', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632126, '0', '191', '8717', '', '', 'hspkahying', 'Room1', 'Morning2', 'Completed', '1', '2021-04-19', '不稳：妹妹，弟弟，窗，桌子，玩具，椅子\r\n复习', 'App', '2021-04-19 11:04:51', '1', 76, 'WSP00046', 'WSP00046_20210419110451', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632125, '0', '192', '9812', '', '', 'hsphuiyin', 'Room2', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:51', '1', 76, 'WSP00032', 'WSP00032_20210419110451', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632124, '0', '90', '6750', '', '', 'hspwey', 'Room9', 'Morning2', 'Completed & Not Stable', '1', '2021-04-19', 'Need more practice. Don\'t get the concept of blending and ejaan', 'App', '2021-04-19 11:04:54', '2', 76, 'HSP00017', 'HSP00017_20210419110454', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632123, '0', '11', '625', '', '', 'sbyenteng ', 'Room1', 'Morning2', 'Completed & Not Stable', '1', '2021-04-19', '复习（10-20）', 'App', '2021-04-19 11:53:47', '1', 66, 'SB00076', 'SB00076_20210419110404', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632122, '0', '217', '9885,9886', '', '', 'tanquiwei', 'Room2', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:19', '2', 31, 'JMA00672', 'JMA00672_20210419110419', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632121, '0', '33', '1902', '', '', 'hspvenus', 'Room2', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:04', '2', 76, 'HSP00002', 'HSP00002_20210419110404', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632120, '0', '192', '9806', '', '', 'hspkahying', 'Room1', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:36', '1', 76, 'WSP00053', 'WSP00053_20210419110436', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632119, '0', '95', '', '', '', 'tsbelinda', 'Room1', 'Afthernoon1', 'Completed', '1', '2021-04-19', '(R) a~u, b~c', 'App', '2021-04-19 11:04:31', '2', 7, 'TS00697', 'TS00697_20210419110431', '2021-04-17', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632118, '0', '90', '6789', '', '', 'hspwey', 'Room9', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:19', '2', 76, 'WSP00021', 'WSP00021_20210419110419', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632117, '0', '59', '4527,4528,4530', '', '', 'tanquiwei', 'Room5', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:33', '4', 31, 'JMA00678', 'JMA00678_20210419110433', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632116, '0', '74', '5851', '', '', 'sdmeichin', 'Room1', 'Morning1', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:55', '6', 12, 'SD00816', 'SD00816_20210419110455', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632115, '0', '51', '3666,3667', '', '', 'slchiewyen', 'Room7', 'Morning1', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:45', '3', 74, 'SL00096', 'SL00096_20210419110445', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632114, '0', '86', '9168', '9168', '还有10个', 'slyixuan', 'Room2', 'Morning2', 'Not complete', '1', '2021-04-19', 'Online', 'App', '2021-04-19 11:04:41', '2', 74, 'SL00074', 'SL00074_20210419110441', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632113, '0', '90', '6767,6768', '', '', 'slchiewyen', 'Room7', 'Morning1', 'Completed', '2', '2021-04-19', '', 'App', '2021-04-19 11:04:51', '2', 74, 'SL00060', 'SL00060_20210419110451', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632112, '0', '223', '10573,10574', '', '', 'jklyuxuan', 'Room1', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:38', '1', 55, 'JKL00217', 'JKL00217_20210419110438', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632111, '0', '90', '6769', '6769', 'Baca p not stable', 'sbyingxin', 'Room2', 'Morning2', 'Not Completed & Not Stable', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:02', '2', 66, 'SB00126', 'SB00126_20210419110402', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632110, '0', '41', '2398', '', '', 'jnbpeggy', 'Room3', 'Morning2', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:08', '5', 28, 'JNB00592', 'JNB00592_20210419110408', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632109, '0', '218', '9910', '9910', '作业没带 没完成', 'hspkahying', 'Room1', 'Morning2', 'Not complete', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:37', '1', 76, 'WSP00039', 'WSP00039_20210419110437', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632108, '0', '54', '4111', '', '', 'mandy', 'Room12', 'Afthernoon1', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:46', '3', 14, 'USJ00504', 'USJ00504_20210419110446', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632107, '0', '49', '9549', '9549', 'Lower part of question havent done as she is still not stable', 'sdmeichin', 'Room1', 'Morning1', 'Not complete', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:16', '6', 12, 'SD00672', 'SD00672_20210419110416', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632106, '0', '96', '8869', '8869', 'R vocabulary not stable ', 'sbyingxin', 'Room3', 'Morning2', 'Not Completed & Not Stable', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:11', '4', 66, 'SB00126', 'SB00126_20210419110411', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632105, '0', '90', '6774', '6774', 'Ejaan till sini', 'ylsoesther', 'Room5', 'Morning1', 'Not complete', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:46', '2', 79, 'YLSONLINE00035', 'YLSONLINE00035_20210419110446', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632104, '0', '90', '6775', '', '', 'ylsoesther', 'Room5', 'Morning1', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:20', '2', 79, 'YLSONLINE00035', 'YLSONLINE00035_20210419110420', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632103, '0', '69', '5509', '5509', '', 'slsiewwen', 'Room1', 'Morning2', 'Not complete', '1', '2021-04-19', 'Online teaching ', 'App', '2021-04-19 11:04:19', '1', 74, 'SL00074', 'SL00074_20210419110419', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes'),
		(1632102, '0', '54', '4092', '', '', 'mandy', 'Room12', 'Afthernoon1', 'Completed', '1', '2021-04-19', '', 'App', '2021-04-19 11:04:17', '3', 14, 'USJ00549', 'USJ00549_20210419110417', '2021-04-19', 0, '1.3.2', 'no', 'No', NULL, '', 'Yes');





		INSERT INTO `teacher` (`id`, `username`, `password`, `fid`, `language1`, `language2`, `language3`, `language4`, `language5`, `language6`, `language7`, `language8`, `language9`, `qms_kpi`, `lms_kpi`, `deleted`, `push_token`, `status`, `position`)
		VALUES
			(1091, 'thanhnguyenlms', 'thanhnguyenlms', '52', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', NULL, 0, 0, 0, '', 0, 'buddy'),
			(1090, 'thanhnguyen', 'thanhnguyen', '52', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', NULL, 0, 0, 0, '', 0, 'teacher');



			INSERT INTO `hqadmin` (`id`, `username`, `password`, `fid`, `position`, `status`, `deleted`)
			VALUES
				(70, 'qmstest', '123456', 52, 'cm', '0', 0);
