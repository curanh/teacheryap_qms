<?php 
    require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }

	// last synced date
	$query_last_sync = "
            SELECT sr.* FROM studentresult sr
            WHERE sr.fid = '".$_SESSION['fid']."' 
            AND sr.deleted='0' ORDER BY sr.created_date DESC LIMIT 1
            ";
    $result_last_sync = mysqli_query($connect, $query_last_sync);            
	while($row_last_sync = mysqli_fetch_array($result_last_sync))
    {
		$last_synced_date = $row_last_sync['created_date'];
		$last_synced_date = date("d/m/Y h:ia",strtotime($last_synced_date));
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <meta charset="UTF-8">
    <link rel="icon" href="images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
	  <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="shake"><a onclick="window.location.href='dashboard.php'" style="color: #85AFF6; cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
          <div class="panel-heading">
          <h3 style="color: #6E6B6B;">Dashboard<h3>
        </div>
          <div class="panel-body">
            <div class="table-responsive">
              	<div class="form-group">


Last tablet data sycned on <b><?php echo $last_synced_date; ?></b><br><br>

<table id="example" class="table table-striped table-bordered" style="width:100%">

<thead>
<tr>
<th width="10%">Last Progress</th>
<th width="15%">Student</th>
<th width="10%">Language</th>
<th width="20%">Level</th>
<th width="35%">Title</th>
<th width="10%">Teacher</th>
</tr>
</thead>
<tbody>
<?php

                      // last synced date
                    /*
                      $query_student = "SELECT sr.reg_no, s.studentname FROM studentresult sr
                                LEFT JOIN student s ON (sr.reg_no=s.reg_no) 
                               WHERE sr.fid = '".$_SESSION['fid']."' AND sr.deleted='0' GROUP BY s.studentname ORDER BY s.studentname ASC";
                    */
                      $query_student = "SELECT s.reg_no, s.studentname FROM student s  
                                        WHERE s.fid = '".$_SESSION['fid']."'
                                        AND s.active='Yes' ORDER BY s.studentname ASC";

                        $result_student = mysqli_query($connect, $query_student);          
                      while($row_student = mysqli_fetch_array($result_student))
                        {
                        $reg_no = $row_student["reg_no"];
                        $studentname = $row_student["studentname"];
                        
                        $query_student_result = "SELECT lg.language_name, lv.level_name, sr.title_id, sr.teachername, sr.insert_date, sr.level_id, sr.method FROM studentresult sr
                                                 LEFT JOIN level lv ON (sr.level_id=lv.level_id) 
                                                 LEFT JOIN language lg ON (lv.language_id = lg.language_id)
                                                 WHERE sr.fid = '".$_SESSION['fid']."' 
                                                 AND sr.deleted='0' 
                                                 AND sr.reg_no='".$reg_no."' 
                                                 ORDER BY sr.insert_date DESC LIMIT 1";
                        $result_student_result = mysqli_query($connect, $query_student_result); 

                        while($row_student_result = mysqli_fetch_array($result_student_result))  
                        {
                          $language_name = $row_student_result["language_name"];
                          $level_name = $row_student_result["level_name"];
                          $level_id = $row_student_result["level_id"];
                          $title_id = $row_student_result["title_id"];
                          $teachername = $row_student_result["teachername"];
                          $insert_date = $row_student_result["insert_date"];
                          $method = $row_student_result["method"];

                          //$insert_date = date("d/m/Y",strtotime($insert_date));

                          echo "<tr><td>"
                          .$insert_date.
                          "</td><td>"
                          .$studentname.
                          "</td><td>"
                          .$language_name.
                          "</td><td>"
                          .$level_name.
                          "</td><td>
                          ";
                          if ($method == 'Yes') {     
                          $query_title = "
                          SELECT * FROM level_title 
                          WHERE status = 1 
                          AND (deleted IS NULL OR deleted = 0) 
                          AND level_id = '".$level_id."' 
                          AND title_id IN (".$title_id.") 
                          ORDER BY sorting ASC";

                          $result_title = mysqli_query($connect,$query_title);
                          while($row_titleid=mysqli_fetch_assoc($result_title))
                          {   

                            echo $row_titleid['title_name'].'<br>';
                            
                          }
                          }else{
                          echo $title_id;
                          }
                          echo "</td><td>"
                          .$teachername.
                          "</td></tr>";
                        }
                      }

                    ?>
</tbody>
</table>

          		</div>
            </div>
          </div>
        </div>

          </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
</script>
</html>
