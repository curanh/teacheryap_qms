<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>CREATE TEACHER ACCOUNT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <!-- <link href="css/teacher.css" rel="stylesheet" media="all"> -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
    <link rel="icon" href="images/admin.png">
  </head>
  <body>
  <?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li class="shake"><a onclick="window.location.href='teacher.php'" style="color: #85AFF6; cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
               <!-- <a href="index.php" class="w3-button btn btn--radius w3-right " style="text-decoration: none; color: #fff; background-color: #DC143C;">&nbsp Logout</a> -->
                    <h3 class="" style="color: #6E6B6B;">Create Teacher Account</h3>
                    <!-- <h3 style="color: #6E6B6B;">Create Teacher Account<h3> -->
                    <form method="POST"  action="teacher.php">
                        <div class="form-group">
                          <label>Username</label>
                          <input class="form-control" type="text" placeholder="Username" name="username" required="">
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input class="form-control" type="text" placeholder="Password" name="password" required="">
                        </div>
                        <div class="form-group">
                          <label>Position</label>
                          <select class="form-control" id="position" name="position" required="">
                           <option value="" selected>Position</option>
                           <option value="teacher">Teacher</option>
                           <option value="buddy">Buddy</option> 
                          </select>
                        </div>
                        <div class="form-group" id="text">
                          <div class="col-md-10">
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language1" value="yes">
                            华语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language2" value="yes">
                            国语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language3" value="yes">
                            珠算 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language4" value="yes">
                            英语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language5" value="yes">
                            PK </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language6" value="yes">
                            TT </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language7" value="yes">
                            MIQ</label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language8" value="yes">
                            HOTS 36</label>
                          </div>
                        </div>
                        <!-- <div class="checkboxes">
                        <label><input type="checkbox" name="language1" value="yes" />华语</label>
                        <label><input type="checkbox" name="language2" value="yes" />国语</label>
                        <label><input type="checkbox" name="language3" value="yes" />珠算</label>
                        <label><input type="checkbox" name="language4" value="yes" />英语</label>
                        <label><input type="checkbox" name="language5" value="yes" />PK</label>
                        <label><input type="checkbox" name="language6" value="yes" />TT</label>
                        </div> -->
                        <button class="btn btn-success" type="submit" name="button">
                          <i class="glyphicon glyphicon-upload"></i>
                          Submit
                        </button>


                        <?php
                         // && isset($_POST['language1']) && isset($_POST['language2']) && isset($_POST['language3']) && isset($_POST['language4']) && isset($_POST['language5']) && isset($_POST['language6'])
                         // && ($_POST['language1']!="")  && ($_POST['language2']!="")  && ($_POST['language3']!="")  && ($_POST['language4']!="")  && ($_POST['language5']!="")  && ($_POST['language6']!="")
                        require_once('dbConfig.php');  
                        if (isset($_POST['button'])){
                            if(isset($_POST['username']) && isset($_POST['password'])){


                            if(($_POST['username'] !="") && ($_POST['password']!="")){
                                $username= strtolower($_POST['username']);
                                $password = strtolower($_POST['password']);
                                $new_name = str_replace(" ", "", $username);
                                $new_pass = str_replace(" ", "", $password);
                                $position = $_POST['position'];
                                $fid = $_SESSION['fid'];

                                if(isset($_POST['language1'])) {
                                    $language1 = $_POST['language1'];
                                }
                                else
                                {
                                    $language1 = "no";
                                }

                                if(isset($_POST['language2'])) {
                                    $language2 = $_POST['language2'];
                                }
                                else
                                {
                                    $language2 = "no";
                                }

                                if(isset($_POST['language3'])) {
                                    $language3 = $_POST['language3'];
                                }
                                else
                                {
                                    $language3 = "no";
                                }

                                if(isset($_POST['language4'])) {
                                    $language4 = $_POST['language4'];
                                }
                                else
                                {
                                    $language4 = "no";
                                }

                                if(isset($_POST['language5'])) {
                                    $language5 = $_POST['language5'];
                                }
                                else
                                {
                                    $language5 = "no";
                                }
                                
                                if(isset($_POST['language6'])) {
                                    $language6 = $_POST['language6'];
                                }
                                else
                                {
                                    $language6 = "no";
                                }

                                if(isset($_POST['language7'])) {
                                    $language7 = $_POST['language7'];
                                }
                                else
                                {
                                    $language7 = "no";
                                }

                                if(isset($_POST['language8'])) {
                                    $language8 = $_POST['language8'];
                                }
                                else
                                {
                                    $language8 = "no";
                                }

                                  $sql= "SELECT * FROM teacher WHERE username='$new_name' LIMIT 1";
                                  $result = mysqli_query($connect, $sql);
                                  $user = mysqli_fetch_assoc($result);
                            
                              
                                  if ($user['username'] === $new_name) {
                                      echo "<script>alert('Username already exists ! Please try again! ');
                                      window.location.href='teacher.php';
                                      </script>";

                                  exit;
                                  }else{
                                    $sql="INSERT INTO teacher (username, password, fid, language1, language2, language3, language4, language5, language6 , language7, language8, qms_kpi, lms_kpi, status, position)
                                    VALUES
                                    ('$new_name','$new_pass', '$fid', '$language1','$language2','$language3','$language4','$language5','$language6','$language7','$language8', '0', '0' ,'0', '$position')";

                                    if (!mysqli_query($connect,$sql))
                                    {
                                        die('Error: ' . mysqli_error($connect));
                                    }
                                    echo "<script>alert('Successful! ')</script>";
                                  }
                                
                                // elseif ($position == 'buddy') {

                                //   $sql= "SELECT * FROM teacher WHERE username='$new_name' LIMIT 1";
                                //   $result = mysqli_query($connect, $sql);
                                //   $user = mysqli_fetch_assoc($result);

                                //   $sql_1= "SELECT * FROM hqadmin WHERE username='$new_name' LIMIT 1";
                                //   $result_1 = mysqli_query($connect, $sql_1);
                                //   $user1 = mysqli_fetch_assoc($result_1);
                            
                              
                                //   if ($user['username'] === $new_name || $user1['username'] === $new_name) {
                                //       echo "<script>alert('Username already exists ! Please try again! ');
                                //       window.location.href='teacher.php';
                                //       </script>";

                                //   exit;
                                //   }
                                //   else{
                                //     $sql="INSERT INTO teacher (username, password, fid, language1, language2, language3, language4, language5, language6 , language7, language8, qms_kpi, lms_kpi, status, position)
                                //     VALUES
                                //     ('$new_name','$new_pass', '$fid', '$language1','$language2','$language3','$language4','$language5','$language6','$language7','$language8', '0', '0' ,'0', '$position')";

                                //     if (!mysqli_query($connect,$sql))
                                //     {
                                //         die('Error: ' . mysqli_error($connect));
                                //     }
                                //     //echo "<script>alert('Successful! ')</script>";

                                //     $sql="INSERT INTO hqadmin (username, password, fid, position, status, deleted)
                                //       VALUES
                                //       ('$new_name','$new_pass', '$fid' ,'$position', '0', '0')";

                                //       if (!mysqli_query($connect,$sql))
                                //       {
                                //           die('Error: ' . mysqli_error($connect));
                                //       }
                                //       echo "<script>alert('Successful! ')</script>";
                                //   }

                                // }
                                               
                            }else{

                               echo "<script>alert('Please fill in all of the data first before Submit!');
                               window.location.href='teacher.php';
                               </script>";
                               exit;
                            }
                        }
                      }
                    ?>
                    </form>
            </div>
          </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
  </body>
</html>
