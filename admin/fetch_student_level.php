<?php
//fetch.php
  session_start();
  if ($_SESSION['fid'] == "") {
    header("Location: index.php");
        
  }
  if(isset($_GET['reg_no']))
  {
    $UserID = base64_decode($_GET['reg_no']);
    $active = base64_decode($_GET['active']);
    //echo $UserID;
    //echo $Level_id;
  }

$fid = $_SESSION['fid'];
$position = $_SESSION['position'];
$username = $_SESSION['name'];
//echo $fid;
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 //echo $query;
}
else
{
 $query = "
  SELECT sr.*, lang.language_name, le.level_name, ti.title_name, st.studentname FROM studentresult sr 
  LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id) 
  LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
  WHERE sr.reg_no='".$UserID."'
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No') GROUP BY le.level_name ORDER BY le.sorting ASC
 ";
}

$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
    <tr>
      <th>Level</th>
      <th>Start date</th>
      <th>Last date</th>
      <th>Class</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
 ';

 while($row = mysqli_fetch_array($result))
 {
  $id= $row['r_id'];
  $level_id = $row["level_id"];
  $studentname = $row["studentname"];
  // echo $row['studentname']; exit();
  $output .= '
  <tbody>
    <tr>
      <td><strong>'.$row["level_name"].'</strong></td>
 ';
  $query_start_date = "SELECT sr.insert_date FROM studentresult sr
  WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
  AND sr.level_id='".$row["level_id"]."' 
  AND sr.reg_no='".$UserID."'
  ORDER BY sr.r_id ASC LIMIT 1";
  $result_start_date = mysqli_query($connect, $query_start_date); 
  //echo $query_start_date; exit();
  while($row_start_date = mysqli_fetch_array($result_start_date))
  {
  $output .= '
  <td>'.$row_start_date["insert_date"].'</td>
  ';
  }

  $query_last_date = "SELECT sr.insert_date, sr.complete, sr.r_id FROM studentresult sr
  WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
  AND sr.level_id='".$row["level_id"]."' 
  AND sr.reg_no='".$UserID."'
  ORDER BY sr.r_id DESC LIMIT 1";
  $result_last_date = mysqli_query($connect, $query_last_date); 
  while($row_last_date = mysqli_fetch_array($result_last_date))
  {
    if ($row_last_date['complete'] == 'yes') {
      $output .= '
      <td>&#x2705;Completed- '.$row_last_date["insert_date"].'</td>
      ';
    }else{
      $output .= '
      <td>&#x274C;Not-Completed-  '.$row_last_date["insert_date"].'</td>
      ';
    }
    $query_count = "SELECT count(*) AS total FROM studentresult sr
    WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
    AND sr.level_id='".$row["level_id"]."' 
    AND sr.reg_no='".$UserID."'";
    $result_count = mysqli_query($connect, $query_count); 
    //echo $query_start_date; exit();
    while($row_count = mysqli_fetch_array($result_count))
    {
    $output .= '
    <td>'.$row_count["total"].'</td>
    ';
      if ($row_last_date['complete'] == 'yes') {
        $output .= '
        <td>
        <button class="btn btn-info" disabled onclick=window.location.href="fetch_complete_level.php?reg_no='.base64_encode($UserID).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('2').'">Complete</button>
        <button class="btn btn-danger" onclick=window.location.href="fetch_complete_level.php?reg_no='.base64_encode($UserID).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('1').'">Not completed</button>
        </td>
        ';
      }else{
        $output .= '
        <td>
        <button class="btn btn-info" onclick=window.location.href="fetch_complete_level.php?reg_no='.base64_encode($UserID).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('2').'">Complete</button>
        <button class="btn btn-danger" disabled onclick=window.location.href="fetch_complete_level.php?reg_no='.base64_encode($UserID).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('1').'">Not completed</button>
        </td>
        ';
        
      }
    }

    if ($position == 'cm') {
    $output .='
    <td>
     <select class="form-control" id="select_option_'.$level_id.'" name="select_option">
          <option value="">Please select reason</option>
          <option value="select wrong level">Choose the wrong level</option>
          <option value="others">Others</option>
        </select>
        </td>
        <td>
        <a class="btn btn-danger" onclick="delelet('.$level_id.')"><i class="glyphicon glyphicon-remove"></i> Delete</a>
        </td>
        <script>
        function delelet($level_id){
            var box = document.getElementById("select_option_"+$level_id+"").value;
            if(box == ""){
              alert("Please select reason");
            }else{

              if (box == "others"){
                var reason = prompt("Please enter your reason");
                  if(reason.length != 0){
                      var del=confirm("Are you sure you want to delete this level all Record?");
                      if (del==true) {
                        window.location.href = "delete.php?level_reason="+reason+"&reg_no='.$UserID.'&level_id="+$level_id+"&fid='.$_SESSION['fid'].'&username='.$_SESSION['name'].'&active='.$active.'&studentname='.$studentname.'";
                        //alert("Pls la");
                      }
                  }else if (reason.length == 0){
                        delelet($level_id);
                  }
              }else if (box == "select wrong level"){
                var del=confirm("Are you sure you want to delete this level all Record?");
                  if (del==true) {
                    window.location.href = "delete.php?level_reason="+box+"&reg_no='.$UserID.'&level_id="+$level_id+"&fid='.$_SESSION['fid'].'&username='.$_SESSION['name'].'&active='.$active.'&studentname='.$studentname.'";
                  }
              }

            }
          }
      </script>
      ';
      }else{
        $output .='
        <td></td>
        <td></td>
        ';

      }
      $output .='
      </tr>
        </tbody>
      ';  
  }

 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>