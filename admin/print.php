<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Student Report</title>
	
	<link rel='stylesheet' type='text/css' href='css/styless.css' />
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
	<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
	<script type='text/javascript' src='js/example.js'></script>
</head>
	<style>
	  .signature, .title { 
	    float:left;
	    border-top: 1px solid #000;
	    width: 200px; 
	    text-align: center;
	  }

	  .print {
	      display:none
	    }
	     @media print {
	      .print {display:block}
	      .btn-print {display:none;}
	    }
	</style>
<body>

	<div id="page-wrap">
		<div id="header">STUDENT REPORT</div>
		<div id="identity">
			<center><img id="image" src="images/admin.png" alt="logo" height="190" width="200" style="margin-top: 5%;"/></center>
			<table id="items">
				<tr>
		          <th>Centre</th>
		          <th>Date</th>
		          <th>Time</th>
		      	</tr>
            	<?php 
                  require_once("dbConfig.php");
                  // student last record
                  $query = "SELECT * FROM centre WHERE fid='".base64_decode($_GET['fid'])."'";
                  $result = mysqli_query($connect, $query);
                  if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_array($result)){
                      // echo "<h5>Level name: ".$row['level_name']."</h5>";
                      echo "<td><center><span>".$row['name']."</span></center></td>";
                    }
 
                  }
            	?>
                    <td><center><?php date_default_timezone_set("Asia/Kuala_Lumpur"); echo  date('d/m/Y');?></center></td>
                    <td><center><?php date_default_timezone_set("Asia/Kuala_Lumpur"); echo  date('h.i a'); ?></center></td>
            </table>

		<div id="customer" style="margin-top: 5%;">
			<h3 style="margin-left: 5%;">Studentname: <?php echo urldecode(base64_decode($_GET['studentname'])); ?></h3>
			
            <table id="meta">
            	<?php 
                  // student last record
		          $query = "SELECT * FROM assess WHERE reg_no = '".base64_decode($_GET['reg_no'])."' AND level_id = '".base64_decode($_GET['level_id'])."'";
		          $result = mysqli_query($connect, $query);
		          if(mysqli_num_rows($result) > 0){
		            while($row = mysqli_fetch_array($result)){
		              echo "<tr>";
                      echo "<td class='meta-head'>".$row['assess1']."</td>";
                      echo "<td><span>".$row['assess_score1']."</span></td>";
                      echo "</tr>";
                      echo "<tr>";
                      echo "<td class='meta-head'>".$row['assess2']."</td>";
                      echo "<td><span>".$row['assess_score2']."</span></td>";
                      echo "</tr>";
                      echo "<tr>";
                      echo "<td class='meta-head'>".$row['assess3']."</td>";
                      echo "<td><span>".$row['assess_score3']."</span></td>";
                      echo "</tr>";
                      echo "<tr>";
                      echo "<td class='meta-head'>".$row['assess4']."</td>";
                      echo "<td><span>".$row['assess_score4']."</span></td>";
                      echo "</tr>";
                      echo "<tr>";
                      echo "<td class='meta-head'>".$row['assess5']."</td>";
                      echo "<td><span>".$row['assess_score5']."</span></td>";
                      echo "</tr>";
		            }

		          }  
            	?>

            </table>
		</div>
		<div id="customer" style="margin-top: 8%;">
			<table id="meta">
		      
		      <tr class="item-row">
		      	  <?php 
		          require_once("dbConfig.php");
		          // student last record
		          $query = "SELECT * FROM level WHERE level_id='".base64_decode($_GET['level_id'])."'";
		          $result = mysqli_query($connect, $query);
		          if(mysqli_num_rows($result) > 0){
		            while($row = mysqli_fetch_array($result)){
		              echo "<tr>";
		              echo "<td class='meta-head'>Level</td>";
		              echo "<td>".$row['level_name']."</td>";
		              echo "</tr>";
		            }

		          }                  
		          // student last record
		          $query = "SELECT * FROM assess WHERE reg_no = '".base64_decode($_GET['reg_no'])."' AND level_id = '".($_GET['level_id'])."'";
		          $result = mysqli_query($connect, $query);
		          if(mysqli_num_rows($result) > 0){
		            while($row = mysqli_fetch_array($result)){
		              echo "<tr>";
		              echo "<td class='meta-head'>Mother tongue</td>";
		              echo "<td>".$row['m_tongue']."</td>";
		              echo "</tr>";
		              echo "<tr>";
		              echo "<td class='meta-head'>Subjects</td>";
		              echo "<td>".$row['subjects']."</td>";
		              echo "</tr>";
		              echo "<tr>";
		              echo "<td class='meta-head'>Personality</td>";
		              echo "<td>".$row['personality']."</td>";
		              echo "</tr>";
		              echo "<tr>";
		              echo "<td class='meta-head'>Remarks</td>";
		              echo "<td>".$row['remark']."</td>";
		              echo "</tr>";
		            }

		          }                  
		          ?>
		      </tr>
		    </table>
		</div>
		<!-- <div id="chartdiv" style="height:250px;"></div> -->
		<center>
		<div id="chartdiv" style="height:350px; margin-top: 35.7%; margin-right: 8%;"></div>
		</center>
		<button  onclick="javascript:window.print()" class="btn-print">Print</button>
	</div>
</div>
	
</body>
<script src="js/amcharts.js" type="text/javascript"></script>
<script src="js/radar.js" type="text/javascript"></script>
<script src="js/dataloader.min.js" type="text/javascript"></script>
<script>
var urlParams = new URLSearchParams(window.location.search);
var reg_no = urlParams.get('reg_no');
var level_id = urlParams.get('level_id');
var chart = AmCharts.makeChart("chartdiv", {
type: "radar",
dataLoader: {
    "url": "data.php?reg_no="+reg_no+"&level_id="+level_id+"",
    "format": "json"
},
categoryField: "title",
startDuration: 2,

valueAxes: [{
    axisAlpha: 0.15,
    minimum: 0,
    dashLength: 3,
    axisTitleOffset: 20,
    gridCount: 5
}],

graphs: [{
	valueField: "assess_title",
    bullet: "round",
    balloonText: "[[title]]  [[value]]",
}],
"export": {
  "enabled": true,
  "libs": {
    "autoLoad": false
  }
}
});
</script>

</html>