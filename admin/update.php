<?php 
    
    require_once("dbConfig.php");

    if(isset($_POST['update']))
    {
        session_start();
        $fid = $_SESSION['fid'];
        $UserID = $_GET['id'];
        $UserName = $_POST['name'];
        $lms_teacher = $_POST['lms_teacher'];
        $password = $_POST['password'];
        $status = $_POST['status'];
        $position = $_POST['position'];
        $new_name = str_replace(" ", "", $UserName);
        $new_pass = str_replace(" ", "", $password);

        if (isset($_POST['language1'])){
        $language1 = $_POST['language1'];
        }
        else
        {
        $language1 = 'no';
        }
        if (isset($_POST['language2'])){
        $language2 = $_POST['language2'];
        }
        else
        {
        $language2 = 'no';
        }
        if (isset($_POST['language3'])){
        $language3 = $_POST['language3'];
        }
        else
        {
        $language3 = 'no';
        }
        if (isset($_POST['language4'])){
        $language4 = $_POST['language4'];
        }
        else
        {
        $language4 = 'no';
        }
        if (isset($_POST['language5'])){
        $language5 = $_POST['language5'];
        }
        else
        {
        $language5 = 'no';
        }
        if (isset($_POST['language6'])){
        $language6 = $_POST['language6'];
        }
        else
        {
        $language6 = 'no';
        }
        if (isset($_POST['language7'])){
        $language7 = $_POST['language7'];
        }
        else
        {
        $language7 = 'no';
        }
        if (isset($_POST['language8'])){
        $language8 = $_POST['language8'];
        }
        else
        {
        $language8 = 'no';
        }
        // echo $lms_teacher; exit();
        if ($lms_teacher != "1") {

            $query_lms_get = " SELECT * FROM examresult WHERE fid = '".$fid."' AND teacher = '".$lms_teacher."' ";
            $result_lms_get = mysqli_query($connect1,$query_lms_get);
            // echo $query_lms_get; exit();
            while($row_lms_get=mysqli_fetch_assoc($result_lms_get))
            {
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $fid = $row_lms_get['fid'];
                $teacher = $row_lms_get['teacher'];
                $exam_id = $row_lms_get['exam_id'];
                $date1 = date('Y-m-d H:i:s');

                $sql="INSERT INTO examresult_backup (fid, qms_teacher,  original_teacher, exam_id, date1)
                      VALUES
                      ('".$fid."', '".$new_name."', '".$teacher."', '".$exam_id."', '".$date1."')";
                if (!mysqli_query($connect,$sql))
                {
                    die('Error: ' . mysqli_error($connect));
                }
            }

            $query_lms_deleted = "UPDATE teacher SET deleted = '1' WHERE username = '".$lms_teacher."' AND fid = '".$fid."'";
            $result_lms_deleted = mysqli_query($connect1,$query_lms_deleted);

            $query_lms = "UPDATE examresult SET teacher = '".$new_name."' WHERE teacher = '".$lms_teacher."' AND fid = '".$fid."'";
            $result_lms = mysqli_query($connect1,$query_lms);

            // echo $query_lms;
        }
        

        $query = "UPDATE teacher SET username = '".$new_name."', password = '".$new_pass."', language1 = '".$language1."' , language2 = '".$language2."', language3 = '".$language3."', language4 = '".$language4."', language5 = '".$language5."', language6 = '".$language6."', language7 = '".$language7."', language8 = '".$language8."', status = '".$status."', position = '".$position."' WHERE id='".$UserID."'";
        $result = mysqli_query($connect,$query);
        

        if($result)
        {   
           echo "<script type='text/javascript'>alert('successfully');
                window.location='edit.php?GetID=".base64_encode($UserID)."'
                </script>";
        }
        else
        {
           
           echo 'error';  //not showing an alert box.
           
        }
    }

    if(isset($_POST['update1']))
    {
        $UserID = $_GET['id'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        

        $query = "UPDATE teacher SET username = '".$username."', password = '".$password."' WHERE id='".$UserID."'";
        $result = mysqli_query($connect,$query);
        
        if($result)
        {   
           echo "<script type='text/javascript'>alert('successfully ');
                window.location='center_password.php?GetID=".base64_encode($UserID)."'
                </script>";
        }
        else
        {
           
           echo 'error';  //not showing an alert box.
           
        }
    }
?>