<?php
//fetch.php
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
        
}
if(isset($_GET['level_id']))
  {
    $level_id = base64_decode($_GET['level_id']);
    $fid = base64_decode($_GET['fid']);
    //$fid = $_GET['fid'];
    //echo $UserID;
  }
require_once("dbConfig.php");
$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
 SELECT sr.reg_no, st.studentname FROM studentresult sr 
 LEFT JOIN student st ON sr.reg_no = st.reg_no 
 WHERE sr.level_id = '$level_id' 
 AND sr.fid = '$fid'
 AND st.active = 'Yes'
 AND (sr.deleted IS NULL OR sr.deleted = '0')
 AND (sr.trial = '' OR sr.trial = 'No')
 AND st.studentname LIKE '%".$search."%'
 GROUP BY sr.reg_no, st.studentname
 ";
}
else
{
 $query = "
  SELECT sr.reg_no, st.studentname FROM studentresult sr 
  LEFT JOIN student st ON sr.reg_no = st.reg_no 
  WHERE sr.level_id = '$level_id' 
  AND sr.fid = '$fid'
  AND st.active = 'Yes'
  AND (sr.deleted IS NULL OR sr.deleted = '0')
  AND (sr.trial = '' OR sr.trial = 'No') 
  GROUP BY sr.reg_no, st.studentname
 ";
 //echo $query; exit();
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
 <thead>
    <tr>
      <th>Reg_No</th>
      <th>Student Name</th>
      <th>Status</th>
    </tr>
  </thead>
 ';
 while($row = mysqli_fetch_array($result))
 {

  $query3 = "
  SELECT count(sr.r_id) AS total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
  LEFT JOIN level le ON (le.level_id=sr.level_id)
  LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
  WHERE sr.level_id = '".$level_id."'
  AND st.active = 'Yes'
  AND sr.reg_no = '".$row['reg_no']."'
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No')
  ";
  if($result3 = mysqli_query($connect, $query3))
  {
               
      while($row3 = mysqli_fetch_array($result3))
      {
          $total_class = $row3['total_class'];
          $total = $row3['total'];

          $query4 = "
          SELECT sr.complete FROM studentresult sr
          LEFT JOIN level le ON (le.level_id=sr.level_id)
          LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
          WHERE sr.level_id = '".$row3['level_id']."'
          AND st.active = 'Yes'
          AND sr.reg_no = '".$row3['reg_no']."'
          AND (sr.deleted IS NULL OR sr.deleted = '0') 
          AND (sr.trial = '' OR sr.trial = 'No')
          ORDER BY sr.r_id DESC LIMIT 1
          ";
          if($result4 = mysqli_query($connect, $query4))
          {
                       
              while($row4 = mysqli_fetch_array($result4))
              {  
          

              if($row4['complete'] == 'yes' AND $total <= $total_class){
                $output .= '
                <tbody>
                <tr>
                <td>'.$row["reg_no"].'</td>
                <td>'.$row["studentname"].'</td>
                <td style="color:#31D919"><strong>PASS</strong></td>
                </tr>
                </tbody>
                ';
              }elseif ($total_class == '0') {
                $output .= '
                <tbody>
                <tr>
                <td>'.$row["reg_no"].'</td>
                <td>'.$row["studentname"].'</td>
                <td style="color:#787878"><strong>No KPI Class</strong></td>
                </tr>
                </tbody>
                ';
              }
              elseif($row4['complete'] == 'yes' AND $total > $total_class) {
                $output .= '
                <tbody>
                <tr>
                <td>'.$row["reg_no"].'</td>
                <td>'.$row["studentname"].'</td>
                <td style="color:#F43939"><strong>FAIL</strong></td>
                </tr>
                </tbody>
                ';
              }elseif($row4['complete'] == 'no' OR $row4['complete'] == ''){
                $output .= '
                <tbody>
                <tr>
                <td>'.$row["reg_no"].'</td>
                <td>'.$row["studentname"].'</td>
                <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
                </tr>
                </tbody>
                ';
              }
            }
          }
        }

  }
  
 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>