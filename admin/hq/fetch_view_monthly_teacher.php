<?php
//fetch.php
include '..//dbConfig.php';

session_start();
if ($_SESSION['name'] == "") {
    header("Location: index.php");
    
}  
$teachername = base64_decode($_GET['teachername']);

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $year = date("Y" , strtotime($search));
 $month = date("m" , strtotime($search));
 $query = "
  SELECT *,la.language_name, le.level_name, st.studentname FROM studentresult sr 
  LEFT JOIN language la ON (sr.language_id = la.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id) 
  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
  WHERE sr.teachername = '".$teachername."' 
  AND st.active = 'Yes' 
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No')
  AND month(sr.insert_date) = '".$month."' 
  AND year(sr.insert_date) = '".$year."' 
  GROUP BY sr.insert_date ORDER BY sr.r_id DESC
 ";
}
else
{
 $year = date("Y");
 $month = date("m");
 $query = "
  SELECT *,la.language_name, le.level_name, st.studentname FROM studentresult sr 
  LEFT JOIN language la ON (sr.language_id = la.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id) 
  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
  WHERE sr.teachername = '".$teachername."' 
  AND st.active = 'Yes' 
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No')
  AND month(sr.insert_date) = '".$month."' 
  AND year(sr.insert_date) = '".$year."' 
  GROUP BY sr.insert_date ORDER BY sr.r_id DESC
 ";
}
 
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>Date</th>
  <th>Morning 1</th>
  <th>Morning 2</th>
  <th>Afternoon 1</th>
  <th>Afternoon 2</th>
  <th>Afternoon 3</th>
  <th>Afternoon 4</th>
  <th>Total</th>
  </tr>
  </thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $new_date = date("d/m/Y", strtotime($row["insert_date"]));
  $output .= '
  <tbody>
  <tr>
  <td>'.$new_date.'</td>
  ';
  $query_time = "
   SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
   LEFT JOIN language la ON (sr.language_id = la.language_id) 
   LEFT JOIN level le ON (sr.level_id = le.level_id) 
   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
   WHERE sr.teachername = '".$teachername."' 
   AND st.active = 'Yes'
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   AND sr.insert_date = '".$row["insert_date"]."' 
   GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
  ";
    if($result_time = mysqli_query($connect, $query_time))
    {
      $mo1 = '0';
      $mo2 = '0';
      $af1 = '0';
      $af2 = '0';
      $af3 = '0';
      $af4 = '0';
      while($rowcount_time=mysqli_fetch_array($result_time))
      {
        $time = $rowcount_time['times1'];
        if ($time == 'Morning1') {
          $mo1 ++;
        }
        if ($time == 'Morning2') {
          $mo2 ++;
        }
        if ($time == 'Afthernoon1') {
          $af1 ++;
        }
        if ($time == 'Afthernoon2') {
          $af2 ++;
        }
        if ($time == 'Afthernoon3') {
          $af3 ++;
        }
        if ($time == 'Afthernoon4') {
          $af4 ++;
        }
        $total_d_time = $mo1 + $mo2 + $af1 + $af2 + $af3 + $af4;
      }
      $output .= '
      <td>'.$mo1.'</td>
      <td>'.$mo2.'</td>
      <td>'.$af1.'</td>
      <td>'.$af2.'</td>
      <td>'.$af3.'</td>
      <td>'.$af4.'</td>
      <td>'.$total_d_time.'</td>
      ';

    }
  $output .= '
  <tr>
  </tbody>
  ';
}
if(isset($_POST["query"]))
{
$search = mysqli_real_escape_string($connect, $_POST["query"]);
$year = date("Y" , strtotime($search));
$month = date("m" , strtotime($search));
$query_time_2 = "
 SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
 LEFT JOIN language la ON (sr.language_id = la.language_id) 
 LEFT JOIN level le ON (sr.level_id = le.level_id) 
 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
 WHERE sr.teachername = '".$teachername."' 
 AND st.active = 'Yes'
 AND (sr.deleted IS NULL OR sr.deleted = '0') 
 AND (sr.trial = '' OR sr.trial = 'No')
 AND month(sr.insert_date) = '".$month."' 
 AND year(sr.insert_date) = '".$year."'  
 GROUP BY sr.insert_date, sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
 ";
}
else{
$year = date("Y");
$month = date("m");
$query_time_2 = "
 SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
 LEFT JOIN language la ON (sr.language_id = la.language_id) 
 LEFT JOIN level le ON (sr.level_id = le.level_id) 
 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
 WHERE sr.teachername = '".$teachername."' 
 AND st.active = 'Yes'
 AND (sr.deleted IS NULL OR sr.deleted = '0') 
 AND (sr.trial = '' OR sr.trial = 'No')
 AND month(sr.insert_date) = '".$month."' 
 AND year(sr.insert_date) = '".$year."' 
 GROUP BY sr.insert_date, sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
 ";
}
    if($result_time_2 = mysqli_query($connect, $query_time_2))
    {
      $total_mo1 = '0';
      $total_mo2 = '0';
      $total_af1 = '0';
      $total_af2 = '0';
      $total_af3 = '0';
      $total_af4 = '0';
      while($rowcount_time_2 = mysqli_fetch_array($result_time_2))
      {
        $time = $rowcount_time_2['times1'];
        if ($time == 'Morning1') {
          $total_mo1 ++;
        }
        if ($time == 'Morning2') {
          $total_mo2 ++;
        }
        if ($time == 'Afthernoon1') {
          $total_af1 ++;
        }
        if ($time == 'Afthernoon2') {
          $total_af2 ++;
        }
        if ($time == 'Afthernoon3') {
          $total_af3 ++;
        }
        if ($time == 'Afthernoon4') {
          $total_af4 ++;
        }
        $total_m_time = $total_mo1 + $total_mo2 + $total_af1 + $total_af2 + $total_af3 + $total_af4;
      }
      $output .= '
      <thead>
      <tr>
        <th>Total</th>
        <th>'.$total_mo1.'</th>
        <th>'.$total_mo2.'</th>
        <th>'.$total_af1.'</th>
        <th>'.$total_af2.'</th>
        <th>'.$total_af3.'</th>
        <th>'.$total_af4.'</th>
        <th>'.$total_m_time.'</th>
      </tr>
      </thead>
      ';
    }

 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>