<?php
//fetch.php
session_start();
if ($_SESSION['name'] == ""){
  header("Location: index.php");      
}

require_once("..//dbConfig.php");
$output = '';
 $query = "
  SELECT * FROM centre ORDER BY name
 ";
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>Center</th>
  <th>Total teacher</th>
  <th>Non update</th>
  <th>Update</th>
  <th></th>
  </tr>
  </thead>
 ';

 while($row = mysqli_fetch_array($result))
 { 
  $output .= '
    <tbody>
      <tr>
        <td><strong>'.$row["name"].'</strong></td>
  ';
      $update = 0;
      $non_update = 0;
      $total = 0;
      $query_te="SELECT * FROM teacher WHERE fid = '".$row['fid']."' AND deleted = '0'";
      $result_te = mysqli_query($connect, $query_te);
      while($row_te = mysqli_fetch_array($result_te))
      {
        if ($row_te > 0) {
          $total++;
        }

      if(isset($_POST["query"]))
      {
      $day = mysqli_real_escape_string($connect, $_POST["query"]);
      $Y = date('Y',strtotime($day));
      $m = date('m',strtotime($day));
      $d = date('d');
      $day = $Y.'-'.$m.'-'.$d;
      $day = date('Y-m-d',strtotime('-30 days',strtotime($day)));
      //echo $day; exit();

      $query_sr="SELECT * FROM studentresult 
      WHERE teachername = '".$row_te['username']."'
      AND insert_date >= '".$day."'
      AND (deleted IS NULL OR deleted = '0') 
      AND (trial = '' OR trial = 'No') ORDER BY insert_date DESC";

      }else{

      $day = date('Y-m-d',strtotime('-30 days'));

      $query_sr="SELECT * FROM studentresult 
      WHERE teachername = '".$row_te['username']."'
      AND insert_date >= '".$day."'
      AND (deleted IS NULL OR deleted = '0') 
      AND (trial = '' OR trial = 'No') ORDER BY insert_date DESC";
      }
      $result_sr = mysqli_query($connect, $query_sr);
      $row_sr = mysqli_fetch_array($result_sr);

      // echo $query_sr; exit();
      if ($row_sr > 0) {
        $update++;
      }else{
        $non_update++; 
      }
      
      }
      $output .= '
      <td>'.$total.'</td>
      <td>'.$non_update.'</td>
      <td>'.$update.'</td>
      <td><button class="btn btn-default" onclick=window.location.href="hq_all_centre_report_teacher.php?fid='.base64_encode($row['fid']).'&day='.base64_encode($day).'"><i class="glyphicon glyphicon-eye-open"></i> View</button></td>
      ';
 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>