<?php
//fetch.php
        session_start();
        if ($_SESSION['name'] == "") {
            header("Location: index.php");
                
        }
        if(isset($_GET['teachername']))
        {
            $teachername = base64_decode($_GET['teachername']);
            $fid = base64_decode($_GET['fid']);
            
        }
        require_once("..//dbConfig.php");
        $output = '';
        if(isset($_POST["query"]))
        {
         $search = mysqli_real_escape_string($connect, $_POST["query"]);
         $query = "
         
         SELECT sr.reg_no, st.studentname FROM studentresult sr 
         LEFT JOIN student st ON sr.reg_no = st.reg_no 
         WHERE sr.teachername = '$teachername' 
         AND sr.fid = '$fid'
         AND st.active = 'Yes'
         AND (sr.trial = '' OR sr.trial = 'No')
         AND (sr.deleted IS NULL OR sr.deleted = '0')
         AND st.studentname LIKE '%".$search."%' GROUP BY sr.reg_no, st.studentname
         ";
        }
        else
        {
         $query = "
         SELECT sr.reg_no, st.studentname FROM studentresult sr 
         LEFT JOIN student st ON sr.reg_no = st.reg_no 
         WHERE sr.teachername = '$teachername' 
         AND sr.fid = '$fid'
         AND st.active = 'Yes'
         AND (sr.trial = '' OR sr.trial = 'No')
         AND (sr.deleted IS NULL OR sr.deleted = '0')
         GROUP BY sr.reg_no, st.studentname
         ";
        }
        $result = mysqli_query($connect, $query);
        if(mysqli_num_rows($result) > 0)
        {
         $output .= '
         <thead>
         <tr>
         <th>Reg_no</th>
         <th>Student Name</th>
         <th>Level</th>
         <th></th>
         <th>Pass</th>
         <th>Failed</th>
         <th>Not Complete</th>
         </tr>
         </thead>
         ';
        while($row = mysqli_fetch_array($result))
        {
          $reg_no = $row['reg_no'];
          $output .= '
          <tbody>
          <tr>
          <td>'.$row["reg_no"].'</td>
          <td>'.$row["studentname"].'</td>
          <td>
          ';

            $query1 = "
            SELECT sr.level_id,sr.reg_no FROM studentresult sr
            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
            WHERE sr.teachername = '$teachername' 
            AND st.active = 'Yes'
            AND sr.reg_no = '$reg_no'
            AND (sr.trial = '' OR sr.trial = 'No')
            AND (sr.deleted IS NULL OR sr.deleted = '0') 
            GROUP BY sr.level_id,sr.reg_no
            ";
            if($result1 = mysqli_query($connect, $query1))
            {
                $pass = 0;
                $failed = 0;
                $not = 0;
                while($row1 = mysqli_fetch_array($result1))
                {
                    //echo $row1["level_id"];
                    
                  $level_id = $row1['level_id'];
                  //echo $level_id;
                  $reg_no = $row1['reg_no'];
                  //echo $reg_no.'<br/>';
                  $query2 = "
                  SELECT SUM(sr.number_of_class) AS total,le.total_class, le.level_name, sr.level_id, sr.reg_no FROM studentresult sr
                  LEFT JOIN level le ON (le.level_id=sr.level_id)
                  LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                  WHERE sr.level_id = '$level_id'
                  AND st.active = 'Yes'
                  AND sr.reg_no = '$reg_no' 
                  AND sr.teachername = '$teachername'
                  AND (sr.trial = '' OR sr.trial = 'No')
                  AND (sr.deleted IS NULL OR sr.deleted = '0')
                  ";
                  if($result2 = mysqli_query($connect, $query2))
                  {
                            
                      while($row2 = mysqli_fetch_array($result2))
                      {
                            $total_class = $row2['total_class'];
                            $total = $row2['total'];
                            $level_name = $row2['level_name'];
                            
                            $query3 = "
                            SELECT sr.complete FROM studentresult sr
                            LEFT JOIN level le ON (le.level_id=sr.level_id)
                            LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                            WHERE sr.level_id = '".$row2['level_id']."'
                            AND st.active = 'Yes'
                            AND sr.reg_no = '".$row2['reg_no']."'
                            AND (sr.deleted IS NULL OR sr.deleted = '0') 
                            AND (sr.trial = '' OR sr.trial = 'No')
                            ORDER BY sr.r_id DESC LIMIT 1
                            ";
                            if($result3 = mysqli_query($connect, $query3))
                            {
                                         
                                while($row3 = mysqli_fetch_array($result3))
                                {  

                                  if ($total_class == 0) {
                                    $output .='
                                    <span style="color: #787878;"> '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b>No calculation kpi</b></span><br>
                                    ';
                                  }
                                  elseif($total <= $total_class AND $row3['complete'] == 'yes'){
                                    $pass ++;
                                    $output .='
                                    '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#31D919;">Pass</b><br>
                                    ';
                                  }
                                  elseif($total > $total_class AND $row3['complete'] == 'yes') {
                                      $failed ++;
                                      $output .='
                                      '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#F43939">Fail</b><br>
                                      ';
                                  }elseif ($row3['complete'] == 'no' OR $row3['complete'] == '') {
                                      $not ++;
                                      $output .='
                                      '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#2B8BA6">Not Complete</b><br>
                                      ';
                                  }

                                }
                            }
                        }  
                    }
                }
              $output .='
              </td>
              <td>
              ';
              // $query3 = "
              // SELECT sr.level_id,sr.reg_no FROM studentresult sr
              // LEFT JOIN student st ON (sr.reg_no = st.reg_no)
              // WHERE sr.teachername = '$teachername' 
              // AND sr.reg_no = '$reg_no'
              // AND st.active = 'Yes'
              // AND (sr.trial = '' OR sr.trial = 'No')
              // AND (sr.deleted IS NULL OR sr.deleted = '0') 
              // GROUP BY sr.level_id,sr.reg_no
              // ";
              // if($result3 = mysqli_query($connect, $query3))
              // {
              //     while($row3 = mysqli_fetch_array($result3))
              //     {
              //         //echo $row1["level_id"];
                      
              //         $level_id = $row3['level_id'];
              //         //echo $level_id;
              //         $reg_no = $row3['reg_no'];
              //         //echo $reg_no.'<br/>';
              //         $query4 = "
              //         SELECT count(sr.level_id) AS total,le.total_class, le.level_name FROM studentresult sr
              //         LEFT JOIN level le ON (le.level_id=sr.level_id) 
              //         WHERE sr.level_id = '$level_id' 
              //         AND sr.reg_no = '$reg_no' 
              //         AND sr.teachername = '$teachername'
              //         AND (sr.trial = '' OR sr.trial = 'No')
              //         AND (sr.deleted IS NULL OR sr.deleted = '0') 
              //         ";
              //         if($result4 = mysqli_query($connect, $query4))
              //         {
                                
              //             while($row4 = mysqli_fetch_array($result4))
              //             {
              //                   $total_class = $row4['total_class'];
              //                   $total = $row4['total'];
              //                   $level_name = $row4['level_name'];
              //                   //echo $total.'<br/>';
              //                   //echo $level_id;
              //                   if ($total_class == 0) {
                                  
              //                   }
              //                   elseif($total > $total_class) {
              //                       $output .='
              //                     '.$level_name.' &nbsp;/&nbsp; class: ('.$total.') &nbsp;/&nbsp; total_class: ('.$total_class.')<br>
              //                     ';
              //                   }
              //               }  
              //           }
              //     }
              // }
              $output .='
              </td>
              <td>'.$pass.'</td>
              <td>'.$failed.'</td>
              <td>'.$not.'</td>
              </tr>
              </tbody>
            '; 
          }
      }
 echo $output;
 //echo '<script language="javascript">alert("message successfully sent")</script>';
}
else
{
 echo 'Data Not Found';
}

?>