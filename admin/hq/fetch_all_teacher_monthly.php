<?php
include '..//dbConfig.php';
session_start();
if ($_SESSION['name'] == "") {
  header("Location: index.php");
}
$response = "";
$response1 = "";
$fid = $_POST["fid"];
$text = $_POST["text"];
$month = date("m", strtotime($text));
$year = date("Y", strtotime($text));
$functionName = $_POST["functionName"];
 // echo '<script type="text/javascript">alert("'.$month.'")</script>';
 // echo '<script type="text/javascript">alert("'.$year.'")</script>';
if($functionName == "fid" AND $fid == "all") {
    $response.='
    Please Select Centre First';
    echo $response;
}
elseif($functionName == "fid" AND $fid != "all") {
   $query = "
   SELECT sr.teachername, sr.insert_date FROM studentresult sr 
   LEFT JOIN teacher te ON (te.username = sr.teachername) 
   LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
   WHERE sr.fid = '$fid' 
   AND s.active = 'Yes'
   AND month(sr.insert_date) = '".$month."' 
   AND year(sr.insert_date) = '".$year."' 
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   GROUP BY sr.teachername ORDER BY sr.teachername
   ";
   //echo '<script type="text/javascript">alert("a")</script>';
  $result = mysqli_query($connect, $query);
  if(mysqli_num_rows($result) > 0)
  {
   $response .= '
    <thead>
    <tr>
    <th>Teacher Name</th>
    <th>Morning 1</th>
    <th>Morning 2</th>
    <th>Afternoon 1</th>
    <th>Afternoon 2</th>
    <th>Afternoon 3</th>
    <th>Afternoon 4</th>
    <th>Total</th>
    </tr>
    </thead>
   ';
   while($row = mysqli_fetch_array($result))
   {
    $response .= '
    <tbody>
    <tr>
    <td>'.$row["teachername"].'</td>
    ';
     $query_time = "
      SELECT sr.times1, sr.reg_no, sr.insert_date FROM studentresult sr 
      LEFT JOIN language la ON (sr.language_id = la.language_id) 
      LEFT JOIN level le ON (sr.level_id = le.level_id) 
      LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
      WHERE sr.teachername = '".$row["teachername"]."' 
      AND st.active = 'Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0')
      AND (sr.trial = '' OR sr.trial = 'No')
      AND month(sr.insert_date) = '".$month."' 
      AND year(sr.insert_date) = '".$year."'
      GROUP BY sr.reg_no, sr.times1, sr.insert_date ORDER BY sr.r_id 
    ";
    
      if($result_time = mysqli_query($connect, $query_time))
      {
        $mo1 = '0';
        $mo2 = '0';
        $af1 = '0';
        $af2 = '0';
        $af3 = '0';
        $af4 = '0';
        $total = '0';
        while($rowcount_time=mysqli_fetch_array($result_time))
        {
          $time = $rowcount_time['times1'];
          if ($time == 'Morning1') {
            $mo1 ++;
          }
          if ($time == 'Morning2') {
            $mo2 ++;
          }
          if ($time == 'Afthernoon1') {
            $af1 ++;
          }
          if ($time == 'Afthernoon2') {
            $af2 ++;
          }
          if ($time == 'Afthernoon3') {
            $af3 ++;
          }
          if ($time == 'Afthernoon4') {
            $af4 ++;
          }
          $total = $mo1 + $mo2 + $af1 + $af2 + $af3 + $af4;
        }
        $response .= '
        <td>'.$mo1.'</td>
        <td>'.$mo2.'</td>
        <td>'.$af1.'</td>
        <td>'.$af2.'</td>
        <td>'.$af3.'</td>
        <td>'.$af4.'</td>
        <td>'.$total.'</td>
        </tr>
        </tbody>
        ';
      }
   }
   
  }
  else{
      $response.='
      No Record';
  }
  echo $response;
}
if($functionName == "text" AND $fid == "all") {
    $response1.='
    Please Select Centre First';
    echo $response1;
}
elseif($functionName == "text" AND $fid != "all") {
   $query = "
   SELECT sr.teachername, sr.insert_date FROM studentresult sr 
   LEFT JOIN teacher te ON (te.username = sr.teachername) 
   LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
   WHERE sr.fid = '$fid' 
   AND s.active = 'Yes'
   AND month(sr.insert_date) = '".$month."' 
   AND year(sr.insert_date) = '".$year."' 
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   GROUP BY sr.teachername ORDER BY sr.teachername
   ";
   //echo '<script type="text/javascript">alert("a")</script>';
  $result = mysqli_query($connect, $query);
  if(mysqli_num_rows($result) > 0)
  {
   $response1 .= '
    <thead>
    <tr>
    <th>Teacher Name</th>
    <th>Morning 1</th>
    <th>Morning 2</th>
    <th>Afternoon 1</th>
    <th>Afternoon 2</th>
    <th>Afternoon 3</th>
    <th>Afternoon 4</th>
    <th>Total</th>
    </tr>
    </thead>
   ';
   while($row = mysqli_fetch_array($result))
   {
    $response1 .= '
    <tbody>
    <tr>
    <td>'.$row["teachername"].'</td>
    ';
     $query_time = "
      SELECT sr.times1, sr.reg_no, sr.insert_date FROM studentresult sr 
      LEFT JOIN language la ON (sr.language_id = la.language_id) 
      LEFT JOIN level le ON (sr.level_id = le.level_id) 
      LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
      WHERE sr.teachername = '".$row["teachername"]."' 
      AND st.active = 'Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0')
      AND (sr.trial = '' OR sr.trial = 'No')
      AND month(sr.insert_date) = '".$month."' 
      AND year(sr.insert_date) = '".$year."'
      GROUP BY sr.reg_no, sr.times1, sr.insert_date ORDER BY sr.r_id 
    ";
    
      if($result_time = mysqli_query($connect, $query_time))
      {
        $mo1 = '0';
        $mo2 = '0';
        $af1 = '0';
        $af2 = '0';
        $af3 = '0';
        $af4 = '0';
        $total = '0';
        while($rowcount_time=mysqli_fetch_array($result_time))
        {
          $time = $rowcount_time['times1'];
          if ($time == 'Morning1') {
            $mo1 ++;
          }
          if ($time == 'Morning2') {
            $mo2 ++;
          }
          if ($time == 'Afthernoon1') {
            $af1 ++;
          }
          if ($time == 'Afthernoon2') {
            $af2 ++;
          }
          if ($time == 'Afthernoon3') {
            $af3 ++;
          }
          if ($time == 'Afthernoon4') {
            $af4 ++;
          }
          $total = $mo1 + $mo2 + $af1 + $af2 + $af3 + $af4;
        }
        $response1 .= '
        <td>'.$mo1.'</td>
        <td>'.$mo2.'</td>
        <td>'.$af1.'</td>
        <td>'.$af2.'</td>
        <td>'.$af3.'</td>
        <td>'.$af4.'</td>
        <td>'.$total.'</td>
        </tr>
        </tbody>
        ';
      }
   }
   
  }
  else{
      $response1.='
      No Record';
  }
  echo $response1;
}

?>
