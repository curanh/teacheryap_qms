<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
   
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Guarantee Improvement Report</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <!-- <img class="ldld" src="your-loader-url"> -->    
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
</head>
</style>
<body>
  <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" style="color: #85AFF6;" class="shake">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li class="shake"><a onclick="window.location.href='hq_gi_report.php'" style="color: #85AFF6;cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
              <div class="panel-heading">
                <h3 style="color: #6E6B6B;">Guarantee Improvement Report<h3>
              </div>
              <div class="panel-body">
                <div id="rootwizard">
                  <div class="navbar">
                      <div class="navbar-inner">
                        <div class="container">
                          <ul class="nav nav-pills">
                              <li class="active"><a href="#tab1" data-toggle="tab">Center Report</a></li>
                            <li><a href="#tab2" data-toggle="tab">Student Report</a></li>
                          </ul>
                        </div>
                      </div>
                  </div>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab1" >
                        <div class="panel-heading">
                          <h3 style="color: #6E6B6B;">Center Report<h3>
                        </div>
                        <div class="panel-body">
                          <div class="table-responsive">
                            <div class="form-group">
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon">Center</span>
                                  <select class="form-control" id="search_text_center" name="search_text_center" style="width: 25%">
                                    <option value="all" selected>Please select a centre</option>
                                    <?php
                                    $query = $connect->query("
                                    SELECT name,fid FROM centre ORDER BY name");
                                     $rowCount = $query->num_rows;
                                      if($rowCount > 0){
                                        while($row = $query->fetch_assoc())
                                        {
                                            echo "<option value='".$row['fid']."'>".$row['name']."</option>";
                                        }
                                      }
                                    ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Language</span>
                                  <select class="form-control" id="search_lang" name="search_lang" style="width: 23.5%">
                                    <option value="all">All</option>
                                    <option value="1">华语</option>
                                    <option value="2">国语</option>
                                    <option value="3">珠算</option>
                                    <option value="4">英语</option>
                                    <option value="5">PK</option>
                                    <option value="6">TT</option>
                                    <option value="7">MIQ</option>
                                    <option value="8">HOTS 36</option>
                                    <option value="9">Online 华语</option>
                                  </select>
                                  </div>
                            </div>
                            <table class="table" id="result">
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="tab2">
                        <div class="tab-pane active" id="tab1" \>
                          <div class="panel-heading">
                            <h3 style="color: #6E6B6B;">Student Report<h3>
                          </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                              <div class="form-group">
                                <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Center</span>
                                  <select class="form-control" id="search_text_fid" name="search_text_fid" style="width: 25%">
                                    <option value="all" selected>Please select a centre</option>
                                    <?php
                                    $query = $connect->query("
                                    SELECT name,fid FROM centre ORDER BY name");
                                     $rowCount = $query->num_rows;
                                      if($rowCount > 0){
                                        while($row = $query->fetch_assoc())
                                        {
                                            echo "<option value='".$row['fid']."'>".$row['name']."</option>";
                                        }
                                      }
                                    ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Age</span>
                                    <select class="form-control" id="search_text_age" name="search_text_age" style="width: 26.2%">
                                      <option value="> 0" selected>All</option>
                                      <option value="<= 6">3 - 6 Age</option>
                                      <option value=">= 7">7 - 12 Age</option>
                                    </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Status</span>
                                    <select class="form-control" id="search_text_status" name="search_text_status" style="width: 25.2%">
                                      <option value="0" selected>All</option>
                                      <option value="1">Pass</option>
                                      <option value="2">Fail</option>
                                      <option value="3">Not Complete</option>
                                      <option value="4">No KPI Class</option>
                                    </select>
                                </div>
                              </div>
                              <table class="table" id="result2">
                              </table>
                            </div>
                          </div>
                      </div>
                  </div>  
                </div>
              </div>
            </div>
          </div>

</body>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="..//js/custom.js"></script>
<script>
$(document).ready(function() {
  $("#result").html("Please Select Centre First");      
  var documentRoot = "fetch_gi_report_center.php";
  // $("#result").html("Pls Search");
  $("#search_text_center").change(function() {
       // alert($("#search_text_fid").val());

      $('#result').html("Loading Please Wait...........");
      var form_data = {
              fid: $("#search_text_center").val(),
              lang: $("#search_lang").val(),
              functionName: 'fid'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data, success: function(response) {
        // alert (response);
        
        $("#result").html(response);

      }});
  });

    // $("#result").html("Pls Search");
  $("#search_lang").change(function() {
       // alert($("#language").val());
       // alert("he");
      $('#result').html("Loading Please Wait...........");
      var form_data_time = {
              lang: $("#search_lang").val(),
              fid: $("#search_text_center").val(),
              functionName: 'lang'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response1) {
        // alert (response);
        
        $("#result").html(response1);

      }});
  });

});
</script>
<script>
$(document).ready(function() {
  $("#result2").html("Please Select Centre First");      
  var documentRoot = "fetch_gi_report_student.php";
  // $("#result").html("Pls Search");
  $("#search_text_fid").change(function() {
       // alert($("#search_text_fid").val());

      $('#result2').html("Loading Please Wait...........");
      var form_data = {
              fid: $("#search_text_fid").val(),
              age: $("#search_text_age").val(),
              status: $("#search_text_status").val(),
              functionName: 'fid'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data, success: function(response) {
        // alert (response);
        
        $("#result2").html(response);

      }});
  });

  // $("#result").html("Pls Search");
  $("#search_text_age").change(function() {
       // alert($("#language").val());
       // alert("he");
      $('#result2').html("Loading Please Wait...........");
      var form_data_time = {
              age: $("#search_text_age").val(),
              fid: $("#search_text_fid").val(),
              status: $("#search_text_status").val(),
              functionName: 'age'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response1) {
        // alert (response);
        
        $("#result2").html(response1);

      }});
  });

  $("#search_text_status").change(function() {
       // alert($("#language").val());
       // alert("he");
      $('#result2').html("Loading Please Wait...........");
      var form_data_time = {
              status: $("#search_text_status").val(),
              age: $("#search_text_age").val(),
              fid: $("#search_text_fid").val(),
              functionName: 'status'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response2) {
        // alert (response);
        
        $("#result2").html(response2);

      }});
  });
});

</script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
</script>
</html>
