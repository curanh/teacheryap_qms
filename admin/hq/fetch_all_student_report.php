<?php
//fetch.php
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");
        
}
  if(isset($_GET['reg_no']) && isset($_GET['level_id']))
  {
    $UserID = base64_decode($_GET['reg_no']);
    $Level_id = base64_decode($_GET['level_id']);
    $fid = base64_decode($_GET['fid']);
    $studentname = base64_decode($_GET['studentname']);
    $active = base64_decode($_GET['active']);
  }

require_once("..//dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 //echo $query;
}

else
{
 $query = "
  SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr 
  LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id) 
  LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
  WHERE sr.reg_no='".$UserID."' AND sr.level_id='".$Level_id."' 
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No') ORDER BY sr.insert_date DESC
 ";
}

$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
    <tr>
      <th>Date</th>
      <th>Teacher</th>
      <th>Feedback</th>
      <th>Title</th>
      <th>Status</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
 ';

 while($row = mysqli_fetch_array($result))
 {
    //echo $level_id;
  $id = $row["r_id"];
    $output .= '
  <tbody>
    <tr>
      <td><strong>'.$row["insert_date"].'</strong></td>
      <td>'.$row["teachername"].'</td>
      <td>'.$row["feedback"].'</td>
      <td>';
      if ($row['method'] == 'Yes') {     
      $query_title = "
      SELECT * FROM level_title 
      WHERE status = 1 
      AND (deleted IS NULL OR deleted = 0) 
      AND level_id = '".$Level_id."' 
      AND title_id IN (".$row["title_id"].") 
      ORDER BY sorting ASC";

      $result_title = mysqli_query($connect,$query_title);
      while($row_titleid=mysqli_fetch_assoc($result_title))
      {   

        $output .= $row_titleid['title_name'].'<br>';        
      }
      }else{
      $output.= $row["title_id"];
      }
      $output .='
      </td>
      <td>'.$row["student_status"].'</td>
      <td>
      <select class="form-control" id="select_option_'.$id.'" name="select_option">
        <option value="">Please select reason</option>
        <option value="select wrong level">Choose the wrong level</option>
        <option value="others">Others</option>
      </select>
      </td>
      <td>
      <a class="btn btn-danger" onclick="delelet('.$id.')"><i class="glyphicon glyphicon-remove"></i> Delete</a>
      </td>
      <script>
      function delelet($id){
          var box = document.getElementById("select_option_"+$id+"").value;
          if(box == ""){
            alert("Please select reason");
          }else{

            if (box == "others"){
              var reason = prompt("Please enter your reason");
                if(reason.length != 0){
                    var del=confirm("Are you sure you want to delete this Record?");
                    if (del==true) {
                      window.location.href = "hq_delete.php?reason="+reason+"&r_id="+$id+"&fid='.$_GET['fid'].'&username='.$_SESSION['name'].'&reg_no='.$_GET['reg_no'].'&level_id='.$_GET['level_id'].'&studentname='.$_GET['studentname'].'&active='.$_GET['active'].'";
                      //alert("Pls la");
                    }
                }else if (reason.length == 0){
                      delelet($id);
                }
            }else if (box == "select wrong level"){
              var del=confirm("Are you sure you want to delete this Record?");
                if (del==true) {
                  window.location.href = "hq_delete.php?reason="+box+"&r_id="+$id+"&fid='.$_GET['fid'].'&username='.$_SESSION['name'].'&reg_no='.$_GET['reg_no'].'&level_id='.$_GET['level_id'].'&studentname='.$_GET['studentname'].'&active='.$_GET['active'].'";
                }
            }

          }
        }
      </script>
  </tr>
   </tbody>
 ';

 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>