<?php
include '..//dbConfig.php';
session_start();
if ($_SESSION['name'] == "") {
  header("Location: index.php");
}
$response = "";
$response1 = "";
$fid = $_POST["fid"];
$text = $_POST["text"];
$functionName = $_POST["functionName"];
 // echo '<script type="text/javascript">alert("'.$date.'")</script>';
 // echo '<script type="text/javascript">alert("'.$time.'")</script>';
if($functionName == "fid" AND $fid == "all") {
    $response.='
    Please Select Centre First';
    echo $response;
}
elseif($functionName == "fid" AND $fid != "all") {
    $query_student = "SELECT * FROM student WHERE fid = '".$fid."' AND active = 'Yes' ORDER BY studentname";
    $result_student = mysqli_query($connect, $query_student);
    $row_student = mysqli_num_rows($result_student);
            //echo '<script type="text/javascript">alert("'.$query_date.'")</script>';
            
            if(mysqli_num_rows($result_student) > 0){
              $response.= '
              <thead>
              <tr>
              <th>Student Name</th>
              <th>Reg No</th>
              <th>Born</th>
              <th>Subject</th>
              <th></th>
              </tr>
              </thead>
              ';
              while($row_student = mysqli_fetch_array($result_student)){
                $born = date('Y',strtotime($row_student["dob"]));
                $response.= '
                <tbody>
                <tr>
                <td>'.$row_student["studentname"].'</td>
                <td>'.$row_student["reg_no"].'</td>
                <td>'.$born.'</td>
                <td>
                ';
                $query_cms = "
                SELECT * FROM student WHERE fid = '$fid' AND active = 'Yes' AND reg_no = '".$row_student['reg_no']."' ORDER BY name
                ";
                $result_cms = mysqli_query($connect2, $query_cms);
                if(mysqli_num_rows($result_cms) > 0)
                {
                  while($row_cms = mysqli_fetch_array($result_cms))
                  {
                    $mand = $row_cms['language_mand'];
                    $bm = $row_cms['language_bm'];
                    $en = $row_cms['language_en'];
                    $ma = $row_cms['language_ma'];
                    $miq = $row_cms['language_miq'];
                      if ($mand == 'Yes') {
                        $response .= '
                        <b>华语 (Chinese)&#x2705;</b><br>
                        ';
                      }
                      if ($bm == 'Yes') {
                        $response .= '
                        <b>国语 (Melayu)&#x2705;</b><br>
                        ';
                      }
                      if ($en == 'Yes') {
                        $response .= '
                        <b>英语 (English)&#x2705;</b><br>
                        ';
                      }
                      if ($ma == 'Yes') {
                        $response .= '
                        <b>珠算 (Abacus)&#x2705;</b><br>
                        ';
                      }
                      if ($miq == 'Yes') {
                        $response .= '
                        <b>MIQ &#x2705;</b><br>
                        ';
                      }
                       
                  }
                }
                $response .='
                </td>
                <td><button class="btn btn-warning" onclick=window.location.href="hq_view_student_report.php?reg_no='.base64_encode($row_student["reg_no"]).'&studentname='.base64_encode(urlencode($row_student["studentname"])).'&fid='.base64_encode($fid).'&active='.base64_encode('Yes').'">Report</button></td>
                </tr>
                </tbody>
                ';
              }
              
            }
            else{
                $response.='
                No Record';
            }
            echo $response;
}
if($functionName == "text" AND $fid == "all") {
    $response1.='
    Please Select Centre First';
    echo $response1;
}
elseif($functionName == "text" AND $fid != "all") {
    $query_student_search = "SELECT * FROM student WHERE fid = '".$fid."' AND studentname LIKE '%".$text."%' AND active = 'Yes' ORDER BY studentname";

          $result_student_search = mysqli_query($connect, $query_student_search);
          $row_student_search = mysqli_num_rows($result_student_search);

              if(mysqli_num_rows($result_student_search) > 0){
                $response1.= '
                <thead>
                <tr>
                <th>Student Name</th>
                <th>Reg No</th>
                <th>Born</th>
                <th>Subject</th>
                <th></th>
                </tr>
                </thead>
                ';
                while($row_student_search = mysqli_fetch_array($result_student_search)){
                  $born = date('Y',strtotime($row_student_search["dob"]));
                  $response1.= '
                  <tbody>
                  <tr>
                  <td>'.$row_student_search["studentname"].'</td>
                  <td>'.$row_student_search["reg_no"].'</td>
                  <td>'.$born.'</td>
                  <td>
                  ';
                  $query_cms = "
                  SELECT * FROM student WHERE fid = '$fid' AND active = 'Yes' AND reg_no = '".$row_student_search['reg_no']."' ORDER BY name
                  ";
                  $result_cms = mysqli_query($connect2, $query_cms);
                  if(mysqli_num_rows($result_cms) > 0)
                  {
                    while($row_cms = mysqli_fetch_array($result_cms))
                    {
                      $mand = $row_cms['language_mand'];
                      $bm = $row_cms['language_bm'];
                      $en = $row_cms['language_en'];
                      $ma = $row_cms['language_ma'];
                      $miq = $row_cms['language_miq'];
                        if ($mand == 'Yes') {
                          $response1 .= '
                          <b>华语 (Chinese)&#x2705;</b><br>
                          ';
                        }
                        if ($bm == 'Yes') {
                          $response1 .= '
                          <b>国语 (Melayu)&#x2705;</b><br>
                          ';
                        }
                        if ($en == 'Yes') {
                          $response1 .= '
                          <b>英语 (English)&#x2705;</b><br>
                          ';
                        }
                        if ($ma == 'Yes') {
                          $response1 .= '
                          <b>珠算 (Abacus)&#x2705;</b><br>
                          ';
                        }
                        if ($miq == 'Yes') {
                          $response1 .= '
                          <b>MIQ &#x2705;</b><br>
                          ';
                        }
                         
                    }
                  }
                  $response1 .='
                  </td>
                  <td><button class="btn btn-warning" onclick=window.location.href="hq_view_student_report.php?reg_no='.base64_encode($row_student_search["reg_no"]).'&studentname='.base64_encode(urlencode($row_student_search["studentname"])).'&fid='.base64_encode($fid).'&active='.base64_encode('Yes').'">Report</button></td>
                  </tr>
                  </tbody>
                  ';
                }
              }
            else{
                $response1.='
                No Record';
            }
            echo $response1;
}


?>
