<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
    if (isset($_POST['button'])){
      if(isset($_POST['search_text'])){
        if(($_POST['search_text'] !="")){
        $fid = $_POST['search_text'];

        }
      }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>NOT COMPLETE REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" style="color: #85AFF6;" class="shake">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li class="shake"><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;color: #85AFF6;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
          <div class="panel-heading">
          <h3 style="color: #6E6B6B;">Not Complete Report<h3>
        </div>
          <form method="POST">
          <div class="panel-body">
            <div class="table-responsive">
                <div class="form-group">
                  <div class="input-group form">
                       <select class="form-control" id="search_text" name="search_text">
                        <option value="no" selected>Please select a centre</option>
                        <?php
                        $query = $connect->query("
                        SELECT name,fid FROM centre ORDER BY name");
                         $rowCount = $query->num_rows;
                          if($rowCount > 0){
                            while($row = $query->fetch_assoc())
                            {
                              if($row['fid'] == $fid){
                                echo "<option value='".$row['fid']."' selected>".$row['name']."</option>";
                              }
                              else{
                                echo "<option value='".$row['fid']."'>".$row['name']."</option>";
                              }
                            }
                          }
                        ?>
                      </select>
                       <span class="input-group-btn">
                         <button class="btn btn-primary" type="submit" name="button">Search</button>
                       </span>
                    </div>
                    <br>                  
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                    <th width="20%">Studen Name</th>
                    <th width="15%">Reg No</th>
                    <th width="10%">Language</th>
                    <th width="20%">Level</th>
                    <th width="10%">Start Date</th>
                    <th width="10%">Last Insert Date</th>
                    <th width="10%">View</th>
                    <th width="10%">Complete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $fid = "";
                    if (isset($_POST['button'])){
                      if(isset($_POST['search_text'])){
                        if(($_POST['search_text'] !="")){
                    $fid = $_POST['search_text'];
                    $query_student = "SELECT s.reg_no, s.studentname, s.active FROM student s  
                    WHERE s.fid = '".$fid."' 
                    AND s.active='Yes' ORDER BY s.studentname ASC";

                    $result_student = mysqli_query($connect, $query_student);          
                    while($row_student = mysqli_fetch_array($result_student))
                      {
                        $reg_no = $row_student["reg_no"];
                        $studentname = $row_student["studentname"];
                        $active = $row_student["active"];

                        $query_student_result = "SELECT DISTINCT level_id FROM studentresult
                        WHERE reg_no = '".$reg_no."'";
                        $result_student_result = mysqli_query($connect, $query_student_result); 
                        while($row_student_result = mysqli_fetch_array($result_student_result))
                        {
                          // $language_name = $row_student_result["language_name"];
                          // $level_name = $row_student_result["level_name"];
                          $level_id = $row_student_result["level_id"];
                          // $r_id = $row_student_result["r_id"];

                          $query = "SELECT lg.language_name, lv.level_name, sr.level_id, sr.complete, sr.r_id FROM studentresult sr
                         LEFT JOIN level lv ON (sr.level_id=lv.level_id) 
                         LEFT JOIN language lg ON (sr.language_id = lg.language_id)
                         WHERE sr.fid = '".$fid."' 
                         AND sr.reg_no = '".$reg_no."' 
                         AND sr.deleted='0' 
                         AND sr.level_id = '".$level_id."'
                         ORDER BY sr.r_id DESC LIMIT 1";
                          $result = mysqli_query($connect, $query); 
                          while($row = mysqli_fetch_array($result))
                          {
                            $language_name = $row["language_name"];
                            $level_name = $row["level_name"];
                            $r_id = $row["r_id"];
                            $complete = $row["complete"];
                            
                            if ($complete == 'no') {
                              //$insert_date = date("d/m/Y",strtotime($insert_date));
                              $query_start_date = "SELECT sr.insert_date FROM studentresult sr
                              WHERE sr.fid = '".$fid."' 
                              AND sr.deleted='0' 
                              AND sr.level_id='".$level_id."' 
                              AND sr.reg_no='".$reg_no."'
                              AND sr.complete='no'
                              ORDER BY sr.insert_date ASC LIMIT 1";
                              $result_start_date = mysqli_query($connect, $query_start_date); 

                              while($row_start_date = mysqli_fetch_array($result_start_date))
                              {
                                $insert_date = $row_start_date["insert_date"];
                                // $insert_date = date("d/m/Y",strtotime($insert_date));

                                $query_last_date = "SELECT sr.insert_date FROM studentresult sr
                                                 WHERE sr.fid = '".$fid."' 
                                                 AND sr.deleted='0' 
                                                 AND sr.level_id='".$level_id."' 
                                                 AND sr.reg_no='".$reg_no."'
                                                 AND sr.complete='no'
                                                 ORDER BY sr.insert_date DESC LIMIT 1";
                                $result_last_date = mysqli_query($connect, $query_last_date); 
                                while($row_last_date = mysqli_fetch_array($result_last_date))
                                {
                                $last_date = $row_last_date["insert_date"];
                                // $last_date = date("d/m/Y",strtotime($last_date));

                                  echo 
                                  '<tr><td>
                                  '.$studentname.'
                                  </td><td>
                                  '.$reg_no.'
                                  </td><td>
                                  '.$language_name.'
                                  </td><td>
                                  '.$level_name.'
                                  </td><td>
                                  '.$insert_date.'
                                  </td><td>
                                  '.$last_date.'
                                  </td>
                                  <td>
                                  <a href="hq_total_student_report.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'&fid='.base64_encode($fid).'" class="btn btn-default"> View</a>
                                  </td>
                                  <td><a href="fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'&id='.base64_encode($r_id).'&complete='.base64_encode('3').'" class="btn btn-info">Complete</a></td>
                                  </tr>';
                                  
                                  
                                }

                                
                              }

                            }
                          }   


                        }
                      }
                    }
                  }
                }
                    ?>
                    </tbody>
                    </table>
                </div>
            </div>
          </div>
        </form>

</body>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="..//js/custom.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
</script>
</html>
