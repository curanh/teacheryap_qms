<?php
include '..//dbConfig.php';
session_start();
if ($_SESSION['name'] == "") {
  header("Location: index.php");
}
$response = "";
$response1 = "";
$fid = $_POST["fid"];
$text = $_POST["text"];
$functionName = $_POST["functionName"];
 // echo '<script type="text/javascript">alert("'.$date.'")</script>';
 // echo '<script type="text/javascript">alert("'.$time.'")</script>';
if($functionName == "fid" AND $fid == "all") {
    $response.='
    Please Select Centre First';
    echo $response;
}
elseif($functionName == "fid" AND $fid != "all") {
    $query_fid = "SELECT * FROM teacher WHERE fid = '".$fid."' AND (deleted IS NULL OR deleted = '0')";
    $result_fid = mysqli_query($connect, $query_fid);
    $row = mysqli_num_rows($result_fid);
            // echo '<script type="text/javascript">alert("'.$query_fid.'")</script>';
            
            if(mysqli_num_rows($result_fid) > 0){
              $response .= '
              <thead>
              <tr>
              <th>#</th>
              <th>Teacher Name</th>
              <th>QMS Pass</th>
              <th>QMS Failed</th>
              <th>LMS Pass</th>
              <th>LMS Failed</th>
              <th>Not Complete</th>
              <th>No calculation kpi</th>
              <th>QMS KPI %</th>
              <th>LMS KPI %</th>
              <th>Total KPI %</th>
              <th></th>
              </tr>
              </thead>
              ';
              while($row = mysqli_fetch_array($result_fid)){
                    $teachername = $row["username"];

                    $query1 = "
                    SELECT sr.level_id,sr.reg_no FROM studentresult sr
                    LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                    WHERE sr.teachername = '$teachername' 
                    AND st.active = 'Yes'
                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
                    AND (sr.trial = '' OR sr.trial = 'No')
                    GROUP BY sr.level_id,sr.reg_no
                    ";
                    if($result1 = mysqli_query($connect, $query1))
                    {
                        //qms
                        $pass = 0;
                        $failed = 0;
                        $not = 0;
                        $no_count = 0;
                        $kpi = 0;
                        //lms
                        $kpi_lms = 0;
                        $total_kpi = 0;
                        //set kpi%
                        $qmskpi = 40;
                        $lmskpi = 30;
                        $level_array = array();
                        while($row1 = mysqli_fetch_array($result1))
                        {

                            $level_id = $row1['level_id'];
                            $reg_no = $row1['reg_no'];

                        $query2 = "
                        SELECT SUM(sr.number_of_class) AS total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
                        LEFT JOIN level le ON (le.level_id=sr.level_id)
                        LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                        WHERE sr.level_id = '$level_id' 
                        AND st.active = 'Yes'
                        AND sr.reg_no = '$reg_no' 
                        AND sr.teachername = '$teachername' 
                        AND (sr.deleted IS NULL OR sr.deleted = '0') 
                        AND (sr.trial = '' OR sr.trial = 'No')
                        ";
                        if($result2 = mysqli_query($connect, $query2))
                        {
                                     
                            while($row2 = mysqli_fetch_array($result2))
                            {
                                  $total_class = $row2['total_class'];
                                  $total = $row2['total'];
                                  
                                  $query3 = "
                                  SELECT sr.complete FROM studentresult sr
                                  LEFT JOIN level le ON (le.level_id=sr.level_id)
                                  LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                                  WHERE sr.level_id = '".$row2['level_id']."'
                                  AND st.active = 'Yes'
                                  AND sr.reg_no = '".$row2['reg_no']."'
                                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                  AND (sr.trial = '' OR sr.trial = 'No')
                                  ORDER BY sr.r_id DESC LIMIT 1
                                  ";
                                  if($result3 = mysqli_query($connect, $query3))
                                  {
                                               
                                      while($row3 = mysqli_fetch_array($result3))
                                      {  
                                    
                                        if ($total_class == 0) {
                                          $no_count ++;
                                        }
                                        elseif($total <= $total_class AND $row3['complete'] == 'yes'){
                                            $pass ++;
                                        }
                                        elseif($total > $total_class AND $row3['complete'] == 'yes') {
                                            $failed ++;
                                        }elseif ($row3['complete'] == 'no' OR $row3['complete'] == '') {
                                            $not ++;
                                        }
                                      
                                        $total_student = $pass + $failed;
                                        if ($total_student > 0) {
                                          $kpi = $pass/$total_student * $qmskpi;
                                        }

                                      }
                                  }
                                  
                            // $query3 = "UPDATE teacher SET  qms_kpi = '$kpi' WHERE username = '$teachername'";
                            // $sql_result3 = mysqli_query($connect,$query3);

                            }          
                          }

                        }

                      }
                      $all_pass = 0;
                      $all_fail = 0;
                      $query_lms = "
                      SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
                      LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                      WHERE sr.teachername = '$teachername' 
                      AND sr.fid = '$fid'
                      AND st.active = 'Yes'
                      AND (sr.trial = '' OR sr.trial = 'No')
                      AND (sr.deleted IS NULL OR sr.deleted = '0')
                      GROUP BY sr.reg_no, st.studentname
                      ";
                    
                      $result_lms = mysqli_query($connect, $query_lms);
                      if(mysqli_num_rows($result_lms) > 0)
                      {
                       
                      
                      while($row_lms = mysqli_fetch_array($result_lms))
                      {
                          $pass_lms = 0;
                          $failed_lms = 0;
                          $reg_no = $row_lms['reg_no'];
                          
                          $level_array = array();
                          $query_lms1 = "
                          SELECT sr.level_id FROM studentresult sr
                          LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                          WHERE sr.teachername = '$teachername' 
                          AND st.active = 'Yes'
                          AND sr.reg_no = '$reg_no'
                          AND (sr.trial = '' OR sr.trial = 'No')
                          AND (sr.deleted IS NULL OR sr.deleted = '0') 
                          GROUP BY sr.level_id
                          ";
                          if($result_lms1 = mysqli_query($connect, $query_lms1))
                          {
                              while($row_lms1 = mysqli_fetch_array($result_lms1))
                              {
                                  $level_id = $row_lms1['level_id'];
                                  // echo $level_id; exit();
                                  if ($level_id != 'Level') {
                                      array_push($level_array, $level_id);
                                  }
                                  
                              }
                              $level = join(",",$level_array);
                              $query_qlms = "
                              SELECT * FROM qlms_level WHERE level_id IN (".$level.")
                              GROUP BY lms_level
                              ";
                              // echo $query_qlms; exit();
                              // echo $query_qlms; exit();
                              if($result_qlms = mysqli_query($connect, $query_qlms))
                              {
                                  while($row_qlms = mysqli_fetch_array($result_qlms))
                                  {
                                      $level_id = $row_qlms['level_id'];
                                      $lms_level = $row_qlms['lms_level'];

                                      if ($lms_level != '') {
                                  
                                          $query_ex = "
                                          SELECT * FROM examresult WHERE level = '".$lms_level."'
                                          AND reg_no = '".$reg_no."' AND fid = '".$fid."'
                                          ";

                                          if($result_ex = mysqli_query($connect1, $query_ex))
                                          {
                                              while($row_ex = mysqli_fetch_array($result_ex))
                                              {

                                                  $status = $row_ex['status1'];
                                                  if ($status == 'Pass'){
                                                      $pass_lms ++;
                                                  }
                                                  if ($status == 'Fail') {
                                                      $failed_lms ++;
                                                  }

                                              }
                                          }
                                      }else{
                                          
                                      }
                                  }
                                  $all_pass += $pass_lms;
                                  $all_fail += $failed_lms;
                                  $total_student = $all_pass + $all_fail;
                                  if ($total_student == 0) {
                                      
                                  }
                                  else{
                                  $kpi_lms = $all_pass/$total_student * $lmskpi;
                                  }
                                  
                              }
                          }  
                      }
                      $total_kpi = $kpi + $kpi_lms;
                  }
                      
                      if ($pass > 0 OR $failed > 0 OR $not > 0 OR $no_count > 0) {
                      $response .='
                      <tbody>
                      <tr>
                      <td>'.$row["id"].'</td>
                      <td>'.$row["username"].'</td>
                      <td>'.$pass.'</td>
                      <td>'.$failed.'</td>
                      <td>'.$all_pass.'</td>
                      <td>'.$all_fail.'</td>
                      <td>'.$not.'</td>
                      <td>'.$no_count.'</td>
                      <td>'.floor($kpi).'</td>
                      <td>'.floor($kpi_lms).'</td>
                      <td>'.floor($total_kpi).'</td>
                      <td><button class="btn btn-info" onclick=window.location.href="hq_student_detail.php?teachername='.base64_encode(urlencode($row["username"])).'&fid='.base64_encode($fid).'">Detail</button></td>
                      </tr>
                      </tbody>
                      ';
                      }
              }
              
            }
            else{
                $response.='
                No Record';
            }
            echo $response;
}
if($functionName == "text" AND $fid == "all") {
    $response1.='
    Please Select Centre First';
    echo $response1;
}
elseif($functionName == "text" AND $fid != "all") {

    $query_text = "SELECT * FROM teacher WHERE username LIKE '%".$text."%' AND fid = '".$fid."' AND (deleted IS NULL OR deleted = '0')";

          $result_text = mysqli_query($connect, $query_text);
          $row_text = mysqli_num_rows($result_text);
              $level_array = '';
              if(mysqli_num_rows($result_text) > 0){
              $response1 .= '
              <thead>
              <tr>
              <th>#</th>
              <th>Teacher Name</th>
              <th>QMS Pass</th>
              <th>QMS Failed</th>
              <th>LMS Pass</th>
              <th>LMS Failed</th>
              <th>Not Complete</th>
              <th>No calculation kpi</th>
              <th>QMS KPI %</th>
              <th>LMS KPI %</th>
              <th>Total KPI %</th>
              <th></th>
              </tr>
              </thead>
              ';
              while($row_text = mysqli_fetch_array($result_text)){


                    $teachername = $row_text["username"];

                    $query1 = "
                    SELECT sr.level_id,sr.reg_no FROM studentresult sr
                    LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                    WHERE sr.teachername = '$teachername' 
                    AND st.active = 'Yes'
                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
                    AND (sr.trial = '' OR sr.trial = 'No')
                    GROUP BY sr.level_id,sr.reg_no
                    ";
                    if($result1 = mysqli_query($connect, $query1))
                    {
                        //qms
                        $pass = 0;
                        $failed = 0;
                        $not = 0;
                        $no_count = 0;
                        $kpi = 0;
                        //lms
                        $kpi_lms = 0;
                        $total_kpi = 0;
                        //set kpi%
                        $qmskpi = 40;
                        $lmskpi = 30;
                        while($row1 = mysqli_fetch_array($result1))
                        {

                            $level_id = $row1['level_id'];
                            $reg_no = $row1['reg_no'];

                        $level_array = array();
                        $query2 = "
                        SELECT SUM(sr.number_of_class) AS total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
                        LEFT JOIN level le ON (le.level_id=sr.level_id)
                        LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                        WHERE sr.level_id = '$level_id' 
                        AND st.active = 'Yes'
                        AND sr.reg_no = '$reg_no' 
                        AND sr.teachername = '$teachername' 
                        AND (sr.deleted IS NULL OR sr.deleted = '0') 
                        AND (sr.trial = '' OR sr.trial = 'No')
                        ";
                        if($result2 = mysqli_query($connect, $query2))
                        {
                                     
                            while($row2 = mysqli_fetch_array($result2))
                            {
                                  $total_class = $row2['total_class'];
                                  $total = $row2['total'];
                                  
                                  $query3 = "
                                  SELECT sr.complete FROM studentresult sr
                                  LEFT JOIN level le ON (le.level_id=sr.level_id)
                                  LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                                  WHERE sr.level_id = '".$row2['level_id']."'
                                  AND st.active = 'Yes'
                                  AND sr.reg_no = '".$row2['reg_no']."'
                                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                  AND (sr.trial = '' OR sr.trial = 'No')
                                  ORDER BY sr.r_id DESC LIMIT 1
                                  ";
                                  if($result3 = mysqli_query($connect, $query3))
                                  {
                                               
                                      while($row3 = mysqli_fetch_array($result3))
                                      {  
                                    
                                        if ($total_class == 0) {
                                          $no_count ++;
                                        }
                                        elseif($total <= $total_class AND $row3['complete'] == 'yes'){
                                            $pass ++;
                                        }
                                        elseif($total > $total_class AND $row3['complete'] == 'yes') {
                                            $failed ++;
                                        }elseif ($row3['complete'] == 'no' OR $row3['complete'] == '') {
                                            $not ++;
                                        }
                                      
                                        $total_student = $pass + $failed;
                                        if ($total_student > 0) {
                                          $kpi = $pass/$total_student * $qmskpi;
                                        }

                                      }
                                  }
                                  
                            // $query3 = "UPDATE teacher SET  qms_kpi = '$kpi' WHERE username = '$teachername'";
                            // $sql_result3 = mysqli_query($connect,$query3);

                            }          
                          }
                        
                        }
             
                      }
                      $all_pass = 0;
                      $all_fail = 0;
                      $query_lms = "
                      SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
                      LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                      WHERE sr.teachername = '$teachername' 
                      AND sr.fid = '$fid'
                      AND st.active = 'Yes'
                      AND (sr.trial = '' OR sr.trial = 'No')
                      AND (sr.deleted IS NULL OR sr.deleted = '0')
                      GROUP BY sr.reg_no, st.studentname
                      ";
                    
                      $result_lms = mysqli_query($connect, $query_lms);
                      if(mysqli_num_rows($result_lms) > 0)
                      {
                       
                      
                      while($row_lms = mysqli_fetch_array($result_lms))
                      {
                          $pass_lms = 0;
                          $failed_lms = 0;
                          $reg_no = $row_lms['reg_no'];
                          
                          $level_array = array();
                          $query_lms1 = "
                          SELECT sr.level_id FROM studentresult sr
                          LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                          WHERE sr.teachername = '$teachername' 
                          AND st.active = 'Yes'
                          AND sr.reg_no = '$reg_no'
                          AND (sr.trial = '' OR sr.trial = 'No')
                          AND (sr.deleted IS NULL OR sr.deleted = '0') 
                          GROUP BY sr.level_id
                          ";
                          if($result_lms1 = mysqli_query($connect, $query_lms1))
                          {
                              while($row_lms1 = mysqli_fetch_array($result_lms1))
                              {
                                  $level_id = $row_lms1['level_id'];
                                  // echo $level_id; exit();
                                  if ($level_id != 'Level') {
                                      array_push($level_array, $level_id);
                                  }
                                  
                              }
                              $level = join(",",$level_array);
                              $query_qlms = "
                              SELECT * FROM qlms_level WHERE level_id IN (".$level.")
                              GROUP BY lms_level
                              ";
                              // echo $query_qlms; exit();
                              // echo $query_qlms; exit();
                              if($result_qlms = mysqli_query($connect, $query_qlms))
                              {
                                  while($row_qlms = mysqli_fetch_array($result_qlms))
                                  {
                                      $level_id = $row_qlms['level_id'];
                                      $lms_level = $row_qlms['lms_level'];

                                      if ($lms_level != '') {
                                  
                                          $query_ex = "
                                          SELECT * FROM examresult WHERE level = '".$lms_level."'
                                          AND reg_no = '".$reg_no."' AND fid = '".$fid."'
                                          ";

                                          if($result_ex = mysqli_query($connect1, $query_ex))
                                          {
                                              while($row_ex = mysqli_fetch_array($result_ex))
                                              {

                                                  $status = $row_ex['status1'];
                                                  if ($status == 'Pass'){
                                                      $pass_lms ++;
                                                  }
                                                  if ($status == 'Fail') {
                                                      $failed_lms ++;
                                                  }

                                              }
                                          }
                                      }else{
                                          
                                      }
                                  }
                                  $all_pass += $pass_lms;
                                  $all_fail += $failed_lms;
                                  $total_student = $all_pass + $all_fail;
                                  if ($total_student == 0) {
                                      
                                  }
                                  else{
                                  $kpi_lms = $all_pass/$total_student * $lmskpi;
                                  }
                                  
                              }
                          }  
                      }
                      $total_kpi = $kpi + $kpi_lms;
                  }
                      if ($pass > 0 OR $failed > 0 OR $not > 0 OR $no_count > 0) {
                      $response1 .='
                      <tbody>
                      <tr>
                      <td>'.$row_text["id"].'</td>
                      <td>'.$row_text["username"].'</td>
                      <td>'.$pass.'</td>
                      <td>'.$failed.'</td>
                      <td>'.$all_pass.'</td>
                      <td>'.$all_fail.'</td>
                      <td>'.$not.'</td>
                      <td>'.$no_count.'</td>
                      <td>'.floor($kpi).'</td>
                      <td>'.floor($kpi_lms).'</td>
                      <td>'.floor($total_kpi).'</td>
                      <td><button class="btn btn-info" onclick=window.location.href="hq_student_detail.php?teachername='.base64_encode(urlencode($row_text["username"])).'&fid='.base64_encode($fid).'">Detail</button></td>
                      </tr>
                      </tbody>
                      ';
                      }
              }
              
            }
            else{
                $response1.='
                No Record';
            }
            echo $response1;
}

?>
