<!DOCTYPE html>
<html lang="en">
<head>
    <title>Teacher List</title>
    <meta charset="UTF-8">
    <link rel="icon" href="images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css">
    <link rel="stylesheet" type="text/css" href="css/selectbox.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    
    <div class="limiter">
        <div class="container-table100">
            <div class="wrap-table100">
              <form method="POST">
                    <div class="table">
                        <div class="form-group">
                        <div class="input-group">
                         <span class="input-group-addon">Search</span>
                         <div class="select">
                          <select name="search_text" id="search_text" class="shortselect">
                                    <option disabled="disabled" selected="selected">Centre</option>
                                    <?php
                                    require_once('dbConfig.php');  
                                    //Get all level data
                                    $query = $connect->query("SELECT * FROM centre ORDER BY name ASC");
                                    //Count total number of rows
                                    $rowCount = $query->num_rows;

                                    if($rowCount > 0){
                                      while($row = $query->fetch_assoc()){ 
                                        echo '<option value="'.$row['fid'].'" ';     
  
                                        echo ' >'.$row['name'].'</option>';
                                      }
                                    }
                                    ?>
                        </select>
                        </div>
                        </div>
                       </div>
                        <p>LMS</p>
                        <div id="result"><center><h3>Loading please wait..........</h3></center></div>
                        <br>
                        

                        

                    </div>
                    <div class="w3-bar">
                         <a href="hq_teacher_list_kpi.php" class="w3-button w3-left" style="text-decoration: none; color: #fff; background-color: #808080;"><i class="fa fa-angle-double-left">&nbsp Back</a></i>
                    </div>
                    </form>
            </div>
        </div>
    </div>


</body>
<script>
$(document).ready(function(){
window.onload=load_data();
 //load_data();

 function load_data(query)
 {
  $.ajax({
   url:"fetch_hq_teacher_lms_kpi.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
$('#search_text').click(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
</html>
