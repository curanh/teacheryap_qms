<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>TEACHER KPI</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
</head>
<body>
  <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" style="color: #85AFF6;" class="shake">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li class="shake"><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="color: #85AFF6;cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>

          <div class="col-md-10">
            <div class="content-box-large">
          <div class="panel-heading">
          <!-- <div class="panel-title">QMS KPI</div> -->
          <h3 style="color: #6E6B6B;">Teacher KPI Report<h3>
        </div>
          <div class="panel-body">
            <div class="table-responsive">
              <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Center</span>
                    <select class="form-control" id="search_text_fid" name="search_text_fid">
                      <option value="all" selected>Please select a centre</option>
                      <?php
                      $query = $connect->query("
                      SELECT name,fid FROM centre ORDER BY name");
                       $rowCount = $query->num_rows;
                        if($rowCount > 0){
                          while($row = $query->fetch_assoc())
                          {
                            echo "<option value='".$row['fid']."'>".$row['name']."</option>";
                          }
                        }
                      ?>
                    </select>
                  </div><br>
                <div class="input-group">
                <span class="input-group-addon">Search</span>
                <input type="text" name="search_text" id="search_text" placeholder="Search by Teacher Name" class="form-control" />
                </div>
              </div>
              <table class="table" id="result">
              </table>
            </div>
          </div>
        </div>
          </div>
          <!-- <div class="col-md-10">
         <div class="row">
          <div class="col-md-6">
            <div class="content-box-large">
              <div class="panel-heading">
              <div class="panel-title">QMS KPI</div>
              
              <div class="panel-options">
                 <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                 <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
              </div>
            </div>
              <div class="panel-body">
                <table class="table" id="result">
                </table>
              </div>
            </div>
          </div>
           <div class="col-md-6">
            <div class="content-box-large">
              <div class="panel-heading">
              <div class="panel-title">LMS KPI</div>
              <div class="panel-options">
              </div>
            </div>
              <div class="panel-body">
                <table class="table table-striped" id="result1">
                </table>
              </div>
            </div>
          </div> -->
        </div>
      </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="..//js/custom.js"></script>
<script>
$(document).ready(function() {
  $("#result").html("Please Select Centre First");      
  var documentRoot = "fetch_teacher_kpi.php";
  // $("#result").html("Pls Search");
  $("#search_text_fid").change(function() {
       // alert($("#language").val());
      $('#result').html("Loading Please Wait...........");
      var form_data = {
              fid: $("#search_text_fid").val(),
              text: $("#search_text").val(),
              functionName: 'fid'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data, success: function(response) {
        // alert (response);
        
        $("#result").html(response);

      }});
  });

  // $("#result").html("Pls Search");
  $("#search_text").keyup(function() {
       // alert($("#language").val());
      $('#result').html("Loading Please Wait...........");
      var form_data_time = {
              text: $("#search_text").val(),
              fid: $("#search_text_fid").val(),
              functionName: 'text'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response1) {
        // alert (response);
        
        $("#result").html(response1);

      }});
  });
});
</script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
    e.preventDefault();
    $("li").removeClass("selected");
    $(this).addClass("selected");
});
</script>
</html>