<?php 
    require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");
    }
    if (isset($_GET['reg_no'])) {
      $active = base64_decode($_GET['active']);
      $reg_no = base64_decode($_GET['reg_no']);
    }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>STUDENT REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
</head>
<body>
  <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li class="shake"><a onclick="window.location.href='hq_student_report.php'" style="color: #85AFF6;cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
        <div class="col-md-10">
          <div class="content-box-large">
          <div class="panel-heading">
            <div class="panel-options">
              <a href="" data-rel="collapse" data-toggle="modal" data-target="#largeModal"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-down"></i></button></a>
            </div>
          <?php
          if ($active == 'Yes') {
             echo '<h3 style="color: #6E6B6B;">Student Report / Level<h3>';
          }
          elseif ($active == 'No') {
             echo '<h3 style="color: #6E6B6B;">Inactive Student Report / Level<h3>';
          } 
          ?>
          </div>
          <div class="panel-body">
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                <div class="content-box-large">
                  <?php
                  if ($active == 'Yes') {
                    echo '<h5 class="modal-title" id="smallmodalLabel">Student detail</h5>';
                  }
                  elseif ($active == 'No') {
                    echo '<h5 class="modal-title" id="smallmodalLabel">Inactive Student detail</h5>';
                  } 
                  ?>
                  <h5>Student Name: <?php echo urldecode(base64_decode($_GET['studentname']));?></h5>
                  <div class="panel-body">
                    <div class="table-responsive">
                    <table class="table" id="result_1">
                    </table>
                    </div>
                  </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            
            <div class="table-responsive">
              <div class="content-box" style="border-left: 6px inset #85AFF6;">
                <div class="panel-title"><h5>Student Name: <?php echo urldecode(base64_decode($_GET['studentname'])); ?></h5></div>
                <?php
                if ($active == 'Yes') {
                   $query_student="SELECT * FROM student WHERE reg_no = '".$reg_no."' AND active = 'Yes'";
                }
                elseif ($active == 'No') {
                   $query_student="SELECT * FROM student WHERE reg_no = '".$reg_no."' AND active = 'No'";
                } 
                $result_student = mysqli_query($connect2, $query_student);
                $row_student = mysqli_fetch_array($result_student);
                if ($row_student > 0) {
                  echo '<div class="panel-title"><h5>Current school: '.$row_student['current_school'].'</h5></div>';
                  echo '<div class="panel-title"><h5>Mother tongue: '.$row_student['lang'].'</h5></div>';
                  echo '<div class="panel-title"><h5>Age: '.$row_student['age'].'</h5></div>';
                }else{
                  echo '<div class="panel-title"><h5>Current school:</h5></div>';
                  echo '<div class="panel-title"><h5>Mother tongue:</h5></div>';
                  echo '<div class="panel-title"><h5>Age:</h5></div>';
                }
                ?>
              </div>
              <table class="table" id="result">
              </table>
              <a href="hq_student_report.php"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a>
            </div>
          </div>
        </div>

          </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="..//js/custom.js"></script>
<script>
$(document).ready(function(){

 load_data();
 load_data1();

 //document.getElementById("teacher_name").value = teacher_name;
 //alert(teacher_name);
 function load_data(query)

 {
 var urlParams = new URLSearchParams(window.location.search);
 var reg_no = urlParams.get('reg_no');
 var studentname = urlParams.get('studentname');
 var fid = urlParams.get('fid');
 var active = urlParams.get('active');
 //alert(teacher_name);
 $('#result').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_view_student_report.php?reg_no="+reg_no+"&studentname="+studentname+"&fid="+fid+"&active="+active+"",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 function load_data1(query)
 {
 var urlParams = new URLSearchParams(window.location.search);
 var reg_no = urlParams.get('reg_no');
 var active = urlParams.get('active');
 //alert(teacher_name);
 $('#result_1').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_student_level.php?reg_no="+reg_no+"&active="+active+"",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result_1').html(data);
   }
  });
 }
});
</script>

<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
</html>