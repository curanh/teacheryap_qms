<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>CREATE CENTRE ACCOUNT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <!-- <link href="..//css/teacher.css" rel="stylesheet" media="all"> -->
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
    <link rel="icon" href="..//images/admin.png">
  </head>
  <body>
    <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li class="shake"><a onclick="window.location.href='create_centre.php'" style="color: #85AFF6;cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
               <!-- <a href="index.php" class="w3-button btn btn--radius w3-right " style="text-decoration: none; color: #fff; background-color: #DC143C;">&nbsp Logout</a> -->
                    <h3 class="" style="color: #6E6B6B;">Create Centre Account</h3>
                    <!-- <h3 style="color: #6E6B6B;">Create Teacher Account<h3> -->
                    <form method="POST"  action="create_centre.php">
                        <div class="form-group">
                          <label>Username</label>
                          <input class="form-control" type="text" placeholder="Username" name="username" required>
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input class="form-control" type="text" placeholder="Password" name="password" required="">
                        </div>
                        <div class="form-group">
                          <label>Centre</label>
                          <select class="form-control" id="centre" name="centre" required="">
                            <option value="" selected>Please select a centre</option>
                            <?php
                            $query = $connect->query("
                            SELECT name,fid FROM centre ORDER BY name");
                             $rowCount = $query->num_rows;
                              if($rowCount > 0){
                                while($row = $query->fetch_assoc())
                                {
                                  echo "<option value='".$row['fid']."'>".$row['name']."</option>";
                                }
                              }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Position</label>
                          <select class="form-control" id="position" name="position" required="">
                            <option value="" selected>Please select a position</option>
                            <option value="cm">Centre Manager</option>
                            <option value="buddy">Buddy</option>
                            <option value="teacher">Teacher</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <div class="col-md-10">
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language1" value="yes">
                              华语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language2" value="yes">
                              国语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language3" value="yes">
                              珠算 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language4" value="yes">
                              英语 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language5" value="yes">
                              PK </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language6" value="yes">
                              TT </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language7" value="yes">
                              MIQ </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language8" value="yes">
                              HOTS 36 </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="language9" value="yes">
                              Online 华语 </label>
                          </div>
                        </div>

                            <!-- <button class="w3-button btn btn--radius btn--green" type="submit" name="button">Submit</button> -->
                            <button class="btn btn-success" type="submit" name="button"><i class="glyphicon glyphicon-upload"></i> Submit</button>
                        <?php
                        if (isset($_POST['button'])){
                            if(isset($_POST['username']) && isset($_POST['password'])){

                              if(($_POST['username'] !="") && ($_POST['password']!="")){
                                  $username= strtolower($_POST['username']);
                                  $password = strtolower($_POST['password']);
                                  $new_name = str_replace(" ", "", $username);
                                  $new_pass = str_replace(" ", "", $password);
                                  $fid = $_POST['centre'];
                                  $position = $_POST['position'];
                                  if(isset($_POST['language1'])) {
                                    $language1 = $_POST['language1'];
                                  }
                                  else
                                  {
                                      $language1 = "";
                                  }

                                  if(isset($_POST['language2'])) {
                                      $language2 = $_POST['language2'];
                                  }
                                  else
                                  {
                                      $language2 = "";
                                  }

                                  if(isset($_POST['language3'])) {
                                      $language3 = $_POST['language3'];
                                  }
                                  else
                                  {
                                      $language3 = "";
                                  }

                                  if(isset($_POST['language4'])) {
                                      $language4 = $_POST['language4'];
                                  }
                                  else
                                  {
                                      $language4 = "";
                                  }

                                  if(isset($_POST['language5'])) {
                                      $language5 = $_POST['language5'];
                                  }
                                  else
                                  {
                                      $language5 = "";
                                  }
                                  
                                  if(isset($_POST['language6'])) {
                                      $language6 = $_POST['language6'];
                                  }
                                  else
                                  {
                                      $language6 = "";
                                  }
                                  if(isset($_POST['language7'])) {
                                      $language7 = $_POST['language7'];
                                  }
                                  else
                                  {
                                      $language7 = "";
                                  }

                                  if(isset($_POST['language8'])) {
                                      $language8 = $_POST['language8'];
                                  }
                                  else
                                  {
                                      $language8 = "";
                                  }

                                  if(isset($_POST['language9'])) {
                                      $language9 = $_POST['language9'];
                                  }
                                  else
                                  {
                                      $language9 = "";
                                  }
                                  
                                  $sql= "SELECT * FROM teacher WHERE username='$new_name' LIMIT 1";
                                  $result = mysqli_query($connect, $sql);
                                  $user = mysqli_fetch_assoc($result);
                            
                              
                                    if ($user['username'] === $new_name) {
                                          echo "<script>alert('Username already exists ! Please try again! ');
                                          window.location.href='create_centre.php';
                                          </script>";
                                    exit;
                                    }
                                $sql="INSERT INTO teacher (username, password, fid, language1, language2, language3, language4, language5, language6, language7, language8, language9, qms_kpi, lms_kpi, status, position)
                                VALUES
                                ('$new_name','$new_pass', '$fid', '$language1','$language2','$language3','$language4','$language5','$language6','$language7','$language8', '$language9', '0', '0', '0', '$position')";

                                  if (!mysqli_query($connect,$sql))
                                  {
                                      die('Error: ' . mysqli_error($connect));
                                  }
                                  echo "<script>alert('Successful! ')</script>";
                                  // echo "<script> window.location.assign('index.php'); </script>";   
                              }
                          }
                        }
                    ?>
                    
                    
                    <br>
                    <br>
                    <div class="w3-bar">
                     <!--  <button class="w3-button w3-left w3-light-grey">&laquo; Previous</button> -->
                     <!--  <button class="w3-button w3-right w3-green"><a href="teacher_list.php"  style="text-decoration: none; color: #fff;">Next &raquo;</button> -->
                     
                    
                    

                    </div>
                    </form>
            </div>
          </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="..//bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="..//js/custom.js"></script>
    <script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
  </body>
</html>
