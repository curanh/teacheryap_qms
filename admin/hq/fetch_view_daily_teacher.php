<?php
//fetch.php
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");
        
}
$teachername = base64_decode($_GET['teachername']);
$language_id = base64_decode($_GET['language_id']);
$date = base64_decode($_GET['date']);
require_once("..//dbConfig.php");

$output = '';

// echo '<script type="text/javascript">alert("'.$_GET["date"].'")</script>';
 // $search = mysqli_real_escape_string($connect, $_GET["date"]);
 $query = "
  SELECT *,la.language_name, le.level_name, st.studentname, sr.method, sr.level_id FROM studentresult sr 
  LEFT JOIN language la ON (sr.language_id = la.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id) 
  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
  WHERE sr.teachername = '".$teachername."' 
  AND st.active = 'Yes'
  AND sr.language_id = '".$language_id."' 
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = '' OR sr.trial = 'No')
  AND sr.insert_date = '".$date."' ORDER BY sr.r_id DESC
 ";
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>Student</th>
  <th>Level</th>
  <th>Title</th>
  </tr>
  </thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $output .= '
  <tbody>
  <tr>
  <td>'.$row["studentname"].'</td>
  <td>'.$row["level_name"].'</td>
  <td>';

  if ($row['method'] == 'Yes') {     
  $query_title = "
  SELECT * FROM level_title 
  WHERE status = 1 
  AND (deleted IS NULL OR deleted = 0) 
  AND level_id = '".$row['level_id']."' 
  AND title_id IN (".$row["title_id"].") 
  ORDER BY sorting ASC";

  $result_title = mysqli_query($connect,$query_title);
  while($row_titleid=mysqli_fetch_assoc($result_title))
  {   

    $output .= $row_titleid['title_name'].'<br>';        
  }
  }else{
  $output.= $row["title_id"];
  }
  
  $output .='
  </td>
  </tbody>
  ';
  }
  
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>