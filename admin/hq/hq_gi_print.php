<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
    $fid = base64_decode($_GET['fid']);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Guarantee Improvement Report</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <!-- <img class="ldld" src="your-loader-url"> -->    
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
</head>
</style>
<body>
  <div class="header" id="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2" id="test">
            <div class="sidebar content-box" style="display: block;">
                <button class="btn btn-warning" style="width: 100%" onclick="pdf()">Print</button>
                <br>
                <br>
                <a href="hq_gi_report.php"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
              <div class="panel-heading">
                <h3 style="color: #6E6B6B;">Guarantee Improvement Report (Student)<h3>
                  <?php
                  $query = $connect->query("
                  SELECT name,fid FROM centre WHERE fid = '$fid' ORDER BY name");
                   $rowCount = $query->num_rows;
                    if($rowCount > 0){
                      while($row = $query->fetch_assoc())
                      {
                          echo "<h3 style='color: #6E6B6B;''>".$row['name']."</h3>";
                      }
                    }
                  ?>
              </div>
        <div class="panel-body">
          <div id="rootwizard">
            <div class="tab-content">
                  <div id="chartContainer" style="height: 250px; width: 100%;"></div>
                  <div class="panel-body">
                    <div class="table-responsive">
                      <br>
                      <br>
                      <table class="table">
                      <?php
                         $query = "
                          SELECT ct.name, st.studentname, st.age, la.language_id, la.language_name, le.level_name, sr.level_id, sr.reg_no FROM studentresult sr
                          LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
                          LEFT JOIN centre ct ON (ct.fid = sr.fid)
                          LEFT JOIN level le ON (le.level_id = sr.level_id)
                          LEFT JOIN language la ON (la.language_id = sr.language_id) 
                          WHERE sr.fid = '$fid'
                          AND st.active ='Yes'
                          AND (sr.deleted IS NULL OR sr.deleted = '0') 
                          AND (sr.trial = 'No' OR sr.trial = '')
                          GROUP BY sr.level_id

                          ";
                          $result = mysqli_query($connect, $query);
                          if(mysqli_num_rows($result) > 0)
                          {
                          $pass = '0';
                          $fail = '0';
                          $not = '0';
                           echo'
                            <thead>
                            <td><button class="btn btn-info" onclick=window.location.href="hq_gi_print.php?fid='.base64_encode($fid).'">Print</button></td>
                            <tr>
                            <th>Center</th>
                            <th>Student Name</th>
                            <th>Age</th>
                            <th>Subject</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>KPI Class</th>
                            <th>Actual Class</th>
                            </tr>
                            </thead>
                           ';

                           while($row = mysqli_fetch_array($result))
                           { 

                            $query4="SELECT * FROM studentresult sr
                            LEFT JOIN student st ON (st.reg_no = sr.reg_no)
                            WHERE sr.fid = '$fid' 
                            AND sr.level_id = '".$row['level_id']."'
                            AND st.active ='Yes'
                            AND (sr.deleted IS NULL OR sr.deleted = '0') 
                            AND (sr.trial = 'No' OR sr.trial = '')
                            GROUP BY sr.reg_no";

                            if($result4 = mysqli_query($connect, $query4))
                            {
                                         
                                while($row4 = mysqli_fetch_array($result4))
                                {

                                  $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
                                  FROM studentresult sr 
                                  LEFT JOIN level le ON (sr.level_id = le.level_id) 
                                  LEFT JOIN language la ON (sr.language_id = la.language_id) 
                                  WHERE sr.reg_no='".$row4['reg_no']."' 
                                  AND sr.level_id = '".$row4['level_id']."' 
                                  AND (sr.deleted IS NULL OR sr.deleted = 0)
                                  AND (sr.trial = 'No' OR sr.trial = '')";

                                  if($result2 = mysqli_query($connect, $query2))
                                  {
                                               
                                      while($row2 = mysqli_fetch_array($result2))
                                      {
                                        $total_class = $row2['total_class'];
                                        $total = $row2['total'];

                                        $query3 = "
                                          SELECT sr.complete FROM studentresult sr
                                          LEFT JOIN level le ON (le.level_id=sr.level_id)
                                          LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                                          WHERE sr.level_id = '".$row2['level_id']."'
                                          AND st.active = 'Yes'
                                          AND sr.reg_no = '".$row2['reg_no']."'
                                          AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                          AND (sr.trial = '' OR sr.trial = 'No')
                                          ORDER BY sr.r_id DESC LIMIT 1
                                          ";
                                          if($result3 = mysqli_query($connect, $query3))
                                          {
                                                       
                                              while($row3 = mysqli_fetch_array($result3))
                                              {
                                                  
                                                  // echo $total_class;
                                                  // echo '<br>';
                                                  // echo $total;
                                                  //exit();
                                                  if($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name'])){
                                                    $pass ++;
                                                    echo'
                                                      <tbody>
                                                        <tr>
                                                          <td><strong>'.$row["name"].'</strong></td>
                                                          <td>'.$row4['studentname'].'</td>
                                                          <td>'.$row['age'].'</td>
                                                          <td>'.$row['language_name'].'</td>
                                                          <td>'.$row['level_name'].'</td>
                                                          <td style="color:#31D919"><strong>PASS</strong></td>
                                                          <td>'.$total_class.'</td>
                                                          <td>'.$row2['total'].'</td> 
                                                    ';
                                                  }elseif ($total_class == '0') {
                                                    echo'
                                                    <tbody>
                                                        <tr>
                                                          <td><strong>'.$row["name"].'</strong></td>
                                                          <td>'.$row4['studentname'].'</td>
                                                          <td>'.$row['age'].'</td>
                                                          <td>'.$row['language_name'].'</td>
                                                          <td>'.$row['level_name'].'</td>
                                                          <td style="color:#787878"><strong>Not KPI Class</strong></td>
                                                          <td>'.$total_class.'</td>
                                                          <td>'.$row2['total'].'</td> 
                                                    ';
                                                  }
                                                  elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name'])) {
                                                    $fail ++;
                                                    echo'
                                                    <tbody>
                                                        <tr>
                                                          <td><strong>'.$row["name"].'</strong></td>
                                                          <td>'.$row4['studentname'].'</td>
                                                          <td>'.$row['age'].'</td>
                                                          <td>'.$row['language_name'].'</td>
                                                          <td>'.$row['level_name'].'</td>
                                                          <td style="color:#F43939"><strong>FAIL</strong></td>
                                                          <td>'.$total_class.'</td>
                                                          <td>'.$row2['total'].'</td> 
                                                    ';
                                                  }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name'])) {
                                                    $not ++;
                                                    echo'
                                                    <tbody>
                                                        <tr>
                                                          <td><strong>'.$row["name"].'</strong></td>
                                                          <td>'.$row4['studentname'].'</td>
                                                          <td>'.$row['age'].'</td>
                                                          <td>'.$row['language_name'].'</td>
                                                          <td>'.$row['level_name'].'</td>
                                                          <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
                                                          <td>'.$total_class.'</td>
                                                          <td>'.$row2['total'].'</td> 
                                                    ';
                                                  }
                                                }    
                                            }

                                      }          
                                    }

                                }
                            }
                              echo'
                              </tbody>
                              </tr>
                              ';
                         }
                        }else{
                          echo '
                          No Record';
                        }
                        $dataPoints = array( 
                            array("label"=>"Not Complete", "symbol" => "Not Complete","y"=>$not),
                            array("label"=>"Pass", "symbol" => "Pass","y"=>$pass),
                            array("label"=>"Fail", "symbol" => "Fail","y"=>$fail),
                        ); 
                        ?>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</body>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="..//js/custom.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
function pdf(){
  document.getElementById("test").style.display = "none";
  document.getElementById("header").style.display = "none";
  window.print();
  location.reload();
}
window.onload = function() {
 
var chart = new CanvasJS.Chart("chartContainer", {
  theme: "light2",
  animationEnabled: true,
  data: [{
    type: "doughnut",
    indexLabel: "{symbol} - {y}",
    yValueFormatString: "#,##0\"\"",
    showInLegend: true,
    legendText: "{label} : {y}",
    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();
 
}
</script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
</script>
</html>
