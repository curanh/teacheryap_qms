<?php 
     require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
 ?>
<?php 
    $UserID = base64_decode($_GET['GetID']);
    $query = " select * from teacher where id='".$UserID."'";
    $result = mysqli_query($connect,$query);

    while($row=mysqli_fetch_assoc($result))
    {
        $UserID = $row['id'];
        $fid = $row['fid'];
        $UserName = $row['username'];
        $password = $row['password'];
        $language1 = $row['language1'];
        $language2 = $row['language2'];
        $language3 = $row['language3'];
        $language4 = $row['language4'];
        $language5 = $row['language5'];
        $language6 = $row['language6'];
        $language7 = $row['language7'];
        $language8 = $row['language8'];
        $language9 = $row['language9'];
        $position = $row['position'];
        $status = $row['status'];
    }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>EDIT TEACHER</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <!-- <link href="..//css/edit.css" rel="stylesheet" media="all"> -->
    <!-- <link href="..//css/teacher.css" rel="stylesheet" media="all"> -->
    <link href="..//css/styles.css" rel="stylesheet">
    <link rel="icon" href="..//images/admin.png">
    <link href="..//css/shake.css" rel="stylesheet">
  </head>
  <body>
    <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li class="shake"><a onclick="window.location.href='centre.php'" style="color: #85AFF6;cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='hq_student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
              <h2 class="" style="color: #6E6B6B;">Edit Centre Account</h2>
                <form action="update.php?id=<?php echo $UserID ?>&fid=<?php echo $fid ?>" method="post">
                  <div class="form-group">
                      <label>Username</label>
                      <input class="form-control" placeholder="Text field" type="text" name="name" value="<?php echo $UserName ?>" READONLY>
                  </div>
                  <div class="form-group">
                      <label>Password</label>
                      <input class="form-control" placeholder="Password" type="text" name="password" value="<?php echo $password ?>">
                  </div>
                  <div class="form-group">
                      <label>Position</label>
                      <select class="form-control" id="position" name="position">
                      <?php
                      if ($position == 'cm') {
                          echo '<option value="cm" selected>Centre Manager</option>';
                          echo '<option value="buddy">Buddy</option>';
                          echo '<option value="teacher">Teacher</option>';
                      }elseif($position == 'buddy'){
                          echo '<option value="cm">Centre Manager</option>';
                          echo '<option value="buddy" selected>Buddy</option>';
                          echo '<option value="teacher">Teacher</option>';
                      }elseif($position == 'teacher'){
                          echo '<option value="cm">Centre Manager</option>';
                          echo '<option value="buddy">Buddy</option>';
                          echo '<option value="teacher" selected>Teacher</option>';

                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" id="select-1" name="status">
                    <?php
                    if ($status == '0') {
                        echo '<option value="0" selected>Active</option>';
                        echo '<option value="1">Inactive</option>';
                    }else{
                        echo '<option value="0">Active</option>';
                        echo '<option value="1" selected>Inactive</option>';
                    } 
                    ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>LMS Teacher</label>
                    <select class="form-control" id="lms_teacher" name="lms_teacher">
                      <option value="1" selected>LMS Teacher</option>
                      <?php
                      $query_lms = " SELECT * FROM teacher WHERE fid='".$fid."' AND (deleted IS NULL OR deleted = 0)";
                      $result_lms = mysqli_query($connect1,$query_lms);

                      while($row_lms=mysqli_fetch_assoc($result_lms))
                      {
                          $username = $row_lms['username'];
                          echo '<option value="'.$username.'">'.$username.'</option>';
                      } 
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <div class="col-md-10">
                    <?php
                          if ($language1 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language1" checked="checked" value="yes">
                                      华语 </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language1" value="yes">
                                      华语 </label>';
                            }
                            ?>

                            <?php
                            if ($language2 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language2" checked="checked" value="yes">
                                      国语 </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language2" value="yes">
                                      国语 </label>';
                            }
                            ?>

                            <?php
                            if ($language3 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language3" checked="checked" value="yes">
                                      珠算 </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language3" value="yes">
                                      珠算 </label>';
                            }
                            ?>

                            <?php
                            if ($language4 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language4" checked="checked" value="yes">
                                      英语 </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language4" value="yes">
                                      英语 </label>';
                            }
                            ?>

                            <?php
                            if ($language5 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language5" checked="checked" value="yes">
                                      PK </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language5" value="yes">
                                      PK </label>';
                            }
                            ?>

                            <?php
                            if ($language6 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language6" checked="checked" value="yes">
                                      TT </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                       <input type="checkbox" name="language6" value="yes">
                                      TT </label>';
                            }
                            ?> 

                            <?php
                            if ($language7 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language7" checked="checked" value="yes">
                                      MIQ </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                       <input type="checkbox" name="language7" value="yes">
                                      MIQ </label>';
                            }
                            ?>

                            <?php
                            if ($language8 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language8" checked="checked" value="yes">
                                      HOTS 36 </label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                       <input type="checkbox" name="language8" value="yes">
                                      HOTS 36 </label>';
                            }
                            ?>

                            <?php
                            if ($language9 == 'yes') {
                                // echo '<label><input type="checkbox" name="language1" value="yes" checked />华语</label>';
                                echo '<label class="checkbox-inline">
                                      <input type="checkbox" name="language9" checked="checked" value="yes">
                                      Online 华语</label>';
                            }
                            else
                            {
                                //echo '<label><input type="checkbox" name="language1" value="yes"/>华语</label>';
                                 echo '<label class="checkbox-inline">
                                       <input type="checkbox" name="language9" value="yes">
                                      Online 华语</label>';
                            }
                            ?>  
                    </div>
                  </div>
                  <button class="btn btn-success" type="submit" name="update">
                      <i class="glyphicon glyphicon-upload"></i>
                      update
                    </button> 
                </form>
            </div>
          </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="..//bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="..//js/custom.js"></script>
     <script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>

</body>
</html>
