<?php 
    require_once("..//dbConfig.php");
    session_start();
    if ($_SESSION['name'] == "") {
        header("Location: index.php");

    }
    $reg_no = base64_decode($_GET['reg_no']);
    $active = base64_decode($_GET['active']);
    $date = base64_decode($_GET['date']);
    $new_date = date("d/m/Y", strtotime($date));
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>STUDENT DAILY REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="..//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="..//css/styles.css" rel="stylesheet">
    <link href="..//css/shake.css" rel="stylesheet">
</head>
<body>
  <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS HQ</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li class="current"><a onclick="window.location.href='dashboard_hq.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='create_centre.php'" style="cursor:pointer;">Create Centre Account</a></li>
                            <li><a onclick="window.location.href='centre.php'" style="cursor:pointer;">Centre Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='hq_centre_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='hq_all_centre_report.php'" style="cursor:pointer;">All Center Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='hq_student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='hq_not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_kpi_report.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li class="shake"><a onclick="window.location.href='hq_student_daily.php'" style="color: #85AFF6;cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='hq_teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='hq_gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>

                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
        <div class="col-md-10">
          <div class="content-box-large">
          <div class="panel-heading">
          <?php
          if ($active == 'Yes') {
            echo '<h3 style="color: #6E6B6B;">Student Daily / Report<h3>';
          }elseif ($active == 'No') {
            echo '<h3 style="color: #6E6B6B;">Inactive Student Daily / Report<h3>';
          }
          ?>
          </div>
          <div class="panel-body">  
            <div class="table-responsive">
              <div class="content-box" style="border-left: 6px inset #85AFF6;">
                <div class="panel-title">
                  <?php
                  $query = $connect->query("SELECT * FROM student WHERE reg_no = '$reg_no' AND active = '$active'");
                    $rowCount = $query->num_rows;
                    if($rowCount > 0){
                      while($row = $query->fetch_assoc()){
                        echo "<span  style='font-size:15px;'>Student Name : ".$row['studentname']."<br></span>";
                        echo "<span style='font-size:15px;'>Age : ".$row['age']."<br></span>";
                        }
                    }

                    $query_level = $connect->query("SELECT le.level_name, la.language_name FROM level le 
                                                    LEFT JOIN language la ON (la.language_id = le.language_id) 
                                                    WHERE le.level_id = ".base64_decode($_GET['level_id'])." AND le.language_id = ".base64_decode($_GET['language_id'])."");
                    $rowCount_level = $query_level->num_rows;
                    if($rowCount_level > 0){
                      while($row_level = $query_level->fetch_assoc()){
                        echo "<span  style='font-size:15px;'>Subject : ".$row_level['language_name']."<br></span>";
                        echo "<span style='font-size:15px;'>Level : ".$row_level['level_name']."<br></span>";
                        }
                    }
                    
                ?>
                <span  style='font-size:15px;'>Date : <?php echo $new_date; ?><br></span>
                </div>
              </div>
              <a href="hq_student_daily.php"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a>
              <table class="table" id="result">
              </table>
            </div>
          </div>
        </div>

          </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="..//bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="..//js/custom.js"></script>
<script>
$(document).ready(function(){

 load_data();

 //document.getElementById("teacher_name").value = teacher_name;
 //alert(teacher_name);
 function load_data(query)

 {
 var urlParams = new URLSearchParams(window.location.search);
 var reg_no = urlParams.get('reg_no');
 var studentname = urlParams.get('studentname');
 var date = urlParams.get('date');
 var level_id = urlParams.get('level_id');
 var language_id = urlParams.get('language_id');
 //alert(teacher_name);
 $('#result').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_view_daily.php?reg_no="+reg_no+"&studentname="+studentname+"&date="+date+"&level_id="+level_id+"&language_id="+language_id+"",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
});
</script>

<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_pass.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
</html>