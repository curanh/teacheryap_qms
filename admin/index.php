<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
    <link rel="icon" href="images/admin.png">
  </head>
  <body class="login-bg">
    <div class="page-content container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-wrapper">
                    <div class="box">
                        <form method="POST" action="index.php">
                        <div class="content-wrap">
                            <div class="card-body"><img src="images/admin.png" width="120">
                            <h6>QMS Centre Admin Login</h6>
                            <input class="form-control" type="text" placeholder="Username" name="username">
                            <input class="form-control" type="password" placeholder="Password" name="password">
                            <div class="action">
                                <button class="btn btn-primary signup" type="submit" name="login">Login</button>
                            </div>                
                        </div>
                    </form>
                    </div>
                </div>
                <div class="already">
                    <div class="splash-footer"><span><font color=black>Copyright &copy; 2020 YELAOSHR</a></span></div>
                </div>
            </div>
        </div>
    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
<?php
    require_once('dbConfig.php');  
    if(isset($_POST['login'])){
        if(isset($_POST['username']) && isset($_POST['password'])){
            if(($_POST['password']!='') && ($_POST['password']!='')){
                $username = $_POST['username'];
                $password = $_POST['password'];
                
                if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_errno();
                }
                $sql="SELECT * FROM teacher WHERE username = '$username' 
                AND password = '$password'
                AND (position = 'cm' OR position = 'buddy')";

                $retrival = mysqli_query($connect,$sql);
                

                if(!$retrival) {
                    die('Could not get data: ' . mysqli_error());
                }

                while($row = mysqli_fetch_array($retrival, MYSQLI_ASSOC)) {
                    $_SESSION['fid'] = $row['fid'];
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['name'] = $row['username'];
                    $_SESSION['position'] = $row['position'];
                    echo "<script>window.location.assign('dashboard.php')</script>";
                } 

                echo "<script>alert('Incorrect username or password.')</script>";
            exit;   

            }else{

                echo "<script>alert('Please fill in all of the data first before Login!')</script>";
            exit;
            }
        }
    }       
?>
</html>
