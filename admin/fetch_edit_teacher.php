<?php
//fetch.php
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
        
}
$fid = $_SESSION['fid'];
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM teacher 
  WHERE fid = '$fid' 
  AND (username LIKE '%".$search."%' OR position LIKE '%".$search."%') 
  AND (position = 'teacher' OR position = 'buddy') 
  AND deleted = '0' ORDER BY username
 ";
}
else
{
 $query = "
  SELECT * FROM teacher WHERE fid = '$fid' AND deleted = '0' AND (position = 'teacher' OR position = 'buddy') ORDER BY username
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
<thead>
<tr>
<th>#</th>
<th>TeacherName</th>
<th>Position</th>
<th>Status</th>
<th></th>
<th></th>
</tr>
</thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $output .= '
  <tbody>
  <tr>
  <td>'.$row["id"].'</td>
  <td>'.$row["username"].'</td>
  ';
  if ($row["position"] == 'cm') {
    $position = 'Centre Manager';
  }elseif($row["position"] == 'buddy'){
    $position = 'Buddy';
  }elseif ($row["position"] == 'teacher') {
    $position = 'Teacher';
  }
  $output .= '
  <td>'.$position.'</td>
  ';

  if ($row["status"] == '0'){
    $output .='
    <td>Active</td>
    ';
  }else{
    $output .='
    <td>Inactive</td>
    ';
  }

  $output .='  
  <td><button class="btn btn-primary" onclick=window.location.href="edit.php?GetID='.base64_encode($row["id"]).'"><i class="glyphicon glyphicon-pencil"></i> Edit</button></td>
  ';
  if ($_SESSION['position'] == 'cm') {
    $output .='
    <td><a class="btn btn-danger" onclick="return deleletconfig()" href="delete.php?Del='.base64_encode($row["id"]).'"><i class="glyphicon glyphicon-remove"></i> Delete</a></td>
    <script>
    function deleletconfig(){
    var del=confirm("Are you sure you want to delete this Teacher?");
    if (del==true){
       alert ("record deleted")
    }
    return del;
    }
    </script>
    </tr>
    </tbody>  
    ';
  }else{
    $output .= '
    <td>
    </td>
    </tr>
    </tbody>
    ';
  }
  
 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>