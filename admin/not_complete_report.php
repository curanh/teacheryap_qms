<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>NOT COMPLETE REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li class="shake"><a onclick="window.location.href='not_complete_report.php'" style="color: #85AFF6;cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!--<li><a onclick="window.location.href='index.php'">Logout</a></li>-->
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
          <div class="content-box-large">
          <div class="panel-heading">
          <!-- <div class="panel-title">Student Report</div> -->
          <h3 style="color: #6E6B6B;">Not Complete Report<h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <!-- <div class="content-box" style="border-left: 6px inset #85AFF6;">
                <div class="panel-title"><h5></h5></div>
              </div> -->
              <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
              <tr>
              <th width="20%">Studen Name</th>
              <th width="15%">Reg No</th>
              <th width="10%">Language</th>
              <th width="20%">Level</th>
              <th width="10%">Start Date</th>
              <th width="10%">Last Insert Date</th>
              <th width="10%">View</th>
              <th width="10%">Complete</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $query_student = "SELECT s.reg_no, s.studentname, s.active FROM student s  
                    WHERE s.fid = '".$_SESSION['fid']."' 
                    AND s.active='Yes' ORDER BY s.studentname ASC";

              $result_student = mysqli_query($connect, $query_student);          
              while($row_student = mysqli_fetch_array($result_student))
                {
                  $reg_no = $row_student["reg_no"];
                  $studentname = $row_student["studentname"];
                  $active = $row_student["active"];

                  $query_student_result = "SELECT DISTINCT level_id FROM studentresult
                  WHERE reg_no = '".$reg_no."'";
                  $result_student_result = mysqli_query($connect, $query_student_result); 
                  while($row_student_result = mysqli_fetch_array($result_student_result))
                  {
                    // $language_name = $row_student_result["language_name"];
                    // $level_name = $row_student_result["level_name"];
                    $level_id = $row_student_result["level_id"];
                    // $r_id = $row_student_result["r_id"];

                    $query = "SELECT lg.language_name, lv.level_name, sr.level_id, sr.complete, sr.r_id FROM studentresult sr
                   LEFT JOIN level lv ON (sr.level_id=lv.level_id) 
                   LEFT JOIN language lg ON (sr.language_id = lg.language_id)
                   WHERE sr.fid = '".$_SESSION['fid']."' 
                   AND sr.reg_no = '".$reg_no."' 
                   AND sr.deleted='0' 
                   AND sr.level_id = '".$level_id."'
                   ORDER BY sr.r_id DESC LIMIT 1";
                    $result = mysqli_query($connect, $query); 
                    while($row = mysqli_fetch_array($result))
                    {
                      $language_name = $row["language_name"];
                      $level_name = $row["level_name"];
                      $r_id = $row["r_id"];
                      $complete = $row["complete"];
                      
                      if ($complete == 'no') {
                        //$insert_date = date("d/m/Y",strtotime($insert_date));
                        $query_start_date = "SELECT sr.insert_date FROM studentresult sr
                        WHERE sr.fid = '".$_SESSION['fid']."' 
                        AND sr.deleted='0' 
                        AND sr.level_id='".$level_id."' 
                        AND sr.reg_no='".$reg_no."'
                        AND sr.complete='no'
                        ORDER BY sr.insert_date ASC LIMIT 1";
                        $result_start_date = mysqli_query($connect, $query_start_date); 

                        while($row_start_date = mysqli_fetch_array($result_start_date))
                        {
                          $insert_date = $row_start_date["insert_date"];
                          // $insert_date = date("d/m/Y",strtotime($insert_date));

                          $query_last_date = "SELECT sr.insert_date FROM studentresult sr
                                           WHERE sr.fid = '".$_SESSION['fid']."' 
                                           AND sr.deleted='0' 
                                           AND sr.level_id='".$level_id."' 
                                           AND sr.reg_no='".$reg_no."'
                                           AND sr.complete='no'
                                           ORDER BY sr.insert_date DESC LIMIT 1";
                          $result_last_date = mysqli_query($connect, $query_last_date); 
                          while($row_last_date = mysqli_fetch_array($result_last_date))
                          {
                          $last_date = $row_last_date["insert_date"];
                          // $last_date = date("d/m/Y",strtotime($last_date));

                            echo 
                            '<tr><td>
                            '.$studentname.'
                            </td><td>
                            '.$reg_no.'
                            </td><td>
                            '.$language_name.'
                            </td><td>
                            '.$level_name.'
                            </td><td>
                            '.$insert_date.'
                            </td><td>
                            '.$last_date.'
                            </td><td><button class="btn btn-default" onclick=window.location.href="total_s_report.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'"> View</button></td>
                            <td><button class="btn btn-info" onclick=window.location.href="fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'&id='.base64_encode($r_id).'&complete='.base64_encode('3').'">Complete</button></td>
                            </tr>';
                            
                            
                          }

                          
                        }

                      }
                    }   


                  }
                }
              ?>
              </tbody>
              </table>

              <!-- <a href="student_report.php"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a> -->
              </div>
          </div>
        </div>

          </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="js/custom.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
</html>