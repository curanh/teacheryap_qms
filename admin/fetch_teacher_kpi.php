<?php
    //fetch.php
    
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
    
    }
    $fid = $_SESSION['fid'];
    require_once("dbConfig.php");
    
    $output = '';
    $level_array = '';
    if(isset($_POST["query"]))
    {
        $search = mysqli_real_escape_string($connect, $_POST["query"]);
        $query = "
        SELECT * FROM teacher
        WHERE fid = '$fid' AND username LIKE '%".$search."%' AND deleted = '0' ORDER BY username
        ";
    }
    else
    {
        $query = "
        SELECT * FROM teacher WHERE fid = '$fid' AND deleted = '0' ORDER BY username
        ";
    }
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
        $output .= '
        <thead>
        <tr>
        <th>#</th>
        <th>Teacher Name</th>
        <th>QMS Pass</th>
        <th>QMS Failed</th>
        <th>LMS Pass</th>
        <th>LMS Failed</th>
        <th>Not Complete</th>
        <th>No calculation kpi</th>
        <th>QMS KPI %</th>
        <th>LMS KPI %</th>
        <th>Total KPI %</th>
        <th></th>
        </tr>
        </thead>
        ';
        while($row = mysqli_fetch_array($result))
        {
            $teachername = $row["username"];
        
            $query1 = "
            SELECT sr.level_id,sr.reg_no FROM studentresult sr
            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
            WHERE sr.teachername = '$teachername' 
            AND st.active = 'Yes'
            AND (sr.deleted IS NULL OR sr.deleted = '0') 
            AND (sr.trial = '' OR sr.trial = 'No')
            GROUP BY sr.level_id,sr.reg_no
            ";
            if($result1 = mysqli_query($connect, $query1))
            {
                //qms
                $pass = 0;
                $failed = 0;
                $not = 0;
                $no_count = 0;
                $kpi = 0;
                //lms
                $kpi_lms = 0;
                $total_kpi = 0;
                //set kpi%
                $qmskpi = 40;
                $lmskpi = 30;
                while($row1 = mysqli_fetch_array($result1))
                {
                    //echo $row1["level_id"];
                    
                    $level_id = $row1['level_id'];
                    $reg_no = $row1['reg_no'];
                    // echo $level_id.'<br/>';
                    $level_array = array();
                    $query2 = "
                    SELECT SUM(sr.number_of_class) AS total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
                    LEFT JOIN level le ON (le.level_id=sr.level_id)
                    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
                    WHERE sr.level_id = '$level_id' 
                    AND sr.reg_no = '$reg_no'
                    AND st.active = 'Yes'
                    AND sr.teachername = '$teachername' 
                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
                    AND (sr.trial = '' OR sr.trial = 'No')
                    ";
                    if($result2 = mysqli_query($connect, $query2))
                    {
                                 
                        while($row2 = mysqli_fetch_array($result2))
                        {

                            $total_class = $row2['total_class'];
                            $total = $row2['total'];

                            $query3 = "
                            SELECT sr.complete FROM studentresult sr
                            LEFT JOIN level le ON (le.level_id=sr.level_id)
                            LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                            WHERE sr.level_id = '".$row2['level_id']."'
                            AND st.active = 'Yes'
                            AND sr.reg_no = '".$row2['reg_no']."'
                            AND (sr.deleted IS NULL OR sr.deleted = '0') 
                            AND (sr.trial = '' OR sr.trial = 'No')
                            ORDER BY sr.r_id DESC LIMIT 1
                            ";
                            if($result3 = mysqli_query($connect, $query3))
                            {
                                         
                                while($row3 = mysqli_fetch_array($result3))
                                {  
                              

                                  if ($total_class == 0) {
                                    $no_count ++;                            
                                  }
                                  elseif($total <= $total_class AND $row3['complete'] == 'yes'){
                                      $pass ++;
                                  }
                                  elseif($total > $total_class AND $row3['complete'] == 'yes') {
                                      $failed ++;
                                  }elseif ($row3['complete'] == 'no' OR $row3['complete'] == '') {
                                      $not ++;
                                  }
                                
                                  $total_student = $pass + $failed;
                                  if ($total_student > 0) {
                                    $kpi = $pass/$total_student * $qmskpi;
                                  }

                                }
                            }
                        
                        // $query3 = "UPDATE teacher SET  qms_kpi = '$kpi' WHERE username = '$teachername'";
                        // $sql_result3 = mysqli_query($connect,$query3);

                        }
                    }
                }             
            } 
            $all_pass = 0;
            $all_fail = 0;
              $query_lms = "
              SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
              LEFT JOIN student st ON (sr.reg_no = st.reg_no)
              WHERE sr.teachername = '$teachername' 
              AND sr.fid = '$fid'
              AND st.active = 'Yes'
              AND (sr.trial = '' OR sr.trial = 'No')
              AND (sr.deleted IS NULL OR sr.deleted = '0')
              GROUP BY sr.reg_no, st.studentname
              ";
            
              $result_lms = mysqli_query($connect, $query_lms);
              if(mysqli_num_rows($result_lms) > 0)
              {
               
              
              while($row_lms = mysqli_fetch_array($result_lms))
              {
                  $pass_lms = 0;
                  $failed_lms = 0;
                  $reg_no = $row_lms['reg_no'];
                  
                  $level_array = array();
                  $query_lms1 = "
                  SELECT sr.level_id FROM studentresult sr
                  LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                  WHERE sr.teachername = '$teachername' 
                  AND st.active = 'Yes'
                  AND sr.reg_no = '$reg_no'
                  AND (sr.trial = '' OR sr.trial = 'No')
                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
                  GROUP BY sr.level_id
                  ";
                  if($result_lms1 = mysqli_query($connect, $query_lms1))
                  {
                      while($row_lms1 = mysqli_fetch_array($result_lms1))
                      {
                          $level_id = $row_lms1['level_id'];
                          // echo $level_id; exit();
                          if ($level_id != 'Level') {
                              array_push($level_array, $level_id);
                          }
                          
                      }
                      $level = join(",",$level_array);
                      $query_qlms = "
                      SELECT * FROM qlms_level WHERE level_id IN (".$level.")
                      GROUP BY lms_level
                      ";
                      // echo $query_qlms; exit();
                      // echo $query_qlms; exit();
                      if($result_qlms = mysqli_query($connect, $query_qlms))
                      {
                          while($row_qlms = mysqli_fetch_array($result_qlms))
                          {
                              $level_id = $row_qlms['level_id'];
                              $lms_level = $row_qlms['lms_level'];

                              if ($lms_level != '') {
                          
                                  $query_ex = "
                                  SELECT * FROM examresult WHERE level = '".$lms_level."'
                                  AND reg_no = '".$reg_no."' AND fid = '".$fid."'
                                  ";

                                  if($result_ex = mysqli_query($connect1, $query_ex))
                                  {
                                      while($row_ex = mysqli_fetch_array($result_ex))
                                      {

                                          $status = $row_ex['status1'];
                                          if ($status == 'Pass'){
                                              $pass_lms ++;
                                          }
                                          if ($status == 'Fail') {
                                              $failed_lms ++;
                                          }

                                      }
                                  }
                              }else{
                                  
                              }
                          }
                          $all_pass += $pass_lms;
                          $all_fail += $failed_lms;
                          $total_student = $all_pass + $all_fail;
                          if ($total_student == 0) {
                              
                          }
                          else{
                          $kpi_lms = $all_pass/$total_student * $lmskpi;
                          }
                          
                      }
                  }  
              }
              $total_kpi = $kpi + $kpi_lms;
          }
            if ($pass > 0 OR $failed > 0 OR $not > 0 OR $no_count > 0) {
            $output .='
            <tbody>
            <tr>
            <td>'.$row["id"].'</td>
            <td>'.$row["username"].'</td>
            <td>'.$pass.'</td>
            <td>'.$failed.'</td>
            <td>'.$all_pass.'</td>
            <td>'.$all_fail.'</td>
            <td>'.$not.'</td>
            <td>'.$no_count.'</td>
            <td>'.floor($kpi).'</td>
            <td>'.floor($kpi_lms).'</td>
            <td>'.floor($total_kpi).'</td>
            <td><button class="btn btn-info" onclick=window.location.href="student_detail.php?teachername='.base64_encode(urlencode($row["username"])).'">Detail</button></td>
            </tr>
            </tbody>
            ';
            }   
        }

        echo $output;
    }
    else
    {
        echo 'Data Not Found';
    }

    
    ?>

