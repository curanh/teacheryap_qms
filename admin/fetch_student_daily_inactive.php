<?php
//fetch.php
session_start();
if ($_SESSION['fid'] == "") {
    header("Location: index.php");        
}
$fid = $_SESSION['fid'];
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
 SELECT count(*) AS total , st.studentname, st.age, st.reg_no FROM studentresult sr 
 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
 WHERE sr.fid = '$fid' 
 AND st.active = 'No'
 AND sr.insert_date = '".$search."' 
 AND (sr.deleted IS NULL OR sr.deleted = '0')
 AND (sr.trial = '' OR sr.trial = 'No')
 GROUP BY st.reg_no ORDER BY st.studentname
 ";
}
else
{
$search= date("Y-m-d");
 $query = "
  SELECT count(*) AS total, st.studentname, st.age, st.reg_no FROM studentresult sr 
  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
  WHERE sr.fid = '$fid'
  AND st.active = 'No'
  AND sr.insert_date = '".$search."' 
  AND (sr.deleted IS NULL OR sr.deleted = '0')
  AND (sr.trial = '' OR sr.trial = 'No')
  GROUP BY st.reg_no ORDER BY st.studentname
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>No</th>
  <th>Student Name</th>
  <th>Reg no</th>
  <th>Age</th>
  <th>Subject</th>
  <th>Level</th>
  <th>Total Class</th>
  </tr>
  </thead>
 ';
 $id = 0;
 while($row = mysqli_fetch_array($result))
 {
  $id++;
  $output .= '
  <tbody>
  <tr>
  <th>'.$id.'</th>
  <td>'.$row["studentname"].'</td>
  <td>'.$row["reg_no"].'</td>
  <td>'.$row["age"].'</td>
  <td>
  ';
  if(isset($_POST["query"]))
  {
   $search = mysqli_real_escape_string($connect, $_POST["query"]);
   $query1 = "
    SELECT la.language_name, le.level_name FROM studentresult sr 
    LEFT JOIN language la ON (sr.language_id = la.language_id) 
    LEFT JOIN level le ON (sr.level_id = le.level_id) 
    WHERE sr.reg_no = '".$row["reg_no"]."' 
    AND (sr.deleted IS NULL OR sr.deleted = '0')
    AND (sr.trial = '' OR sr.trial = 'No')
    AND sr.insert_date = '".$search."' GROUP BY la.language_name ORDER BY sr.r_id
  "; 
  }
  else
  {
  $query1 = "
   SELECT la.language_name, le.level_name FROM studentresult sr 
   LEFT JOIN language la ON (sr.language_id = la.language_id) 
   LEFT JOIN level le ON (sr.level_id = le.level_id) 
   WHERE sr.reg_no = '".$row["reg_no"]."' 
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   AND sr.insert_date = '".date("Y-m-d")."' GROUP BY la.language_name ORDER BY sr.r_id
  ";
  }
  
    if($result1 = mysqli_query($connect, $query1))
    {
      while($rowcount=mysqli_fetch_array($result1))
      {
        $language_name = $rowcount['language_name'];
        $output .= '
        '.$language_name.'<br>
        ';
      }
    }
  $output .= '
  </td>
  <td>
  ';
  if(isset($_POST["query"]))
  {
   $search = mysqli_real_escape_string($connect, $_POST["query"]);
   $query1 = "
    SELECT le.level_name, le.level_id, la.language_id FROM studentresult sr 
    LEFT JOIN language la ON (sr.language_id = la.language_id) 
    LEFT JOIN level le ON (sr.level_id = le.level_id) 
    WHERE sr.reg_no = '".$row["reg_no"]."' 
    AND (sr.deleted IS NULL OR sr.deleted = '0')
    AND (sr.trial = '' OR sr.trial = 'No') 
    AND sr.insert_date = '".$search."' GROUP BY le.level_name ORDER BY sr.r_id
  ";
  }
  else
  {
  $query1 = "
   SELECT le.level_name, le.level_id, la.language_id FROM studentresult sr 
   LEFT JOIN language la ON (sr.language_id = la.language_id) 
   LEFT JOIN level le ON (sr.level_id = le.level_id) 
   WHERE sr.reg_no = '".$row["reg_no"]."' 
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   AND sr.insert_date = '".date("Y-m-d")."' GROUP BY le.level_name ORDER BY sr.r_id
  ";
  }
  
    if($result1 = mysqli_query($connect, $query1))
    {
      while($rowcount=mysqli_fetch_array($result1))
      {
        $level_name = $rowcount['level_name'];
        $output .= '
        <a href="student_daily_view.php?reg_no='.base64_encode($row["reg_no"]).'&studentname='.base64_encode($row["studentname"]).'&date='.base64_encode($search).'&level_id='.base64_encode($rowcount['level_id']).'&language_id='.base64_encode($rowcount["language_id"]).'&active='.base64_encode('No').'">'.$level_name.'</a><br>
        ';
      }
    }
    if(isset($_POST["query"]))
    {
     $search = mysqli_real_escape_string($connect, $_POST["query"]);
     $query_class = "
     SELECT max(number_of_class) total_class FROM studentresult sr 
     LEFT JOIN teacher te ON (te.username = sr.teachername) 
     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
     WHERE sr.reg_no = '".$row["reg_no"]."' 
     AND sr.insert_date = '".$search."' 
     AND (sr.deleted IS NULL OR sr.deleted = '0') 
     AND (sr.trial = '' OR sr.trial = 'No')
     GROUP BY sr.reg_no";
    }
    else
    {
    $search= date("Y-m-d");
    $query_class = "
     SELECT max(number_of_class) total_class FROM studentresult sr 
     LEFT JOIN teacher te ON (te.username = sr.teachername) 
     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
     WHERE sr.reg_no = '".$row["reg_no"]."' 
     AND sr.insert_date = '".$search."' 
     AND (sr.deleted IS NULL OR sr.deleted = '0') 
     AND (sr.trial = '' OR sr.trial = 'No')
     GROUP BY sr.reg_no";
    }
    if($result_class = mysqli_query($connect, $query_class))
    {
      $sum = '0';
      while($row_class=mysqli_fetch_array($result_class))
      {
        $sum += $row_class['total_class'];
      }
      $output .= '
      </td>
      <td>'.$sum.'</td>
      </tr>
      </tbody>
      ';
    }
 }
 echo $output;
}
else
{
 echo 'No Data From Today';
}

?>