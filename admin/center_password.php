<?php 
    session_start();
    require_once("dbConfig.php");
    $UserID = base64_decode($_GET['GetID']);
    $query = "select * from teacher where id='".$UserID."'";
    $result = mysqli_query($connect,$query);
        while($row=mysqli_fetch_assoc($result))
    {
        $UserID = $row['id'];
        $username = $row['username'];
        $password = $row['password'];

    }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>CHANGE PASSWORD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <!-- <link href="css/teacher.css" rel="stylesheet" media="all"> -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
    <link rel="icon" href="images/admin.png">
  </head>
  <body>
  <?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
            <form action="update.php?id=<?php echo $UserID ?>" method="post">   
            <div class="tab-pane" id="tab2">
                        <h3 class="" style="color: #6E6B6B;">Change Password</h3>
                        <br>
                        <br>
                        <fieldset>
                            <div class="form-group">
                                <label>Username</label>
                                <input class="form-control" placeholder="Text field" name="username" type="text" value="<?php echo $username ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="form-control" placeholder="Password" name="password" type="text" value="<?php echo $password ?>">
                            </div>
                            <button class="btn btn-info" type="submit" name="update1"><i class="glyphicon glyphicon-refresh"></i> Update</button>
                        </fieldset>                 
                </div> 
                </form>   
            </div>
          </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      var color = 'color';
      $.ajax({
       url:"fetch1.php?color="+color+"",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
</html>
  </body>
</html>

