<div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-6">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS Admin</h1>
              </div>
          </div>
        
          <div class="col-md-6">
              <div class="navbar navbar-inverse" role="banner"> 
                
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>