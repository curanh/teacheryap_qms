<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }
    $active = base64_decode($_GET['active']);
    $reg_no = base64_decode($_GET['reg_no']);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>STUDENT REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">

</head>
<body>
  <div class="header">
      <div class="container">
        <div class="row">
           <div class="col-md-5">
              <!-- Logo -->
              <div class="logo">
                 <h1 style="color: #ffffff;">QMS Admin</h1>
              </div>
          </div>
          <div class="col-md-5">
          </div>
          <div class="col-md-2">
              <div class="navbar navbar-inverse" role="banner"> 
                <br>        
                <h5 style="color: #ffffff;"><span style="
                height: 8px;
                width: 8px;
                background-color: #2ECC71;
                border-radius: 50%;
                display: inline-block;"></span>&nbspWelcome <?php echo  $_SESSION["name"];?>&nbsp<button onclick="window.location.href='index.php'"><span class="glyphicon glyphicon-log-out" style="color: #85AFF6;"></span></button></h5>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li class="shake"><a onclick="window.location.href='student_report.php'" style="color: #85AFF6;cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>

          <div class="col-md-10">
            <div class="row">
              <div class="col-md-6">
                <div class="content-box-large">
                  <div class="panel-heading">
                  <!-- <div class="panel-title">Student detail</div> -->
                  <?php
                  if ($active == 'Yes') {
                     echo '<h3 style="color: #6E6B6B;">Student detail<h3>';
                  }
                  elseif ($active == 'No') {
                     echo '<h3 style="color: #6E6B6B;">Inactive Student detail<h3>';
                  } 
                  ?>
                  <!-- <h3 style="color: #6E6B6B;">Student detail<h3> -->
                </div>
                  <div class="panel-body" style="height:230px;">
                  <h5>Student Name: <?php echo urldecode(base64_decode($_GET['studentname']));?></h5>
                  <?php 
                  require_once("dbConfig.php");

                  $query_age = "SELECT * FROM student WHERE reg_no='".$reg_no."'";
                  $result_age = mysqli_query($connect, $query_age);
                  if(mysqli_num_rows($result_age) > 0){
                    while($row_age = mysqli_fetch_array($result_age)){
                      echo "<h5>Age: ".$row_age['age']."</h5>";
                    }
 
                  }

                  // student last record
                  $query_level = "SELECT * FROM level WHERE level_id='".base64_decode($_GET['level_id'])."' ";
                  $result_level = mysqli_query($connect, $query_level);
                  if(mysqli_num_rows($result_level) > 0){
                    while($row_level = mysqli_fetch_array($result_level)){
                      echo "<h5>Level Name: ".$row_level['level_name']."</h5>";
                    }
 
                  }                  
                  
                  require_once("dbConfig.php");
                  // student last record
                  $query = "SELECT * FROM assess WHERE reg_no = '".base64_decode($_GET['reg_no'])."' AND level_id = '".base64_decode($_GET['level_id'])."'";
                  $result = mysqli_query($connect, $query);
                  if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_array($result)){
                      // echo "<h5>Mother tongue: ".$row['m_tongue']."</h5>";
                      // echo "<h5>Subjects: ".$row['subjects']."</h5>";
                      // echo "<h5>Personality: ".$row['personality']."</h5>";
                      // echo "<h5>Remark: ".$row['remark']."</h5>";
                    }
 
                  }                  
                  ?>

                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="content-box-large">
                  <div class="panel-heading">
                  <!-- <div class="panel-title">Student chart</div> -->
                  <?php
                  if ($active == 'Yes') {
                     echo '<h3 style="color: #6E6B6B;">Student chart<h3>';
                  }
                  elseif ($active == 'No') {
                     echo '<h3 style="color: #6E6B6B;">Inactive Student chart<h3>';
                   } 
                  ?>
                  <!-- <h3 style="color: #6E6B6B;">Student chart<h3> -->
                  <div class="panel-options">
                  </div>
                </div>
                  <div class="panel-body">
                  <div id="chartdiv" style="height:200px;"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-box-large">
              <div class="panel-heading">
                <h3 style="color: #6E6B6B;">LMS<h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <div class="form-group">
                </div>
                <table class="table" id="result_lms">
                </table>
                <a href="view_student_report.php?reg_no=<?php echo $_GET['reg_no']; ?>&studentname=<?php echo $_GET['studentname'];?>&active=<?php echo $_GET['active'];?>"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a>
                </div>
              </div>
            </div>
            <div class="content-box-large">
              <a href="not_complete_report.php"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Not Complete Report</button></a>
              <div class="panel-heading">
                <!-- <div class="panel-title">Student Report</div> -->
                <?php
                if ($active == 'Yes') {
                   echo '<h3 style="color: #6E6B6B;">Student Report / Level / Report<h3>';
                }
                elseif ($active == 'No') {
                   echo '<h3 style="color: #6E6B6B;">Inactive Student Report / Level / Report<h3>';
                 } 
                ?>
                <!-- <h3 style="color: #6E6B6B;">Student Report / Report<h3> -->
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <div class="form-group">
                </div>
                <table class="table" id="result">
                </table>
                <a href="view_student_report.php?reg_no=<?php echo $_GET['reg_no']; ?>&studentname=<?php echo $_GET['studentname'];?>&active=<?php echo $_GET['active'];?>"><button class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></a>
                </div>
              </div>
            </div>
            <button class="btn btn-default" onclick=window.location.href="print.php?reg_no=<?php echo $_GET['reg_no'];?>&studentname=<?php echo urlencode($_GET['studentname']);?>&level_id=<?php echo $_GET['level_id'];?>&fid=<?php echo base64_encode($_SESSION['fid']);?>"><i class="glyphicon glyphicon-print"></i> Print</button>
          </div>
      </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/amcharts.js" type="text/javascript"></script>
<script src="js/radar.js" type="text/javascript"></script>
<script src="js/dataloader.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script>
var urlParams = new URLSearchParams(window.location.search);
var reg_no = urlParams.get('reg_no');
var level_id = urlParams.get('level_id');
var chart = AmCharts.makeChart("chartdiv", {
type: "radar",
dataLoader: {
    "url": "data.php?reg_no="+reg_no+"&level_id="+level_id+"",
    "format": "json"
},
categoryField: "title",
startDuration: 2,

valueAxes: [{
    axisAlpha: 0.15,
    minimum: 0,
    dashLength: 3,
    axisTitleOffset: 20,
    gridCount: 5
}],

graphs: [{
    valueField: "assess_title",
    bullet: "round",
    balloonText: "[[title]]  [[value]]",
}],
"export": {
  "enabled": true,
  "libs": {
    "autoLoad": false
  }
}
});
</script>
<script>
$(document).ready(function(){
 load_data();
 //document.getElementById("teacher_name").value = teacher_name;
 //alert(teacher_name);
 function load_data(query)
 {
 var urlParams = new URLSearchParams(window.location.search);
 var reg_no = urlParams.get('reg_no');
 var level_id = urlParams.get('level_id');
 var studentname = urlParams.get('studentname');
 var active = urlParams.get('active');
 //alert(teacher_name);
 $('#result').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_all_student_report.php?reg_no="+reg_no+"&level_id="+level_id+"&studentname="+studentname+"&active="+active+"",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
});

$(document).ready(function(){
 load_data();
 //document.getElementById("teacher_name").value = teacher_name;
 //alert(teacher_name);
 function load_data(query)
 {
 var urlParams = new URLSearchParams(window.location.search);
 var reg_no = urlParams.get('reg_no');
 var level_id = urlParams.get('level_id');
 var studentname = urlParams.get('studentname');
 //alert(teacher_name);
 $('#result_lms').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_all_student_report_lms.php?reg_no="+reg_no+"&level_id="+level_id+"&studentname="+studentname+"",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result_lms').html(data);
   }
  });
 }
});
</script>

<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});

    
    </script>
</html>