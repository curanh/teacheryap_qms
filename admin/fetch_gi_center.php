<?php
include 'dbConfig.php';
session_start();
if ($_SESSION['fid'] == "") {
  header("Location: index.php");
}
$response = "";
$fid = $_SESSION["fid"];

  if(isset($_POST["query"])){
      $search = mysqli_real_escape_string($connect, $_POST["query"]);
      $query = "
      SELECT ct.name, st.studentname, st.age, la.language_id, la.language_name, le.level_name, sr.level_id, sr.reg_no FROM centre ct
      LEFT JOIN student st ON (st.fid = ct.fid) 
      LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
      LEFT JOIN level le ON (le.level_id = sr.level_id)
      LEFT JOIN language la ON (la.language_id = sr.language_id) 
      WHERE ct.fid = '$fid'
      AND la.language_id = '$search'
      AND st.active ='Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0') 
      AND (sr.trial = '' OR sr.trial = 'No')
      GROUP BY sr.level_id 
      ORDER BY sr.level_id ASC
    ";
    }else{
      $query = "
      SELECT ct.name, st.studentname, st.age, la.language_id, la.language_name, le.level_name, sr.level_id, sr.reg_no FROM centre ct
      LEFT JOIN student st ON (st.fid = ct.fid) 
      LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
      LEFT JOIN level le ON (le.level_id = sr.level_id)
      LEFT JOIN language la ON (la.language_id = sr.language_id) 
      WHERE ct.fid = '$fid'
      AND st.active ='Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0') 
      AND (sr.trial = '' OR sr.trial = 'No')
      GROUP BY sr.level_id 
      ORDER BY sr.level_id ASC
      ";
    }
    
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
     $response .= '
      <thead>
        <tr>
          <th>Centre</th>
          <th>Subject</th>
          <th>Level</th>
          <th>Total Student</th>
          <th>Pass</th>
          <th>Fail</th>
          <th>Not Complete</th>
          <th>No KPI Class</th>
          <th></th>
        </tr>
      </thead>
     ';

     while($row = mysqli_fetch_array($result))
     { 
     
      $query2="SELECT sr.level_id, le.total_class
      FROM studentresult sr 
      LEFT JOIN level le ON (sr.level_id = le.level_id) 
      LEFT JOIN language la ON (sr.language_id = la.language_id) 
      WHERE sr.reg_no='".$row['reg_no']."' 
      AND sr.level_id = '".$row['level_id']."' 
      AND (sr.deleted IS NULL OR sr.deleted = 0)
      AND (sr.trial = '' OR sr.trial = 'No')
      GROUP BY sr.level_id";

      if($result2 = mysqli_query($connect, $query2))
      {
          $pass = '0';
          $fail = '0';
          $not = '0';
          $no = '0';
          while($row2 = mysqli_fetch_array($result2))
          {
            if (!empty($row['level_name'])) {
             $response .= '
              <tbody>
                <tr>
                  <td><strong>'.$row["name"].'</strong></td>
                  <td>'.$row['language_name'].'</td>
                  <td>'.$row['level_name'].'</td>
              ';
            }
            
            $query_row = "
            SELECT sr.r_id, sr.level_id, sr.reg_no FROM studentresult sr
            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
            WHERE sr.level_id = '".$row["level_id"]."' 
            AND sr.fid = $fid 
            AND st.active = 'Yes'
            AND (sr.deleted IS NULL OR sr.deleted = '0') 
            AND (sr.trial = '' OR sr.trial = 'No')
            GROUP BY sr.reg_no
            ";
            if($result_row = mysqli_query($connect, $query_row))
            {
              $rowcount=mysqli_num_rows($result_row);
            }
            if ($rowcount > 0 AND !empty($row['level_name'])) {
              $response .='
              <td>'.$rowcount.'</td>
              ';
            }
            if($result_row = mysqli_query($connect, $query_row))
            {
                         
                while($row_row = mysqli_fetch_array($result_row))
                {

                  $query3 = "
                  SELECT count(sr.r_id) AS total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
                  LEFT JOIN level le ON (le.level_id=sr.level_id)
                  LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                  WHERE sr.level_id = '".$row['level_id']."'
                  AND st.active = 'Yes'
                  AND sr.reg_no = '".$row_row['reg_no']."'
                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
                  AND (sr.trial = '' OR sr.trial = 'No')
                  ";
                  if($result3 = mysqli_query($connect, $query3))
                  {
                               
                    while($row3 = mysqli_fetch_array($result3))
                    {
                        $total_class = $row3['total_class'];
                        $total = $row3['total'];

                        $query4 = "
                        SELECT sr.complete FROM studentresult sr
                        LEFT JOIN level le ON (le.level_id=sr.level_id)
                        LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                        WHERE sr.level_id = '".$row3['level_id']."'
                        AND st.active = 'Yes'
                        AND sr.reg_no = '".$row3['reg_no']."'
                        AND (sr.deleted IS NULL OR sr.deleted = '0') 
                        AND (sr.trial = '' OR sr.trial = 'No')
                        ORDER BY sr.r_id DESC LIMIT 1
                        ";
                        if($result4 = mysqli_query($connect, $query4))
                        {
                                     
                            while($row4 = mysqli_fetch_array($result4))
                            {                            

                            if($row4['complete'] == 'yes' AND $total <= $total_class){
                              $pass ++;
                              // echo '<script language="javascript">';
                              // echo 'alert("message successfully sent")';
                              // echo '</script>';
                            }elseif ($total_class == '0') {
                              $no ++;
                            }
                            elseif($row4['complete'] == 'yes' AND $total > $total_class) {
                              $fail ++;
                            }elseif($row4['complete'] == 'no' OR $row4['complete'] == '') {
                              $not ++;
                            }
                          }
                        }
                      }
                    }
                }
                  if (!empty($row['level_name'])) {
                  $response .='
                  <td>'.$pass.'</td>
                  <td>'.$fail.'</td>
                  <td>'.$not.'</td>
                  <td>'.$no.'</td>
                  <td><button class="btn btn-default" onclick=window.location.href="gi_view.php?fid='.base64_encode($fid).'&level_id='.base64_encode($row["level_id"]).'"><i class="glyphicon glyphicon-eye-open"></i> View</button></td>
                  '; 
                }
                
            }
          }          
        }
        
          $response .= '
          </tbody>
          </tr>
          ';
     }
      echo $response;
    }else{
      $response.='
      No Record';
    }
?>