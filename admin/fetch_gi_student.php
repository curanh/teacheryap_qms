<?php
include 'dbConfig.php';
session_start();
if ($_SESSION['fid'] == "") {
  header("Location: index.php");
}
$response = "";
$response1 = "";
$response2 = "";
$fid = $_SESSION["fid"];
$functionName = $_POST['functionName'];
if(isset($_POST['age'])) {
$age = $_POST["age"];
}
if (isset($_POST['status'])) {
  $status = $_POST["status"];
}

if($functionName == "age") {
    $query = "
    SELECT ct.name, st.studentname, st.age, la.language_id, le.level_name, la.language_name, sr.level_id, sr.reg_no FROM centre ct
    LEFT JOIN student st ON (st.fid = ct.fid) 
    LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
    LEFT JOIN level le ON (le.level_id = sr.level_id)
    LEFT JOIN language la ON (la.language_id = sr.language_id) 
    WHERE ct.fid = '$fid'
    AND st.age ".$age."
    AND st.active = 'Yes'
    AND (sr.deleted IS NULL OR sr.deleted = '0') 
    AND (sr.trial = 'No' OR sr.trial = '')
    GROUP BY sr.level_id 
    ORDER BY sr.level_id ASC
    ";
    //echo $query; exit();
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
     $response1 .= '
      <thead>
      <td><button class="btn btn-info" onclick=window.location.href="gi_print.php?fid='.base64_encode($fid).'">Print</button></td>
      <tr>
      <th>Center</th>
      <th>Student Name</th>
      <th>Age</th>
      <th>Subject</th>
      <th>Level</th>
      <th>Status</th>
      <th>KPI Class</th>
      <th>Actual Class</th>
      </tr>
      </thead>
     ';

     while($row = mysqli_fetch_array($result))
     { 
     

      $query4="SELECT * FROM studentresult sr
      LEFT JOIN student st ON (st.reg_no = sr.reg_no)
      WHERE sr.fid = '$fid' 
      AND sr.level_id = '".$row['level_id']."'
      AND st.active ='Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0') 
      AND (sr.trial = 'No' OR sr.trial = '')
      GROUP BY sr.reg_no";

      if($result4 = mysqli_query($connect, $query4))
      {
                   
          while($row4 = mysqli_fetch_array($result4))
          {
            $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
            FROM studentresult sr 
            LEFT JOIN level le ON (sr.level_id = le.level_id) 
            LEFT JOIN language la ON (sr.language_id = la.language_id) 
            WHERE sr.reg_no='".$row4['reg_no']."' 
            AND sr.level_id = '".$row4['level_id']."' 
            AND (sr.deleted IS NULL OR sr.deleted = 0)
            AND (sr.trial = 'No' OR sr.trial = '')";

            if($result2 = mysqli_query($connect, $query2))
            {
                         
                while($row2 = mysqli_fetch_array($result2))
                {
                  $total_class = $row2['total_class'];
                  $total = $row2['total'];

                  $query3 = "
                    SELECT sr.complete FROM studentresult sr
                    LEFT JOIN level le ON (le.level_id=sr.level_id)
                    LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                    WHERE sr.level_id = '".$row2['level_id']."'
                    AND st.active = 'Yes'
                    AND sr.reg_no = '".$row2['reg_no']."'
                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
                    AND (sr.trial = '' OR sr.trial = 'No')
                    ORDER BY sr.r_id DESC LIMIT 1
                    ";
                    if($result3 = mysqli_query($connect, $query3))
                    {
                                 
                        while($row3 = mysqli_fetch_array($result3))
                        {
                            
                            // echo $total_class;
                            // echo '<br>';
                            // echo $total;
                            //exit();
                          if ($total_class == '0' AND ($status == '0' OR $status == '4')) {
                                $response1 .= '
                                <tbody>
                                    <tr>
                                      <td><strong>'.$row["name"].'</strong></td>
                                      <td>'.$row4['studentname'].'</td>
                                      <td>'.$row['age'].'</td>
                                      <td>'.$row['language_name'].'</td>
                                      <td>'.$row['level_name'].'</td>
                                      <td style="color:#787878"><strong>No KPI Class</strong></td>
                                      <td>'.$total_class.'</td>
                                      <td>'.$row2['total'].'</td> 
                                ';
                            }
                            elseif($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '1')){
                              $response1 .= '
                                <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#31D919"><strong>PASS</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }
                            elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '2')) {
                              $response1 .= '
                              <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#F43939"><strong>FAIL</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '3')) {
                              $response1 .= '
                              <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }
                          }    
                      }
                }          
            }
          }
      }

          $response1 .= '
          </tbody>
          </tr>
          ';
     }
      echo $response1;
    }else{
      $response1.='
      No Record';
    }
}

if($functionName == "status") {
    // echo $text; exit();
    $query = "
    SELECT ct.name, st.studentname, st.age, la.language_id, la.language_name, le.level_name, sr.level_id, sr.reg_no FROM centre ct
    LEFT JOIN student st ON (st.fid = ct.fid) 
    LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
    LEFT JOIN level le ON (le.level_id = sr.level_id)
    LEFT JOIN language la ON (la.language_id = sr.language_id) 
    WHERE ct.fid = '$fid'
    AND st.age ".$age." 
    AND st.active = 'Yes'
    AND (sr.deleted IS NULL OR sr.deleted = '0') 
    AND (sr.trial = 'No' OR sr.trial = '')
    GROUP BY sr.level_id 
    ORDER BY sr.level_id ASC
    ";
    //echo $query; exit();
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
     $response2 .= '
      <thead>
      <td><button class="btn btn-info" onclick=window.location.href="gi_print.php?fid='.base64_encode($fid).'">Print</button></td>
      <tr>
      <th>Center</th>
      <th>Student Name</th>
      <th>Age</th>
      <th>Subject</th>
      <th>Level</th>
      <th>Status</th>
      <th>KPI Class</th>
      <th>Actual Class</th>
      </tr>
      </thead>
     ';

     while($row = mysqli_fetch_array($result))
     { 
     
      
     $query4="SELECT * FROM studentresult sr
      LEFT JOIN student st ON (st.reg_no = sr.reg_no)
      WHERE sr.fid = '$fid' 
      AND sr.level_id = '".$row['level_id']."'
      AND st.active ='Yes'
      AND (sr.deleted IS NULL OR sr.deleted = '0') 
      AND (sr.trial = 'No' OR sr.trial = '')
      GROUP BY sr.reg_no";

      if($result4 = mysqli_query($connect, $query4))
      {
                   
          while($row4 = mysqli_fetch_array($result4))
          {

            $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
            FROM studentresult sr 
            LEFT JOIN level le ON (sr.level_id = le.level_id) 
            LEFT JOIN language la ON (sr.language_id = la.language_id) 
            WHERE sr.reg_no='".$row4['reg_no']."' 
            AND sr.level_id = '".$row4['level_id']."' 
            AND (sr.deleted IS NULL OR sr.deleted = 0)
            AND (sr.trial = 'No' OR sr.trial = '')";

            if($result2 = mysqli_query($connect, $query2))
            {
                         
                while($row2 = mysqli_fetch_array($result2))
                {
                  $total_class = $row2['total_class'];
                  $total = $row2['total'];

                  $query3 = "
                    SELECT sr.complete FROM studentresult sr
                    LEFT JOIN level le ON (le.level_id=sr.level_id)
                    LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                    WHERE sr.level_id = '".$row2['level_id']."'
                    AND st.active = 'Yes'
                    AND sr.reg_no = '".$row2['reg_no']."'
                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
                    AND (sr.trial = '' OR sr.trial = 'No')
                    ORDER BY sr.r_id DESC LIMIT 1
                    ";
                    if($result3 = mysqli_query($connect, $query3))
                    {
                                 
                        while($row3 = mysqli_fetch_array($result3))
                        {
                            
                            // echo $total_class;
                            // echo '<br>';
                            // echo $total;
                            //exit();
                          if ($total_class == '0' AND ($status == '0' OR $status == '4')) {
                                $response2 .= '
                                <tbody>
                                    <tr>
                                      <td><strong>'.$row["name"].'</strong></td>
                                      <td>'.$row4['studentname'].'</td>
                                      <td>'.$row['age'].'</td>
                                      <td>'.$row['language_name'].'</td>
                                      <td>'.$row['level_name'].'</td>
                                      <td style="color:#787878"><strong>No KPI Class</strong></td>
                                      <td>'.$total_class.'</td>
                                      <td>'.$row2['total'].'</td> 
                                ';
                            }
                            elseif($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '1')){
                              $response2 .= '
                                <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#31D919"><strong>PASS</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }
                            elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '2')) {
                              $response2 .= '
                              <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#F43939"><strong>FAIL</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '3')) {
                              $response2 .= '
                              <tbody>
                                  <tr>
                                    <td><strong>'.$row["name"].'</strong></td>
                                    <td>'.$row4['studentname'].'</td>
                                    <td>'.$row['age'].'</td>
                                    <td>'.$row['language_name'].'</td>
                                    <td>'.$row['level_name'].'</td>
                                    <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
                                    <td>'.$total_class.'</td>
                                    <td>'.$row2['total'].'</td> 
                              ';
                            }
                          }    
                      }

                }          
              }
          }
      }
        
          $response2 .= '
          </tbody>
          </tr>
          ';
     }
      echo $response2;
    }else{
      $response2.='
      No Record';
    }
}


?>
