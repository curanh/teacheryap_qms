<?php
require_once __DIR__ . '/mpdf/autoload.php';
//$mpdf->autoScriptToLang = true;
require_once('dbConfig.php');
$reg_no = base64_decode($_GET['reg_no']);
$level_id = base64_decode($_GET['level_id']);
$studentname = urldecode(base64_decode($_GET['studentname']));
$fid = base64_decode($_GET['fid']);

$sql = "SELECT *  FROM centre WHERE fid = '".$fid."' ORDER BY centre.name";
$result = mysqli_query($connect, $sql);

$html .= '
<!DOCTYPE html>
<html lang="cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/mpdf.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 brandSection">
                <div class="row">
                    <div class="col-md-12 col-sm-12 header">
                        <div class="col-md-3 col-sm-3 headerLeft">
                           <img id="image" src="images/admin.png" alt="logo" height="60" width="60"/>
                        </div>
                        <div class="col-md-9 col-sm-9 headerRight">     
                        <p>QMS - 进度表<p>               
                        </div>
                        </div>
                        <centre>
                        <div class="col-md12 col-sm12 content">
                        <table class = "table">
                        
';
                    while($row = mysqli_fetch_array($result)){
                    date_default_timezone_set("Asia/Kuala_Lumpur");
                    $date = date('d/m/Y');
                    $time = date('h.i a');
                    $html .= '
                    <tr>
                      <td><strong>'.$row['name'].'</strong></td>
                      <td><center>&nbsp;&nbsp;Print Date: '.$date.'<center></td>
                    </tr>
                    <tr>
                      <td>Student Name: '.$studentname.'</td>
                      <td><center>Print Time: '.$time.'</center></td>
                    </tr>
                    ';
                    $sql_level = "SELECT * FROM level WHERE level_id = '".$level_id."'";
                    $result_level = mysqli_query($connect, $sql_level);
                    while($row_level = mysqli_fetch_array($result_level)){
                    $html .='
                    <tr>
                      <td>Level Name : '.$row_level['level_name'].'</td>
                    </tr>
                    ';
                    }
                    $html .= '
                    
                    ';
                    }
                    

                    $html .= '
                    </table>
                    </div>
                    </centre>
                    <div class="col-md-12 col-sm-12 content">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 panelPart">
                              <div class="panel panel-default">
                                  <div class="panel-body">
                                      <b>Record</b>
                                  </div>
                                  <table id="customers">
                                    <thead>
                                    <tr>
                                      <th>Date</th>
                                      <th>Teacher</th>
                                      <th>Title</th>
                                      <th>Feedback</th>
                                      <th>Status</th>
                                    </tr>
                                    </thead>
                    ';
                    $query = "
                      SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr 
                      LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
                      LEFT JOIN level le ON (sr.level_id = le.level_id) 
                      LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
                      WHERE sr.reg_no='".$reg_no."' AND sr.level_id='".$level_id."' 
                      AND (sr.deleted IS NULL OR sr.deleted = '0') 
                      AND (sr.trial = '' OR sr.trial = 'No') ORDER BY sr.insert_date DESC
                     ";
                     $result_sr = mysqli_query($connect, $query);
                     
                     while($row_sr = mysqli_fetch_array($result_sr)){
                     $newDate = date("d/m/Y", strtotime($row_sr["insert_date"]));
                     $html .='
                        <tbody>
                        <tr>
                          <td>'.$newDate.'</td>
                          <td>'.$row_sr["teachername"].'</td>
                          <td>';

                          if ($row_sr['method'] == 'Yes') {     
                          $query_title = "
                          SELECT * FROM level_title 
                          WHERE status = 1 
                          AND (deleted IS NULL OR deleted = 0) 
                          AND level_id = '".$level_id."' 
                          AND title_id IN (".$row_sr["title_id"].") 
                          ORDER BY sorting ASC";

                          $result_title = mysqli_query($connect,$query_title);
                          while($row_titleid=mysqli_fetch_assoc($result_title))
                          {   

                            $html .= $row_titleid['title_name'].'<br>';        
                          }
                          }else{
                          $html.= $row_sr["title_id"];
                          }


                      $html .='
                          </td>
                          <td>'.$row_sr["feedback"].'</td>
                          <td>'.$row_sr["student_status"].'</td>
                        </tr>
                        </tbody>
                     ';
                    }
                    $html .=' </table>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>      
</body>
</html>
';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'zh-cn', 'format' => 'A4']);
    $file_name = 'QMS - 进度表 ('.$studentname.').pdf';
    $mpdf->SetTitle('QMS - 进度表 ('.$studentname.')');
    //$mpdf->autoScriptToLang = true;
    $mpdf->autoLangToFont = true;
    $mpdf->useAdobeCJK = true;
    //call watermark content aand image
    // $mpdf->SetAutoFont();
    $mpdf->WriteHTML($html);
    //save the file put which location you need folder/filname
    $mpdf->Output($file_name, 'D');
    //out put in browser below output function
    $mpdf->Output();
?>