<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>STUDENT DAILY REPORT</title>
    <meta charset="UTF-8">
    <link rel="icon" href="images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
</head>
<body>
<?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" class="shake" style="color: #85AFF6;">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li class="shake"><a onclick="window.location.href='student_daily.php'" style="color: #85AFF6;cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
          <div class="row">
          <div class="col-md-12">
            <div class="content-box-large">
              <div class="panel-heading">
            </div>
              <div class="panel-body">
                <div id="rootwizard">
                <div class="navbar">
                  <div class="navbar-inner">
                    <div class="container">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#tab1" data-toggle="tab">Student Daily Report</a></li>
                  <li><a href="#tab2" data-toggle="tab">Inactive Student Daily Report</a></li>
                </ul>
                 </div>
                  </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1" >
                    <div class="panel-heading">
                      <!-- <div class="panel-title">Student Daily</div> -->
                      <h3 style="color: #6E6B6B;">Student Daily Report<h3>
                    </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <div class="form-group">
                        <div class="input-group">
                        <span class="input-group-addon">Search</span>
                        <input type="date" name="search_text" id="search_text" class="form-control" style="width: 15%" />
                        </div>
                      </div>
                          <table class="table" id="result">
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="tab2">
                      <div class="panel-heading">
                      <h3 style="color: #6E6B6B;">Inactive Student Daily Report<h3>
                    </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <div class="form-group">
                        <div class="input-group">
                        <span class="input-group-addon">Search</span>
                        <input type="date" name="search_text_inactive" id="search_text_inactive" class="form-control" style="width: 15%" />
                        </div>
                      </div>
                          <table class="table" id="result_inactive">
                          </table>
                        </div>
                      </div>
                    </div>
                </div>  
              </div>
              </div>
            </div>
          </div>
        </div>
          </div>
</body>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
  var date = new Date();
    var day = date.getDate(),
    month = date.getMonth() + 1,
    year = date.getFullYear(),
    hour = date.getHours(),
    min  = date.getMinutes();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;

    var today = year + "-" + month + "-" + day;
    var today1 = year + "-" + month + "-" + day;
    document.getElementById('search_text').value = today;
    document.getElementById('search_text_inactive').value = today1;
</script>
<script>
$(document).ready(function(){
window.onload=load_data();
window.onload=load_data1();
 //load_data();

 function load_data(query)
 {
  $('#result').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_student_daily.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#result').html("Loading Please Wait...........");
 $('#search_text').change(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
 function load_data1(query)
 {
  $('#result_inactive').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_student_daily_inactive.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result_inactive').html(data);
   }
  });
 }
 $('#result_inactive').html("Loading Please Wait...........");
 $('#search_text_inactive').change(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data1(search);
  }
  else
  {
   load_data1();
  }
 });
});
</script>

<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
    </script>
</html>