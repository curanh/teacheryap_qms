<?php
//fetch.php
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
        
}
$fid = $_SESSION['fid'];
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM student 
  WHERE fid = '$fid' AND studentname LIKE '%".$search."%' AND active = 'No' ORDER BY studentname
 ";
}
else
{
 $query = "
  SELECT * FROM student WHERE fid = '$fid' AND active = 'No' ORDER BY studentname
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>Student Name</th>
  <th>Reg No</th>
  <th>Born</th>
  <th>Subject</th>
  <th></th>
  </tr>
  </thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $born = date('Y',strtotime($row["dob"]));
  $output .= '
  <tbody>
  <tr>
  <td>'.$row["studentname"].'</td>
  <td>'.$row["reg_no"].'</td>
  <td>'.$born.'</td>
  <td>
  ';
  $query_cms = "
  SELECT * FROM student WHERE fid = '$fid' AND active = 'No' AND reg_no = '".$row['reg_no']."' ORDER BY name
  ";
  $result_cms = mysqli_query($connect2, $query_cms);
  if(mysqli_num_rows($result_cms) > 0)
  {
    while($row_cms = mysqli_fetch_array($result_cms))
    {
      $mand = $row_cms['language_mand'];
      $bm = $row_cms['language_bm'];
      $en = $row_cms['language_en'];
      $ma = $row_cms['language_ma'];
      $miq = $row_cms['language_miq'];
        if ($mand == 'Yes') {
          $output .= '
          <b>华语 (Chinese)&#x2705;</b><br>
          ';
        }
        if ($bm == 'Yes') {
          $output .= '
          <b>国语 (Melayu)&#x2705;</b><br>
          ';
        }
        if ($en == 'Yes') {
          $output .= '
          <b>英语 (English)&#x2705;</b><br>
          ';
        }
        if ($ma == 'Yes') {
          $output .= '
          <b>珠算 (Abacus)&#x2705;</b><br>
          ';
        }
        if ($miq == 'Yes') {
          $output .= '
          <b>MIQ &#x2705;</b><br>
          ';
        }
         
    }
  }
  $output .='
  </td>
  <td><button class="btn btn-warning" onclick=window.location.href="view_student_report.php?reg_no='.base64_encode($row["reg_no"]).'&studentname='.base64_encode(urlencode($row["studentname"])).'&active='.base64_encode('No').'">Report</button></td>
  </tr>
  </tbody>
  ';
 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>