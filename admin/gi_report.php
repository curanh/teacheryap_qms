<?php 
     require_once("dbConfig.php");
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");

    }
   
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Guarantee Improvement Report</title>
    <meta charset="UTF-8">
    <link rel="icon" href="..//images/admin.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <link rel="stylesheet" type="text/css" href="css/teacher_list/util.css">
    <link rel="stylesheet" type="text/css" href="css/teacher_list/main.css"> -->
    <!-- <img class="ldld" src="your-loader-url"> -->    
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
</head>
</style>
<body>
<?php include "header.php"; ?>
    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                   <li><a onclick="window.location.href='dashboard.php'" style="cursor:pointer;"><i class="glyphicon glyphicon-home"></i>Dashboard</a></li>
                    <!-- <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li> -->
                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Teacher / Centre
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                        <ul>
                            <li><a onclick="window.location.href='teacher.php'" style="cursor:pointer;">Create Account</a></li>
                            <li><a onclick="window.location.href='teacher_list.php'" style="cursor:pointer;">Edit Account</a></li>
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="" style="color: #85AFF6;" class="shake">
                            <i class="glyphicon glyphicon-list-alt"></i>Report 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a onclick="window.location.href='center_report.php'" style="cursor:pointer;">Center Report</a></li>
                            <li><a onclick="window.location.href='teacher_main_report.php'" style="cursor:pointer;">Teacher Report</a></li>
                            <li><a onclick="window.location.href='student_report.php'" style="cursor:pointer;">Student Report</a></li>
                            <li><a onclick="window.location.href='not_complete_report.php'" style="cursor:pointer;">Not Complete Report</a></li>
                            <li><a onclick="window.location.href='teacher_list_kpi.php'" style="cursor:pointer;">Teacher KPI Report</a></li>
                            <li><a onclick="window.location.href='student_daily.php'" style="cursor:pointer;">Student Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_daily.php'" style="cursor:pointer;">Teacher Daily Report</a></li>
                            <li><a onclick="window.location.href='teacher_monthly.php'" style="cursor:pointer;">Teacher Monthly Report</a></li>
                            <li class="shake"><a onclick="window.location.href='gi_report.php'" style="cursor:pointer;color: #85AFF6;">Guarantee Improvement Report</a></li>
                            <!-- <li><a onclick="window.location.href='total_kpi.php'">Total KPI</a></li> -->
                        </ul>
                    </li>

                    <li class="submenu">
                         <a href="">
                            <i class="glyphicon glyphicon-user"></i>Account 
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li id="result1" style="cursor:pointer;"></li>
                            <!-- <li><a onclick="window.location.href='index.php'">Logout</a></li> -->
                            
                        </ul>
                    </li>
                </ul>
             </div>
          </div>
          <div class="col-md-10">
            <div class="content-box-large">
              <div class="panel-heading">
                <h3 style="color: #6E6B6B;">Guarantee Improvement Report<h3>
              </div>
              <div class="panel-body">
                <div id="rootwizard">
                  <div class="navbar">
                      <div class="navbar-inner">
                        <div class="container">
                          <ul class="nav nav-pills">
                              <li class="active"><a href="#tab1" data-toggle="tab">Center Report</a></li>
                            <li><a href="#tab2" data-toggle="tab">Student Report</a></li>
                          </ul>
                        </div>
                      </div>
                  </div>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab1" >
                        <div class="panel-heading">
                          <h3 style="color: #6E6B6B;">Center Report<h3>
                        </div>
                        <div class="panel-body">
                          <div class="table-responsive">
                            <div class="form-group">
                              <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Language</span>
                                  <select class="form-control" id="search_lang" name="search_lang" style="width: 23.5%">
                                    <option value="">All</option>
                                    <option value="1">华语</option>
                                    <option value="2">国语</option>
                                    <option value="3">珠算</option>
                                    <option value="4">英语</option>
                                    <option value="5">PK</option>
                                    <option value="6">TT</option>
                                    <option value="7">MIQ</option>
                                    <option value="8">HOTS 36</option>
                                    <option value="9">Online 华语</option>
                                  </select>
                                  </div>
                            </div>
                            <table class="table" id="result">
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="tab2">
                        <div class="tab-pane active" id="tab1">
                          <div class="panel-heading">
                            <h3 style="color: #6E6B6B;">Student Report<h3>
                          </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                              <div class="form-group">
                                <div class="input-group">
                                  <span class="input-group-addon">Age</span>
                                    <select class="form-control" id="search_text_age" name="search_text_age" style="width: 26.2%">
                                      <option value="> 0" selected>All</option>
                                      <option value="<= 6">3 - 6 Age</option>
                                      <option value=">= 7">7 - 12 Age</option>
                                    </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span class="input-group-addon">Status</span>
                                    <select class="form-control" id="search_text_status" name="search_text_status" style="width: 25.2%">
                                      <option value="0" selected>All</option>
                                      <option value="1">Pass</option>
                                      <option value="2">Fail</option>
                                      <option value="3">Not Complete</option>
                                      <option value="4">No KPI Class</option>
                                    </select>
                                </div>
                              </div>
                              <table class="table" id="result2">
                                <?php
                                $fid = $_SESSION['fid'];
                                $query = "
                                SELECT ct.name, st.studentname, st.age, la.language_id, le.level_name, la.language_name, sr.level_id, sr.reg_no FROM centre ct
                                LEFT JOIN student st ON (st.fid = ct.fid) 
                                LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
                                LEFT JOIN level le ON (le.level_id = sr.level_id)
                                LEFT JOIN language la ON (la.language_id = sr.language_id) 
                                WHERE ct.fid = '$fid'
                                AND st.active = 'Yes'
                                AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                AND (sr.trial = 'No' OR sr.trial = '')
                                GROUP BY sr.level_id 
                                ORDER BY sr.level_id ASC
                                ";
                                //echo $query; exit();
                                $result = mysqli_query($connect, $query);
                                if(mysqli_num_rows($result) > 0)
                                {
                                 echo '
                                  <thead>
                                  <td><button class="btn btn-info" onclick=window.location.href="gi_print.php?fid='.base64_encode($fid).'">Print</button></td>
                                  <tr>
                                  <th>Center</th>
                                  <th>Student Name</th>
                                  <th>Age</th>
                                  <th>Subject</th>
                                  <th>Level</th>
                                  <th>Status</th>
                                  <th>KPI Class</th>
                                  <th>Actual Class</th>
                                  </tr>
                                  </thead>
                                 ';

                                 while($row = mysqli_fetch_array($result))
                                 { 
                                 

                                  $query4="SELECT * FROM studentresult sr
                                  LEFT JOIN student st ON (st.reg_no = sr.reg_no)
                                  WHERE sr.fid = '$fid' 
                                  AND sr.level_id = '".$row['level_id']."'
                                  AND st.active ='Yes'
                                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                  AND (sr.trial = 'No' OR sr.trial = '')
                                  GROUP BY sr.reg_no";

                                  if($result4 = mysqli_query($connect, $query4))
                                  {
                                               
                                      while($row4 = mysqli_fetch_array($result4))
                                      {
                                        $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
                                        FROM studentresult sr 
                                        LEFT JOIN level le ON (sr.level_id = le.level_id) 
                                        LEFT JOIN language la ON (sr.language_id = la.language_id) 
                                        WHERE sr.reg_no='".$row4['reg_no']."' 
                                        AND sr.level_id = '".$row4['level_id']."' 
                                        AND (sr.deleted IS NULL OR sr.deleted = 0)
                                        AND (sr.trial = 'No' OR sr.trial = '')";

                                        if($result2 = mysqli_query($connect, $query2))
                                        {
                                                     
                                            while($row2 = mysqli_fetch_array($result2))
                                            {
                                              $total_class = $row2['total_class'];
                                              $total = $row2['total'];

                                              $query3 = "
                                                SELECT sr.complete FROM studentresult sr
                                                LEFT JOIN level le ON (le.level_id=sr.level_id)
                                                LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                                                WHERE sr.level_id = '".$row2['level_id']."'
                                                AND st.active = 'Yes'
                                                AND sr.reg_no = '".$row2['reg_no']."'
                                                AND (sr.deleted IS NULL OR sr.deleted = '0') 
                                                AND (sr.trial = '' OR sr.trial = 'No')
                                                ORDER BY sr.r_id DESC LIMIT 1
                                                ";
                                                if($result3 = mysqli_query($connect, $query3))
                                                {
                                                             
                                                    while($row3 = mysqli_fetch_array($result3))
                                                    {
                                                        
                                                        // echo $total_class;
                                                        // echo '<br>';
                                                        // echo $total;
                                                        //exit();
                                                        if($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name'])){
                                                          echo '
                                                            <tbody>
                                                              <tr>
                                                                <td><strong>'.$row["name"].'</strong></td>
                                                                <td>'.$row4['studentname'].'</td>
                                                                <td>'.$row['age'].'</td>
                                                                <td>'.$row['language_name'].'</td>
                                                                <td>'.$row['level_name'].'</td>
                                                                <td style="color:#31D919"><strong>PASS</strong></td>
                                                                <td>'.$total_class.'</td>
                                                                <td>'.$row2['total'].'</td> 
                                                          ';
                                                        }elseif ($total_class == '0') {
                                                          echo'
                                                          <tbody>
                                                              <tr>
                                                                <td><strong>'.$row["name"].'</strong></td>
                                                                <td>'.$row4['studentname'].'</td>
                                                                <td>'.$row['age'].'</td>
                                                                <td>'.$row['language_name'].'</td>
                                                                <td>'.$row['level_name'].'</td>
                                                                <td style="color:#787878"><strong>No KPI Class</strong></td>
                                                                <td>'.$total_class.'</td>
                                                                <td>'.$row2['total'].'</td> 
                                                          ';
                                                        }
                                                        elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name'])) {
                                                          echo '
                                                          <tbody>
                                                              <tr>
                                                                <td><strong>'.$row["name"].'</strong></td>
                                                                <td>'.$row4['studentname'].'</td>
                                                                <td>'.$row['age'].'</td>
                                                                <td>'.$row['language_name'].'</td>
                                                                <td>'.$row['level_name'].'</td>
                                                                <td style="color:#F43939"><strong>FAIL</strong></td>
                                                                <td>'.$total_class.'</td>
                                                                <td>'.$row2['total'].'</td> 
                                                          ';
                                                        }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name'])) {
                                                          echo '
                                                          <tbody>
                                                              <tr>
                                                                <td><strong>'.$row["name"].'</strong></td>
                                                                <td>'.$row4['studentname'].'</td>
                                                                <td>'.$row['age'].'</td>
                                                                <td>'.$row['language_name'].'</td>
                                                                <td>'.$row['level_name'].'</td>
                                                                <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
                                                                <td>'.$total_class.'</td>
                                                                <td>'.$row2['total'].'</td> 
                                                          ';
                                                        }
                                                      }    
                                                  }
                                            }          
                                        }
                                      }
                                  }

                                      echo '
                                      </tbody>
                                      </tr>
                                      ';
                                 }
                                 
                                }else{
                                  echo '
                                  No Record';
                                } 
                                ?>
                              </table>
                            </div>
                          </div>
                      </div>
                  </div>  
                </div>
              </div>
            </div>
          </div>

</body>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="js/custom.js"></script>
<script>
$(document).ready(function(){
window.onload=load_data();
 //load_data();

 function load_data(query)
 {
  $('#result').html("Loading Please Wait...........");
  $.ajax({
   url:"fetch_gi_center.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#result').html("Loading Please Wait...........");
 $('#search_lang').change(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
<script>
$(document).ready(function() {
  var documentRoot = "fetch_gi_student.php";
  // $("#result").html("Pls Search");
  $("#search_text_age").change(function() {
       // alert($("#language").val());
       // alert("he");
      $('#result2').html("Loading Please Wait...........");
      var form_data_time = {
              age: $("#search_text_age").val(),
              status: $("#search_text_status").val(),
              functionName: 'age'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response1) {
        // alert (response);
        
        $("#result2").html(response1);

      }});
  });

  $("#search_text_status").change(function() {
       // alert($("#language").val());
       // alert("he");
      $('#result2').html("Loading Please Wait...........");
      var form_data_time = {
              status: $("#search_text_status").val(),
              age: $("#search_text_age").val(),
              functionName: 'status'
          };
          // $("#level").html("loading......");
      $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response2) {
        // alert (response);
        
        $("#result2").html(response2);

      }});
  });
});

</script>
<script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch1.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result1').html(data);
       }
      });
     }
     });

    $("li").click(function(e) {
  e.preventDefault();
  $("li").removeClass("selected");
  $(this).addClass("selected");
});
</script>
</html>
