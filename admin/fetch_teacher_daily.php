<?php
//fetch.php
    session_start();
    if ($_SESSION['fid'] == "") {
        header("Location: index.php");
        
}
$fid = $_SESSION['fid'];
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
 SELECT te.username FROM studentresult sr 
 LEFT JOIN teacher te ON (te.username = sr.teachername) 
 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
 WHERE sr.fid = '$fid' 
 AND s.active = 'Yes'
 AND sr.insert_date = '".$search."' 
 AND (sr.deleted IS NULL OR sr.deleted = '0') 
 AND (sr.trial = '' OR sr.trial = 'No')
 GROUP BY te.username ORDER BY te.username
 ";
}
else
{
$search= date("Y-m-d");
 $query = "
 SELECT te.username FROM studentresult sr 
 LEFT JOIN teacher te ON (te.username = sr.teachername) 
 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
 WHERE sr.fid = '$fid' 
 AND s.active = 'Yes'
 AND sr.insert_date = '".$search."' 
 AND (sr.deleted IS NULL OR sr.deleted = '0') 
 AND (sr.trial = '' OR sr.trial = 'No')
 GROUP BY te.username ORDER BY te.username
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
  <tr>
  <th>Teacher Name</th>
  <th>Subject</th>
  <th>Level</th>
  <th>Time</th>
  <th>Total Class</th>
  </tr>
  </thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $output .= '
  <tbody>
  <tr>
  <td>'.$row["username"].'</td>
  <td>
  ';
  if(isset($_POST["query"]))
  {
   $search = mysqli_real_escape_string($connect, $_POST["query"]);
   $query1 = "
    SELECT la.language_name, le.level_name, la.language_id FROM studentresult sr 
    LEFT JOIN language la ON (sr.language_id = la.language_id) 
    LEFT JOIN level le ON (sr.level_id = le.level_id) 
    LEFT JOIN student st ON (sr.reg_no = st.reg_no)
    WHERE sr.teachername = '".$row["username"]."' 
    AND st.active = 'Yes'
    AND (sr.deleted IS NULL OR sr.deleted = '0') 
    AND (sr.trial = '' OR sr.trial = 'No')
    AND sr.insert_date = '".$search."' 
    GROUP BY la.language_name ORDER BY la.language_id
  "; 
  }
  else
  {
  $search= date("Y-m-d");
  $query1 = "
  SELECT la.language_name, le.level_name, la.language_id FROM studentresult sr 
  LEFT JOIN language la ON (sr.language_id = la.language_id) 
  LEFT JOIN level le ON (sr.level_id = le.level_id)
  LEFT JOIN student st ON (sr.reg_no = st.reg_no)
  WHERE sr.teachername = '".$row["username"]."' 
  AND st.active = 'Yes'
  AND (sr.deleted IS NULL OR sr.deleted = '0')
  AND (sr.trial = '' OR sr.trial = 'No') 
  AND sr.insert_date = '".$search."' 
  GROUP BY la.language_name ORDER BY la.language_id
  ";
  }
    if($result1 = mysqli_query($connect, $query1))
    {
      while($rowcount=mysqli_fetch_array($result1))
      {
        $language_name = $rowcount['language_name'];
        $output .= '
        <a href="teacher_daily_view.php?teachername='.base64_encode($row["username"]).'&date='.base64_encode($search).'&language_id='.base64_encode($rowcount["language_id"]).'">'.$language_name.'</a><br>
        ';
      }
    }
  $output .= '
  </td>
  <td>
  ';
  if(isset($_POST["query"]))
  {
   $search = mysqli_real_escape_string($connect, $_POST["query"]);
   $query1 = "
    SELECT le.level_name, le.level_id, la.language_id, st.studentname, sr.reg_no FROM studentresult sr 
    LEFT JOIN language la ON (sr.language_id = la.language_id) 
    LEFT JOIN level le ON (sr.level_id = le.level_id) 
    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
    WHERE sr.teachername = '".$row["username"]."' 
    AND st.active = 'Yes'
    AND (sr.deleted IS NULL OR sr.deleted = '0')
    AND (sr.trial = '' OR sr.trial = 'No')
    AND sr.insert_date = '".$search."'  
    GROUP BY le.level_id, sr.reg_no ORDER BY sr.r_id 
  ";
  }
  else
  {
  $search= date("Y-m-d");
  $query1 = "
   SELECT le.level_name, le.level_id, la.language_id, st.studentname, sr.reg_no FROM studentresult sr 
   LEFT JOIN language la ON (sr.language_id = la.language_id) 
   LEFT JOIN level le ON (sr.level_id = le.level_id) 
   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
   WHERE sr.teachername = '".$row["username"]."' 
   AND st.active = 'Yes'
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   AND sr.insert_date = '".$search."'  
   GROUP BY le.level_id, sr.reg_no ORDER BY sr.r_id
  ";
  }
  
    if($result1 = mysqli_query($connect, $query1))
    {
      while($rowcount=mysqli_fetch_array($result1))
      {
        $level_name = $rowcount['level_name'];
        $studentname = $rowcount['studentname'];
        $output .= '
        '.$studentname.' - '.$level_name.'<br>
        ';
      }
    }
  $output .= '
  </td>
  <td>
  ';
  if(isset($_POST["query"]))
  {
   $search = mysqli_real_escape_string($connect, $_POST["query"]);
   $query_time = "
    SELECT sr.times1, sr.reg_no, le.level_id FROM studentresult sr 
    LEFT JOIN language la ON (sr.language_id = la.language_id) 
    LEFT JOIN level le ON (sr.level_id = le.level_id) 
    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
    WHERE sr.teachername = '".$row["username"]."' 
    AND st.active = 'Yes'
    AND (sr.deleted IS NULL OR sr.deleted = '0')
    AND (sr.trial = '' OR sr.trial = 'No')
    AND sr.insert_date = '".$search."'
    GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id 
  ";
  }
  else
  {
  $search= date("Y-m-d");
  $query_time = "
   SELECT sr.times1, sr.reg_no, le.level_id FROM studentresult sr 
   LEFT JOIN language la ON (sr.language_id = la.language_id) 
   LEFT JOIN level le ON (sr.level_id = le.level_id) 
   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
   WHERE sr.teachername = '".$row["username"]."' 
   AND st.active = 'Yes'
   AND (sr.deleted IS NULL OR sr.deleted = '0') 
   AND (sr.trial = '' OR sr.trial = 'No')
   AND sr.insert_date = '".$search."'
   GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id 
  ";
  }
  
    if($result_time = mysqli_query($connect, $query_time))
    {
      $mo1 = '0';
      $mo2 = '0';
      $af1 = '0';
      $af2 = '0';
      $af3 = '0';
      $af4 = '0';
      while($rowcount_time=mysqli_fetch_array($result_time))
      {
        $time = $rowcount_time['times1'];
        if ($time == 'Morning1') {
          $mo1 ++;
        }
        if ($time == 'Morning2') {
          $mo2 ++;
        }
        if ($time == 'Afthernoon1') {
          $af1 ++;
        }
        if ($time == 'Afthernoon2') {
          $af2 ++;
        }
        if ($time == 'Afthernoon3') {
          $af3 ++;
        }
        if ($time == 'Afthernoon4') {
          $af4 ++;
        }
      }
      $output .= '
      Morning 1 : '.$mo1.' <br>
      Morning 2 : '.$mo2.'<br>
      Afternoon 1 : '.$af1.'<br>
      Afternoon 2 : '.$af2.'<br>
      Afternoon 3 : '.$af3.'<br>
      Afternoon 4 : '.$af4.'<br>
      ';
    }

    if(isset($_POST["query"]))
    {
     $search = mysqli_real_escape_string($connect, $_POST["query"]);
     $query_class = "
     SELECT max(number_of_class) total_class FROM studentresult sr 
     LEFT JOIN teacher te ON (te.username = sr.teachername) 
     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
     WHERE sr.fid = '".$fid."' 
     AND st.active = 'Yes'
     AND te.username='".$row["username"]."'
     AND sr.insert_date = '".$search."' 
     AND (sr.deleted IS NULL OR sr.deleted = '0') 
     AND (sr.trial = '' OR sr.trial = 'No')
     GROUP BY sr.reg_no";
    }
    else
    {
    $search= date("Y-m-d");
    $query_class = "
     SELECT max(number_of_class) total_class FROM studentresult sr 
     LEFT JOIN teacher te ON (te.username = sr.teachername) 
     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
     WHERE sr.fid = '".$fid."' 
     AND st.active = 'Yes'
     AND te.username='".$row["username"]."'
     AND sr.insert_date = '".$search."' 
     AND (sr.deleted IS NULL OR sr.deleted = '0') 
     AND (sr.trial = '' OR sr.trial = 'No')
     GROUP BY sr.reg_no";
    }
    if($result_class = mysqli_query($connect, $query_class))
    {
      $sum = '0';
      while($row_class=mysqli_fetch_array($result_class))
      {
        $sum += $row_class['total_class'];
      }
      $output .= '
      </td>
      <td>'.$sum.'</td>
      </tr>
      </tbody>
      ';
    }
 }
 echo $output;
}
else
{
 echo 'No Data From Today';
}

?>