<?php 
    require_once("dbConfig.php");
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    $fid = $_COOKIE['fid'];
    $position = $_COOKIE['position'];
    $notifi_class = new Noti();
    $view_notifi = $notifi_class->notification($fid,$position);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>
<style type="text/css">
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #808080;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
</style>
<body class="animsition">
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled" style="margin-left: 50%;">
                            <li>
                                <a href="student_list.php">
                                    <i class="fa fa-id-badge"></i>
                                    Registered student</a>
                            </li>
                        </ul>
                    </div>

                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="profile/dashboard_profile.php">
                                            <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>

            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a href="student_list.php">
                            <i class="fa fa-id-badge"></i>
                            Registered student</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">

                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        
                        <div class="image">
                            <img src="Images/Logo/YelaoShrBaby.png">
                        </div>
                        <div class="content">
                            <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="profile/dashboard_profile.php">
                                            <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                <a href="#" onclick="logout()">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-lg-12">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40 au-card--border">
                                    <div class="au-card-title">
                                        <div class="bg-overlay bg-overlay" style="background-color:  #ffcc66;">
                                            <img src="Images/gif/giphy.gif" width="100" height="100" style="margin-left: 190px; margin-top: 1px;">
                                        </div>
                                        <h3><i class="zmdi zmdi-notifications"></i>Notification</h3>
                                    </div>
                                    <div class="au-task js-list-load au-task--border">
                                        <div class="au-task-list js-scrollbar3">
                                            <?php echo $view_notifi; ?>
                                        </div>
                                        <div class="au-task__footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->
            <center>
                <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
            </center>
            <!-- DATA TABLE-->
            <section class="p-t-20" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script type="text/javascript">
    function logout() {
        // document.getElementById("myDiv").style.display = "none";
            var result = confirm('Are you sure to LOGOUT?');
            if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }
            
    }   

    </script>
</body>

</html>
<!-- end document-->
