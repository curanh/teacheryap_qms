<?php
//fetch.php
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");
        
}
$fid = $_COOKIE['fid'];
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM student 
  WHERE fid = '$fid' AND active = 'Yes' AND studentname LIKE '%".$search."%' ORDER BY studentname
 ";
}
else
{
 $query = "
  SELECT * FROM student WHERE fid = '$fid' AND active = 'Yes' ORDER BY studentname
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
<thead>
    <tr>
        <th>Studentname</th>
        <th>Last update</th>
    </tr>
</thead>
 ';
 while($row = mysqli_fetch_array($result))
 {
    $reg_no = $row["reg_no"];
    $studentname = $row["studentname"];
    $studentname = trim($studentname, " ");
    $studentname = rtrim($studentname, " ");
	
    $query_sr = "
    SELECT * FROM studentresult WHERE reg_no = '".$reg_no."' AND (deleted IS NULL OR deleted = 0) ORDER BY date1 DESC LIMIT 1
    ";

    $result_sr = mysqli_query($connect, $query_sr);
    $row_sr = mysqli_num_rows($result_sr);

    if(mysqli_num_rows($result_sr) > 0){
        while($row_sr = mysqli_fetch_array($result_sr))
        {
        $insert_date = $row_sr["date1"];
        $new_date = date("d/m/Y",strtotime($insert_date));
        $output .= '
          <tbody>
              <tr class="tr-shadow">
                  <td>'.$row["studentname"].' ('.$row["reg_no"].')</td>
                  <td>'.$new_date.'</td>
                  <td>
                      <div class="table-data-feature">
                          <a href="progress.php?id='.$row["reg_no"].'&student_name='.urlencode($studentname).'&trial=No"  class="item" data-toggle="tooltip" data-placement="top" title="输入进度表">
                              <i class="zmdi zmdi-edit"></i>
                          </a>
                      </div>
                  </td>
              </tr>
          </tbody>
          ';
        }
      }
      else{
        $output .= '
        <tbody>
            <tr class="tr-shadow">
                <td>'.$row["studentname"].' ('.$row["reg_no"].')</td>
                <td></td>
                <td>
                    <div class="table-data-feature">
                        <a href="progress.php?id='.$row["reg_no"].'&student_name='.urlencode($studentname).'&trial=No"  class="item" data-toggle="tooltip" data-placement="top" title="输入进度表">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                </td>
            </tr>
        </tbody>
        ';
      }
 }
 echo $output;



}
else
{
 echo 'Data Not Found';
}

?>