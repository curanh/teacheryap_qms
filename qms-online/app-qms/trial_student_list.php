<?php 
    require_once("dbConfig.php");
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    $records_class = new Records();
 ?>
 <?php
    if (isset($_POST['button'])){
        if(isset($_POST['Studentname']) && isset($_POST['Age']) && isset($_POST['ic']) && isset($_POST['Gender'])){
            if(($_POST['Studentname'] !="") && ($_POST['Age']!="") && ($_POST['ic']!="") && ($_POST['Gender']!="")){

                $name = $_POST['Studentname'];
                $age = $_POST['Age'];
                $ic = $_POST['ic'];
                $gender = $_POST['Gender'];
                $fid = $_COOKIE['fid'];
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $date = date('Y-m-d H:i:s');

                $sql= "SELECT * FROM trial_student WHERE ic_number='$ic' LIMIT 1";
                $result = mysqli_query($connect, $sql);
                $user = mysqli_fetch_assoc($result);
                
                $sql_lms= "SELECT * FROM new_student WHERE ic='$ic' LIMIT 1";
                $result_lms = mysqli_query($connect1, $sql_lms);
                $user_lms = mysqli_fetch_assoc($result_lms);
                $qms = '0';
                $lms = '0';
                if ($user['ic_number'] != $ic AND $user_lms['ic'] != $ic) {

                    $sql1="INSERT INTO trial_student (studentname, age, gender, ic_number, fid, create_date, active)
                      VALUES('$name','$age', '$gender', '$ic','$fid','$date', 'Yes')";

                    if (!mysqli_query($connect,$sql1))
                    {
                        die('Error: ' . mysqli_error($connect));
                    }

                    $sql2="insert into new_student(fid, name, ic, age, gender, date)
                    values('".$fid."', '".$name."', '".$ic."', '".$age."', '".$gender."', '".$date."')";

                    if (!mysqli_query($connect1,$sql2))
                    {
                        die('Error: ' . mysqli_error($connect1));
                    }

                    $lms = '1';
                    $qms = '1';

                }elseif($user['ic_number'] == $ic AND $user_lms['ic'] != $ic){

                    $sql2="insert into new_student(fid, name, ic, age, gender, date)
                    values('".$fid."', '".$name."', '".$ic."', '".$age."', '".$gender."', '".$date."')";

                    if (!mysqli_query($connect1,$sql2))
                    {
                        die('Error: ' . mysqli_error($connect1));
                    }
                    $lms = '1';

                }elseif($user['ic_number'] != $ic AND $user_lms['ic'] == $ic){
                    
                    $sql1="INSERT INTO trial_student (studentname, age, gender, ic_number, fid, create_date, active)
                      VALUES('$name','$age', '$gender', '$ic','$fid','$date', 'Yes')";

                    if (!mysqli_query($connect,$sql1))
                    {
                        die('Error: ' . mysqli_error($connect));
                    }
                    $qms = '1';

                }elseif ($user['ic_number'] == $ic AND $user_lms['ic'] == $ic) {
                    echo "<script>alert('IC NO already exists in QMS / LMS trial ! Please try again! ');
                    window.location.href='trial_student_list.php';
                    </script>";
                    exit();
                }
                
                if ($qms == '1' AND $lms == '1') {
                    echo "<script>alert('Successful create trial student in QMS / LMS')</script>";
                }elseif ($qms == '1' AND $lms != '1') {
                    echo "<script>alert('IC NO already exists in LMS , Successful create trial student in QMS')</script>";
                }elseif ($qms != '1' AND $lms == '1') {
                    echo "<script>alert('IC NO already exists in QMS , Successful create trial student in LMS')</script>";
                }

                
                // echo "<script> window.location.assign('index.php'); </script>";
            
            }else{
            exit;

            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>
<style type="text/css">
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #808080;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
</style>
<body class="animsition">
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled" style="margin-left: 60%;">
                            <li>
                                <a href="student_list.php">
                                    <i class="fa fa-id-badge"></i>
                                    Registered student</a>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>

            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a href="student_list.php">
                            <i class="fa fa-id-badge"></i>
                            Registered student</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src="Images/Logo/YelaoShrBaby.png">
                        </div>
                        <div class="content">
                            <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="#" onclick="logout()">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome to <b><u>Trial Student</u></b>
                                <span>!</span>
                            </h1>
                            <br>
                            <button type="button" name="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#largeModal">
                            <i class="fa fa-angle-double-down"></i>&nbsp;My Inserted Records</button>
                            <button type="button" name="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#largeModal1">
                            <i class="fa fa-angle-double-down"></i>&nbsp;Create Student</button>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->
            <center>
                <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
            </center>
            <!-- DATA TABLE-->
            <section class="p-t-20" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Student Data</h3>
                            <input class="au-input--w300 au-input--style2" type="text" name="search_text" id="search_text" placeholder="Search by studentname">
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2" id="result">
                                </table>
                            </div>
                            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">My Inserted Records</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                              <label for="date-input" class=" form-control-label">Date :</label>
                                              <input class="au-input--w300 au-input--style2" type="date" name="search_date" id="search_date" placeholder="Search by date">
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Time :</label>
                                            <select style="padding: 12px;" name="search_time" id="search_time" class="au-input--w300 au-input--style2">
                                              <option value="all" selected>All</option>
                                              <option value="Morning1">Morning 1</option>
                                              <option value="Morning2">Morning 2</option>
                                              <option value="Afthernoon1">Afternoon 1</option>
                                              <option value="Afthernoon2">Afternoon 2</option>
                                              <option value="Afthernoon3">Afternoon 3</option>
                                              <option value="Afthernoon4">Afternoon 4</option>
                                            </select>
                                          </div>
                                        </div>
                                        <br>
                                        <div id="result1">
                                        <?php
                                        echo $records_class->trial_my_insert_records(); 
                                        ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
            <!--Create Student -->
            <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="largeModalLabel">Create Student</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <div class="card">
                                <div class="card-body card-block">
                                    <form class="form-horizontal" method="POST">
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                            <div class="form-group">
                                                <label for="Studentname" class="control-label mb-1">Student_name</label>
                                                <input id="Studentname" name="Studentname" type="text" class="form-control" aria-required="true" aria-invalid="false" required="">
                                            </div>
                                            </div>
                                            <div class="col col-md-12">
                                            <div class="form-group">
                                                <label for="Age" class="control-label mb-1">Age</label>
                                                <input id="Age" name="Age" type="number" class="form-control" aria-required="true" aria-invalid="false" required="">
                                            </div>
                                            </div>
                                            <div class="col col-md-12">
                                                <label for="ic" class="control-label mb-1">IC No</label>
                                                <div class="input-group">
                                                    <input id="ic" name="ic" placeholder="IC No" class="form-control" required="" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        type = "number"
                                                        maxlength = "12">
                                                    <div class="input-group-btn">
                                                    </div>
                                                </div>
                                                <p style="color: red;">* xxxxxxxxxxxx (No need '-')</p>
                                                <br>
                                                <select style="padding: 12px;" name="Gender" id="Gender" class="au-input--w300 au-input--style2" required="">
                                                  <option value="" selected>Select Gender</option>  
                                                  <option value="Male">Male</option>
                                                  <option value="Female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-success btn-sm" name="button">
                                        Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script>
    $(document).ready(function(){

     load_data();

     function load_data(query)
     {
      $.ajax({
       url:"fetch_trial.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#result').html(data);
       }
      });
     }
     $('#search_text').keyup(function(){
      var search = $(this).val();
      if(search != '')
      {
       load_data(search);
      }
      else
      {
       load_data();
      }
     });
    });


    $(document).ready(function() {
        
        var documentRoot = "fetch_trial_record.php";

        $("#search_date").change(function() {
             // alert($("#language").val());

            var form_data = {
                    date: $("#search_date").val(),
                    time: $("#search_time").val(),
                    functionName: 'click_date'
                };
                // $("#level").html("loading......");
            $.ajax({type: "POST", url: documentRoot , data: form_data, success: function(response) {
              // alert (response);
              
              $("#result1").html(response);

            }});
        });


        $("#search_time").change(function() {
             // alert($("#language").val());

            var form_data_time = {
                    time: $("#search_time").val(),
                    date: $("#search_date").val(),
                    functionName: 'click_time'
                };
                // $("#level").html("loading......");
            $.ajax({type: "POST", url: documentRoot , data: form_data_time, success: function(response1) {
              // alert (response);
              
              $("#result1").html(response1);

            }});
        });
      });
    </script>
    <script type="text/javascript">
    function logout() {
            var result = confirm('Are you sure to LOGOUT?');
            if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }
            
        }    
    var date = new Date();
    var day = date.getDate(),
    month = date.getMonth() + 1,
    year = date.getFullYear(),
    hour = date.getHours(),
    min  = date.getMinutes();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;

    var today = year + "-" + month + "-" + day;
    document.getElementById('search_date').value = today;
    </script>
    <script>
    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
      } else {
        mybutton.style.display = "none";
      }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
    </script>

</body>

</html>
<!-- end document-->
