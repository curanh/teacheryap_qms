<?php 
    require_once("dbConfig.php");
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }

    if(isset($_GET['reg_no'])){
      $fid = $_COOKIE['fid'];
      $reg_no = $_GET['reg_no'];
      $student_name = $_GET['student_name'];
      // echo $student_name; exit();
      $level = $_GET['level'];
      $language = $_GET['language'];
      $teacher = $_GET['teacher'];
      $active = $_GET['active'];
    }
    if (isset($_POST['button'])){

        if(empty($_POST['inline-radios'])){
            $errorMsg = '请选择阅读字卡速度在第1页 Please Select Flash Card Reading Speed in page 1';
        }
        elseif(empty($_POST['inline-radios1'])){
            $errorMsg = '请选择阅读课文速度在第1页 Please Select Text Reading in page 1';
        }
        elseif(empty($_POST['inline-radios2'])){
            $errorMsg = '请选择完成功课的速度在第1页 Please Select Completing Workbook Speed in page 1';
        }
        elseif(empty($_POST['inline-radios3'])){
            $errorMsg = '请选择吸收能力在第1页 Please Select Absorptive Capability in page 1';
        }
        elseif(empty($_POST['inline-radios4'])){
            $errorMsg = '请选择专注力在第1页 Please Select Concerntration in page 1';
        }

      if(!isset($errorMsg)){
        // $m_tongue= $_POST['m_tongue'];
        // $others = $_POST['others'];
        $radios = $_POST['inline-radios'];
        $radios1 = $_POST['inline-radios1'];
        $radios2 = $_POST['inline-radios2'];
        $radios3 = $_POST['inline-radios3'];
        $radios4 = $_POST['inline-radios4'];
        $assess = '记忆力评估';
        $assess1 = '阅读课文速度';
        $assess2 = '完成功课的速度';
        $assess3 = '吸收能力';
        $assess4 = '专注力';
        

        $sql="INSERT INTO assess (fid, assess1, assess_score1, assess2, assess_score2, assess3, assess_score3, assess4, assess_score4, assess5, assess_score5, teacher, reg_no, language_id, level_id)VALUES('".$fid."', '".$assess."', '".$radios."' , '".$assess1."', '".$radios1."', '".$assess2."', '".$radios2."', '".$assess3."', '".$radios3."', '".$assess4."', '".$radios4."', '".$teacher."', '".$reg_no."', '".$language."', '".$level."')";
        if (!mysqli_query($connect,$sql))
        {
            die('Error: ' . mysqli_error($connect));
        }
        else{
          $last_id = $connect->insert_id;
          if (isset($_POST['checkbox'])) 
          {
            // print_r($_POST['checkbox']);
            $checkbox_array = $_POST['checkbox'];
            
            // print_r($checkbox_array);
            $query1= "SELECT * FROM personality_criteria";
            $result1 = mysqli_query($connect, $query1);
            $row1 = mysqli_num_rows($result1);

            if(mysqli_num_rows($result1) > 0){
              while($row1 = mysqli_fetch_array($result1)){
                if (in_array($row1['id'],$checkbox_array)){

                    $id = $row1['id'];
                    $zh_title = $row1['zh_title'];
                    $en_title = $row1['en_title'];

                    $query_insert = "INSERT INTO personality_answer (`assess_id`, `personality_id`, `zh_title`, `en_title`, `input_value`) VALUES ('".$last_id."', '".$id."', '".$zh_title."', '".$en_title."', '')";
                      $sql_result3 = mysqli_query($connect,$query_insert);
                      
                        echo "<script>alert('Successful!')</script>";
                        if (isset($active)) {
                          echo '<script type="text/javascript">location.href = "profile/student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level).'&studentname='.base64_encode($student_name).'&active='.base64_encode($active).'";</script>';
                        }else{
                          echo '<script type="text/javascript">location.href = "progress.php?id='.$reg_no.'&student_name='.urlencode($student_name).'&trial=No";</script>';
                        }
                }
              }
            }
          }
        }
      }
    }
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
    <!-- <link href="css/accordion.css" rel="stylesheet" media="all"> -->

</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h1 style="color: #ffffff;">QMS</h1>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->
        <center>
          <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
        </center>
        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h1 style="color: #ffffff;">QMS</h1>
                            <p>Online version</p>
                        </a>
                        <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
        </div>
        <!-- END HEADER MOBILE -->
        <center>
          <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
        </center>
        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7" id="myDiv">
            <!--FORM-->
            <section class="p-t-20">
              <div class="container">
              <form method="post" class="form-horizontal">
              <div class="card">
                  <div class="card-header">
                    <h4>学生能力评估 Students Ability Assessment</h4>
                  </div>
                  <?php
                  if(isset($errorMsg)){       
                  ?>
                  <div class="alert alert-danger" role="alert">
                      <center><strong><?php echo $errorMsg; ?></strong></center>
                  </div>
                  <?php
                  }
                  ?>
                  <div class="card-body">
                    <div class="tab-content pl-3 p-1" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card-body card-block">
                          <div style="
                          display: flex;
                          justify-content: space-between;
                          width: 100%;">
                            <h4>Ability Assessment</h4>
                            <?php
                            if (isset($active)) {
                              echo '<a href="profile/student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level).'&studentname='.base64_encode($student_name).'&active='.base64_encode($active).'"><button class="btn btn-info">Back</button></a>';   
                            }else{
                              echo '<a href="progress.php?id=<?php echo $reg_no ?>&student_name=<?php echo $student_name ?>&trial=No"><button class="btn btn-info">Back</button></a>';
                            }
                            ?>
                          </div>
                          
                          <br>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label class=" form-control-label">Student :</label>
                              </div>
                              <div class="col-12 col-md-9">  
                              <input class="input100" type="text" id="student_name" name="student_name" placeholder="student_name" value="<?php echo $student_name ?>" READONLY>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label class=" form-control-label">Teacher :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <!-- <input type="text" id="student_name" name="student_name" placeholder="Text" class="form-control" value="" READONLY> -->
                                  <input class="input100" type="text" id="student_name" name="student_name" placeholder="student_name" value="<?php echo $_COOKIE["name"];?>" READONLY>
                              </div>
                          </div>
                          <!-- <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><span style="color: red"></span>母语 Mother Tongue :</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <select id="m_tongue" name="m_tongue" class="form-control">
                            <option value="" selected>请选择母语 Please Select Mother Tongue</option>
                            <option value="华 Chinese">华 Chinese</option>
                            <option value="英 English">英 English</option>
                            <option value="巫 Malay">巫 Malay</option>
                            <option value="方言 Dialect">方言 Dialect</option>
                            </select>
                            </div>
                          </div> -->
                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">记忆力评估<br>Memory assessment</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio" class="form-check-label ">
                                        <input type="radio" id="inline-radio" name="inline-radios" value="1" class="form-check-input">
                                        1分(需要每天不断复习，重复很多次)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio1" class="form-check-label ">
                                        <input type="radio" id="inline-radio1" name="inline-radios" value="2" class="form-check-input">
                                        2分(需要重复2-3次，过后比较可以记得)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio2" class="form-check-label ">
                                        <input type="radio" id="inline-radio2" name="inline-radios" value="3" class="form-check-input">
                                        3分(需要重复2-3次)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio3" class="form-check-label ">
                                        <input type="radio" id="inline-radio3" name="inline-radios" value="4" class="form-check-input">
                                        4分(重复1-2次就可以记得很稳定)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio4" class="form-check-label ">
                                        <input type="radio" id="inline-radio4" name="inline-radios" value="5" class="form-check-input">
                                        5分(说一次就记得，非常厉害)
                                    </label>
                                </div>
                            </div>
                          </div>

                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">阅读课文速度<br>Text Reading</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio5" class="form-check-label ">
                                        <input type="radio" id="inline-radio5" name="inline-radios1" value="1" class="form-check-input">
                                        1分(非常慢，不能跟着进度完成)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio6" class="form-check-label ">
                                        <input type="radio" id="inline-radio6" name="inline-radios1" value="2" class="form-check-input">
                                        2分(比较慢，需要花比较多的时间才能完成)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio7" class="form-check-label ">
                                        <input type="radio" id="inline-radio7" name="inline-radios1" value="3" class="form-check-input">
                                        3分(速度中等，当天可以完成进度)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio8" class="form-check-label ">
                                        <input type="radio" id="inline-radio8" name="inline-radios1" value="4" class="form-check-input">
                                        4分(速度快，当天完成进度，不必纠正)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio9" class="form-check-label ">
                                        <input type="radio" id="inline-radio9" name="inline-radios1" value="5" class="form-check-input">
                                        5分(非常快，可以完成多格进度)
                                    </label>
                                </div>
                            </div>
                          </div>

                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">完成功课的速度<br>Completing Workbook Speed</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio10" class="form-check-label ">
                                        <input type="radio" id="inline-radio10" name="inline-radios2" value="1" class="form-check-input">
                                        1分(写字非常缓慢，不能理解习题或者发音)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio11" class="form-check-label ">
                                        <input type="radio" id="inline-radio11" name="inline-radios2" value="2" class="form-check-input">
                                        2分(写字比较慢，对于题目/Ejaan需要重复讲解几次才可以完成)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio12" class="form-check-label ">
                                        <input type="radio" id="inline-radio12" name="inline-radios2" value="3" class="form-check-input">
                                        3分(速度中等，偶尔督促，大部分可以自行完成)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio13" class="form-check-label ">
                                        <input type="radio" id="inline-radio13" name="inline-radios2" value="4" class="form-check-input">
                                        4分(速度快，可以独立完成，大部分都写对答案)
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio14" class="form-check-label ">
                                        <input type="radio" id="inline-radio14" name="inline-radios2" value="5" class="form-check-input">
                                        5分(速度非常快，一次过可以完成非常多习题/Ejaan，大部分都对)
                                    </label>
                                </div>
                            </div>
                          </div>

                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">吸收能力<br>Absorptive Capability<br>(字卡/PKTT背字&Bab理解/口诀)</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio15" class="form-check-label ">
                                        <input type="radio" id="inline-radio15" name="inline-radios3" value="1" class="form-check-input">
                                        1分（不能在当天吸收完设定好的进度，时常需要暂停进度为孩子不断复习）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio16" class="form-check-label ">
                                        <input type="radio" id="inline-radio16" name="inline-radios3" value="2" class="form-check-input">
                                        2分（不能在当天吸收完设定好的进度，需要用1-3天来复习）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio17" class="form-check-label ">
                                        <input type="radio" id="inline-radio17" name="inline-radios3" value="3" class="form-check-input">
                                        3分（能在当天吸收完设定好的进度，但还是需要为孩子做复习）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio18" class="form-check-label ">
                                        <input type="radio" id="inline-radio18" name="inline-radios3" value="4" class="form-check-input">
                                        4分（能完成当天的进度，复习速度页很快和稳定）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio19" class="form-check-label ">
                                        <input type="radio" id="inline-radio19" name="inline-radios3" value="5" class="form-check-input">
                                        5分（能完成当天的进度，进度可以走很快，复习速度非常快和稳定)
                                    </label>
                                </div>
                            </div>
                          </div>

                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">专注力<br>Concerntration</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio20" class="form-check-label ">
                                        <input type="radio" id="inline-radio20" name="inline-radios4" value="1" class="form-check-input">
                                        1分（上课时间不能超过5分钟，非常走神，需要进行多轮教学）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio21" class="form-check-label ">
                                        <input type="radio" id="inline-radio21" name="inline-radios4" value="2" class="form-check-input">
                                        2分（容易被环境干扰，需要拉回注意力）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio22" class="form-check-label ">
                                        <input type="radio" id="inline-radio22" name="inline-radios4" value="3" class="form-check-input">
                                        3分（能专注上课，偶尔需要拉回专注力而已）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio23" class="form-check-label ">
                                        <input type="radio" id="inline-radio23" name="inline-radios4" value="4" class="form-check-input">
                                        4分（能专注上课，进度顺畅和快）
                                    </label>
                                </div>
                                <br>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio24" class="form-check-label ">
                                        <input type="radio" id="inline-radio24" name="inline-radios4" value="5" class="form-check-input">
                                        5分（能专注上课，进度快而稳，能在短时间内完成进度。)
                                    </label>
                                </div>
                            </div>
                          </div>
                        <div class="row form-group">
                          <div class="card-body card-block">
                          <h4>Students Personality</h4>
                          </div>
                        </div>
                          <div class="col col-md-9">
                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox1" class="form-check-label ">
                                          <input type="checkbox" id="checkbox1" name="checkbox[]" value="1" class="form-check-input">温和善良 Kind
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox2" class="form-check-label ">
                                          <input type="checkbox" id="checkbox2" name="checkbox[]" value="2" class="form-check-input">善解人意 Understanding
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox3" class="form-check-label ">
                                          <input type="checkbox" id="checkbox3" name="checkbox[]" value="3" class="form-check-input">懒洋洋 Lazy
                                      </label>
                                  </div>
                              </div>
                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox4" class="form-check-label ">
                                          <input type="checkbox" id="checkbox4" name="checkbox[]" value="4" class="form-check-input">行动缓慢 Slow
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox5" class="form-check-label ">
                                          <input type="checkbox" id="checkbox5" name="checkbox[]" value="5" class="form-check-input">有规律 Consistentding
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox6" class="form-check-label ">
                                          <input type="checkbox" id="checkbox6" name="checkbox[]" value="6" class="form-check-input">随和 Easy going
                                      </label>
                                  </div>
                              </div>
                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox7" class="form-check-label ">
                                          <input type="checkbox" id="checkbox7" name="checkbo[]" value="7" class="form-check-input">不可捉摸 Unpredictable
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox8" class="form-check-label ">
                                          <input type="checkbox" id="checkbox8" name="checkbo[]" value="8" class="form-check-input">精力充沛 Energetic
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox9" class="form-check-label ">
                                          <input type="checkbox" id="checkbox9" name="checkbo[]" value="9" class="form-check-input">好奇 Curious
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox10" class="form-check-label ">
                                          <input type="checkbox" id="checkbox10" name="checkbox[]" value="10" class="form-check-input">开心快乐 Happy
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox11" class="form-check-label ">
                                          <input type="checkbox" id="checkbox11" name="checkbox[]" value="11" class="form-check-input">害羞内向 Shy & Introvert
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox12" class="form-check-label ">
                                          <input type="checkbox" id="checkbox12" name="checkbox[]" value="12" class="form-check-input">愁眉苦脸 Sad
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox13" class="form-check-label ">
                                          <input type="checkbox" id="checkbox13" name="checkbox[]" value="13" class="form-check-input">创意 Creative
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox14" class="form-check-label ">
                                          <input type="checkbox" id="checkbox14" name="checkbox[]" value="14" class="form-check-input">专注 Focus
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox15" class="form-check-label ">
                                          <input type="checkbox" id="checkbox15" name="checkbox[]" value="15" class="form-check-input">固执 Stubborn
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox16" class="form-check-label ">
                                          <input type="checkbox" id="checkbox16" name="checkbox[]" value="16" class="form-check-input">东张西望 Easy to get distracted
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox17" class="form-check-label ">
                                          <input type="checkbox" id="checkbox17" name="checkbox[]" value="17" class="form-check-input">天生不能正确发音，不勉强, 多鼓励 Unable to pronounce accurately, do not force, needs more encouragement
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox18" class="form-check-label ">
                                          <input type="checkbox" id="checkbox18" name="checkbox[]" value="18" class="form-check-input">不合作，态度随便，需严肃对待 Non-cooperative, casual attitude, requires strict treatment
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox19" class="form-check-label ">
                                          <input type="checkbox" id="checkbox19" name="checkbox[]" value="19" class="form-check-input">不能集中精神读书，需要安静的环境 Unable to focus, requires a quiet environment
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox20" class="form-check-label ">
                                          <input type="checkbox" id="checkbox20" name="checkbox[]" value="20" class="form-check-input">资质高，教学进度可以快 Talented, teaching progression can be step up
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox21" class="form-check-label ">
                                          <input type="checkbox" id="checkbox21" name="checkbox[]" value="21" class="form-check-input">不能安静坐下读书，需分三次读 Unable to focus/sit quietly, needs to reading multiple times
                                      </label>
                                  </div>

                                  <div class="checkbox">
                                      <label for="checkbox32" class="form-check-label ">
                                          <input type="checkbox" id="checkbox32" name="checkbox[]" value="22" class="form-check-input">会比较好动和吵闹，需严肃对待 Active & noisy, requires strict treatment
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox22" class="form-check-label ">
                                          <input type="checkbox" id="checkbox22" name="checkbox[]" value="23" class="form-check-input">没有自信心，需多鼓励 Lack of self confidence, requires more encouragement
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox23" class="form-check-label ">
                                          <input type="checkbox" id="checkbox23" name="checkbox[]" value="24" class="form-check-input">孩子情绪敏感，老师要多留意语气 Sensitive & emotional, requires gentle approach
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox24" class="form-check-label ">
                                          <input type="checkbox" id="checkbox24" name="checkbox[]" value="25" class="form-check-input">记忆力很弱，需多重复和多复习，教学进度需要慢下来 Weak memory, requires repetitive & revision, teaching progression needs to be slow down
                                      </label>
                                  </div>
                              </div>

                              <div class="form-check">
                                  <div class="checkbox">
                                      <label for="checkbox25" class="form-check-label ">
                                          <input type="checkbox" id="checkbox25" name="checkbox[]" value="26" class="form-check-input">非常安静，不敢表达，说话和读书很小声，怕生 Very quiet, afraid to express ownself, very solf-spoken, shy
                                      </label>
                                  </div>
                                  <div class="checkbox">
                                      <label for="checkbox26" class="form-check-label ">
                                          <input type="checkbox" id="checkbox26" name="checkbox[]" value="27" class="form-check-input">家长有交代孩子不可被严厉喝骂，因为有阴影 Parents mentioned specifically that child must not be scolded harshly due to previous traumatic
                                      </label>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      </div> -->
                      <!-- <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="card-body card-block">
                          <h4>Special Remarks</h4>
                        </div>
                        <div class="col col-md-9">
                          <div class="form-check">
                                <div class="checkbox">
                                    <label for="checkbox27" class="form-check-label ">
                                        <input type="checkbox" id="checkbox27" name="checkbox[]" value="28" class="form-check-input">学习/听觉/发音/行为障碍 Learning/Hearing/Pronounciation/Behavior difficulties
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox28" class="form-check-label ">
                                        <input type="checkbox" id="checkbox28" name="checkbox[]" value="29" class="form-check-input">自闭症 Autism
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox29" class="form-check-label ">
                                        <input type="checkbox" id="checkbox29" name="checkbox[]" value="30" class="form-check-input">
                                        英文背景 English educated
                                    </label>
                                </div>
                            </div>

                            <div class="form-check">
                                <div class="checkbox">
                                    <label for="31" class="form-check-label ">
                                        <input type="checkbox" id="30" name="checkbox[]" value="31" class="form-check-input">
                                        唐氏儿症 Down syndrome
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="32" class="form-check-label ">
                                        <input type="checkbox" id="31" name="checkbox[]" value="32" class="form-check-input">
                                        过动儿 Hyperactive (ADHD/ADD)
                                    </label>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="textarea-input" class=" form-control-label">其他 Others :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <textarea name="others" id="others" rows="5" placeholder="Others...." class="form-control"></textarea>
                              </div>
                          </div>
                        </div>
                        
                      </div>
 -->                    </div>
                  </div>
                  <div class="card-footer">
                    <button class="btn btn-success" type="submit" name="button">Save</button>
                  </div>
              </div>
              </form>
            </div>
            </section>
            <!-- END FORM-->
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script type="text/javascript">
    function logout() {
      var result = confirm('Are you sure to LOGOUT?');
        if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }   
    }

    </script>
    </body>
    </html>