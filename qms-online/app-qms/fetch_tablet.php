<?php
//fetch.php
  error_reporting (E_ALL ^ E_NOTICE);
  if ($_COOKIE['fid'] == "") {
        header("Location: login.php");
        
  }
  if(isset($_GET['reg_no']))
  {
    $reg_no = $_GET['reg_no'];
    $student_name = $_GET['student_name'];
    $student_name = trim($student_name, " ");
    $student_name = rtrim($student_name, " ");
    $trial1 = $_GET['trial'];
    //$trial = $_GET['trial'];
    //echo $Level_id;
    $student_name = urlencode($student_name);
    // echo $student_name; exit();
  }
$fid = $_COOKIE['fid'];
$position = $_COOKIE['position'];
// echo $position; exit();
require_once("dbConfig.php");

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
  $query = "
  SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr LEFT JOIN language lang ON (sr.language_id = lang.language_id) LEFT JOIN level le ON (sr.level_id = le.level_id) LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) WHERE sr.reg_no='".$reg_no."' AND (sr.deleted IS NULL OR sr.deleted = 0) AND sr.language_id LIKE '%".$search."%' ORDER BY sr.insert_date DESC, sr.r_id DESC
  ";
}

else
{
 $query = "
 SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr LEFT JOIN language lang ON (sr.language_id = lang.language_id) LEFT JOIN level le ON (sr.level_id = le.level_id) LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) WHERE sr.reg_no='".$reg_no."' AND (sr.deleted IS NULL OR sr.deleted = 0) ORDER BY sr.insert_date DESC, sr.r_id DESC
 ";
}

$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <thead>
    <tr class="table100-head">
      <th>Date</th>
      <th>Level</th>
      <th>Status</th>
      <th>Class</th>
    </tr>
  </thead>
 ';

 while($row = mysqli_fetch_array($result))
 {
  //echo $level_id;
  date_default_timezone_set("Asia/Kuala_Lumpur");
  $todaydate = date("Y-m-d");
  $title = $row["title_id"];
  $c_id = $row["continue_id"];
  $insert_date = $row["insert_date"];
  $record_trial = $row["record_trial"];
  $new_date = date("d/m/Y",strtotime($insert_date));
  if ($row["student_status"] == 'Completed' || $row["student_status"] == 'Completed & Not Stable') {
  $output .='
    <tbody>
    <tr>
      <td><strong>'.$new_date.'</strong></td>
      <td>'.$row["level_name"].'</td>
      <td style="color:limegreen;">'.$row["student_status"].'</td>
      <td colspan="4">'.$row["number_of_class"].'</td>
    </tr>
    <tr>
    <td colspan="2">Title :
    ';
    
    if ($row['method'] == 'Yes') {
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$title.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
    }else{
      $output .='
        '.$row["title_id"].'
      ';  
    }
    
  $output .='
    </td>
    <td colspan="4">Feedback : '.$row["feedback"].'</td>
    </tr>
    <tr>
      <td colspan="5">Teacher : '.$row["teachername"].'</td> 
      <td>
      ';
      if ($position == 'teacher' AND $insert_date == $todaydate AND strtolower($row["teachername"]) == strtolower($_COOKIE["name"])) {
        $output .= '
        <div class="table-data-feature">
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
            <i class="zmdi zmdi-delete"></i>
          </button>
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      if ($position == 'buddy') {

        $output .= '
        <div class="table-data-feature">
        ';
        if ( $insert_date == $todaydate AND strtolower($row["teachername"]) == strtolower($_COOKIE["name"])) {
          $output .= '
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
              <i class="zmdi zmdi-delete"></i>
            </button>
          ';
        }
        $output .= '
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      if ($position == 'cm') {
        $output .= '
        <div class="table-data-feature">
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
            <i class="zmdi zmdi-delete"></i>
          </button>
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      $output .='
      </td> 
    </tr>
    ';
    if ($record_trial == 'Yes') {
      $output .='
      <tr>
      <td colspan="6">Trial Record</td> 
      </tr>
      ';
    }
    $output .='
   </tbody>
  ';
  }elseif($row["student_status"] != 'Completed' || $row["student_status"] != 'Completed & Not Stable'){
  $output .='
  <tbody>
    <tr>
      <td><strong>'.$new_date.'</strong></td>
      <td>'.$row["level_name"].'</td>
      <td style="color:#FF0000;">'.$row["student_status"].'</td>
      <td colspan="4">'.$row["number_of_class"].'</td>
    </tr>
    <tr>
    <td colspan="2">Title :
    ';
      if ($row['method'] == 'Yes') {
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$title.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
    }else{
      $output .='
        '.$row["title_id"].'
      ';  
    }    $output .='
    </td>
    <td colspan="4">Feedback : '.$row["feedback"].'</td>
    </tr>
    <tr>
       <td colspan="7">Continue Title : <span style="color:#FF0000;">
       ';
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$c_id.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
       $output .= '
       </span></td>
    </tr>
    <tr>
    <td colspan="7">Reminder :  <span style="color:#FF0000;"> '.$row["continue_title"].' </span></td> 
    </tr>
    <tr>
      <td colspan="5">Teacher : '.$row["teachername"].'</td> 
      <td>
      ';
      if ($position == 'teacher' AND $insert_date == $todaydate AND strtolower($row["teachername"]) == strtolower($_COOKIE["name"])) {
        $output .= '
        <div class="table-data-feature">
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
            <i class="zmdi zmdi-delete"></i>
          </button>
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      if ($position == 'buddy') {

        $output .= '
        <div class="table-data-feature">
        ';
        if ( $insert_date == $todaydate AND strtolower($row["teachername"]) == strtolower($_COOKIE["name"])) {
          $output .= '
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
              <i class="zmdi zmdi-delete"></i>
            </button>
          ';
        }
        $output .= '
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      if ($position == 'cm') {
        $output .= '
        <div class="table-data-feature">
          <button onclick="delete1('.$row["r_id"].');"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
            <i class="zmdi zmdi-delete"></i>
          </button>
          <a href="progress_edit.php?r_id='.$row["r_id"].'&student_name='.$student_name.'&id='.$reg_no.'&trial='.$trial1.'&status='.urlencode($row["student_status"]).'" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
          <i class="zmdi zmdi-edit"></i>
        </div>
        ';
      }
      $output .='
      </td> 
    </tr>
    ';
    if ($record_trial == 'Yes') {
      $output .='
      <tr>
      <td colspan="6">Trial Record</td> 
      </tr>
      ';
    }
    $output .='
   </tbody>
  ';
  }elseif(strtolower($row["teachername"]) != strtolower($_COOKIE["name"]) AND ($row["student_status"] == 'Completed' ||  $row["student_status"] == 'Completed & Not Stable')){
  $output .='
  <tbody>
    <tr>
      <td><strong>'.$new_date.'</strong></td>
      <td>'.$row["level_name"].'</td>
      <td style="color:limegreen;">'.$row["student_status"].'</td>
      <td colspan="4">'.$row["number_of_class"].'</td>
    </tr>
    <tr>
    <td colspan="2">Title :
    ';
    if ($row['method'] == 'Yes') {
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$title.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
    }else{
      $output .='
        '.$row["title_id"].'
      ';  
    }    $output .='
    </td>
    <td colspan="4">Feedback : '.$row["feedback"].'</td>
    </tr>
    <tr>
      <td colspan="5">Teacher : '.$row["teachername"].'</td> 
      <td>
      <div class="table-data-feature">
      </div>
      </td> 
    </tr>
   </tbody>
  ';
  }elseif(strtolower($row["teachername"]) != strtolower($_COOKIE["name"]) AND ($row["student_status"] != 'Completed' || $row["student_status"] != 'Completed & Not Stable')){
  $output .='
  <tbody>
    <tr>
      <td><strong>'.$new_date.'</strong></td>
      <td>'.$row["level_name"].'</td>
      <td style="color:#FF0000;">'.$row["student_status"].'</td>
      <td colspan="4">'.$row["number_of_class"].'</td>
    </tr>
    <tr>
    <td colspan="2">Title :
    ';
     if ($row['method'] == 'Yes') {
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$title.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
    }else{
      $output .='
        '.$row["title_id"].'
      ';  
    }    $output .='
    </td>
    <td colspan="4">Feedback : '.$row["feedback"].'</td>
    </tr>
    <tr>
       <td colspan="7">Continue Title : <span style="color:#FF0000;">
       ';
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$c_id.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }  
    $output .= '
       </span></td>
    </tr>
    <tr>
    <td colspan="7">Reminder :  <span style="color:#FF0000;"> '.$row["continue_title"].' </span></td> 
    </tr>
    <tr>
      <td colspan="5">Teacher : '.$row["teachername"].'</td> 
      <td>
      <div class="table-data-feature">
      </div>
      </td> 
    </tr>
    ';
    if ($record_trial == 'Yes') {
      $output .='
      <tr>
      <td colspan="6">Trial Record</td> 
      </tr>
      ';
    }
    $output .='
   </tbody>
  ';
  }
  else{
    $output .='
  <tbody>
    <tr>
      <td><strong>'.$new_date.'</strong></td>
      <td>'.$row["level_name"].'</td>
      <td>'.$row["student_status"].'</td>
      <td colspan="4">'.$row["number_of_class"].'</td>
    </tr>
    <tr>
    <td colspan="2">Title :
    ';
     if ($row['method'] == 'Yes') {
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$title.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }
    }else{
      $output .='
        '.$row["title_id"].'
      ';  
    }
    $output .='
    </td>
    <td colspan="4">Feedback : '.$row["feedback"].'</td>
    </tr>
    <tr>
      <td colspan="7">Continue Title : <span style="color:#FF0000;">
       ';
      $query2 = "
      SELECT * FROM level_title WHERE title_id IN (".$c_id.")
      ";
      if($result2 = mysqli_query($connect, $query2))
      {        
        while($row2 = mysqli_fetch_array($result2))
        {
        $output .='
        '.$row2["title_name"].'<br>
        ';  
        }  
      }

       $output .= '
       </span></td>
    </tr>
    <tr>
    <td colspan="7">Reminder :  <span style="color:#FF0000;"> '.$row["continue_title"].' </span></td> 
    </tr>
    <tr>
      <td colspan="5">Teacher : '.$row["teachername"].'</td> 
      <td>
      <div class="table-data-feature">
      </div>
      </td> 
    </tr>
    ';
    if ($record_trial == 'Yes') {
      $output .='
      <tr>
      <td colspan="6">Trial Record</td> 
      </tr>
      ';
    }
    $output .='
   </tbody>
  ';
  }
 }
 echo $output;
}
else
{
 echo 'Data Not Found';
}

?>