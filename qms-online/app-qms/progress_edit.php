<?php 
    require_once("dbConfig.php");
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }

    $id = $_GET['r_id'];
    $student_name = $_GET['student_name'];
    $student_name = trim($student_name, " ");
    $student_name = rtrim($student_name, " ");
    $student_name = urlencode($student_name);
    $trial = $_GET['trial'];
    $reg_no = $_GET['id'];
    $query = "SELECT * FROM studentresult WHERE r_id='".$id."'";
    $result = mysqli_query($connect,$query);

    while($row=mysqli_fetch_assoc($result))
    {
        $date = $row['insert_date'];
        $language_id = $row['language_id'];
        $teachername = $row['teachername'];
        $level_id = $row['level_id'];
        $title_id = $row['title_id'];
        $dis = str_replace("<br/>","👈",$title_id);
        $display_title_id = str_replace(",",",/",$title_id);
        $continue_title = $row['continue_title'];
        $continue_id = $row['continue_id'];
        $dis_continue_id = str_replace(",",",/",$continue_id);
        $complete = $row['complete'];
        $student_status = $row['student_status'];
        $class = $row['number_of_class'];
        $room = $row['class_room'];
        $time = $row['times1'];
        $feedback = $row['feedback'];
        $g_id = $row['g_id'];
        $method = $row['method'];
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
    <!-- <link href="css/accordion.css" rel="stylesheet" media="all"> -->

</head>
<style type="text/css">
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #808080;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
</style>

<body class="animsition">
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <div class="page-wrapper">
      <input type="hidden" name="reg_no" id="reg_no" value="">
      <input type="hidden" name="language_id" id="language_id" value="">
      <input type="hidden" name="level_id" id="level_id" value="">
      <input type="hidden" name="title_id" id="title_id" value="">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h1 style="color: #ffffff;">QMS</h1>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->
        
        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h1 style="color: #ffffff;">QMS</h1>
                            <p>Online version</p>
                        </a>
                        <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
        </div>
        <!-- END HEADER MOBILE -->
        <center>
          <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
        </center>
        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7" id="myDiv">
            <!--FORM-->
            <section class="p-t-20">
              <div class="container">
              <form action="update.php?id=<?php echo $id ?>&reg=<?php echo $reg_no ?>&student_name=<?php echo $student_name ?>&trial=<?php echo $trial ?>&g_id=<?php echo $g_id ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
              <div class="card">
                  <div class="card-header">
                      <strong>Edit Progress
                      </strong>
                  </div>
                    
                      <div class="card-body card-block">
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label class=" form-control-label">Student :</label>
                              </div>
                              <div class="col-12 col-md-9">  
                              <input class="input100" type="text" id="student_name" name="student_name" placeholder="student_name" value="<?php echo $student_name ?>" READONLY>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label class=" form-control-label">Teacher :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <!-- <input type="text" id="student_name" name="student_name" placeholder="Text" class="form-control" value="" READONLY> -->
                                  <input class="input100" type="text" id="student_name" name="student_name" placeholder="student_name" value="<?php echo $teachername; ?>" READONLY>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Date :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <input type="Date" id="insert_date" name="insert_date" class="form-control" required="" value="<?php echo $date;?>">
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Language :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                <select name="language" id="language" class="form-control" required="">
                                <option value="">Language</option>
                                <?php
                                  $query_teacher = $connect->query("SELECT * FROM teacher WHERE username ='".$_COOKIE["name"]."'");
                                  $rowCount_teacher = $query_teacher->num_rows;
                                  if($rowCount_teacher > 0){
                                    while($row_teacher = $query_teacher->fetch_assoc()){
                                      $language1 = $row_teacher['language1'];
                                      $language2 = $row_teacher['language2'];
                                      $language3 = $row_teacher['language3'];
                                      $language4 = $row_teacher['language4'];
                                      $language5 = $row_teacher['language5'];
                                      $language6 = $row_teacher['language6'];
                                      $language7 = $row_teacher['language7'];
                                      $language8 = $row_teacher['language8'];
                                      $language9 = $row_teacher['language9'];

                                      $language_array = [];
                                      if ($language1 == "yes"){
                                      array_push($language_array, "1");
                                      }
                                      if ($language2 == "yes"){
                                      array_push($language_array, "2");
                                      }
                                      if ($language3 == "yes"){
                                      array_push($language_array, "3");
                                      }
                                      if ($language4 == "yes"){
                                      array_push($language_array, "4");
                                      }
                                      if ($language5 == "yes"){
                                      array_push($language_array, "5");
                                      }
                                      if ($language6 == "yes"){
                                      array_push($language_array, "6");
                                      } 
                                      if ($language7 == "yes"){
                                      array_push($language_array, "7");
                                      }
                                      if ($language8 == "yes"){
                                      array_push($language_array, "8");
                                      }
                                      if ($language9 == "yes"){
                                      array_push($language_array, "9");
                                      } 
                                      $new = join(",",$language_array);

                                    
                                  // language list
                                  $query_language = $connect->query("SELECT * FROM language WHERE status = 1 AND language_id IN (".$new.") ORDER BY language_id ASC");
                                  $rowCount_language = $query_language->num_rows;
                                  if($rowCount_language > 0){
                                    while($row_language = $query_language->fetch_assoc()){

                                        if($row_language['language_id'] == $language_id) {
                                            echo "<option value='".$row_language['language_id']."' SELECTED>".$row_language['language_name']."</option>";
                                        }
                                        else {
                                            echo "<option value='".$row_language['language_id']."'>".$row_language['language_name']."</option>";
                                        }
                                      }
                                    }  
                                  }
                                }                             
                              ?>
                              </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Level :</label>
                              </div>
                              <div class="col-12 col-md-9">
                              <select name="level" id="level" class="form-control" required="">
                              <option value="">Level</option>
                              <?php
                              $query_level = $connect->query("SELECT * FROM level WHERE status = 1 AND language_id = '".$language_id."' AND deleted = 0 ORDER BY  sorting ASC");
                                $rowCount_level = $query_level->num_rows;
                                if($rowCount_level > 0){
                                  while($row_level = $query_level->fetch_assoc()){
                                    $query_sr = $connect->query("SELECT COUNT(sr.r_id) AS total,sr.level_id 
                                    FROM studentresult sr 
                                    LEFT JOIN level le ON (sr.level_id = le.level_id) 
                                    LEFT JOIN language la ON (sr.language_id = la.language_id) 
                                    WHERE sr.reg_no='".$reg_no."' AND sr.level_id = '".$row_level['level_id']."' AND (sr.deleted IS NULL OR sr.deleted = 0) AND sr.complete = 'yes' GROUP BY sr.level_id");
                                     $rowCount_sr = $query_sr->num_rows;
                                      if($rowCount_sr > 0){
                                      echo "<option value='".$row_level['level_id']."'>&#9989; ".$row_level['level_name']."</option>";
                                      //echo "<option value='".$row_level['level_id']."'>line 507 ".$row_level['level_name']."</option>";                                    
                                      }elseif($row_level['level_id'] == $level_id){
                                        echo "<option value='".$row_level['level_id']."'SELECTED>".$row_level['level_name']."</option>";
                                      }else{
                                        echo "<option value='".$row_level['level_id']."'>".$row_level['level_name']."</option>";
                                      }    
                                        
                                }

                              }
                              ?>
                              </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Title :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <select name="title" id="title" multiple="" class="form-control">
                                  <option disabled>Title</option>
                                   <?php
                                   if ($method == 'Yes') {
                                    $title = explode(",",$title_id); 
                                    $query2 = "
                                    SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$level_id."' ORDER BY sorting ASC
                                    ";
                                    if($result2 = mysqli_query($connect, $query2))
                                    {        
                                      while($row2 = mysqli_fetch_array($result2))
                                      {
                                        
                                        if (in_array($row2['title_id'], $title)) {
                                            echo "<option value='".$row2['title_id']."' SELECTED>".$row2['title_name']."</option>";
                                        }
                                        else {
                                            echo "<option value='".$row2['title_id']."'>".$row2['title_name']."</option>";
                                        }

                                      }  
                                    }
                                  }else{
                                    $query_title = $connect->query("SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) ORDER BY sorting ASC");
                                    $rowCount_title = $query_title->num_rows;
                                    if($rowCount_title> 0){
                                      while($row_title = $query_title->fetch_assoc()){
                                        // $title_id = $row_title['title_id'];
                                        // $new_title = preg_replace("/\s(\S*)$/", " ", $row_title['title_name']);
                                        $title = rtrim($row_title['title_name'], "
                                        ");
                                        $last_title = preg_replace( "/\r|\n/", " ", $title);
                                        $tle = preg_replace("/\s\s+/", " ",$last_title);
                                        $tl = trim($tle, " ");
                                        //echo '<script type="text/javascript">alert("'.$tl.'")</script>';
                                        //echo '<script type="text/javascript">alert("'.$tle.'")</script>';
                                        // echo '<script type="text/javascript">alert("'.$new_title.'")</script>';
                                         //query2
                                          if($row_title['level_id'] == $level_id) {
                                            $query="SELECT * FROM studentresult WHERE r_id = '".$id."' AND language_id = '".$language_id."' AND level_id = '".$level_id."' AND reg_no = '".$_GET['id']."' AND title_id LIKE '%".$tl."%' AND (deleted IS NULL OR deleted = 0)";
                                            $result = mysqli_query($connect, $query);
                                            $row_sr = mysqli_num_rows($result);
                                            
                                              
                                            if($row_sr > 0){
                                            echo "<option value='".$row_title['title_id']."' SELECTED>".$row_title['title_name']."</option>";
                                            // echo '<script type="text/javascript">alert("'.$row['title_id'].'")</script>';
                                            }else{
                                             echo "<option value='".$row_title['title_id']."'>".$row_title['title_name']."</option>"; 
                                            }

                                            //query2
                                          }
                                          else{
                                          }
                                    }
                                  }
                                  }
                                  ?>
                                  </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="textarea-input" class=" form-control-label"></label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <?php
                                  if ($method == 'Yes') {
                                    $query_titleid = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$level_id."' AND title_id IN (".$title_id.") ORDER BY sorting ASC";
                                    $result_titleid = mysqli_query($connect,$query_titleid);
                                    echo '<textarea name="display" id="display" rows="5" placeholder="Title" class="form-control" READONLY>';
                                    while($row_title=mysqli_fetch_assoc($result_titleid))
                                    {
                                      // $title_name = $row_title['title_name'];
                                      // $title = str_replace(" ", "", $title_name);
                                      echo $row_title['title_name'];
                                      echo "👈 \n";
                                    }
                                  }else{
                                    echo '<textarea name="display" id="display" rows="5" placeholder="Title" class="form-control" READONLY>';
                                    echo $dis;
                                  }
                                  
                                  echo '</textarea>';
                                  ?>
                              </div>
                          </div>
                          <div class="row form-group" hidden="">
                              <div class="col col-md-3">
                                  <label for="textarea-input" class=" form-control-label"></label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <textarea name="display_id" id="display_id" rows="5" placeholder="Title ID" class="form-control" READONLY><?php echo $display_title_id;?></textarea>
                              </div>
                          </div>

                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="date-input" class=" form-control-label"></label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <div class="form-check">
                                      <div class="checkbox">
                                          <label for="checkbox1" class="form-check-label ">
                                              <?php
                                              if ($complete == 'yes') {
                                                echo "<input checked='checked' type='checkbox' name='complete' id='complete' value='yes' class='form-check-input' onclick='myFunction()'>Completed This Level";
                                              }
                                              else{
                                                echo "<input type='checkbox' name='complete' id='complete' value='yes' class='form-check-input' onclick='myFunction()'>Completed This Level";
                                              }
                                              ?>
                                              
                                          </label>
                                          <p id="text" style="display:none; color: red; font: bold;">* Are you sure this level's all titles are completed? Ticked this means next class will be a new level</p>
                                          <p id="text1" style="display:none; color: red; font: bold;">* 确定这个level的所有Title都已经完成了吗？打勾表示下一堂课将进入全新的一个level</p>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Status :</label>
                              </div>
                              <div class="col-12 col-md-9">
                              <select id="status" name="status" class="form-control" required="" onchange="c()">
                              <option value="" selected>Status</option>
                              <?php
                              if ($student_status == 'Completed') {
                                echo "<option value='Completed' SELECTED>Completed 完成</option>";
                              }
                              else{
                                echo "<option value='Completed'>Completed 完成</option>";
                              }
                              if ($student_status == 'Not complete' OR $student_status == 'Not completed') {
                                echo "<option value='Not complete' SELECTED>Not complete 未完成</option>";
                              }
                              else{
                                echo "<option value='Not complete'>Not complete 未完成</option>";
                              }
                              // if ($student_status == 'Not stable') {
                              //   echo "<option value='Not stable' SELECTED>Please select other status</option>";
                              // }
                              if ($student_status == 'Not Completed & Not Stable') {
                                echo "<option value='Not Completed & Not Stable' SELECTED>Not Completed & Not Stable 未完成和不稳</option>";
                              }
                              else{
                                echo "<option value='Not Completed & Not Stable'>Not Completed & Not Stable 未完成和不稳</option>";
                              }
                              if ($student_status == 'Completed & Not Stable') {
                                echo "<option value='Completed & Not Stable' SELECTED>Completed & Not Stable 完成和不稳</option>";
                              }
                              else{
                                echo "<option value='Completed & Not Stable'>Completed & Not Stable 完成和不稳</option>";
                              }
			                        if ($student_status == 'Skip') {
                                echo "<option value='Skip' SELECTED>Skip 跳過</option>";
                              }
                              else{
                                echo "<option value='Skip'>Skip 跳過</option>";
                              }
                              ?>
                              </select>
                    
                            </div>
                          </div>


                          <?php
                              if ($_GET['status'] == 'Completed' OR $_GET['status'] == 'Skip' OR $_GET['status'] == 'Completed & Not Stable') {

                                echo '
                                <div id="text3" style="display: none;">
                                  <div class="row form-group" >
                                    <div class="col col-md-3">
                                        <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Continu Title :</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="continu" id="continu" multiple="" class="form-control">
                                        
                                        </select>
                                        <p style="color: red">* Reminder (please enter not complete / not stable title) </p>
                                    </div>
                                    
                                  </div>
                                  <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Reminder :</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="reminder" id="reminder" rows="5" placeholder="Reminder" class="form-control"></textarea>
                                        <p style="color: red">* Reminder (please enter not complete / not stable title) </p>
                                    </div>
                                  </div>
                                </div>
                                ';

                                
                                
                              }else{

                                if ($method == 'Yes') {
                                  $continu = explode(",",$continue_id);
                                  $query3 = "
                                    SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND title_id IN (".$title_id.") ORDER BY sorting ASC
                                    ";
                                    if($result3 = mysqli_query($connect, $query3))
                                    {        
                                  echo '
                                        <div id="text3" style="display: block;">
                                          <div class="row form-group" >
                                            <div class="col col-md-3">
                                                <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Continu Title :</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="continu" id="continu" multiple="" class="form-control">
                                                <option disabled>Coutinu Title</option>
                                  ';
                                      while($row3 = mysqli_fetch_array($result3))
                                      {
                                        if (in_array($row3['title_id'], $continu)) {
                                            echo "<option value='".$row3['title_id']."' SELECTED>".$row3['title_name']."</option>";
                                        }else{
                                            echo "<option value='".$row3['title_id']."'>".$row3['title_name']."</option>";
                                        }
                                      }  
                                    }
                                }else{

                                  $query_title = $connect->query("SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) ORDER BY sorting ASC");
                                    $rowCount_title = $query_title->num_rows;
                                    if($rowCount_title> 0){
                                      echo '
                                        <div id="text3" style="display: block;">
                                          <div class="row form-group" >
                                            <div class="col col-md-3">
                                                <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Continu Title :</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="continu" id="continu" multiple="" class="form-control">
                                                <option disabled>Coutinu Title</option>
                                  ';
                                      while($row_title = $query_title->fetch_assoc()){
                                        // $title_id = $row_title['title_id'];
                                        // $new_title = preg_replace("/\s(\S*)$/", " ", $row_title['title_name']);
                                        $title = rtrim($row_title['title_name'], "
                                        ");
                                        $last_title = preg_replace( "/\r|\n/", " ", $title);
                                        $tle = preg_replace("/\s\s+/", " ",$last_title);
                                        $tl = trim($tle, " ");
                                        //echo '<script type="text/javascript">alert("'.$tl.'")</script>';
                                        //echo '<script type="text/javascript">alert("'.$tle.'")</script>';
                                        // echo '<script type="text/javascript">alert("'.$new_title.'")</script>';
                                         //query2
                                          if($row_title['level_id'] == $level_id) {
                                            $query="SELECT * FROM studentresult WHERE r_id = '".$id."' AND language_id = '".$language_id."' AND level_id = '".$level_id."' AND reg_no = '".$_GET['id']."' AND title_id LIKE '%".$tl."%' AND (deleted IS NULL OR deleted = 0)";
                                            $result = mysqli_query($connect, $query);
                                            $row_sr = mysqli_num_rows($result);
                                            
                                              
                                            if($row_sr > 0){
                                            echo "<option value='".$row_title['title_id']."' SELECTED>".$row_title['title_name']."</option>";
                                            // echo '<script type="text/javascript">alert("'.$row['title_id'].'")</script>';
                                            }else{
                                             echo "<option value='".$row_title['title_id']."'>".$row_title['title_name']."</option>"; 
                                            }

                                            //query2
                                          }
                                          else{
                                          }
                                    }
                                  }
                                }
                                
                                echo '
                                    </select>
                                        <p style="color: red">* Continu Title (Which one title is Not Complete ?? Please select)</p>
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="date-input" class=" form-control-label"><span style="color: red">*</span>Reminder :</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        
                                ';
                                echo '<textarea name="reminder" id="reminder" rows="5" placeholder="Reminder" class="form-control">'.$continue_title.'</textarea>';
                                echo '
                                        <p style="color: red">* Reminder (please enter not complete / not stable title)</p>
                                    </div>
                                  </div>
                                </div>
                                    ';
                              
                              }
                              ?>
                          <div class="row form-group" hidden="">
                              <div class="col col-md-3">
                                  <label for="textarea-input" class=" form-control-label"></label>
                              </div>
                              <div class="col-12 col-md-9">
                                <textarea name="dis_continu_id" id="dis_continu_id" rows="5" placeholder="Continu Title ID" class="form-control" READONLY><?php echo $dis_continue_id;?></textarea>
                              </div>
                          </div>

                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Class :</label>
                              </div>
                              <div class="col-12 col-md-9">
                              <select id="sclass" name="sclass" class="form-control" required="">
                              <!-- <option value="" selected>Class</option> -->
                              <?php
                              if ($class == '1') {
                                echo "<option value='1' SELECTED>1</option>";
                              }
                              else{
                                echo "<option value='1'>1</option>";
                              }
                              if ($class == '2') {
                                echo "<option value='2' SELECTED>2</option>";
                              }
                              else{
                                echo "<option value='2'>2</option>";
                              } 
                              if ($class == '3') {
                                echo "<option value='3' SELECTED>3</option>";
                              }
                              else{
                                echo "<option value='3'>3</option>";
                              }
                              if ($class == '4') {
                                echo "<option value='4' SELECTED>4</option>";
                              }
                              else{
                                echo "<option value='4'>4</option>";
                              }
                              ?>
                              </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Class Room :</label>
                              </div>
                              <div class="col-12 col-md-9">
                              <select id="room" name="room" class="form-control" required="">
                              <!-- <option value="" selected>Class Room</option> -->
                              <?php
                              if ($room == 'Room1') {
                                echo "<option value='Room1' SELECTED>Room 1</option>";
                              }
                              else{
                                echo "<option value='Room1'>Room 1</option>";
                              }
                              if ($room == 'Room2') {
                                echo "<option value='Room2' SELECTED>Room 2</option>";
                              }
                              else{
                                echo "<option value='Room2'>Room 2</option>";
                              }
                              if ($room == 'Room3') {
                                echo "<option value='Room3' SELECTED>Room 3</option>";
                              }
                              else{
                                echo "<option value='Room3'>Room 3</option>";
                              }
                              if ($room == 'Room4') {
                                echo "<option value='Room4' SELECTED>Room 4</option>";
                              }
                              else{
                                echo "<option value='Room4'>Room 4</option>";
                              }
                              if ($room == 'Room5') {
                                echo "<option value='Room5' SELECTED>Room 5</option>";
                              }
                              else{
                                echo "<option value='Room5'>Room 5</option>";
                              }
                              if ($room == 'Room6') {
                                echo "<option value='Room6' SELECTED>Room 6</option>";
                              }
                              else{
                                echo "<option value='Room6'>Room 6</option>";
                              }
                              if ($room == 'Room7') {
                                echo "<option value='Room7' SELECTED>Room 7</option>";
                              }
                              else{
                                echo "<option value='Room7'>Room 7</option>";
                              }
                              if ($room == 'Room8') {
                                echo "<option value='Room8' SELECTED>Room 8</option>";
                              }
                              else{
                                echo "<option value='Room8'>Room 8</option>";
                              }
                              if ($room == 'Room9') {
                                echo "<option value='Room9' SELECTED>Room 9</option>";
                              }
                              else{
                                echo "<option value='Room9'>Room 9</option>";
                              }
                              if ($room == 'Room10') {
                                echo "<option value='Room10' SELECTED>Room 10</option>";
                              }
                              else{
                                echo "<option value='Room10'>Room 10</option>";
                              }
                              if ($room == 'Room11') {
                                echo "<option value='Room11' SELECTED>Room 11</option>";
                              }
                              else{
                                echo "<option value='Room11'>Room 11</option>";
                              }
                              if ($room == 'Room12') {
                                echo "<option value='Room12' SELECTED>Room 12</option>";
                              }
                              else{
                                echo "<option value='Room12'>Room 12</option>";
                              }
                              if ($room == 'Room13') {
                                echo "<option value='Room13' SELECTED>Room 13</option>";
                              }
                              else{
                                echo "<option value='Room13'>Room 13</option>";
                              }
                              if ($room == 'Room14') {
                                echo "<option value='Room14' SELECTED>Room 14</option>";
                              }
                              else{
                                echo "<option value='Room14'>Room 14</option>";
                              }
                              if ($room == 'Room15') {
                                echo "<option value='Room15' SELECTED>Room 15</option>";
                              }
                              else{
                                echo "<option value='Room15'>Room 15</option>";
                              }
                              if ($room == 'Room16') {
                                echo "<option value='Room16' SELECTED>Room 16</option>";
                              }
                              else{
                                echo "<option value='Room16'>Room 16</option>";
                              }
                              if ($room == 'Room17') {
                                echo "<option value='Room17' SELECTED>Room 17</option>";
                              }
                              else{
                                echo "<option value='Room17'>Room 17</option>";
                              }
                              if ($room == 'Room18') {
                                echo "<option value='Room18' SELECTED>Room 18</option>";
                              }
                              else{
                                echo "<option value='Room18'>Room 18</option>";
                              }
                              if ($room == 'Room19') {
                                echo "<option value='Room19' SELECTED>Room 19</option>";
                              }
                              else{
                                echo "<option value='Room19'>Room 19</option>";
                              }
                              if ($room == 'Room20') {
                                echo "<option value='Room20' SELECTED>Room 20</option>";
                              }
                              else{
                                echo "<option value='Room20'>Room 20</option>";
                              }
                              ?>
                              </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="select" class=" form-control-label"><span style="color: red">*</span>Time :</label>
                              </div>
                              <div class="col-12 col-md-9">
                              <select id="time1" name="time1" class="form-control" required="">
                              <!-- <option value="" selected>Time</option> -->
                              <?php
                              if ($time == 'Morning1') {
                                echo "<option value='Morning1' SELECTED>Morning 1</option>";
                              }
                              else{
                                echo "<option value='Morning1'>Morning 1</option>";
                              }
                              if ($time == 'Morning2') {
                                echo "<option value='Morning2' SELECTED>Morning 2</option>";
                              }
                              else{
                                echo "<option value='Morning2'>Morning 2</option>";
                              }
                              if ($time == 'Afthernoon1') {
                                echo "<option value='Afthernoon1' SELECTED>Afternoon 1</option>";
                              }
                              else{
                                echo "<option value='Afthernoon1'>Afternoon 1</option>";
                              }
                              if ($time == 'Afthernoon2') {
                                echo "<option value='Afthernoon2' SELECTED>Afternoon 2</option>";
                              }
                              else{
                                echo "<option value='Afthernoon2'>Afternoon 2</option>";
                              }
                              if ($time == 'Afthernoon3') {
                                echo "<option value='Afthernoon3' SELECTED>Afternoon 3</option>";
                              }
                              else{
                                echo "<option value='Afthernoon3'>Afternoon 3</option>";
                              }
                              if ($time == 'Afthernoon4') {
                                echo "<option value='Afthernoon4' SELECTED>Afternoon 4</option>";
                              }
                              else{
                                echo "<option value='Afthernoon4'>Afternoon 4</option>";
                              }
                              ?>
                              </select>
                              </div>
                          </div>
                          <div class="row form-group">
                              <div class="col col-md-3">
                                  <label for="textarea-input" class=" form-control-label">Feedback :</label>
                              </div>
                              <div class="col-12 col-md-9">
                                  <textarea name="feedback" id="feedback" rows="5" placeholder="Feedback...." class="form-control"><?php echo $feedback; ?></textarea>
                              </div>
                          </div>
                  </div>
                  <div class="card-footer">
                      <button class="btn btn-success" type="submit" name="update">
                      Save</button>
                      <a href="progress.php?id=<?php echo $reg_no ?>&student_name=<?php echo $student_name ?>&trial=<?php echo $trial ?>"><button class="btn btn-info">Back</button></a>
                       
                  </div>
              </div>
              </form>
            </div>
            </section>
            <!-- END FORM-->
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script>
    function logout() {
      var result = confirm('Are you sure to LOGOUT?');
        if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }   
    }

    window.onload=showid();
    function showid() {
      var urlParams = new URLSearchParams(window.location.search);
      var reg_no = urlParams.get('id');
      var student_name = urlParams.get('student_name');
      document.getElementById("reg_no").value = reg_no;
      document.getElementById("student_name").value = student_name;

      // document.getElementById("teacher").value = teacher;
    }
    function c(){
      var status = document.getElementById("status").value;
      var text3 = document.getElementById("text3");
      var text4 = document.getElementById("text4");
      if (status == 'Not complete' || status == 'Not Completed & Not Stable'){
        text3.style.display = "block";
        text4.style.display = "block";
       
      } else {
        text3.style.display = "none";
        text4.style.display = "none";
      }
    }
    function replace(){
      var display = document.getElementById('display').value;
      var continu = document.getElementById('continu');
      continu.value = display;
      var display_id = document.getElementById('display_id').value;
      var continu_id = document.getElementById('continu_id');
      continu_id.value = display_id;

    }
    
    function getSelectedOptions(sel, fn) {
    var opts = [], opt;
              
          // loop through options in select list
      for (var i=0, len=sel.options.length; i<len; i++) {
        opt = sel.options[i];          
              // check if selected
          if ( opt.selected ) {
                  // add to array of option elements to return from this function
               opts.push(opt);            
                  // invoke optional callback function if provided
              if (fn) {
                  fn(opt);
                }
            }
        }
              
          // return array containing references to selected option elements
          return opts;

    }

                // example callback function (selected options passed one by one)
      function callback(opt) {
          // display in textarea for this example
          var display = document.getElementById('display');
          var display_id = document.getElementById('display_id');
          display.innerHTML += opt.text +'👈 \n';
          display_id.innerHTML += opt.value +',/';
          //var disp = display.replace("&#129488", "<br/>");
              // can access properties of opt, such as...
              //alert( opt.value )
              //alert( opt.text )
              //alert( opt.form )
          var display = document.getElementById('display').value;
          var display_id = document.getElementById('display_id').value;
          var id = display_id.split(",/");
          var name = display.split("👈");
          document.getElementById("continu").innerHTML ='<option disabled>Coutinu Title</option>';
          for(var i = 0; i < name.length && id.length; i++){

            if (id[i]) {
              document.getElementById("continu").innerHTML += '<option value='+id[i]+'>'+name[i]+'</option>';
            }
            
          }
      }

      function getSelectedOptions1(sel, fn) {
        var opts1 = [], opt1;
                
            // loop through options in select list
        for (var i=0, len=sel.options.length; i<len; i++) {
          opt1 = sel.options[i];          
                // check if selected
            if ( opt1.selected ) {
                    // add to array of option elements to return from this function
                 opts1.push(opt1);            
                    // invoke optional callback function if provided
                if (fn) {
                    fn(opt1);
                  }
              }
          }
              
          // return array containing references to selected option elements
          return opts1;

    }

                // example callback function (selected options passed one by one)
      function callback1(opt1) {
          var dis_continu_id = document.getElementById('dis_continu_id');
          dis_continu_id.innerHTML += opt1.value +',/';
          //var disp = display.replace("&#129488", "<br/>");
              // can access properties of opt, such as...
              //alert( opt.value )
              //alert( opt.text )
              //alert( opt.form )
      }

        document.getElementById('continu').onchange = function(e) {
          // get reference to display textarea
          var dis_continu_id = document.getElementById('dis_continu_id');


          dis_continu_id.innerHTML = '';
          // callback fn handles selected options
          getSelectedOptions1(this, callback1);
            

          var str_test = dis_continu_id.innerHTML.slice(0, -2);
          dis_continu_id.innerHTML = str_test;
          
      };

       document.getElementById('title').onchange = function(e) {
          // get reference to display textarea
          var display = document.getElementById('display');
          var display_id = document.getElementById('display_id');


          display.innerHTML = ''; // reset
          display_id.innerHTML = '';
          // callback fn handles selected options
          getSelectedOptions(this, callback);
              

          var str = display.innerHTML.slice(0, -2);
          var res = str.replace(/✅/g, "");
          var dis = res.replace(/<br ?\/?>/g, "");
          // document.write(str.replace(/✅/g, ""))
          display.innerHTML = dis;

          var str_test = display_id.innerHTML.slice(0, -2);
          display_id.innerHTML = str_test;
          //var new_dis = dis.replace(/🧐/g, "<br/>");
          //var last_dis = new_dis.replace(/<br?\/?>/g, "");
          // var new_text = str.replace('✓&lt;br/&gt; ', '');
          // display.innerHTML = new_text;
          // display.innerHTML = new_dis;
          // alert(new_dis);
      };
    if (document.getElementById("complete").checked == true) {
        text.style.display = "block";
        text1.style.display = "block";
    }
    else{
        text.style.display = "none";
        text1.style.display = "none";
    }
    </script>
    <script>
    $(document).ready(function() {
        
        var documentRoot = "ajaxData.php";

        $("#language").change(function() {
             // alert($("#language").val());

            var form_data_lang = {
                    selectedID: $("#language").val(),
                    reg_no: $("#reg_no").val(),
                    functionName: 'language'
                };
                // $("#level").html("loading......");
            $.ajax({type: "POST", url: documentRoot , data: form_data_lang, success: function(response) {
              // alert (response);
              
              $("#level").html(response);

              var form_data_level = {
                    selectedID1: $("#level").val(),
                    selectedID: $("#language").val(),
                    reg_no: $("#reg_no").val(),
                    functionName1: 'level'
                };

                // $("#title").html("loading......");
            $.ajax({type: "POST", url: documentRoot , data: form_data_level, success: function(response1) {
               // alert (response1);
                    
                    $("#title").html(response1);
            }});

            }});
        });

        $("#level").change(function() {
             
            var form_data_level = {
                    selectedID1: $("#level").val(),
                    selectedID: $("#language").val(),
                    reg_no: $("#reg_no").val(),
                    functionName1: 'level'
                };
                // $("#title").html("loading......");
            $.ajax({type: "POST", url: documentRoot , data: form_data_level, success: function(response1) {
               // alert (response1);
                    
                    $("#title").html(response1);
            }});
        });

        
      });
    </script>

    </body>

</html>