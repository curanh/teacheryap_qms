<?php 
    
    use PHPMailer\PHPMailer\PHPMailer;
    require_once('PHPMailer/PHPMailer.php');
    require_once('PHPMailer/Exception.php');
    require_once('PHPMailer/OAuth.php');
    require_once('PHPMailer/POP3.php');
    require_once('PHPMailer/SMTP.php');
    require_once("dbConfig.php");

    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    $fid = $_COOKIE['fid'];
    $name = $_COOKIE['name'];
    $position = $_COOKIE['position'];
    $query = "SELECT * FROM centre WHERE fid = '".$fid."'";
    $result = mysqli_query($connect,$query);

    while($row=mysqli_fetch_assoc($result))
    {
        $centre = $row['name'];
    }
    if (isset($_POST['button'])) {
        
        // echo $_POST['username']; exit();
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $username = $_POST['username'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];
        $date1 = date('Y-m-d H:i:s');

        for($y=1;$y<=1;$y++) {
            if(isset($_FILES['image'.$y])) {
                $file = $_FILES['image'.$y]['tmp_name'];
                $sourceProperties = getimagesize($file);
                $fileNewName = "support_".$fid."_".$username."_".date("YmdHis")."_".rand(100,999);
                $folderPath = "support/";
                $ext = pathinfo($_FILES['image'.$y]['name'], PATHINFO_EXTENSION);
                $imageType = $sourceProperties['mime'];
                $targetWidth =800;
                $ratio = 800 / $sourceProperties[0];
                $targetHeight = $sourceProperties[1] * $ratio;
                
                switch ($imageType) {
                    case "image/png":
                        $imageResourceId = imagecreatefrompng($file);
                        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
                        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $sourceProperties[0],$sourceProperties[1]);
                        imagepng($targetLayer,$folderPath. $fileNewName. ".". $ext);
                        $image_url[$y] = $fileNewName.".".$ext;
                        break;

                    case "image/gif":
                        $imageResourceId = imagecreatefromgif($file);
                        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
                        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $sourceProperties[0],$sourceProperties[1]);
                        imagegif($targetLayer,$folderPath. $fileNewName. ".". $ext);
                        $image_url[$y] = $fileNewName.".".$ext;
                        break;
                   
                    case "image/jpeg":
                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
                        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $sourceProperties[0],$sourceProperties[1]);
                        imagejpeg($targetLayer,$folderPath. $fileNewName. ".". $ext);
                        $image_url[$y] = $fileNewName.".".$ext;
                        break;

                    default:
                        break;
                }
            }
        }
        $error = "no";
        if ($error == 'no') {
            $sql_support="INSERT INTO support (fid, username, subject, message, image_1, date_time, status)
            VALUES
            ('".$fid."', '".$username."','".$subject."','".$message."', '".$image_url[1]."', '".$date1."', '1')";

                if (!mysqli_query($connect,$sql_support))
                {
                    die('Error: ' . mysqli_error($connect));
                }

                $mail = new PHPMailer;
                $mail->isSMTP();                                            // Set mailer to use SMTP
                $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
                $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                $mail->Username   = 'notification-noreply@yelaoshr.edu.my'; // SMTP username
                $mail->Password   = 'noreply123abc#';                       // SMTP password
                $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                $mail->Port       = 587;
                $mail->setFrom('notification-noreply@yelaoshr.edu.my', 'YELAOSHR');
                $mail->addAddress('chienming@yelaoshr.edu.my', 'Chien Ming Woo');
                $mail->Subject = "QMS Support";
                $mail->isHTML(true);
                $mail->Body .= '
                <html> <!-- #A3D0F8 -->
                <head><meta charset="UTF-8"></head>
                    <body style="color: #000; font-size: 16px; text-decoration: none; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; background-color: #efefef;">
                        <div id="wrapper" style="max-width: 600px; margin: auto auto; padding: 20px;">
                            <div id="logo" style="">
                            </div>
                            <div id="content" style="font-size: 16px; padding: 25px; background-color: #fff;
                                moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;
                                border-color: #A3D0F8; border-width: 4px 1px; border-style: solid;">
                                <h1 style="margin: 0px;"><a href="https://www.teacheryap.com/" target="_blank"><img style="max-height: 75px;" src="https://www.teacheryap.com/wp-content/uploads/2019/04/cropped-A5F7AEFD-1C79-4F40-B756-2E35BC3C8854.png"></a></h1>
                                <h1 style="font-size: 22px;"><center>Support</center></h1>
                                <p>Centre: '.$centre.'</p>
                                <p>Teacher: '.$username.'</p>
                                <p>Subject: '.$subject.'</p>
                                <p>Message: '.$message.'</p>
                                <p>Date: '.$date1.'</p>
                            </div>
                        </div>
                    </body>
                </html>
                ';
                // echo $mail; exit();
                if ($mail->send()){
                    // $successMsg1 = "$femail"."&nbsp;".'已发送验证 Sent verification';
                    echo "<script>alert('Successful! ')</script>";
                }  
            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>
<style type="text/css">
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #808080;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
</style>
<body class="animsition">
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled" style="margin-left: 50%;">
                            <li>
                                <a href="student_list.php">
                                    <i class="fa fa-id-badge"></i>
                                    Registered student</a>
                            </li>
                        </ul>
                    </div>

                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="profile/dashboard_profile.php">
                                            <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>

            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a href="student_list.php">
                            <i class="fa fa-id-badge"></i>
                            Registered student</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">

                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        
                        <div class="image">
                            <img src="Images/Logo/YelaoShrBaby.png">
                        </div>
                        <div class="content">
                            <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="profile/dashboard_profile.php">
                                    <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                <a href="#" onclick="logout()">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><img src="Images/gif/support.gif" width="100" height="100"><br><h4>Support Form</h4></div>
                                <div class="card-body card-block">
                                    <form action="support.php" method="post" data-parsley-validate="" class="form-horizontal" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Centre</div>
                                                <input type="text" id="centre" name="centre" value="<?php echo $centre; ?>"  class="form-control" READONLY>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-home"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Username</div>
                                                <input type="text" id="username" name="username" value="<?php echo $name; ?>"  class="form-control" READONLY>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Subject</div>
                                                <input type="text" id="subject" name="subject" class="form-control" placeholder="Subject....">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-tasks"></i>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Message <i class="fa fa-comment"></i></div>
                                                <textarea name="message" id="message" rows="3" placeholder="Your Message...." class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Images</div>
                                                <input type="file" name="image1" class="form-control" accept="image/*" capture="camera">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-file"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions form-group">
                                            <button type="submit" name="button" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->
            <center>
                <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
            </center>
            <!-- DATA TABLE-->
            <section class="p-t-20" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script type="text/javascript">
    function logout() {
        // document.getElementById("myDiv").style.display = "none";
            var result = confirm('Are you sure to LOGOUT?');
            if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }
            
    }   

    </script>
</body>

</html>
<!-- end document-->
