<?php 
    
    require_once("dbConfig.php");

    if(isset($_POST['update']))
    {
        $id = $_GET['id'];
        $reg_no = $_GET['reg'];
        $student_name = $_GET['student_name'];
        $trial = $_GET['trial'];
        $g_id = $_GET['g_id'];
        $feedback = $_POST['feedback'];
        $status= $_POST['status'];
        $sclass = $_POST['sclass'];
        $language = $_POST['language'];
        $level= $_POST['level'];
        $display_id = $_POST['display_id'];
        $dis = str_replace("/","",$display_id);
        $room = $_POST['room'];
        $time1= $_POST['time1'];
        $insert_date = $_POST['insert_date'];
        $dis_continu_id = $_POST['dis_continu_id'];
        $continu = str_replace("/","",$dis_continu_id);
        $reminder = $_POST['reminder'];
        if(isset($_POST['complete'])) {
            $complete = $_POST['complete'];
        }
        else
        {
            $complete = "no";
        }
        $check_id = str_replace(',/', '', $display_id);
        if (is_numeric($check_id)) {
            $method = 'Yes';
        }else{
            $method = NULL;
        }
        // echo $method;
        $query_log = "SELECT * FROM studentresult WHERE r_id = '".$id."'";
        $result_log = mysqli_query($connect,$query_log);
        if(mysqli_num_rows($result_log) > 0)
        {
            while($row = mysqli_fetch_array($result_log))
            {   
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $edit_date = date('Y-m-d H:i:s');
                $teacher = $row['teachername'];
                $language_id = $row['language_id'];
                $level_id = $row['level_id'];
                $title_id = $row['title_id'];
                $class_room = $row['class_room'];
                $times1 = $row['times1'];
                $student_status = $row['student_status'];
                $number_of_class = $row['number_of_class'];
                $feedback_log = $row['feedback'];
                $fid = $row['fid'];
                $reg_no = $row['reg_no'];
                $g_id = $row['g_id'];
                $insert_date_log = $row['insert_date'];
                $complete_log = $row['complete'];
                $trial = $row['trial'];
                $continue_reminder_log = $row['continue_title'];
                $continue_id_log = $row['continue_id'];

                $sql="INSERT INTO edit_log (edit_name, edit_date, r_id, language_id, level_id, title_id, continue_id, continue_title, class_room, times1, student_status, number_of_class, feedback, fid, reg_no, g_id, insert_date, complete, trial)                    VALUES
                    ('$teacher', '$edit_date', '$id', '$language_id', '$level_id', ('".addslashes($title_id)."'), ('".addslashes($continue_id_log)."'), ('".addslashes($continue_reminder_log)."'), '$class_room', '$times1', '$student_status', '$number_of_class', ('".addslashes($feedback_log)."'), '$fid', '$reg_no', '$g_id', '$insert_date_log', '$complete_log', '$trial')";
                if (!mysqli_query($connect,$sql))
                {
                    die('Error: ' . mysqli_error($connect));
                }
            }
        }

        if ($status == 'Completed' OR $status == 'Completed & Not Stable' OR $status == 'Skip') {
            $null = '';
            $query = "UPDATE studentresult SET level_id = '".$level."', title_id = ('".addslashes($dis)."'), continue_id = '".$null."', continue_title = '".$null."', class_room = '".$room."' , times1 = '".$time1."', student_status = '".$status."', number_of_class = '".$sclass."', feedback = '".$feedback."', language_id = '".$language."', insert_date = '".$insert_date."', complete = '".$complete."', method = '".$method."' WHERE r_id='".$id."'";
            $result = mysqli_query($connect,$query);

            if (!mysqli_query($connect,$query))
            {
                  die('Error: ' . mysqli_error($connect));
            }
        }
        else{
           $query = "UPDATE studentresult SET level_id = '".$level."', title_id = ('".addslashes($dis)."'), continue_id = ('".addslashes($continu)."'), continue_title = ('".addslashes($reminder)."'), class_room = '".$room."' , times1 = '".$time1."', student_status = '".$status."', number_of_class = '".$sclass."', feedback = '".$feedback."', language_id = '".$language."', insert_date = '".$insert_date."', complete = '".$complete."' , method = '".$method."' WHERE r_id='".$id."'";
           $result = mysqli_query($connect,$query); 

            if (!mysqli_query($connect,$query))
            {
                  die('Error: ' . mysqli_error($connect));
            }
        }
        
        //echo '<script type="text/javascript">alert("'.$query.'")</script>';
        if($result)
        {   

           echo "<script type='text/javascript'>alert('successfully ');
                var urlParams = new URLSearchParams(window.location.search);
                var reg_no = urlParams.get('reg');
                var student_name = urlParams.get('student_name');
                var student_name = encodeURIComponent(student_name);
                var trial = urlParams.get('trial');
                window.location='progress.php?id='+reg_no+'&student_name='+student_name+'&trial='+trial+''
                 </script>";
            
        }
        else
        {
           echo 'error';  //not showing an alert box.
        }
    }
    else
    {   
        
        
    }
?>