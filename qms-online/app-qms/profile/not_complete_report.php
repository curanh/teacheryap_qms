<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Not Complete Report</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Not Complete Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Not Complete Report</li>
            </ol>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th width="20%">Studen Name</th>
                  <th width="15%">Reg No</th>
                  <th width="10%">Language</th>
                  <th width="20%">Level</th>
                  <th width="10%">Start Date</th>
                  <th width="10%">Last Insert Date</th>
                  <th width="10%">View</th>
                  <th width="10%">Complete</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
              $query_student = "SELECT s.reg_no, s.studentname, s.active FROM student s  
                    WHERE s.fid = '".$_COOKIE['fid']."' 
                    AND s.active='Yes' ORDER BY s.studentname ASC";

              $result_student = mysqli_query($connect, $query_student);          
              while($row_student = mysqli_fetch_array($result_student))
                {
                  $reg_no = $row_student["reg_no"];
                  $studentname = $row_student["studentname"];
                  $active = $row_student["active"];

                  $query_student_result = "SELECT DISTINCT level_id FROM studentresult
                  WHERE reg_no = '".$reg_no."'";
                  $result_student_result = mysqli_query($connect, $query_student_result); 
                  while($row_student_result = mysqli_fetch_array($result_student_result))
                  {
                    // $language_name = $row_student_result["language_name"];
                    // $level_name = $row_student_result["level_name"];
                    $level_id = $row_student_result["level_id"];
                    // $r_id = $row_student_result["r_id"];

                    $query = "SELECT lg.language_name, lv.level_name, sr.level_id, sr.complete, sr.r_id FROM studentresult sr
                   LEFT JOIN level lv ON (sr.level_id=lv.level_id) 
                   LEFT JOIN language lg ON (sr.language_id = lg.language_id)
                   WHERE sr.fid = '".$_COOKIE['fid']."' 
                   AND sr.reg_no = '".$reg_no."' 
                   AND sr.deleted='0' 
                   AND sr.level_id = '".$level_id."'
                   ORDER BY sr.r_id DESC LIMIT 1";
                    $result = mysqli_query($connect, $query); 
                    while($row = mysqli_fetch_array($result))
                    {
                      $language_name = $row["language_name"];
                      $level_name = $row["level_name"];
                      $r_id = $row["r_id"];
                      $complete = $row["complete"];
                      
                      if ($complete == 'no') {
                        //$insert_date = date("d/m/Y",strtotime($insert_date));
                        $query_start_date = "SELECT sr.insert_date FROM studentresult sr
                        WHERE sr.fid = '".$_COOKIE['fid']."' 
                        AND sr.deleted='0' 
                        AND sr.level_id='".$level_id."' 
                        AND sr.reg_no='".$reg_no."'
                        AND sr.complete='no'
                        ORDER BY sr.insert_date ASC LIMIT 1";
                        $result_start_date = mysqli_query($connect, $query_start_date); 

                        while($row_start_date = mysqli_fetch_array($result_start_date))
                        {
                          $insert_date = $row_start_date["insert_date"];
                          // $insert_date = date("d/m/Y",strtotime($insert_date));

                          $query_last_date = "SELECT sr.insert_date FROM studentresult sr
                                           WHERE sr.fid = '".$_COOKIE['fid']."' 
                                           AND sr.deleted='0' 
                                           AND sr.level_id='".$level_id."' 
                                           AND sr.reg_no='".$reg_no."'
                                           AND sr.complete='no'
                                           ORDER BY sr.insert_date DESC LIMIT 1";
                          $result_last_date = mysqli_query($connect, $query_last_date); 
                          while($row_last_date = mysqli_fetch_array($result_last_date))
                          {
                          $last_date = $row_last_date["insert_date"];
                          // $last_date = date("d/m/Y",strtotime($last_date));

                            echo 
                            '<tr><td>
                            '.$studentname.'
                            </td><td>
                            '.$reg_no.'
                            </td><td>
                            '.$language_name.'
                            </td><td>
                            '.$level_name.'
                            </td><td>
                            '.$insert_date.'
                            </td><td>
                            '.$last_date.'
                            </td><td><button class="btn btn-default" onclick=window.location.href="student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'"> View</button></td>
                            <td><button class="btn btn-info" onclick=window.location.href="../class/fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'&id='.base64_encode($r_id).'&complete='.base64_encode('3').'">Complete</button></td>
                            </tr>';
                            
                          }
                          
                        }

                      }
                    }   


                  }
                }
              ?>
                  </tbody>
                </table>

              </div>
            </div>
            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
