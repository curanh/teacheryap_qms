<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    if (isset($_GET['reg_no'])) {

      $reg_no = base64_decode($_GET['reg_no']);
      $studentname = urldecode(base64_decode($_GET['studentname']));
      $active = base64_decode($_GET['active']);
    }
$position = $_COOKIE['position'];
$student_level_class = new Sreport();
$search = "";
$student_level = $student_level_class->student_level($search,$reg_no,$studentname,$active);
$student_level_complete = $student_level_class->student_level_complete($reg_no,$active);
if (isset($_POST['button'])) {
  $search = $_POST['search_text'];
  $student_level = $student_level_class->student_level($search,$reg_no,$studentname,$active);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Student Report Level</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Student Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Level</a></li>
              <li class="breadcrumb-item active">Home</li>
              <li class="breadcrumb-item active">Student Report</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <form method="POST">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <a href="student_report.php" class="btn btn-default">Back</a>
                  <br>
                  <br>
                  <?php
                  if ($position != 'teacher') {
                    echo '
                    <div id="accordion">
                      <div class="card card-primary card-outline">
                      <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                          <div class="card-header">
                              <h4 class="card-title w-100">
                                  Level (touch me)
                              </h4>
                          </div>
                      </a>
                      <div id="collapseOne" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                          <div class="card-body table-responsive p-0">
                          <table class="table table-hover text-nowrap">
                        ';
                              echo $student_level_complete;
                              
                    echo '
                          </table>
                          </div>
                          </div>
                      </div>
                      </div>
                    </div>
                    ';
                   } 
                  ?>
                  
                  <div class="callout callout-info">
                    <?php 
                      echo "<h5>Student name: ".$studentname."</h5>";
                      $query_student="SELECT * FROM student 
                      WHERE reg_no = '".$reg_no."' AND active = '".$active."' ";
                      $result_student = mysqli_query($connect2, $query_student);
                      $row_student = mysqli_fetch_array($result_student);
                      if ($row_student > 0) {
                        echo '<div class="panel-title"><h5>Current school: '.$row_student['current_school'].'</h5></div>';
                        echo '<div class="panel-title"><h5>Mother tongue: '.$row_student['lang'].'</h5></div>';
                        echo '<div class="panel-title"><h5>Age: '.$row_student['age'].'</h5></div>';
                      }else{
                        echo '<div class="panel-title"><h5>Current school:</h5></div>';
                        echo '<div class="panel-title"><h5>Mother tongue:</h5></div>';
                        echo '<div class="panel-title"><h5>Age:</h5></div>';
                      }
                    ?>
                  </div>
                    <div class="input-group input-group-lg">
                        <select class="form-control" id="search_text" name="search_text">
                          <option value="">All</option>
                          <option value="1">华语</option>
                          <option value="2">国语</option>
                          <option value="3">珠算</option>
                          <option value="4">英语</option>
                          <option value="5">PK</option>
                          <option value="6">TT</option>
                          <option value="7">MIQ</option>
                          <option value="8">HOTS 36</option>
                        </select>
                        <div class="input-group-append">
                            <button type="submit" name="button" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                  <?php
                    echo $student_level; 
                  ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
