<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    if (isset($_GET['reg_no'])) {

      $reg_no = base64_decode($_GET['reg_no']);
      $level_id = base64_decode($_GET['level_id']);
      $studentname = urldecode(base64_decode($_GET['studentname']));
      $active = base64_decode($_GET['active']);

    }
$fid = $_COOKIE['fid'];
$student_details = new Sreport();
$details = $student_details->student_level_details($reg_no,$level_id);
$student_details_lms = new Sreport();
$details_lms = $student_details_lms->student_level_details_lms($reg_no,$level_id,$fid,$studentname,$active);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Student Report Level Details</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Student Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Level</a></li>
              <li class="breadcrumb-item active">Home</li>
              <li class="breadcrumb-item active">Student Report</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <form method="POST">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <!-- AREA CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Student Detail</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <?php 
                      echo "<h5>Student name: ".$studentname."</h5>";
                      $query_age = "SELECT * FROM student WHERE reg_no='".$reg_no."'";
                      $result_age = mysqli_query($connect, $query_age);
                      if(mysqli_num_rows($result_age) > 0){
                        while($row_age = mysqli_fetch_array($result_age)){
                          echo "<h5>Age: ".$row_age['age']."</h5>";
                        }
     
                      }

                      // student last record
                      $query_level = "SELECT * FROM level WHERE level_id='".base64_decode($_GET['level_id'])."' ";
                      $result_level = mysqli_query($connect, $query_level);
                      if(mysqli_num_rows($result_level) > 0){
                        while($row_level = mysqli_fetch_array($result_level)){
                          $language = $row_level['language_id'];
                          echo "<h5>Level Name: ".$row_level['level_name']."</h5>";
                        }
     
                      }                  
                              
                  ?>
                </div>
              </div>
              <!-- /.card-body -->
            </div>

          </div>
          <div class="col-md-6">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Student Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                <div id="chartdiv" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></div>
                </div>
                <center>
                <?php
                $query_last_date = "SELECT sr.insert_date, sr.complete, sr.r_id FROM studentresult sr
                WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
                AND sr.level_id='".$level_id."' 
                AND sr.reg_no='".$reg_no."'
                ORDER BY sr.r_id DESC LIMIT 1";
                $result_last_date = mysqli_query($connect, $query_last_date); 
                while($row_last_date = mysqli_fetch_array($result_last_date))
                {
                  if ($row_last_date['complete'] == 'yes') {
                    $query = "SELECT * FROM assess WHERE reg_no = '".$reg_no."' AND level_id = '".$level_id."'";
                    $sql_result = mysqli_query($connect,$query);
                    $rowcount=mysqli_num_rows($sql_result);
                    if ($rowcount == 0) {
                      echo '<a href="../survey_form.php?reg_no='.$reg_no.'&level='.$level_id.'&language='.$language.'&teacher='.$_COOKIE["name"].'&student_name='.urlencode($studentname).'&active='.$active.'" class="btn btn-primary">Survey Form</a>';
                    }
                  }

                }
                ?>
                </center>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">LMS</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                  <?php
                    echo $details_lms;
                  ?>
                  </table>
                  <br>
                  <a href="student_report_level.php?reg_no=<?php echo $_GET['reg_no']; ?>&studentname=<?php echo $_GET['studentname'];?>&active=<?php echo $_GET['active'];?>" class="btn btn-default">Back</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">QMS</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-body table-responsive p-0">
                  <a href="not_complete_report.php" class="btn btn-default">Not Complete Report</a>
                  <table class="table table-hover text-nowrap">
                  <?php
                    echo $details;
                  ?>
                  </table>
                  <br>
                  <a href="student_report_level.php?reg_no=<?php echo $_GET['reg_no']; ?>&studentname=<?php echo $_GET['studentname'];?>&active=<?php echo $_GET['active'];?>" class="btn btn-default">Back</a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<script src="..//js/amcharts.js" type="text/javascript"></script>
<script src="..//js/radar.js" type="text/javascript"></script>
<script src="..//js/dataloader.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  var urlParams = new URLSearchParams(window.location.search);
  var reg_no = urlParams.get('reg_no');
  var level_id = urlParams.get('level_id');
  var chart = AmCharts.makeChart("chartdiv", {
  type: "radar",
  dataLoader: {
      "url": "chart.php?reg_no="+reg_no+"&level_id="+level_id+"",
      "format": "json"
  },
  categoryField: "title",
  startDuration: 2,

  valueAxes: [{
      axisAlpha: 0.15,
      minimum: 0,
      dashLength: 3,
      axisTitleOffset: 20,
      gridCount: 5
  }],

  graphs: [{
      valueField: "assess_title",
      bullet: "round",
      balloonText: "[[title]]  [[value]]",
  }],
  "export": {
    "enabled": true,
    "libs": {
      "autoLoad": false
    }
  }
  });
</script>
</body>
</html>
