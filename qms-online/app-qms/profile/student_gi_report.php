<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
$fid = $_COOKIE['fid'];
$status = '0';
$age = '> 0';
$gi_report = new Sreport();
$gi_report = $gi_report->gi_report($status,$age,$fid);
if (isset($_POST['button'])) {
  $status = $_POST['search_text'];
  $age = $_POST['search_age'];
  $gi_report = new Sreport();
  $gi_report = $gi_report->gi_report($status,$age,$fid);
}
$query = "
SELECT ct.name, st.studentname, st.age, la.language_id, le.level_name, la.language_name, 
sr.level_id, sr.reg_no 
FROM centre ct
LEFT JOIN student st ON (st.fid = ct.fid) 
LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
LEFT JOIN level le ON (le.level_id = sr.level_id)
LEFT JOIN language la ON (la.language_id = sr.language_id) 
WHERE ct.fid = '$fid'
AND st.active = 'Yes'
AND (sr.deleted IS NULL OR sr.deleted = '0') 
AND (sr.trial = 'No' OR sr.trial = '')
GROUP BY sr.level_id 
ORDER BY sr.level_id ASC
";
//echo $query; exit();
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $pass = '0';
 $fail = '0';
 $not = '0';
 while($row = mysqli_fetch_array($result))
 { 
 

  $query4="SELECT * FROM studentresult sr
  LEFT JOIN student st ON (st.reg_no = sr.reg_no)
  WHERE sr.fid = '$fid' 
  AND sr.level_id = '".$row['level_id']."'
  AND st.active ='Yes'
  AND (sr.deleted IS NULL OR sr.deleted = '0') 
  AND (sr.trial = 'No' OR sr.trial = '')
  GROUP BY sr.reg_no";

  if($result4 = mysqli_query($connect, $query4))
  {
               
      while($row4 = mysqli_fetch_array($result4))
      {
        $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
        FROM studentresult sr 
        LEFT JOIN level le ON (sr.level_id = le.level_id) 
        LEFT JOIN language la ON (sr.language_id = la.language_id) 
        WHERE sr.reg_no='".$row4['reg_no']."' 
        AND sr.level_id = '".$row4['level_id']."' 
        AND (sr.deleted IS NULL OR sr.deleted = 0)
        AND (sr.trial = 'No' OR sr.trial = '')";

        if($result2 = mysqli_query($connect, $query2))
        {
                     
            while($row2 = mysqli_fetch_array($result2))
            {
              $total_class = $row2['total_class'];
              $total = $row2['total'];

              $query3 = "
                SELECT sr.complete FROM studentresult sr
                LEFT JOIN level le ON (le.level_id=sr.level_id)
                LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
                WHERE sr.level_id = '".$row2['level_id']."'
                AND st.active = 'Yes'
                AND sr.reg_no = '".$row2['reg_no']."'
                AND (sr.deleted IS NULL OR sr.deleted = '0') 
                AND (sr.trial = '' OR sr.trial = 'No')
                ORDER BY sr.r_id DESC LIMIT 1
                ";
                if($result3 = mysqli_query($connect, $query3))
                {
                             
                    while($row3 = mysqli_fetch_array($result3))
                    {
                        
                      if ($total_class == '0') {
                        }
                        elseif($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name']) AND !empty($total_class)){
                          $pass ++;
                        }
                        elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name']) AND !empty($total_class)) {
                          $fail ++;
                        }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name']) AND !empty($total_class)) {
                          $not ++;
                        }
                      }
                  }
            }          
        }
      }
  }

 }
}
$dataPoints = array( 
array("label"=>"Not Complete", "symbol" => "Not Complete","y"=>$not),
array("label"=>"Pass", "symbol" => "Pass","y"=>$pass),
array("label"=>"Fail", "symbol" => "Fail","y"=>$fail),
);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Guarantee Improvement Report</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Guarantee Improvement Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Guarantee Improvement Report</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <form method="POST">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Student Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <div id="chartContainer" style="height: 250px; width: 100%;"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>

          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <select class="form-control" id="search_age" name="search_age">
                          <option value="> 0" selected>All</option>
                          <?php
                          if ($age == "<= 6") {
                            echo '<option value="<= 6" selected>3 - 6 Age</option>';
                          }else{
                            echo '<option value="<= 6">3 - 6 Age</option>';
                          }
                          if ($age == ">= 7") {
                            echo '<option value=">= 7" selected>7 - 12 Age</option>';
                          }else{
                            echo '<option value=">= 7">7 - 12 Age</option>';
                          }
                          ?>
                        </select>

                        <select class="form-control" id="search_text" name="search_text">
                          <option value="0" selected>All</option>
                          <?php
                          if ($status == "1") {
                            echo '<option value="1" selected>Pass</option>';
                          }else{
                            echo '<option value="1">Pass</option>';
                          }
                          if ($status == "2") {
                            echo '<option value="2" selected>Fail</option>';
                          }else{
                            echo '<option value="2">Fail</option>';
                          }
                          if ($status == "3") {
                            echo '<option value="3" selected>Not Complete</option>';
                          }else{
                            echo '<option value="3">Not Complete</option>';
                          }
                          if ($status == "4") {
                            echo '<option value="4" selected>No KPI Class</option>';
                          }else{
                            echo '<option value="4">No KPI Class</option>';
                          }
                          ?>
                        </select>
                        <div class="input-group-append">
                            <button type="submit" name="button" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                  <?php
                    echo $gi_report;
                  ?>
                  </table>
                  <br>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<script src="..//js/amcharts.js" type="text/javascript"></script>
<script src="..//js/radar.js" type="text/javascript"></script>
<script src="..//js/dataloader.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  window.onload = function() {
 
  var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",
    animationEnabled: true,
    data: [{
      type: "doughnut",
      indexLabel: "{symbol} - {y}",
      yValueFormatString: "#,##0\"\"",
      showInLegend: true,
      legendText: "{label} : {y}",
      dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render();
   
  }
</script>
</body>
</html>
