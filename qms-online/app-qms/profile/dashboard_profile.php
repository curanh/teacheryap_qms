<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
  $position = $_COOKIE['position'];
  $name = $_COOKIE['name'];

  // last synced date
  $query_last_sync = "
  SELECT * FROM studentresult
  WHERE fid = '".$_COOKIE['fid']."'
  AND deleted='0' ORDER BY created_date DESC LIMIT 1
  ";
    $result_last_sync = mysqli_query($connect, $query_last_sync);            
  while($row_last_sync = mysqli_fetch_array($result_last_sync))
    {
    $last_synced_date = $row_last_sync['created_date'];
    $last_synced_date = date("d/m/Y h:ia",strtotime($last_synced_date));
  } 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Last tablet data sycned on <b><?php echo $last_synced_date; ?></b></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th width="10%">Last Progress</th>
                  <th width="15%">Student</th>
                  <th width="10%">Language</th>
                  <th width="20%">Level</th>
                  <th width="35%">Title</th>
                  <th width="10%">Teacher</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    if ($position == 'teacher') {
                      $query_text = " AND sr.teachername = '$name' ";
                    }else{
                      $query_text="";
                    }
                    $query_student = "SELECT s.reg_no, s.studentname FROM student s  
                    WHERE s.fid = '".$_COOKIE['fid']."' 
                    AND s.active='Yes' ORDER BY s.studentname ASC";

                      $result_student = mysqli_query($connect, $query_student);          
                    while($row_student = mysqli_fetch_array($result_student))
                      {
                      $reg_no = $row_student["reg_no"];
                      $studentname = $row_student["studentname"];
                      
                      $query_student_result = "SELECT sr.method, lg.language_name, lv.level_name, sr.title_id, sr.teachername, sr.insert_date, sr.level_id FROM studentresult sr
                       LEFT JOIN level lv ON (sr.level_id=lv.level_id) 
                       LEFT JOIN language lg ON (lv.language_id = lg.language_id)
                       WHERE sr.fid = '".$_COOKIE['fid']."'
                       ".$query_text." 
                       AND sr.deleted='0' 
                       AND sr.reg_no='".$reg_no."' 
                       ORDER BY sr.insert_date DESC LIMIT 1";
                      $result_student_result = mysqli_query($connect, $query_student_result); 

                      while($row_student_result = mysqli_fetch_array($result_student_result))
                      {
                        $language_name = $row_student_result["language_name"];
                        $level_name = $row_student_result["level_name"];
                        $title_id = $row_student_result["title_id"];
                        $teachername = $row_student_result["teachername"];
                        $insert_date = $row_student_result["insert_date"];
                        $level_id = $row_student_result["level_id"];
                        $method = $row_student_result["method"];

                        //$insert_date = date("d/m/Y",strtotime($insert_date));

                        echo "<tr><td>
                             ".$insert_date."
                             </td><td>
                             ".$studentname."
                             </td><td>
                             ".$language_name."
                             </td><td>
                             ".$level_name."
                             </td><td>
                             ";
                        if ($method == 'Yes') {
                        
                          $query_title = "
                          SELECT * FROM level_title 
                          WHERE status = 1 
                          AND (deleted IS NULL OR deleted = 0) 
                          AND level_id = '".$level_id."' 
                          AND title_id IN (".$title_id.") 
                          ORDER BY sorting ASC";

                          $result_title = mysqli_query($connect,$query_title);
                          while($row_titleid=mysqli_fetch_assoc($result_title))
                          {   

                            echo $row_titleid['title_name'].'<br>';
                            
                          }

                        }else{
                          echo $title_id;
                        }

                        echo "</td><td>
                             ".$teachername."
                             </td></tr>";
                      }
                    }
                  ?>
                  </tbody>
                </table>

              </div>
            </div>
            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
