<?php
require_once("..//dbConfig.php");
    session_start();
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
$fid = $_COOKIE['fid'];
$teachername = base64_decode($_GET['teachername']);
$teacher_kpi_details_class = new Report();
$search_text = "";
$teacher_kpi_details = $teacher_kpi_details_class->teacher_kpi_details($search_text,$fid,$teachername);
if (isset($_POST['button'])) {
$search_text = $_POST['search_text'];
$teacher_kpi_details = $teacher_kpi_details_class->teacher_kpi_details($search_text,$fid,$teachername);
$class = 'show';
}

$teacher_kpi_details_class_lms = new Report();
$search_text_lms = "";
$teacher_kpi_details_lms = $teacher_kpi_details_class_lms->teacher_kpi_details_lms($search_text_lms,$fid,$teachername);
if (isset($_POST['button1'])) {
$search_text_lms = $_POST['search_text_lms'];
$teacher_kpi_details_lms = $teacher_kpi_details_class_lms->teacher_kpi_details_lms($search_text_lms,$fid,$teachername);
$class1 = 'show';
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Teacher KPI Report</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="..//plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="..//dist/css/adminlte.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="..//plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="..//Images/Logo/YelaoShrBaby.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_COOKIE['name']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="dashboard_profile.php" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">All Report</li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                All Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="teacher_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_daily.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Daily Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_report_monthly.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher Monthly Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="not_complete_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Not Complete Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="teacher_kpi_report.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Teacher KPI Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="student_gi_report.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Guarantee Improvement Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="../student_list.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">BACK</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Teacher KPI Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Details</a></li>
              <li class="breadcrumb-item active">Home</li>
              <li class="breadcrumb-item active">Teacher KPI Report</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <form method="POST">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
              <!-- /.card-header -->
              <div id="accordion">
                <div class="card card-primary card-outline">
                  <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                      <div class="card-header">
                          <h4 class="card-title w-100">
                              QMS (touch me)
                          </h4>
                      </div>
                  </a>
                  <?php
                    if (!empty($class)) {
                      echo '<div id="collapseOne" class="collapse show" data-parent="#accordion">';
                    }else{
                      echo '<div id="collapseOne" class="collapse" data-parent="#accordion">';
                    }
                  ?>
                  <!-- <div id="collapseOne" class="collapse" data-parent="#accordion"> -->
                    <div class="card-body">
                      <div class="card-body table-responsive p-0">
                      <div class="form-group">
                          <div class="card-body table-responsive p-0">
                            <div class="input-group input-group-lg">
                                <input type="text" class="form-control" id="search_text" name="search_text" placeholder="Search by Student Name">
                                <div class="input-group-append">
                                    <button type="submit" name="button" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <table class="table table-hover text-nowrap">
                            <?php
                              echo $teacher_kpi_details;
                            ?>
                            </table>
                            <br>
                            <a href="teacher_kpi_report.php" class="btn btn-default">Back</a>
                          </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <div id="accordion">
                <div class="card card-primary card-outline">
                  <a class="d-block w-100" data-toggle="collapse" href="#collapseOne1">
                      <div class="card-header">
                          <h4 class="card-title w-100">
                              LMS (touch me)
                          </h4>
                      </div>
                  </a>
                  <?php
                    if (!empty($class1)) {
                      echo '<div id="collapseOne1" class="collapse show" data-parent="#accordion">';
                    }else{
                      echo '<div id="collapseOne1" class="collapse" data-parent="#accordion">';
                    }
                  ?>
                    <div class="card-body">
                      <div class="card-body table-responsive p-0">
                      <div class="form-group">
                          <div class="card-body table-responsive p-0">
                            <div class="input-group input-group-lg">
                                <input type="text" class="form-control" id="search_text" name="search_text_lms" placeholder="Search by Student Name">
                                <div class="input-group-append">
                                    <button type="submit" name="button1" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <table class="table table-hover text-nowrap">
                            <?php
                              echo $teacher_kpi_details_lms;
                            ?>
                            </table>
                            <br>
                            <a href="teacher_kpi_report.php" class="btn btn-default">Back</a>
                          </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="..//plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="..//plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="..//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="..//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="..//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="..//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="..//plugins/jszip/jszip.min.js"></script>
<script src="..//plugins/pdfmake/pdfmake.min.js"></script>
<script src="..//plugins/pdfmake/vfs_fonts.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="..//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="..//dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page specific script -->
<script>

  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
