<?php 
    require_once("dbConfig.php");
    if ($_COOKIE['fid'] == "") {
        header("Location: login.php");

    }
    $fid = $_COOKIE['fid'];
    $position = $_COOKIE['position'];
    $level_id = $_GET['level_id'];
    $reg_no = $_GET['regno'];
    $teachername = $_GET['teachername'];
    $title_id = $_GET['title_id']; 

    if ($position == 'teacher') {
      $query_set = "teacher_read = 1";
    }elseif ($position == 'buddy') {
      $query_set = "buddy_read = 1";
    }elseif ($position == 'cm') {
      $query_set = "cm_read = 1";
    }
        // $update = explode(',', $g_id);
        // echo $value;
        // echo "<br>";
        $query = "UPDATE notifications SET ".$query_set." WHERE teachername='".$teachername."' AND reg_no = '".$reg_no."' 
        AND level_id = '".$level_id."' AND title_id = '".$title_id."'";
        // echo $query;
        $result = mysqli_query($connect,$query); 

        if (!mysqli_query($connect,$query))
        {
              die('Error: ' . mysqli_error($connect));
        }
  
    // echo $value;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>QMS</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>
<style type="text/css">
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #808080;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
</style>
<body class="animsition">
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="Images/Logo/YelaoShrBaby.png">
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a href="profile/dashboard_profile.php">
                                            <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                        <a href="#" onclick="logout()">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="student_list.php">
                            <h3 style="color: #ffffff;">QMS</h3>
                            <p>Online version</p>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">

                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        
                        <div class="image">
                            <img src="Images/Logo/YelaoShrBaby.png">
                        </div>
                        <div class="content">
                            <a class="js-acc-btn"><?php echo $_COOKIE['name']; ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="profile/dashboard_profile.php">
                                            <i class="zmdi zmdi-assignment-account"></i>My Profile</a>
                                <a href="#" onclick="logout()">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb3">
                                <div class="au-breadcrumb-left">
                                    <!-- <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item  seprate">
                                            <a href="all_notifications.php">Notifications</a>
                                        </li>
                                        <li class="list-inline-item">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item seprate">Notifications Detail</li>
                                    </ul>-->
                                    <li class="list-inline-item seprate">Notifications Detail</li>
                                    <br>
                                    <br>                                  
                                    <a href="all_notifications.php" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <div class="alert alert-secondary" role="alert">
                                <?php
                                  $query_g_id = "SELECT * FROM notifications WHERE teachername='".$teachername."' AND reg_no = '".$reg_no."' 
                                  AND level_id = '".$level_id."' AND title_id = '".$title_id."'
                                  ";
                                  // echo $query_student_result;
                                  $result_g_id = mysqli_query($connect, $query_g_id); 
                                  $g_array = array();
                                  while($row_g_id = mysqli_fetch_array($result_g_id))
                                  {
                                    $value = $row_g_id['g_id'];
                                    array_push($g_array, $value);
                                    $g_id = join(",",$g_array);
                                    $query_get = "SELECT la.language_name, le.level_name, st.studentname,sr.teachername FROM studentresult sr
                                    LEFT JOIN language la ON (la.language_id = sr.language_id)
                                    LEFT JOIN level le ON (le.level_id = sr.level_id)
                                    LEFT JOIN student st ON (st.reg_no = sr.reg_no)
                                    WHERE sr.g_id = '".$value."'
                                    AND st.active = 'Yes'
                                    ";
                                    // echo $query_student_result;
                                    $result_get = mysqli_query($connect, $query_get); 

                                    while($row_get = mysqli_fetch_array($result_get))
                                    {
                                      $teachername = $row_get['teachername'];
                                      $language_name = $row_get['language_name'];
                                      $level_name = $row_get['level_name'];
                                      $studentname = $row_get['studentname'];
                                      
                                    }

                                  }
                                  echo '<div class="panel-title">Teachername: '.$teachername.'</div>';
                                  echo '<div class="panel-title">Language: '.$language_name.'</div>';
                                  echo '<div class="panel-title">Level: '.$level_name.'</div>';
                                  echo '<div class="panel-title">Student: '.$studentname.'</div>';
     
                                ?>
                            </div>
                            <div class="welcome2-inner m-t-60">
                            <table id="example1" class="table table-data2">
                              <thead>
                              <tr>
                              <th width="10%">Date</th>
                              <th width="15%">status</th>
                              <th width="15%">Title id</th>
                              <th width="15%">Continue Title</th>
                              <th width="15%">Reminder</th>
                              <th width="15%">Feedback</th>
                              <!-- <th width="10%">Teacher</th> -->
                              </tr>
                              </thead>
                              <tbody>
                              <?php
                              $update = explode(',', $g_id);
                              foreach ($update as $key => $value1) {

                                  $query_student_result = "SELECT * FROM studentresult WHERE g_id = '".$value1."' ";
                                  $result_student_result = mysqli_query($connect, $query_student_result); 

                                  while($row_student_result = mysqli_fetch_array($result_student_result))
                                  {
                                    $title_id = $row_student_result["title_id"];
                                    $insert_date = $row_student_result["insert_date"];
                                    $continue_id = $row_student_result["continue_id"];
                                    $reminder = $row_student_result["continue_title"];
                                    $feedback = $row_student_result["feedback"];
                                    $student_status = $row_student_result["student_status"];
                                    $level_id = $row_student_result["level_id"];

                                    $insert_date = date("d/m/Y",strtotime($insert_date));

                                    echo "<tr><td>
                                         ".$insert_date."
                                         </td><td>
                                         ".$student_status."
                                         </td><td>
                                         ";
                                    
                                      $query_title = "
                                      SELECT * FROM level_title 
                                      WHERE status = 1 
                                      AND (deleted IS NULL OR deleted = 0) 
                                      AND level_id = '".$level_id."' 
                                      AND title_id IN (".$title_id.") 
                                      ORDER BY sorting ASC";

                                      $result_title = mysqli_query($connect,$query_title);
                                      while($row_titleid=mysqli_fetch_assoc($result_title))
                                      {   

                                        echo $row_titleid['title_name'].'<br>';
                                        
                                      }

                                    echo "</td><td>";
                                      $query_continue = "
                                      SELECT * FROM level_title 
                                      WHERE status = 1 
                                      AND (deleted IS NULL OR deleted = 0) 
                                      AND level_id = '".$level_id."' 
                                      AND title_id IN (".$continue_id.") 
                                      ORDER BY sorting ASC";

                                      $result_continue = mysqli_query($connect,$query_continue);
                                      while($row_continue=mysqli_fetch_assoc($result_continue))
                                      {   

                                        echo $row_continue['title_name'].'<br>';
                                        
                                      }
                                    echo "</td>";
                                    echo "<td>
                                         ".$reminder."
                                         </td>
                                         <td>
                                         ".$feedback."
                                         </td>
                                         ";
                                  }
                                  
                              }  
                              ?>
                              </tbody>
                            </table>      
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->
            <center>
                <div id="loader" style="display: none;"><img src="Images/gif/bye.gif" width="200" height="200"></div>
            </center>
            <!-- DATA TABLE-->
            <section class="p-t-20" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <script type="text/javascript">
    function logout() {
        // document.getElementById("myDiv").style.display = "none";
            var result = confirm('Are you sure to LOGOUT?');
            if (result) {
                document.getElementById("myDiv").style.display = "none";
                document.getElementById("loader").style.display = "block";
                setTimeout(function() {
                    window.location.href = "login.php";
                }, 1800);
                // document.getElementById("loader").style.display = "block";
                // window.location.href = "login.php";
            }
            
    }   

    </script>
</body>

</html>
<!-- end document-->
