<?php

/**
  * 
  */
 class Records
 {

 	function my_insert_records()
    {
    	$value = "";
    	include 'dbConfig.php';
        $date = date("Y-m-d");
		// echo '<script type="text/javascript">alert("'.$date.'")</script>';
		$query = $connect->query("SELECT sr.*, lang.language_name, le.level_name, ti.title_name,
		st.studentname 
		FROM studentresult sr 
		LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
		LEFT JOIN level le ON (sr.level_id = le.level_id) 
		LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
		LEFT JOIN student st ON (sr.reg_no = st.reg_no) 
		WHERE sr.teachername='".$_COOKIE['name']."' 
		AND (sr.deleted IS NULL OR sr.deleted = 0) 
		AND sr.insert_date = '".$date."'
		AND sr.trial = 'No' 
		ORDER BY sr.insert_date DESC, sr.r_id DESC");
		  $rowCount = $query->num_rows;
		  if($rowCount > 0){
		    while($row = $query->fetch_assoc()){
		        if ($row['title_id'] == '') {
		            $row['title_id'] = '<br>';
		        }
		      $insert_date = $row["insert_date"];
		      $new_date = date("d/m/Y",strtotime($insert_date));
		      if ($row['student_status'] == 'Completed' OR $row['student_status'] == 'Skip' OR $row['student_status'] == 'Completed & Not Stable') {

		       $value .="<br>
		        &nbsp;<span><strong>".$new_date."</strong><br></span>
		        <span>Name : ".$row['studentname']."<br></span>
		        <span>Time : ".$row['times1']."<br></span>
		        <span>Level : ".$row['level_name']."<br></span>
		        <span>Title :
		       ";
		       $query_titleid = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['title_id'].") ORDER BY sorting ASC";
              	$result_titleid = mysqli_query($connect,$query_titleid);
              	while($row_title=mysqli_fetch_assoc($result_titleid))
              	{
                	$value .="".$row_title['title_name']."
                	<br>";
              	}
		       $value.="
		       	</span>
		        <span>Feedback : ".$row['feedback']."</span><br>
		       	<span>Status : <span style='color:limegreen;'>".$row['student_status']."</span></span>
		        <hr>";

		      }
		      else{
		       $value .="<br>
		        &nbsp;<span><strong>".$new_date."</strong><br></span>
		        <span>Name : ".$row['studentname']."<br></span>
		        <span>Time : ".$row['times1']."<br></span>
		        <span>Level : ".$row['level_name']."<br></span>
		        <span>Title :";
		        $query_titleid = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['title_id'].") ORDER BY sorting ASC";
              	$result_titleid = mysqli_query($connect,$query_titleid);
              	while($row_title=mysqli_fetch_assoc($result_titleid))
              	{
                	$value .="".$row_title['title_name']."
                	<br>";
              	}
              	$value.="
              	</span>
		        <span>Feedback : ".$row['feedback']."</span><br>
		       	<span>Status : <span style='color:#FF0000;'>".$row['student_status']."</span></span><br>
		       	<span>Continue Title : <span style='color:#FF0000;'>";
		       	$query_titleco = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['continue_id'].") ORDER BY sorting ASC";
	              $result_titleco = mysqli_query($connect,$query_titleco);
	              while($row_titco=mysqli_fetch_assoc($result_titleco))
	              {
	                $value .= "".$row_titco['title_name']."
	                <br>";
	              }
	             $value.="
	            </span>
	            </span>
		       	<span>Reminder : <span style='color:red;'>".$row['continue_title']."</span></span>
		        <hr>";
		      }

		      }
		  }else{
		     $value = "No Record";
		  }

		  return($value);
    }

    function trial_my_insert_records(){
    	include 'dbConfig.php';
    	$date = date("Y-m-d");
        // echo '<script type="text/javascript">alert("'.$date.'")</script>';
        $query = $connect->query("SELECT sr.*, lang.language_name, le.level_name, ti.title_name, st.studentname 
        FROM studentresult sr 
        LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
        LEFT JOIN level le ON (sr.level_id = le.level_id) 
        LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
        LEFT JOIN trial_student st ON (sr.reg_no = st.ic_number) 
        WHERE sr.teachername='".$_COOKIE['name']."' 
        AND (sr.deleted IS NULL OR sr.deleted = 0) 
        AND sr.insert_date = '".$date."'
        AND sr.trial = 'Yes' 
        ORDER BY sr.insert_date DESC, sr.r_id DESC");
          $rowCount = $query->num_rows;
          if($rowCount > 0){
            while($row = $query->fetch_assoc()){
                if ($row['title_id'] == '') {
                    $row['title_id'] = '<br>';
                }
              $insert_date = $row["insert_date"];
              $new_date = date("d/m/Y",strtotime($insert_date));
              if ($row['student_status'] == 'Completed' OR $row['student_status'] == 'Skip' OR $row['student_status'] == 'Completed & Not Stable') {

		       $value .="<br>
		        &nbsp;<span><strong>".$new_date."</strong><br></span>
		        <span>Name : ".$row['studentname']."<br></span>
		        <span>Time : ".$row['times1']."<br></span>
		        <span>Level : ".$row['level_name']."<br></span>
		        <span>Title :
		       ";
		       $query_titleid = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['title_id'].") ORDER BY sorting ASC";
              	$result_titleid = mysqli_query($connect,$query_titleid);
              	while($row_title=mysqli_fetch_assoc($result_titleid))
              	{
                	$value .="".$row_title['title_name']."
                	<br>";
              	}
		       $value.="
		       	</span>
		        <span>Feedback : ".$row['feedback']."</span><br>
		       	<span>Status : <span style='color:limegreen;'>".$row['student_status']."</span></span>
		        <hr>";

		      }
		      else{
		       $value .="<br>
		        &nbsp;<span><strong>".$new_date."</strong><br></span>
		        <span>Name : ".$row['studentname']."<br></span>
		        <span>Time : ".$row['times1']."<br></span>
		        <span>Level : ".$row['level_name']."<br></span>
		        <span>Title :";
		        $query_titleid = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['title_id'].") ORDER BY sorting ASC";
              	$result_titleid = mysqli_query($connect,$query_titleid);
              	while($row_title=mysqli_fetch_assoc($result_titleid))
              	{
                	$value .="".$row_title['title_name']."
                	<br>";
              	}
              	$value.="
              	</span>
		        <span>Feedback : ".$row['feedback']."</span><br>
		       	<span>Status : <span style='color:#FF0000;'>".$row['student_status']."</span></span><br>
		       	<span>Continue Title : <span style='color:#FF0000;'>";
		       	$query_titleco = "SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) AND level_id = '".$row['level_id']."' AND title_id IN (".$row['continue_id'].") ORDER BY sorting ASC";
	              $result_titleco = mysqli_query($connect,$query_titleco);
	              while($row_titco=mysqli_fetch_assoc($result_titleco))
	              {
	                $value .= "".$row_titco['title_name']."
	                <br>";
	              }
	             $value.="
	            </span>
	            </span>
		       	<span>Reminder : <span style='color:red;'>".$row['continue_title']."</span></span>
		        <hr>";
		      }

		      }
		  }else{
		     $value = "No Record";
		  }

          return($value);
}

 } 
?>