<?php

/**
  * 
  */
 class report
 {

 	function teacher_report($search,$name)
    {
    	$fid = $_COOKIE['fid'];
    	$value = "";
    	include '..//dbConfig.php';
    	if ($search == "") {
    	$query = $connect->query("SELECT la.language_name, le.level_name, le.level_id FROM language la 
	  	LEFT JOIN level le ON(la.language_id = le.language_id) 
	  	ORDER BY la.language_id
	 	");
    	}else{
    	$query = $connect->query("SELECT la.language_name, le.level_name, le.level_id FROM language la 
	  	LEFT JOIN level le ON(la.language_id = le.language_id) 
	  	WHERE la.language_id = '".$search."' 
	  	ORDER BY la.language_id
	 	");
    	}
		  $rowCount = $query->num_rows;
		  if($rowCount > 0){
		  	$value .= '
			  <thead>
			    <tr>
			      <th>Language</th>
			      <th>Level</th>
			      <th>Student</th>
			      <th>Total Class</th>
			      <th>All Student</th>
			    </tr>
			  </thead>
			 ';
		    while($row = $query->fetch_assoc())
		    {
		    	  $level_id = $row['level_id'];
				    //echo $level_id;
		    	  
				  $query1 = "
				    SELECT sr.reg_no FROM studentresult sr
				    LEFT JOIN student st ON (sr.reg_no = st.reg_no)
				    WHERE sr.level_id = $level_id 
				    AND st.active = 'Yes'
				    AND sr.fid = '".$fid."'
				    AND (sr.deleted IS NULL OR sr.deleted = '0') 
				    AND (sr.trial = '' OR sr.trial = 'No')
				    AND sr.teachername = '".$name."' GROUP BY sr.reg_no
				  ";
				  // echo $query1; exit();


				  if($result1 = mysqli_query($connect, $query1))
				  {
				  $rowcount=mysqli_num_rows($result1);
				  }
				   $query2 = "
				     SELECT sr.r_id FROM studentresult sr
				     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
				     WHERE sr.level_id = $level_id 
				     AND st.active = 'Yes'
				     AND sr.fid = '".$fid."' 
				     AND (sr.deleted IS NULL OR sr.deleted = '0') 
				     AND (sr.trial = '' OR sr.trial = 'No')
				     AND sr.teachername = '".$name."'
				  	";
				  	// echo $query2; exit();
				  if($result2 = mysqli_query($connect, $query2))
				  //($row2 = mysqli_fetch_array($result2))
				  {
				    $rowcount1=mysqli_num_rows($result2);
				  }
				  if ($rowcount > 0) {
				  $value .='
				  <tbody>
				    <tr>
				      <td><strong>'.$row["language_name"].'</strong></td>
				      <td>'.$row["level_name"].'</td>
				      <td>'.$rowcount.'</td>
				      <td>'.$rowcount1.'</td>
				      <td><a href="teacher_report_student.php?fid='.base64_encode($fid).'&level_id='.base64_encode($row["level_id"]).'&teachername='.base64_encode(urlencode($name)).'" class="btn btn-default">View</a></td>
				    </tr>
				   </tbody>
				  ';
				  }
		    }
		  }
		  return($value);
    }

    function all_teacher_report($search){
    		$fid = $_COOKIE['fid'];
    		$value = "";
    		include '..//dbConfig.php';
    		if($search == "")
			{
			 $query = "
			  SELECT * FROM teacher WHERE fid = '$fid' AND deleted = '0' ORDER BY username
			 ";
			}
			else
			{
			$query = "
			  SELECT * FROM teacher 
			  WHERE fid = '$fid' AND username LIKE '%".$search."%' AND deleted = '0' ORDER BY username
			 ";
			}
			$result = mysqli_query($connect, $query);
			if(mysqli_num_rows($result) > 0)
			{
			 $value .= '
			 <thead>
			  <tr>
			  <th>#</th>
			  <th>Teacher Name</th>
			  <th></th>
			  </tr>
			  </thead>
			 ';
			 while($row = mysqli_fetch_array($result))
			 {
			  $value .= '
			  <tbody>
			  <tr>
			  <td>'.$row["id"].'</td>
			  <td>'.$row["username"].'</td>
			  <td><a href="teacher_report_details.php?teacher_name='.base64_encode(urlencode($row["username"])).'" class="btn btn-warning">Report</a></td>
			  </tr>
			  </tbody>
			  ';
			 }
		}
		return($value);
    }


    function teacher_report_student($search_name,$fid,$teachername,$level_id){
    	$position = $_COOKIE['position'];
    	if ($position == 'teacher') {
    		$query_value = " AND sr.teachername = '$teachername' ";
    	}else{
    		$query_value='';
    	}
    	$value = "";
    	include '..//dbConfig.php';
    	if($search_name == "")
		{
		 $query = "
		  SELECT sr.reg_no, st.studentname FROM studentresult sr 
		  LEFT JOIN student st ON sr.reg_no = st.reg_no
		  WHERE sr.level_id = '$level_id' 
		  AND sr.fid = '$fid' 
		  AND st.active = 'Yes'
		  ".$query_value."
		  AND (sr.deleted IS NULL OR sr.deleted = '0') 
		  AND (sr.trial = '' OR sr.trial = 'No')
		  GROUP BY sr.reg_no, st.studentname
		 ";
		 // echo $query; exit();
		}
		else
		{
		$query = "
		 SELECT sr.reg_no, st.studentname FROM studentresult sr 
		 LEFT JOIN student st ON sr.reg_no = st.reg_no 
		 WHERE sr.level_id = '$level_id' 
		 AND sr.fid = '$fid'
		 AND st.active = 'Yes'
		 ".$query_value."
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 AND st.studentname LIKE '%".$search_name."%' 
		 GROUP BY sr.reg_no, st.studentname
		 ";
		}
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		 <thead>
		    <tr>
		      <th>Reg_No</th>
		      <th>Student Name</th>
		      <th>Total Class</th>
		      <th></th>
		    </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $query2 = "
		     SELECT sr.r_id FROM studentresult sr
		     WHERE sr.level_id = $level_id 
		     AND sr.fid = $fid 
		     ".$query_value."
		     AND (sr.deleted IS NULL OR sr.deleted = '0') 
		     AND (sr.trial = '' OR sr.trial = 'No')
		     AND sr.reg_no = '".$row["reg_no"]."'
		  ";
		 if($result2 = mysqli_query($connect, $query2))
		  //($row2 = mysqli_fetch_array($result2))
		    
		   {
		    $rowcount1=mysqli_num_rows($result2); 
		    $value .= '
		    <tbody>
		    <tr>
		    <td>'.$row["reg_no"].'</td>
		    <td>'.$row["studentname"].'</td>
		    <td>'.$rowcount1.'</td>
		    <td><a href="teacher_student_report.php?reg_no='.base64_encode($row["reg_no"]).'&teachername='.base64_encode(urlencode($teachername)).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode(urlencode($row["studentname"])).'&fid='.base64_encode($fid).'" class="btn btn-default">View</a></td>
		    </tr>
		    </tbody>
		  ';
		  }
		 }

		}
		else
		{
		 $value = 'Data Not Found';
		}

		return ($value);
    }


    function teacher_student_report ($fid,$teachername,$level_id,$reg_no){
    	$position = $_COOKIE['position'];
    	if ($position == 'teacher') {
    		$query_value = " AND sr.teachername = '$teachername' ";
    	}else{
    		$query_value='';
    	}
    	$value = "";
    	include '..//dbConfig.php';
    	$query = "
		SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr 
		LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
		LEFT JOIN level le ON (sr.level_id = le.level_id) 
		LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
		LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		WHERE sr.reg_no='".$reg_no."' AND sr.level_id='".$level_id."'
		AND st.active = 'Yes' 
		".$query_value." 
		AND sr.fid = '".$fid."' 
		AND (sr.deleted IS NULL OR sr.deleted = '0')
		AND (sr.trial = '' OR sr.trial = 'No') ORDER BY sr.insert_date DESC
		";

		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		    <tr>
		      <th>Date</th>
		      <th>Level</th>
		      <th>Teacher</th>
		      <th>Feedback</th>
		      <th>Title</th>
		      <th>Status</th>
		    </tr>
		  </thead>
		 ';

		 while($row = mysqli_fetch_array($result))
		 {
		    //echo $level_id;
		    $value .= '
			<tbody>
			    <tr>
			      <td><strong>'.$row["insert_date"].'</strong></td>
			      <td>'.$row["level_name"].'</td>
			      <td>'.$row["teachername"].'</td>
			      <td>'.$row["feedback"].'</td>
			      <td>
			';
			if ($row['method'] == 'Yes') {
				$query_title = "
				SELECT * FROM level_title 
				WHERE status = 1 
				AND (deleted IS NULL OR deleted = 0) 
				AND level_id = '".$level_id."' 
				AND title_id IN (".$row['title_id'].") 
				ORDER BY sorting ASC";

	          	$result_title = mysqli_query($connect,$query_title);
	          	while($row_titleid=mysqli_fetch_assoc($result_title))
	          	{   

	                $value.=''.$row_titleid['title_name'].'<br>';
	              
	          	}			
	        }else{
	        	$value.=''.$row['title_id'].'<br>';
	        }

				

			$value .= '
			</td>
			<td>'.$row["student_status"].'</td>
		 	';

		  	$value .='
		  		</tr>
		   	</tbody>
		  	';

		 }
		}
		else
		{
		 $value = 'Data Not Found';
		}
		return($value);
    }

    function teacher_daily($search_date,$fid,$teachername){
    	$position = $_COOKIE['position'];
    	if ($position == 'teacher') {
    		$query_value = " AND sr.teachername = '$teachername' ";
    	}else{
    		$query_value='';
    	}
    	$value = "";
    	include '..//dbConfig.php';
    	if($search_date == "")
		{
		 $search_date= date("Y-m-d");
		 $query = "
		 SELECT te.username FROM studentresult sr 
		 LEFT JOIN teacher te ON (te.username = sr.teachername) 
		 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
		 WHERE sr.fid = '$fid' 
		 AND s.active = 'Yes'
		 AND sr.insert_date = '".$search_date."'
		 ".$query_value." 
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 GROUP BY te.username ORDER BY te.username
		 ";
		}
		else
		{
		 $query = "
		 SELECT te.username FROM studentresult sr 
		 LEFT JOIN teacher te ON (te.username = sr.teachername) 
		 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
		 WHERE sr.fid = '$fid' 
		 AND s.active = 'Yes'
		 AND sr.insert_date = '".$search_date."'
		 ".$query_value." 
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 GROUP BY te.username ORDER BY te.username
		 ";
		}
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>Teacher Name</th>
		  <th>Subject</th>
		  <th>Level</th>
		  <th>Time</th>
		  <th>Total Class</th>
		  </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $value .= '
		  <tbody>
		  <tr>
		  <td>'.$row["username"].'</td>
		  <td>
		  ';
		  if($search_date == "")
		  {
		    $search_date= date("Y-m-d");
			$query1 = "
			SELECT la.language_name, le.level_name, la.language_id FROM studentresult sr 
			LEFT JOIN language la ON (sr.language_id = la.language_id) 
			LEFT JOIN level le ON (sr.level_id = le.level_id)
			LEFT JOIN student st ON (sr.reg_no = st.reg_no)
			WHERE sr.teachername = '".$row["username"]."' 
			AND st.active = 'Yes'
			AND (sr.deleted IS NULL OR sr.deleted = '0')
			AND (sr.trial = '' OR sr.trial = 'No') 
			AND sr.insert_date = '".$search_date."' 
			GROUP BY la.language_name ORDER BY la.language_id
			";
		  }
		  else
		  {
		  	$query1 = "
		    SELECT la.language_name, le.level_name, la.language_id FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		    WHERE sr.teachername = '".$row["username"]."' 
		    AND st.active = 'Yes'
		    AND (sr.deleted IS NULL OR sr.deleted = '0') 
		    AND (sr.trial = '' OR sr.trial = 'No')
		    AND sr.insert_date = '".$search_date."' 
		    GROUP BY la.language_name ORDER BY la.language_id
		  	";
		  }
		    if($result1 = mysqli_query($connect, $query1))
		    {
		      while($rowcount=mysqli_fetch_array($result1))
		      {
		        $language_name = $rowcount['language_name'];
		        $value .= '
		        <a href="teacher_report_daily_details.php?teachername='.base64_encode($row["username"]).'&date='.base64_encode($search_date).'&language_id='.base64_encode($rowcount["language_id"]).'">'.$language_name.'</a><br>
		        ';
		      }
		    }
		  $value .= '
		  </td>
		  <td>
		  ';
		  if($search_date == "")
		  { 
		  $search_date= date("Y-m-d");
		  $query1 = "
		   SELECT le.level_name, le.level_id, la.language_id, st.studentname, sr.reg_no FROM studentresult sr 
		   LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		   WHERE sr.teachername = '".$row["username"]."' 
		   AND st.active = 'Yes'
		   AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   AND (sr.trial = '' OR sr.trial = 'No')
		   AND sr.insert_date = '".$search_date."'  
		   GROUP BY le.level_id, sr.reg_no ORDER BY sr.r_id
		  ";
		  }
		  else
		  {
		   	$query1 = "
		    SELECT le.level_name, le.level_id, la.language_id, st.studentname, sr.reg_no FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		    WHERE sr.teachername = '".$row["username"]."' 
		    AND st.active = 'Yes'
		    AND (sr.deleted IS NULL OR sr.deleted = '0')
		    AND (sr.trial = '' OR sr.trial = 'No')
		    AND sr.insert_date = '".$search_date."'  
		    GROUP BY le.level_id, sr.reg_no ORDER BY sr.r_id 
		  ";
		  }
		  
		    if($result1 = mysqli_query($connect, $query1))
		    {
		      while($rowcount=mysqli_fetch_array($result1))
		      {
		        $level_name = $rowcount['level_name'];
		        $studentname = $rowcount['studentname'];
		        $value .= '
		        '.$studentname.' - '.$level_name.'<br>
		        ';
		      }
		    }
		  $value .= '
		  </td>
		  <td>
		  ';
		  if($search_date == "")
		  {
		    $search_date= date("Y-m-d");
		  	$query_time = "
		   	SELECT sr.times1, sr.reg_no, le.level_id FROM studentresult sr 
		   	LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   	LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   	LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		   	WHERE sr.teachername = '".$row["username"]."' 
		   	AND st.active = 'Yes'
		   	AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   	AND (sr.trial = '' OR sr.trial = 'No')
		   	AND sr.insert_date = '".$search_date."'
		   	GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id 
		  	";
		  }
		  else
		  {
		   	$query_time = "
		    SELECT sr.times1, sr.reg_no, le.level_id FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		    WHERE sr.teachername = '".$row["username"]."' 
		    AND st.active = 'Yes'
		    AND (sr.deleted IS NULL OR sr.deleted = '0')
		    AND (sr.trial = '' OR sr.trial = 'No')
		    AND sr.insert_date = '".$search_date."'
		    GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id 
		  	";
		  }
		  
		    if($result_time = mysqli_query($connect, $query_time))
		    {
		      $mo1 = '0';
		      $mo2 = '0';
		      $af1 = '0';
		      $af2 = '0';
		      $af3 = '0';
		      $af4 = '0';
		      while($rowcount_time=mysqli_fetch_array($result_time))
		      {
		        $time = $rowcount_time['times1'];
		        if ($time == 'Morning1') {
		          $mo1 ++;
		        }
		        if ($time == 'Morning2') {
		          $mo2 ++;
		        }
		        if ($time == 'Afthernoon1') {
		          $af1 ++;
		        }
		        if ($time == 'Afthernoon2') {
		          $af2 ++;
		        }
		        if ($time == 'Afthernoon3') {
		          $af3 ++;
		        }
		        if ($time == 'Afthernoon4') {
		          $af4 ++;
		        }
		      }
		      $value .= '
		      Morning 1 : '.$mo1.' <br>
		      Morning 2 : '.$mo2.'<br>
		      Afternoon 1 : '.$af1.'<br>
		      Afternoon 2 : '.$af2.'<br>
		      Afternoon 3 : '.$af3.'<br>
		      Afternoon 4 : '.$af4.'<br>
		      ';
		    }

		    if($search_date == "")
		    {
		     $search_date= date("Y-m-d");
		     $query_class = "
		     SELECT max(number_of_class) total_class FROM studentresult sr 
		     LEFT JOIN teacher te ON (te.username = sr.teachername) 
		     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		     WHERE sr.fid = '".$fid."' 
		     AND st.active = 'Yes'
		     AND te.username='".$row["username"]."'
		     AND sr.insert_date = '".$search_date."' 
		     AND (sr.deleted IS NULL OR sr.deleted = '0') 
		     AND (sr.trial = '' OR sr.trial = 'No')
		     GROUP BY sr.reg_no";
		    }
		    else
		    {
		    
		    $query_class = "
		     SELECT max(number_of_class) total_class FROM studentresult sr 
		     LEFT JOIN teacher te ON (te.username = sr.teachername) 
		     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		     WHERE sr.fid = '".$fid."' 
		     AND st.active = 'Yes'
		     AND te.username='".$row["username"]."'
		     AND sr.insert_date = '".$search_date."' 
		     AND (sr.deleted IS NULL OR sr.deleted = '0') 
		     AND (sr.trial = '' OR sr.trial = 'No')
		     GROUP BY sr.reg_no";
		    }
		    if($result_class = mysqli_query($connect, $query_class))
		    {
		      $sum = '0';
		      while($row_class=mysqli_fetch_array($result_class))
		      {
		        $sum += $row_class['total_class'];
		      }
		      $value .= '
		      </td>
		      <td>'.$sum.'</td>
		      </tr>
		      </tbody>
		      ';
		    }
		 }

		}
		else
		{
		 $value = 'No Data From Today';
		}

		return($value);
    }

    function teacher_daily_details ($teachername,$language_id,$date){
    	 include '..//dbConfig.php';
    	 $value = "";
    	 $query = "
		  SELECT *,la.language_name, le.level_name, st.studentname, sr.level_id, sr.title_id FROM studentresult sr 
		  LEFT JOIN language la ON (sr.language_id = la.language_id) 
		  LEFT JOIN level le ON (sr.level_id = le.level_id) 
		  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		  WHERE sr.teachername = '".$teachername."' 
		  AND st.active = 'Yes'
		  AND sr.language_id = '".$language_id."' 
		  AND (sr.deleted IS NULL OR sr.deleted = '0') 
		  AND (sr.trial = '' OR sr.trial = 'No')
		  AND sr.insert_date = '".$date."' ORDER BY sr.r_id DESC
		 ";
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>Student</th>
		  <th>Level</th>
		  <th>Title</th>
		  </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $level_id = $row['level_id'];
		  $value .= '
		  <tbody>
		  <tr>
		  <td>'.$row["studentname"].'</td>
		  <td>'.$row["level_name"].'</td>
		  <td>
		  ';

		  	if ($row['method'] == 'Yes') {
		  		$query_title = "
				SELECT * FROM level_title 
				WHERE status = 1 
				AND (deleted IS NULL OR deleted = 0) 
				AND level_id = '".$level_id."' 
				AND title_id IN (".$row['title_id'].") 
				ORDER BY sorting ASC";

		      	$result_title = mysqli_query($connect,$query_title);
		      	while($row_titleid=mysqli_fetch_assoc($result_title))
		      	{   

		            $value.=''.$row_titleid['title_name'].'<br>';
		          
		      	}
		  	}else{
		  		$value.=''.$row['title_id'].'';
		  	}
		  	
		  $value .='
		  </td>
		  </tbody>
		  ';
		  }
		  
		}
		else
		{
		 $value = 'Data Not Found';
		}
		return ($value);
    }

    function teacher_monthly($search_date,$fid,$teachername){

    	include '..//dbConfig.php';
    	$value = "";
    	if($search_date == "")
		{
		 $year = date("Y");
		 $month = date("m");
		 $query = "
		  SELECT *,la.language_name, le.level_name, st.studentname FROM studentresult sr 
		  LEFT JOIN language la ON (sr.language_id = la.language_id) 
		  LEFT JOIN level le ON (sr.level_id = le.level_id) 
		  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		  WHERE sr.teachername = '".$teachername."' 
		  AND st.active = 'Yes'
		  AND sr.fid = '".$fid."'
		  AND (sr.deleted IS NULL OR sr.deleted = '0') 
		  AND (sr.trial = '' OR sr.trial = 'No')
		  AND month(sr.insert_date) = '".$month."' 
		  AND year(sr.insert_date) = '".$year."'
		  GROUP BY sr.insert_date ORDER BY sr.insert_date DESC
		 ";
		}
		else
		{
		 $year = date("Y" , strtotime($search_date));
		 $month = date("m" , strtotime($search_date));
		 $query = "
		  SELECT *,la.language_name, le.level_name, st.studentname FROM studentresult sr 
		  LEFT JOIN language la ON (sr.language_id = la.language_id) 
		  LEFT JOIN level le ON (sr.level_id = le.level_id) 
		  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		  WHERE sr.teachername = '".$teachername."' 
		  AND st.active = 'Yes'
		  AND sr.fid = '".$fid."'
		  AND (sr.deleted IS NULL OR sr.deleted = '0') 
		  AND (sr.trial = '' OR sr.trial = 'No')
		  AND month(sr.insert_date) = '".$month."' 
		  AND year(sr.insert_date) = '".$year."' 
		  GROUP BY sr.insert_date ORDER BY sr.insert_date DESC
		 ";
		}
		 
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>Date</th>
		  <th>Morning 1</th>
		  <th>Morning 2</th>
		  <th>Afternoon 1</th>
		  <th>Afternoon 2</th>
		  <th>Afternoon 3</th>
		  <th>Afternoon 4</th>
		  <th>Total</th>
		  </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $new_date = date("d/m/Y", strtotime($row["insert_date"]));
		  $value .= '
		  <tbody>
		  <tr>
		  <td>'.$new_date.'</td>
		  ';
		  $query_time = "
		   SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
		   LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		   WHERE sr.teachername = '".$teachername."' 
		   AND st.active = 'Yes'
		   AND sr.fid = '".$fid."'
		   AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   AND (sr.trial = '' OR sr.trial = 'No')
		   AND sr.insert_date = '".$row["insert_date"]."' 
		   GROUP BY sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
		  ";
		    if($result_time = mysqli_query($connect, $query_time))
		    {
		      $mo1 = '0';
		      $mo2 = '0';
		      $af1 = '0';
		      $af2 = '0';
		      $af3 = '0';
		      $af4 = '0';
		      while($rowcount_time=mysqli_fetch_array($result_time))
		      {
		        $time = $rowcount_time['times1'];
		        if ($time == 'Morning1') {
		          $mo1 ++;
		        }
		        if ($time == 'Morning2') {
		          $mo2 ++;
		        }
		        if ($time == 'Afthernoon1') {
		          $af1 ++;
		        }
		        if ($time == 'Afthernoon2') {
		          $af2 ++;
		        }
		        if ($time == 'Afthernoon3') {
		          $af3 ++;
		        }
		        if ($time == 'Afthernoon4') {
		          $af4 ++;
		        }
		        $total_d_time = $mo1 + $mo2 + $af1 + $af2 + $af3 + $af4;
		      }
		      $value .= '
		      <td>'.$mo1.'</td>
		      <td>'.$mo2.'</td>
		      <td>'.$af1.'</td>
		      <td>'.$af2.'</td>
		      <td>'.$af3.'</td>
		      <td>'.$af4.'</td>
		      <td>'.$total_d_time.'</td>
		      ';

		    }
		  $value .= '
		  <tr>
		  </tbody>
		  ';
		}
		if($search_date == "")
		{
		$year = date("Y");
		$month = date("m");
		$query_time_2 = "
		 SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
		 LEFT JOIN language la ON (sr.language_id = la.language_id) 
		 LEFT JOIN level le ON (sr.level_id = le.level_id) 
		 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		 WHERE sr.teachername = '".$teachername."' 
		 AND st.active = 'Yes'
		 AND sr.fid = '".$fid."'
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 AND month(sr.insert_date) = '".$month."' 
		 AND year(sr.insert_date) = '".$year."'  
		 GROUP BY sr.insert_date, sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
		 ";
		}
		else{
		$year = date("Y" , strtotime($search_date));
		$month = date("m" , strtotime($search_date));
		$query_time_2 = "
		 SELECT sr.insert_date, sr.reg_no, sr.times1 FROM studentresult sr 
		 LEFT JOIN language la ON (sr.language_id = la.language_id) 
		 LEFT JOIN level le ON (sr.level_id = le.level_id) 
		 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		 WHERE sr.teachername = '".$teachername."' 
		 AND st.active = 'Yes'
		 AND sr.fid = '".$fid."'
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 AND month(sr.insert_date) = '".$month."' 
		 AND year(sr.insert_date) = '".$year."'  
		 GROUP BY sr.insert_date, sr.reg_no, sr.times1 ORDER BY sr.r_id DESC
		 ";
		}
		    if($result_time_2 = mysqli_query($connect, $query_time_2))
		    {
		      $total_mo1 = '0';
		      $total_mo2 = '0';
		      $total_af1 = '0';
		      $total_af2 = '0';
		      $total_af3 = '0';
		      $total_af4 = '0';
		      while($rowcount_time_2 = mysqli_fetch_array($result_time_2))
		      {
		        $time = $rowcount_time_2['times1'];
		        if ($time == 'Morning1') {
		          $total_mo1 ++;
		        }
		        if ($time == 'Morning2') {
		          $total_mo2 ++;
		        }
		        if ($time == 'Afthernoon1') {
		          $total_af1 ++;
		        }
		        if ($time == 'Afthernoon2') {
		          $total_af2 ++;
		        }
		        if ($time == 'Afthernoon3') {
		          $total_af3 ++;
		        }
		        if ($time == 'Afthernoon4') {
		          $total_af4 ++;
		        }
		        $total_m_time = $total_mo1 + $total_mo2 + $total_af1 + $total_af2 + $total_af3 + $total_af4;
		      }
		      $value .= '
		      <thead>
		      <tr>
		        <th>Total</th>
		        <th>'.$total_mo1.'</th>
		        <th>'.$total_mo2.'</th>
		        <th>'.$total_af1.'</th>
		        <th>'.$total_af2.'</th>
		        <th>'.$total_af3.'</th>
		        <th>'.$total_af4.'</th>
		        <th>'.$total_m_time.'</th>
		      </tr>
		      </thead>
		      ';
		    }
		}
		else
		{
		 $value = 'Data Not Found';
		}

		return($value);
    }

    function teacher_kpi ($fid,$teachername)
    {
   	$position = $_COOKIE['position'];
	if ($position == 'teacher') {
		$query_value1 = " AND username = '$teachername' ";
	}else{
		$query_value1='';
	}
    include '..//dbConfig.php';
   	// $value = "";
    $output = '';

    $query = "
    SELECT * FROM teacher 
    WHERE fid = '$fid'
    ".$query_value1." 
    AND deleted = '0' ORDER BY username
    ";
    // echo $query; exit();
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
        $output .= '
        <thead>
        <tr>
        <th>#</th>
        <th>Teacher Name</th>
        <th>QMS Pass</th>
        <th>QMS Failed</th>
        <th>LMS Pass</th>
        <th>LMS Failed</th>
        <th>Not Complete</th>
        <th>No calculation kpi</th>
        <th>QMS KPI %</th>
        <th>LMS KPI %</th>
        <th>Total KPI %</th>
        <th></th>
        </tr>
        </thead>
        ';
        while($row = mysqli_fetch_array($result))
        {
            $teachername = $row["username"];
            $query_level = "
            SELECT sr.level_id,sr.reg_no FROM studentresult sr
            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
            WHERE sr.teachername = '$teachername' 
            AND st.active = 'Yes'
            AND (sr.deleted IS NULL OR sr.deleted = '0') 
            AND (sr.trial = '' OR sr.trial = 'No')
            GROUP BY sr.level_id,sr.reg_no
            ";
            if($result_level = mysqli_query($connect, $query_level))
            {	
            	//qms
                $pass = 0;
                $failed = 0;
                $not = 0;
                $no_count = 0;
                $kpi = 0;
                //lms
                $kpi_lms = 0;
                $total_kpi = 0;
                //set kpi%
                $qmskpi = 40;
                $lmskpi = 30;
                while($row_level = mysqli_fetch_array($result_level))
                {
                	$level_id = $row_level['level_id'];
                    $reg_no = $row_level['reg_no'];
                    
            		$query_maxclass = "
					SELECT SUM(total) AS total1
					FROM (
					SELECT MAX(number_of_class) total, insert_date FROM studentresult
					WHERE level_id = '$level_id'
					AND reg_no = '$reg_no'
					AND (deleted IS NULL OR deleted = '0') 
					AND (trial = '' OR trial = 'No')
					GROUP BY insert_date
					) studentresult
					";
					if($result_maxclass = mysqli_query($connect, $query_maxclass))
					{	
						$max_class = 0;
					    while($row_maxclass = mysqli_fetch_array($result_maxclass))
					    {
					    	$max_class = $row_maxclass['total1'];
					    	// $total_class = substr($total_c, -1);
					    }
					}
					
					$query_teacherclass = "
			        SELECT COUNT(distinct number_of_class) total, teachername FROM studentresult
                    WHERE level_id = '$level_id'
                    AND reg_no = '$reg_no'
                    AND teachername = '$teachername'
                    AND (deleted IS NULL OR deleted = '0') 
                    AND (trial = '' OR trial = 'No')
                    GROUP BY insert_date
			        ";
			        if($result_teacherclass = mysqli_query($connect, $query_teacherclass))
			        {	
			        	$teacher_class = 0;
			            while($row_teacherclass = mysqli_fetch_array($result_teacherclass))
			            {
			            	$teacher_class += $row_teacherclass['total'];
			            	$teachername_final = $row_teacherclass['teachername'];
			            	$final = $teacher_class/$max_class * 100;
			            				            	
			            }
			            $level_array = array();
	                    $query2 = "
	                    SELECT SUM(total) AS total1,total_class,level_id,reg_no
                        FROM (
                        SELECT COUNT(distinct sr.number_of_class) total,le.total_class, sr.level_id, sr.reg_no FROM studentresult sr
	                    LEFT JOIN level le ON (le.level_id=sr.level_id)
	                    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
	                    WHERE sr.level_id = '$level_id' 
	                    AND sr.reg_no = '$reg_no'
	                    AND st.active = 'Yes'
	                    AND sr.teachername = '$teachername' 
	                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
	                    AND (sr.trial = '' OR sr.trial = 'No')
	                    GROUP BY sr.insert_date
	                    ) studentresult
	                    ";
	                    if($result2 = mysqli_query($connect, $query2))
	                    {
	                                 
	                        while($row2 = mysqli_fetch_array($result2))
	                        {	

	                            $total_class = $row2['total_class'];
	                            $total = $row2['total1'];

	                            $query3 = "
	                            SELECT sr.complete FROM studentresult sr
	                            LEFT JOIN level le ON (le.level_id=sr.level_id)
	                            LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
	                            WHERE sr.level_id = '".$row2['level_id']."'
	                            AND st.active = 'Yes'
	                            AND sr.reg_no = '".$row2['reg_no']."'
	                            AND (sr.deleted IS NULL OR sr.deleted = '0') 
	                            AND (sr.trial = '' OR sr.trial = 'No')
	                            ORDER BY sr.r_id DESC LIMIT 1
	                            ";
	                            if($result3 = mysqli_query($connect, $query3))
	                            {
	                                         
	                                while($row3 = mysqli_fetch_array($result3))
	                                {  
	                              

	                                  if ($total_class == 0) {
	                                    $no_count ++;                            
	                                  }
	                                  elseif($total <= $total_class AND $row3['complete'] == 'yes' AND floor($final) >= 50){
	                                      $pass ++;
	                                  }
	                                  elseif($total > $total_class AND $row3['complete'] == 'yes' AND floor($final) >= 50) {
	                                      $failed ++;
	                                  }elseif ($row3['complete'] == 'no' OR $row3['complete'] == '' AND floor($final) >= 50) {
	                                      $not ++;
	                                  }
	                                
	                                  $total_student = $pass + $failed;
	                                  if ($total_student > 0) {
	                                    $kpi = $pass/$total_student * $qmskpi;
	                                  }

	                                }
	                            }
	                        
	                        // $query3 = "UPDATE teacher SET  qms_kpi = '$kpi' WHERE username = '$teachername'";
	                        // $sql_result3 = mysqli_query($connect,$query3);

	                        }
	                    }
			        }
                }
            $all_pass = 0;
                  $all_fail = 0;
                  $query_lms = "
                  SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
                  LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                  WHERE sr.teachername = '$teachername' 
                  AND sr.fid = '$fid'
                  AND st.active = 'Yes'
                  AND (sr.trial = '' OR sr.trial = 'No')
                  AND (sr.deleted IS NULL OR sr.deleted = '0')
                  GROUP BY sr.reg_no, st.studentname
                  ";
                
                  $result_lms = mysqli_query($connect, $query_lms);
                  if(mysqli_num_rows($result_lms) > 0)
                  {
                   
                  
                  while($row_lms = mysqli_fetch_array($result_lms))
                  {
                      $pass_lms = 0;
                      $failed_lms = 0;
                      $reg_no = $row_lms['reg_no'];
                      
                      $level_array = array();
                      $query_lms1 = "
                      SELECT sr.level_id FROM studentresult sr
                      LEFT JOIN student st ON (sr.reg_no = st.reg_no)
                      WHERE sr.teachername = '$teachername' 
                      AND st.active = 'Yes'
                      AND sr.reg_no = '$reg_no'
                      AND (sr.trial = '' OR sr.trial = 'No')
                      AND (sr.deleted IS NULL OR sr.deleted = '0') 
                      GROUP BY sr.level_id
                      ";
                      if($result_lms1 = mysqli_query($connect, $query_lms1))
                      {
                          while($row_lms1 = mysqli_fetch_array($result_lms1))
                          {
                              $level_id = $row_lms1['level_id'];
                              // echo $level_id; exit();
                              $query_maxclass1 = "
                              SELECT SUM(total) AS total1
                              FROM (
                              SELECT MAX(number_of_class) total, insert_date FROM studentresult
                              WHERE level_id = '$level_id'
                              AND reg_no = '$reg_no'
                              AND (deleted IS NULL OR deleted = '0') 
                              AND (trial = '' OR trial = 'No')
                              GROUP BY insert_date
                              ) studentresult
                              ";
                              if($result_maxclass1 = mysqli_query($connect, $query_maxclass1))
                              {   
                                  $max_class1 = 0;
                                  while($row_maxclass1 = mysqli_fetch_array($result_maxclass1))
                                  {
                                      $max_class1 = $row_maxclass1['total1'];
                                      // $total_class = substr($total_c, -1);
                                  }
                              }
                              
                              $query_teacherclass1 = "
                              SELECT COUNT(distinct number_of_class) total, teachername FROM studentresult
                              WHERE level_id = '$level_id'
                              AND reg_no = '$reg_no'
                              AND teachername = '$teachername'
                              AND (deleted IS NULL OR deleted = '0') 
                              AND (trial = '' OR trial = 'No')
                              GROUP BY insert_date
                              ";
                              if($result_teacherclass1 = mysqli_query($connect, $query_teacherclass1))
                              {   
                                  $teacher_class1 = 0;
                                  while($row_teacherclass1 = mysqli_fetch_array($result_teacherclass1))
                                  {
                                      $teacher_class1 += $row_teacherclass1['total'];
                                      $teachername_final1 = $row_teacherclass1['teachername'];
                                      $final1 = $teacher_class1/$max_class1 * 100;
                                                                  
                                  }

                                if ($level_id != 'Level') {
                                  array_push($level_array, $level_id);
                                }

                              }
                              
                          }
                          if (floor($final1) >= 50) {
                          $level = join(",",$level_array);
                          $query_qlms = "
                          SELECT * FROM qlms_level WHERE level_id IN (".$level.")
                          GROUP BY lms_level
                          ";
                          // echo $query_qlms; exit();
                          // echo $query_qlms; exit();
                          if($result_qlms = mysqli_query($connect, $query_qlms))
                          {
                              while($row_qlms = mysqli_fetch_array($result_qlms))
                              {
                                  $level_id = $row_qlms['level_id'];
                                  $lms_level = $row_qlms['lms_level'];

                                  if ($lms_level != '') {
                              
                                      $query_ex = "
                                      SELECT * FROM examresult WHERE level = '".$lms_level."'
                                      AND reg_no = '".$reg_no."' AND fid = '".$fid."'
                                      ";

                                      if($result_ex = mysqli_query($connect1, $query_ex))
                                      {
                                          while($row_ex = mysqli_fetch_array($result_ex))
                                          {

                                              $status = $row_ex['status1'];
                                              if ($status == 'Pass'){
                                                  $pass_lms ++;
                                              }
                                              if ($status == 'Fail') {
                                                  $failed_lms ++;
                                              }

                                          }
                                      }
                                  }else{
                                      
                                  }
                              }
                              $all_pass += $pass_lms;
                              $all_fail += $failed_lms;
                              $total_student = $all_pass + $all_fail;
                              if ($total_student == 0) {
                                  
                              }
                              else{
                              $kpi_lms = $all_pass/$total_student * $lmskpi;
                              }
                              
                          }
                        }
                      }  
                  }
                  $total_kpi = $kpi + $kpi_lms;
              }            
            } 
            if ($pass > 0 OR $failed > 0 OR $not > 0 OR $no_count > 0) {
		    $output .='
		    <tbody>
		    <tr>
		    <td>'.$row["id"].'</td>
		    <td>'.$row["username"].'</td>
		    <td>'.$pass.'</td>
		    <td>'.$failed.'</td>
		    <td>'.$all_pass.'</td>
		    <td>'.$all_fail.'</td>
		    <td>'.$not.'</td>
		    <td>'.$no_count.'</td>
		    <td>'.floor($kpi).'</td>
		    <td>'.floor($kpi_lms).'</td>
		    <td>'.floor($total_kpi).'</td>
		    <td><a href="teacher_kpi_details_report.php?teachername='.base64_encode(urlencode($row["username"])).'" class="btn btn-info">Detail</a></td>
		    </tr>
		    </tbody>
		    ';
		    }
        }
        
    }
	    return($output);
    }

    function teacher_kpi_details($search_text,$fid,$teachername){
    			$value = "";
    			include '..//dbConfig.php';
		    	if($search_text == "")
		        {
		         $query = "
		         SELECT sr.reg_no, st.studentname FROM studentresult sr 
		         LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		         WHERE sr.teachername = '$teachername' 
		         AND sr.fid = '$fid'
		         AND st.active = 'Yes'
		         AND (sr.trial = '' OR sr.trial = 'No')
		         AND (sr.deleted IS NULL OR sr.deleted = '0')
		         GROUP BY sr.reg_no, st.studentname
		         ";
		        }
		        else
		        {
		         $query = "
		         SELECT sr.reg_no, st.studentname FROM studentresult sr 
		         LEFT JOIN student st ON (sr.reg_no = st.reg_no) 
		         WHERE sr.teachername = '$teachername' 
		         AND sr.fid = '$fid'
		         AND st.active = 'Yes'
		         AND (sr.trial = '' OR sr.trial = 'No')
		         AND (sr.deleted IS NULL OR sr.deleted = '0')
		         AND st.studentname LIKE '%".$search_text."%' GROUP BY sr.reg_no, st.studentname
		         ";
		        }
		        $result = mysqli_query($connect, $query);
		        if(mysqli_num_rows($result) > 0)
		        {
		         $value .= '
		         <thead>
		         <tr>
		         <th>Reg_no</th>
		         <th>Student Name</th>
		         <th>Level</th>
		         <th></th>
		         <th>Pass</th>
		         <th>Failed</th>
		         <th>Not Complete</th>
		         </tr>
		         </thead>
		         ';
		        while($row = mysqli_fetch_array($result))
		        {
		          $reg_no = $row['reg_no'];
		          $value .= '
		          <tbody>
		          <tr>
		          <td>'.$row["reg_no"].'</td>
		          <td>'.$row["studentname"].'</td>
		          <td>
		          ';

		            $query1 = "
		            SELECT sr.level_id,sr.reg_no FROM studentresult sr
		            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		            WHERE sr.teachername = '$teachername' 
		            AND st.active = 'Yes'
		            AND sr.reg_no = '$reg_no'
		            AND (sr.trial = '' OR sr.trial = 'No')
		            AND (sr.deleted IS NULL OR sr.deleted = '0') 
		            GROUP BY sr.level_id,sr.reg_no
		            ";
		            if($result1 = mysqli_query($connect, $query1))
		            {
		                $pass = 0;
		                $failed = 0;
		                $not = 0;
		                while($row1 = mysqli_fetch_array($result1))
		                {
		                    //echo $row1["level_id"];
		                    
		                  $level_id = $row1['level_id'];
		                  //echo $level_id;
		                  $reg_no = $row1['reg_no'];
		                  //echo $reg_no.'<br/>';

		                $query_maxclass = "
						SELECT SUM(total) AS total1
						FROM (
						SELECT MAX(number_of_class) total, insert_date FROM studentresult
						WHERE level_id = '$level_id'
						AND reg_no = '$reg_no'
						AND (deleted IS NULL OR deleted = '0') 
						AND (trial = '' OR trial = 'No')
						GROUP BY insert_date
						) studentresult
						";
						if($result_maxclass = mysqli_query($connect, $query_maxclass))
						{	
							$max_class = 0;
						    while($row_maxclass = mysqli_fetch_array($result_maxclass))
						    {
						    	$max_class += $row_maxclass['total1'];
						    	// $total_class = substr($total_c, -1);
						    }
						}
						
						$query_teacherclass = "
				        SELECT COUNT(distinct number_of_class) total, teachername FROM studentresult
                    	WHERE level_id = '$level_id'
                    	AND reg_no = '$reg_no'
                    	AND teachername = '$teachername'
                    	AND (deleted IS NULL OR deleted = '0') 
                    	AND (trial = '' OR trial = 'No')
                    	GROUP BY insert_date
				        ";
				        if($result_teacherclass = mysqli_query($connect, $query_teacherclass))
				        {	
				        	$teacher_class = 0;
				            while($row_teacherclass = mysqli_fetch_array($result_teacherclass))
				            {
				            	$teacher_class += $row_teacherclass['total'];
				            	$teachername_final = $row_teacherclass['teachername'];
				            	$final = $teacher_class/$max_class * 100;
				            				            	
				            }

				              $query2 = "
				              SELECT SUM(total) AS total1,total_class,level_name,level_id,reg_no
                        	  FROM (
                        	  SELECT COUNT(distinct sr.number_of_class) AS total,le.total_class, le.level_name, sr.level_id, sr.reg_no FROM studentresult sr
			                  LEFT JOIN level le ON (le.level_id=sr.level_id)
			                  LEFT JOIN student st ON (sr.reg_no = st.reg_no)
			                  WHERE sr.level_id = '$level_id' 
			                  AND st.active = 'Yes'
			                  AND sr.reg_no = '$reg_no' 
			                  AND sr.teachername = '$teachername'
			                  AND (sr.trial = '' OR sr.trial = 'No')
			                  AND (sr.deleted IS NULL OR sr.deleted = '0') 
			                  GROUP BY sr.insert_date
			                  ) studentresult
			                  ";
			                  if($result2 = mysqli_query($connect, $query2))
			                  {
			                    
			                      while($row2 = mysqli_fetch_array($result2))
			                      {

			                          $total_class = $row2['total_class'];
			                          $total = $row2['total1'];
			                          $level_name = $row2['level_name'];

			                          $query3 = "
			                          SELECT sr.complete FROM studentresult sr
			                          LEFT JOIN level le ON (le.level_id=sr.level_id)
			                          LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
			                          WHERE sr.level_id = '".$row2['level_id']."'
			                          AND st.active = 'Yes'
			                          AND sr.reg_no = '".$row2['reg_no']."'
			                          AND (sr.deleted IS NULL OR sr.deleted = '0') 
			                          AND (sr.trial = '' OR sr.trial = 'No')
			                          ORDER BY sr.r_id DESC LIMIT 1
			                          ";
			                          if($result3 = mysqli_query($connect, $query3))
			                          {
			                                       
			                              while($row3 = mysqli_fetch_array($result3))
			                              {  

			                                if ($total_class == 0) {
			                                  $value .='
			                                  <span style="color: #787878;"> '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b>No calculation kpi</b></span><br>
			                                  ';
			                                }
			                                elseif($total <= $total_class AND $row3['complete'] == 'yes' AND floor($final) >= 50){
			                                  $pass ++;
			                                  $value .='
			                                  '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#31D919;">Pass</b><br>
			                                  ';
			                                }
			                                elseif($total > $total_class AND $row3['complete'] == 'yes' AND floor($final) >= 50) {
			                                    $failed ++;
			                                    $value .='
			                                    '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#F43939">Fail</b><br>
			                                    ';
			                                }elseif ($row3['complete'] == 'no' OR $row3['complete'] == ''){
			                                    $not ++;
			                                    $value .='
			                                    '.$level_name.' &nbsp;/&nbsp; Actual Class: ('.$total.') &nbsp;/&nbsp; KPI Class: ('.$total_class.') - <b style="color:#2B8BA6">Not Complete</b><br>
			                                    ';
			                                }

			                              }
			                          }

			                        }  
			                    }
				        	}
		                 
		                }
		              if ($pass > 0 OR $failed > 0 OR $not > 0) {
		              $value .='
		              </td>
		              <td>
		              ';
		              $value .='
		              </td>
		              <td>'.$pass.'</td>
		              <td>'.$failed.'</td>
		              <td>'.$not.'</td>
		              </tr>
		              </tbody>
		              ';
		          	  }
		          }
		      }
		}
		else
		{
		 $value = 'Data Not Found';
		}

		return($value);
    }

    function teacher_kpi_details_lms($search_text_lms,$fid,$teachername){
    	$output = "";
    	include '..//dbConfig.php';
        if($search_text_lms == "")
        {
            $query = "
            SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
            LEFT JOIN student st ON (sr.reg_no = st.reg_no) 
            WHERE sr.teachername = '$teachername' 
            AND sr.fid = '$fid'
            AND st.active = 'Yes'
            AND (sr.trial = '' OR sr.trial = 'No')
            AND (sr.deleted IS NULL OR sr.deleted = '0')
            GROUP BY sr.reg_no, st.studentname
            ";

        }
        else
        {
            $query = "
            SELECT sr.reg_no, st.studentname, sr.level_id FROM studentresult sr 
            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
            WHERE sr.teachername = '$teachername' 
            AND sr.fid = '$fid'
            AND st.active = 'Yes'
            AND (sr.trial = '' OR sr.trial = 'No')
            AND (sr.deleted IS NULL OR sr.deleted = '0')
            AND st.studentname LIKE '%".$search_text_lms."%'
            GROUP BY sr.reg_no, st.studentname
            ";
        }
        // echo $query; exit();
        $result = mysqli_query($connect, $query);
        if(mysqli_num_rows($result) > 0)
        {
	        $output .= '
	        <thead>
	        <tr>
	        <th>Reg_no</th>
	        <th>Student Name</th>
	        <th>Exam result</th>
	        </tr>
	        </thead>
	        ';
        	while($row = mysqli_fetch_array($result))
        	{
        		$reg_no = $row['reg_no'];
	            $level_array = array();
	            $query1 = "
	            SELECT sr.level_id FROM studentresult sr
	            LEFT JOIN student st ON (sr.reg_no = st.reg_no)
	            WHERE sr.teachername = '$teachername' 
	            AND st.active = 'Yes'
	            AND sr.reg_no = '$reg_no'
	            AND (sr.trial = '' OR sr.trial = 'No')
	            AND (sr.deleted IS NULL OR sr.deleted = '0') 
	            GROUP BY sr.level_id
	            ";
	            if($result1 = mysqli_query($connect, $query1))
	            {
	                while($row1 = mysqli_fetch_array($result1))
	                {
	                	$level_id = $row1['level_id'];
	                    // echo $level_id; exit();
	                    $query_maxclass = "
	                    SELECT SUM(total) AS total1
	                    FROM (
	                    SELECT MAX(number_of_class) total, insert_date FROM studentresult
	                    WHERE level_id = '$level_id'
	                    AND reg_no = '$reg_no'
	                    AND (deleted IS NULL OR deleted = '0') 
	                    AND (trial = '' OR trial = 'No')
	                    GROUP BY insert_date
	                    ) studentresult
	                    ";
	                    if($result_maxclass = mysqli_query($connect, $query_maxclass))
	                    {   
	                        $max_class = 0;
	                        while($row_maxclass = mysqli_fetch_array($result_maxclass))
	                        {
	                            $max_class = $row_maxclass['total1'];
	                            // $total_class = substr($total_c, -1);
	                        }
	                    }
	                    
	                    $query_teacherclass = "
	                    SELECT COUNT(distinct number_of_class) total, teachername FROM studentresult
	                    WHERE level_id = '$level_id'
	                    AND reg_no = '$reg_no'
	                    AND teachername = '$teachername'
	                    AND (deleted IS NULL OR deleted = '0') 
	                    AND (trial = '' OR trial = 'No')
	                    GROUP BY insert_date
	                    ";
	                    if($result_teacherclass = mysqli_query($connect, $query_teacherclass))
	                    {   
	                        $teacher_class = 0;
	                        while($row_teacherclass = mysqli_fetch_array($result_teacherclass))
	                        {
	                            $teacher_class += $row_teacherclass['total'];
	                            $teachername_final = $row_teacherclass['teachername'];
	                            $final = $teacher_class/$max_class * 100;
	                                                        
	                        }
	                    }
	                    if ($level_id != 'Level' AND floor($final) >= 50) {
	                        array_push($level_array, $level_id);
	                    }
	                }
	                if (floor($final) >= 50) {
                    $output .= '
                    <tbody>
                    <tr>
                    <td>'.$row["reg_no"].'</td>
                    <td>'.$row["studentname"].'</td>
                    <td>
                    ';
	                    $level = join(",",$level_array);
		                $query_qlms = "
		                SELECT * FROM qlms_level WHERE level_id IN (".$level.")
		                GROUP BY lms_level
		                ";
		                // echo $query_qlms; exit();
		                // echo $query_qlms; exit();
		                if($result_qlms = mysqli_query($connect, $query_qlms))
		                {
		                    while($row_qlms = mysqli_fetch_array($result_qlms))
		                    {
		                        $level_id = $row_qlms['level_id'];
		                        $lms_level = $row_qlms['lms_level'];

		                        if ($lms_level != '') {
		                    
		                            $query_ex = "
		                            SELECT * FROM examresult WHERE level = '".$lms_level."'
		                            AND reg_no = '".$reg_no."' AND fid = '".$fid."'
		                            ";
		                            // echo $query_ex;
		                            // echo "<br>";
		                            
		                            if($result_ex = mysqli_query($connect1, $query_ex))
		                            {
		                                while($row_ex = mysqli_fetch_array($result_ex))
		                                {
		                                $level_name = $row_ex['level'];  
		                                $total = $row_ex['totalp'];
		                                $status = $row_ex['status1'];
		                                $actions = $row_ex['actions'];

		                                if($status == 'Pass'){
		                                    $output .='
		                                    <a href="student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode($row["studentname"]).'&fid='.base64_encode($fid).'&active='.base64_encode('Yes').'">'.$level_name.'</a> &nbsp;/&nbsp; Total percentage: ('.$total.') &nbsp;/&nbsp; Actions: ('.$actions.') &nbsp;/&nbsp; status - <b style="color:#31D919">Pass</b><br>
		                                    ';
		                                }elseif($status == 'Fail'){
		                                    $output .='
		                                    <a href="student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode($row["studentname"]).'&fid='.base64_encode($fid).'&active='.base64_encode('Yes').'">'.$level_name.'</a> &nbsp;/&nbsp; Total percentage: ('.$total.') &nbsp;/&nbsp; Actions: ('.$actions.') &nbsp;/&nbsp; status - <b style="color:#F43939">Fail</b><br>
		                                    ';
		                                }
		                                
		                                }
		                            }
		                        }else{
		                            
		                        }
		                    }
		                }
		                $output .='
			            </td>
			            </tr>
			            </tbody>
			            ';
	                }
	                
	            }

        	}

    	}

	return($output);
    }


    function all_teacher_monthly ($search_date,$fid){
    	include '..//dbConfig.php';
    	$value = "";
    	if($search_date == "")
		{
		 $year = date("Y");
		 $month = date("m");
		 $query = "
		 SELECT sr.teachername, sr.insert_date FROM studentresult sr 
		 LEFT JOIN teacher te ON (te.username = sr.teachername) 
		 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
		 WHERE sr.fid = '$fid' 
		 AND s.active = 'Yes'
		 AND month(sr.insert_date) = '".$month."' 
		 AND year(sr.insert_date) = '".$year."' 
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 GROUP BY sr.teachername ORDER BY sr.teachername
		 ";
		}
		else
		{
		 $year = date("Y" , strtotime($search_date));
		 $month = date("m" , strtotime($search_date));
		 $query = "
		 SELECT sr.teachername, sr.insert_date FROM studentresult sr 
		 LEFT JOIN teacher te ON (te.username = sr.teachername) 
		 LEFT JOIN student s ON (sr.reg_no = s.reg_no) 
		 WHERE sr.fid = '$fid' 
		 AND s.active = 'Yes'
		 AND month(sr.insert_date) = '".$month."' 
		 AND year(sr.insert_date) = '".$year."' 
		 AND (sr.deleted IS NULL OR sr.deleted = '0') 
		 AND (sr.trial = '' OR sr.trial = 'No')
		 GROUP BY sr.teachername ORDER BY sr.teachername
		 ";
		}
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>Teacher Name</th>
		  <th>Morning 1</th>
		  <th>Morning 2</th>
		  <th>Afternoon 1</th>
		  <th>Afternoon 2</th>
		  <th>Afternoon 3</th>
		  <th>Afternoon 4</th>
		  <th>Total</th>
		  </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $value .= '
		  <tbody>
		  <tr>
		  <td>'.$row["teachername"].'</td>
		  ';
		  if(isset($_POST["query"]))
		  {
		   $search = mysqli_real_escape_string($connect, $_POST["query"]);
		   $month = date("m", strtotime($search));
		   $year = date("Y", strtotime($search));
		   $query_time = "
		    SELECT sr.times1, sr.reg_no, sr.insert_date FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		    WHERE sr.teachername = '".$row["teachername"]."' 
		    AND st.active = 'Yes'
		    AND (sr.deleted IS NULL OR sr.deleted = '0')
		    AND (sr.trial = '' OR sr.trial = 'No')
		    AND month(sr.insert_date) = '".$month."' 
		    AND year(sr.insert_date) = '".$year."'
		    GROUP BY sr.reg_no, sr.times1, sr.insert_date ORDER BY sr.r_id 
		  ";
		  }
		  else
		  {
		  $month = date("m");
		  $year = date("Y");
		  $query_time = "
		   SELECT sr.times1, sr.reg_no, sr.insert_date FROM studentresult sr 
		   LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		   WHERE sr.teachername = '".$row["teachername"]."' 
		   AND st.active = 'Yes'
		   AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   AND (sr.trial = '' OR sr.trial = 'No')
		   AND month(sr.insert_date) = '".$month."' 
		   AND year(sr.insert_date) = '".$year."'
		   GROUP BY sr.reg_no, sr.times1, sr.insert_date ORDER BY sr.r_id 
		  ";
		  }
		  
		    if($result_time = mysqli_query($connect, $query_time))
		    {
		      $mo1 = '0';
		      $mo2 = '0';
		      $af1 = '0';
		      $af2 = '0';
		      $af3 = '0';
		      $af4 = '0';
		      $total = '0';
		      while($rowcount_time=mysqli_fetch_array($result_time))
		      {
		        $time = $rowcount_time['times1'];
		        if ($time == 'Morning1') {
		          $mo1 ++;
		        }
		        if ($time == 'Morning2') {
		          $mo2 ++;
		        }
		        if ($time == 'Afthernoon1') {
		          $af1 ++;
		        }
		        if ($time == 'Afthernoon2') {
		          $af2 ++;
		        }
		        if ($time == 'Afthernoon3') {
		          $af3 ++;
		        }
		        if ($time == 'Afthernoon4') {
		          $af4 ++;
		        }
		        $total = $mo1 + $mo2 + $af1 + $af2 + $af3 + $af4;
		      }
		      $value .= '
		      <td>'.$mo1.'</td>
		      <td>'.$mo2.'</td>
		      <td>'.$af1.'</td>
		      <td>'.$af2.'</td>
		      <td>'.$af3.'</td>
		      <td>'.$af4.'</td>
		      <td>'.$total.'</td>
		      ';
		    }
		 }

		}

		return($value);
    }

    function all_teacher_monthly_report($search_name){
    		$fid = $_COOKIE['fid'];
    		$value = "";
    		include '..//dbConfig.php';
    		if($search_name == "")
			{
			 $query = "
			  SELECT * FROM teacher WHERE fid = '$fid' AND deleted = '0' ORDER BY username
			 ";
			}
			else
			{
			$query = "
			  SELECT * FROM teacher 
			  WHERE fid = '$fid' AND username LIKE '%".$search_name."%' AND deleted = '0' ORDER BY username
			 ";
			}
			$result = mysqli_query($connect, $query);
			if(mysqli_num_rows($result) > 0)
			{
			 $value .= '
			 <thead>
			  <tr>
			  <th>#</th>
			  <th>Teacher Name</th>
			  <th></th>
			  </tr>
			  </thead>
			 ';
			 while($row = mysqli_fetch_array($result))
			 {
			  $value .= '
			  <tbody>
			  <tr>
			  <td>'.$row["id"].'</td>
			  <td>'.$row["username"].'</td>
			  <td><a href="teacher_report_monthly.php?teacher_name='.base64_encode(urlencode($row["username"])).'" class="btn btn-warning">Report</a></td>
			  </tr>
			  </tbody>
			  ';
			 }
		}
		return($value);
    }

 } 
?>