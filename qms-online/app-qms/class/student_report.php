<?php
/**
 * 
 */
class sreport
{
	
	function student_list($search_name)
	{	
		$fid = $_COOKIE['fid'];
		$value = "";
		include '..//dbConfig.php';
		if($search_name == "")
		{
		$query = "
		  SELECT * FROM student 
		  WHERE fid = '$fid' 
		  AND active = 'Yes' 
		  ORDER BY studentname
		";
		}
		else
		{
		$query = "
		  SELECT * FROM student 
		  WHERE fid = '$fid' 
		  AND studentname LIKE '%".$search_name."%' 
		  AND active = 'Yes' ORDER BY studentname
		";
		}
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>Student Name</th>
		  <th>Reg No</th>
		  <th>Born</th>
		  <th>Subject</th>
		  <th></th>
		  </tr>
		  </thead>
		 ';
		 while($row = mysqli_fetch_array($result))
		 {
		  $born = date('Y',strtotime($row["dob"]));
		  $value .= '
		  <tbody>
		  <tr>
		  <td>'.$row["studentname"].'</td>
		  <td>'.$row["reg_no"].'</td>
		  <td>'.$born.'</td>
		  <td>
		  ';
		  $query_cms = "
		  SELECT * FROM student WHERE fid = '$fid' AND active = 'Yes' AND reg_no = '".$row['reg_no']."' ORDER BY name
		  ";
		  $result_cms = mysqli_query($connect2, $query_cms);
		  if(mysqli_num_rows($result_cms) > 0)
		  {
		    while($row_cms = mysqli_fetch_array($result_cms))
		    {
		      $mand = $row_cms['language_mand'];
		      $bm = $row_cms['language_bm'];
		      $en = $row_cms['language_en'];
		      $ma = $row_cms['language_ma'];
		      $miq = $row_cms['language_miq'];
		        if ($mand == 'Yes') {
		          $value .= '
		          <b>华语 (Chinese)&#x2705;</b><br>
		          ';
		        }
		        if ($bm == 'Yes') {
		          $value .= '
		          <b>国语 (Melayu)&#x2705;</b><br>
		          ';
		        }
		        if ($en == 'Yes') {
		          $value .= '
		          <b>英语 (English)&#x2705;</b><br>
		          ';
		        }
		        if ($ma == 'Yes') {
		          $value .= '
		          <b>珠算 (Abacus)&#x2705;</b><br>
		          ';
		        }
		        if ($miq == 'Yes') {
		          $value .= '
		          <b>MIQ &#x2705;</b><br>
		          ';
		        }
		         
		    }
		  }
		  $value .='
		  </td>
		  <td>
		  <a href="student_report_level.php?reg_no='.base64_encode($row["reg_no"]).'&studentname='.base64_encode(urlencode($row["studentname"])).'&active='.base64_encode('Yes').'"class="btn btn-warning">Report</a>
		  </td>
		  </tr>
		  </tbody>
		  ';
		 }

		}
		else
		{
		 $value = 'Data Not Found';
		}

		return($value);
	}

	function student_level ($search,$reg_no,$studentname,$active){

		$value = "";
		include '..//dbConfig.php';
		if($search == "")
		{
		  
		 $query = "
		  SELECT la.language_name, le.level_name, le.level_id FROM language la 
		  LEFT JOIN level le ON(la.language_id = le.language_id) ORDER BY la.language_id
		 ";
		}
		else
		{
		 $query = "
		  SELECT la.language_name, le.level_name, le.level_id FROM language la 
		  LEFT JOIN level le ON(la.language_id = le.language_id) 
		  WHERE la.language_id = '".$search."' ORDER BY la.language_id
		 ";
		}

		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		    <tr>
		      <th>Language</th>
		      <th>Level</th>
		      <th>Total Class</th>
		      <th></th>
		    </tr>
		  </thead>
		 ';

		 while($row = mysqli_fetch_array($result))
		 {
		    $level_id = $row['level_id'];
		    //echo $level_id;
		  $query2 = "
		     SELECT r_id FROM studentresult 
		     WHERE level_id = $level_id 
		     AND (deleted IS NULL OR deleted = '0')
		     AND (trial = '' OR trial = 'No') 
		     AND reg_no = '".$reg_no."'
		  ";
		 if($result2 = mysqli_query($connect, $query2))
		  //($row2 = mysqli_fetch_array($result2))
		   {
		    $rowcount1=mysqli_num_rows($result2);
		   }

		  if ($rowcount1 > 0) {
		    $value .='
		    <tbody>
		      <tr>
		        <td><strong>'.$row["language_name"].'</strong></td>
		        <td>'.$row["level_name"].'</td>
		        <td>'.$rowcount1.'</td>
		        <td><a href="student_report_level_details.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($row["level_id"]).'&studentname='.base64_encode($studentname).'&active='.base64_encode($active).'" class="btn btn-default">View</a>
		        </td>
		      </tr>
		    </tbody>
		    ';
		   }
		  

		 }

		}
		else
		{
		 $value = 'Data Not Found';
		}
		return ($value);
	}

	function student_level_details($reg_no,$level_id){
		$position = $_COOKIE['position'];
		$value = "";
		include '..//dbConfig.php';
		$query = "
		SELECT sr.*, lang.language_name, le.level_name, ti.title_name FROM studentresult sr 
		LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
		LEFT JOIN level le ON (sr.level_id = le.level_id) 
		LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
		WHERE sr.reg_no='".$reg_no."' AND sr.level_id='".$level_id."' 
		AND (sr.deleted IS NULL OR sr.deleted = '0') 
		AND (sr.trial = '' OR sr.trial = 'No') ORDER BY sr.insert_date DESC
		";

		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		    <tr>
		      <th>Date</th>
		      <th>Teacher</th>
		      <th>Feedback</th>
		      <th>Title</th>
		      <th>Status</th>
		    </tr>
		  </thead>
		 ';

		 while($row = mysqli_fetch_array($result))
		 {
		    //echo $level_id;
		   $id = $row["r_id"];
		    $value .= '
			  <tbody>
			    <tr>
			      <td><strong>'.$row["insert_date"].'</strong></td>
			      <td>'.$row["teachername"].'</td>
			      <td>'.$row["feedback"].'</td>
			      <td>
			';
			if ($row['method'] == 'Yes') {
				$query_title = "
				SELECT * FROM level_title 
				WHERE status = 1 
				AND (deleted IS NULL OR deleted = 0) 
				AND level_id = '".$level_id."' 
				AND title_id IN (".$row['title_id'].") 
				ORDER BY sorting ASC";

	          	$result_title = mysqli_query($connect,$query_title);
	          	while($row_titleid=mysqli_fetch_assoc($result_title))
	          	{   

	                $value.=''.$row_titleid['title_name'].'<br>';
	              
	          	}
			}else{
				$value.=''.$row['title_id'].'';
			}
			
			$value .='
				</td>
			    <td>'.$row["student_status"].'</td>
			';
			if ($position == 'cm') {

		      $value .='
		      <td>
		      <select class="form-control" id="select_option_'.$id.'" name="select_option">
		        <option value="">Please select reason</option>
		        <option value="select wrong level">Choose the wrong level</option>
		        <option value="others">Others</option>
		      </select>
		      </td>
		      <td>
		      <a class="btn btn-danger" onclick="delelet('.$id.')"><i class="glyphicon glyphicon-remove"></i> Delete</a>
		      </td>
		      <script>
		     function delelet($id){
		          var box = document.getElementById("select_option_"+$id+"").value;
		          if(box == ""){
		            alert("Please select reason");
		          }else{

		            if (box == "others"){
		              var reason = prompt("Please enter your reason");
		                if(reason.length != 0){
		                    var del=confirm("Are you sure you want to delete this Record?");
		                    if (del==true) {
		                      window.location.href = "../delete.php?reason="+reason+"&r_id="+$id+"&fid='.$_COOKIE['fid'].'&username='.$_COOKIE['name'].'&reg_no='.$_GET['reg_no'].'&level_id='.$_GET['level_id'].'&studentname='.$_GET['studentname'].'&active='.$_GET['active'].'";
		                      //alert("Pls la");
		                    }
		                }else if (reason.length == 0){
		                      delelet($id);
		                }
		            }else if (box == "select wrong level"){
		              var del=confirm("Are you sure you want to delete this Record?");
		                if (del==true) {
		                  window.location.href = "../delete.php?reason="+box+"&r_id="+$id+"&fid='.$_COOKIE['fid'].'&username='.$_COOKIE['name'].'&reg_no='.$_GET['reg_no'].'&level_id='.$_GET['level_id'].'&studentname='.$_GET['studentname'].'&active='.$_GET['active'].'";
		                }
		            }

		          }
		        }
		      </script>
		      ';
		      }
			$value .='
			  	</tr>
			   </tbody>
		 	';
		 }
		
		}
		else
		{
		 $value = 'Data Not Found';
		}

		return($value);

	}


	function student_level_details_lms($reg_no,$level_id,$fid,$studentname,$active){

		$output = '';
		include '..//dbConfig.php';
		$query = "
		 SELECT * FROM qlms_level WHERE level_id = '".$level_id."'
		";

		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $output .= '
	
		  <thead>
		    <tr>
		      <th>Date</th>
		      <th>Teacher</th>
		      <th>Level</th>
		      <th>Total percentage</th>
		      <th>Remarks</th>
		      <th>Action</th>
		      <th>Status</th>
		      <th></th>
		    </tr>
		  </thead>
		 ';

		 while($row = mysqli_fetch_array($result))
		 {

		    $lms_level = $row['lms_level'];
		    $query_ex = "
		    SELECT * FROM examresult WHERE level = '".$lms_level."'
		    AND reg_no = '".$reg_no."' AND fid = '".$fid."'
		    ";
		    
		    if($result_ex = mysqli_query($connect1, $query_ex))
		    {

		        while($row_ex = mysqli_fetch_array($result_ex))
		        {
		          $status = $row_ex['status1'];
		          $output .= '
		          <tbody>
		          <tr>
		            <td><strong>'.$row_ex["date1"].'</strong></td>
		            <td>'.$row_ex["teacher"].'</td>
		            <td>'.$row_ex["level"].'</td>
		            <td>'.$row_ex["totalp"].'</td>
		            <td>'.$row_ex["remarks"].'</td>
		            <td>'.$row_ex["actions"].'</td>
		          ';
		          if ($status == 'Pass') {
		            $output .= '
		            <td style="color: #31D919;">'.$row_ex["status1"].'</td>
		            ';
		          }
		          if ($status == 'Fail') {
		            $output .= '
		            <td style="color: #F43939;">'.$row_ex["status1"].'</td>
		            ';
		          }

		          $output .= '
		          <td>
		          	<a href="view_screenshot.php?reg_no='.base64_encode($reg_no).'&level_id='.base64_encode($level_id).'&studentname='.base64_encode(urlencode($studentname)).'&active='.base64_encode($active).'&url='.$row_ex["imageurl"].'" class="btn btn-primary">Screenshot</a>
		          </td>
		          ';

		        }

		      $output.='
		      </tr>
		        </tbody>
		      
		      ';
		    }
		 }

		}

		return($output);
	}


	function student_daily($search_date,$fid){
		$value = "";
		include '..//dbConfig.php';
		if($search_date == "")
		{
		 $search_date= date("Y-m-d");
		 $query = "
		  SELECT count(*) AS total, st.studentname, st.age, st.reg_no FROM studentresult sr 
		  LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		  WHERE sr.fid = '$fid'
		  AND st.active = 'Yes'
		  AND sr.insert_date = '".$search_date."' 
		  AND (sr.deleted IS NULL OR sr.deleted = '0')
		  AND (sr.trial = '' OR sr.trial = 'No')
		  GROUP BY st.reg_no ORDER BY st.studentname
		 ";
		}
		else
		{
		 $query = "
		 SELECT count(*) AS total , st.studentname, st.age, st.reg_no FROM studentresult sr 
		 LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		 WHERE sr.fid = '$fid' 
		 AND st.active = 'Yes'
		 AND sr.insert_date = '".$search_date."' 
		 AND (sr.deleted IS NULL OR sr.deleted = '0')
		 AND (sr.trial = '' OR sr.trial = 'No')
		 GROUP BY st.reg_no ORDER BY st.studentname
		 ";
		}
		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		  <tr>
		  <th>No</th>
		  <th>Student Name</th>
		  <th>Reg no</th>
		  <th>Age</th>
		  <th>Subject</th>
		  <th>Level</th>
		  <th>Total Class</th>
		  </tr>
		  </thead>
		 ';
		 $id = 0;
		 while($row = mysqli_fetch_array($result))
		 {
		  $id++;
		  $value .= '
		  <tbody>
		  <tr>
		  <th>'.$id.'</th>
		  <td>'.$row["studentname"].'</td>
		  <td>'.$row["reg_no"].'</td>
		  <td>'.$row["age"].'</td>
		  <td>
		  ';
		  if($search_date == "")
		  {
		   $search_date= date("Y-m-d");
		   $query1 = "
		   SELECT la.language_name, le.level_name FROM studentresult sr 
		   LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   WHERE sr.reg_no = '".$row["reg_no"]."' 
		   AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   AND (sr.trial = '' OR sr.trial = 'No')
		   AND sr.insert_date = '".$search_date."' GROUP BY la.language_name ORDER BY sr.r_id
		  ";
		  }
		  else
		  {
		  $query1 = "
		    SELECT la.language_name, le.level_name FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    WHERE sr.reg_no = '".$row["reg_no"]."' 
		    AND (sr.deleted IS NULL OR sr.deleted = '0')
		    AND (sr.trial = '' OR sr.trial = 'No')
		    AND sr.insert_date = '".$search_date."' GROUP BY la.language_name ORDER BY sr.r_id
		  "; 
		  }
		  
		    if($result1 = mysqli_query($connect, $query1))
		    {
		      while($rowcount=mysqli_fetch_array($result1))
		      {
		        $language_name = $rowcount['language_name'];
		        $value .= '
		        '.$language_name.'<br>
		        ';
		      }
		    }
		  $value .= '
		  </td>
		  <td>
		  ';
		  if($search_date == "")
		  {
		   $search_date= date("Y-m-d");
		   $query1 = "
		   SELECT le.level_name, le.level_id, la.language_id FROM studentresult sr 
		   LEFT JOIN language la ON (sr.language_id = la.language_id) 
		   LEFT JOIN level le ON (sr.level_id = le.level_id) 
		   WHERE sr.reg_no = '".$row["reg_no"]."' 
		   AND (sr.deleted IS NULL OR sr.deleted = '0') 
		   AND (sr.trial = '' OR sr.trial = 'No')
		   AND sr.insert_date = '".$search_date."' GROUP BY le.level_name ORDER BY sr.r_id
		  ";
		  }
		  else
		  {
		  $query1 = "
		    SELECT le.level_name, le.level_id, la.language_id FROM studentresult sr 
		    LEFT JOIN language la ON (sr.language_id = la.language_id) 
		    LEFT JOIN level le ON (sr.level_id = le.level_id) 
		    WHERE sr.reg_no = '".$row["reg_no"]."' 
		    AND (sr.deleted IS NULL OR sr.deleted = '0')
		    AND (sr.trial = '' OR sr.trial = 'No') 
		    AND sr.insert_date = '".$search_date."' GROUP BY le.level_name ORDER BY sr.r_id
		  ";
		  }
		  
		    if($result1 = mysqli_query($connect, $query1))
		    {
		      while($rowcount=mysqli_fetch_array($result1))
		      {
		        $level_name = $rowcount['level_name'];
		        $value .= '
		        <a href="student_report_daily_details.php?reg_no='.base64_encode($row["reg_no"]).'&studentname='.base64_encode($row["studentname"]).'&date='.base64_encode($search_date).'&level_id='.base64_encode($rowcount['level_id']).'&language_id='.base64_encode($rowcount["language_id"]).'&active='.base64_encode('Yes').'">'.$level_name.'</a><br>
		        ';
		      }
		    }

		    if($search_date == "")
		    {
		     $search_date= date("Y-m-d");
		     $query_class = "
		     SELECT max(number_of_class) total_class FROM studentresult sr 
		     LEFT JOIN teacher te ON (te.username = sr.teachername) 
		     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		     WHERE sr.reg_no = '".$row["reg_no"]."' 
		     AND sr.insert_date = '".$search_date."' 
		     AND (sr.deleted IS NULL OR sr.deleted = '0') 
		     AND (sr.trial = '' OR sr.trial = 'No')
		     GROUP BY sr.reg_no";
		    }
		    else
		    {
		     $query_class = "
		     SELECT max(number_of_class) total_class FROM studentresult sr 
		     LEFT JOIN teacher te ON (te.username = sr.teachername) 
		     LEFT JOIN student st ON (sr.reg_no = st.reg_no)
		     WHERE sr.reg_no = '".$row["reg_no"]."'
		     AND sr.insert_date = '".$search_date."' 
		     AND (sr.deleted IS NULL OR sr.deleted = '0') 
		     AND (sr.trial = '' OR sr.trial = 'No')
		     GROUP BY sr.reg_no";
		    }
		    if($result_class = mysqli_query($connect, $query_class))
		    {
		      $sum = '0';
		      while($row_class=mysqli_fetch_array($result_class))
		      {
		        $sum += $row_class['total_class'];
		      }
		      $value .= '
		      </td>
		      <td>'.$sum.'</td>
		      </tr>
		      </tbody>
		      ';
		    }

		 }
		}
		else
		{
		 $value = 'No Data From Today';
		}

		return($value);

	}

	function student_daily_details ($reg_no,$fid,$level_id,$language_id,$active,$date){
		$value = "";
		include '..//dbConfig.php';
		$title_array = array();
		$title_array_old = array();
		$continu_array = array();

		$query_sr = "
		SELECT * FROM studentresult WHERE language_id = '".$language_id."' AND level_id = '".$level_id."' AND reg_no = '".$reg_no."' AND (deleted IS NULL OR deleted = 0) AND student_status = 'Completed'
		";
		if($result_sr = mysqli_query($connect, $query_sr))
		{        

		  while($row_sr = mysqli_fetch_array($result_sr))
		  {
		  	
		      $title_id = $row_sr['title_id'];
		      array_push($title_array, $title_id);
		      $new_title = join(",",$title_array);

		    
		  }  
		}

		$query_co = "
		SELECT * FROM studentresult WHERE language_id = '".$language_id."' AND level_id = '".$level_id."' AND reg_no = '".$reg_no."' AND (deleted IS NULL OR deleted = 0) ORDER BY r_id DESC LIMIT 1
		";
		if($result_co = mysqli_query($connect, $query_co))
		{        
		  while($row_co = mysqli_fetch_array($result_co))
		  {
		      $continue_title = $row_co['continue_id'];

		      array_push($continu_array, $continue_title);
		      $new_continue = join(",",$continu_array);
		      //print_r($title);    
		    
		  }  
		}

		$query_title = $connect->query("SELECT * FROM level_title WHERE status = 1 AND (deleted IS NULL OR deleted = 0) ORDER BY sorting ASC");
		$rowCount_title = $query_title->num_rows;
		if($rowCount_title> 0){
		  while($row_title = $query_title->fetch_assoc()){
		    // $title_id = $row_title['title_id'];
		    // $new_title = preg_replace("/\s(\S*)$/", " ", $row_title['title_name']);
		    $title = rtrim($row_title['title_name'], "
		    ");
		    $last_title = preg_replace( "/\r|\n/", " ", $title);
		    $tle = preg_replace("/\s\s+/", " ",$last_title);
		    $tl = trim($tle, " ");
		    //echo '<script type="text/javascript">alert("'.$tle.'")</script>';
		    //echo '<script type="text/javascript">alert("'.$new_title.'")</script>';
		    //query2
		      if($row_title['level_id'] == $level_id) {

		        $query="SELECT * FROM studentresult WHERE language_id = '".$language_id."' AND level_id = '".$level_id."' AND reg_no = '".$reg_no."' AND title_id LIKE '%".$tl."%' AND (deleted IS NULL OR deleted = 0) AND student_status = 'Completed' AND (method IS NULL OR method = '')";
		        $result = mysqli_query($connect, $query);
		        $row_sr = mysqli_num_rows($result);

		        $title = $row_sr['title_name'];
		        $title_name = str_replace("(Buku Latihan)","<br>(Buku Latihan)",$title);
		        if ($row_sr > 0) {
		          array_push($title_array_old, $row_title['title_id']);
		          
		        }
		        $old_title = join(",",$title_array_old);
		      }
		  }
		}

		$query2 = "
		SELECT * FROM level_title WHERE level_id = '".$level_id."' AND status = 1 AND (deleted IS NULL OR deleted = 0) ORDER BY sorting ASC
		";
		if($result2 = mysqli_query($connect, $query2))
		{        
		  $value .= '
		  <thead>
		  <tr>
		  <th>Title</th>
		  <th>Date</th>
		  <th>Teacher</th>
		  </tr>
		  </thead>
		 ';
		 
		 
		  while($row2 = mysqli_fetch_array($result2))
		  {
		  	
		    $title = explode(",",$new_title);
		    $title1 = explode(",",$old_title);
		    $continu = explode(",",$new_continue);

		    
            // $row_sr1 = mysqli_fetch_array($result_sr1);
            // $teachername = $row_sr1['teachername'];
			// echo $row_sr1['teachername']; exit();
			
			// array_push($array_title, $row2['title_id']);
			// $title_a = join(",",$array_title);
		    $previous_title = '';
			if ((in_array($row2['title_id'], $title) || in_array($row2['title_id'], $title1)) && !in_array($row2['title_id'], $continu))
			{
				$value .= '
    	  		<tr>
    	  		';    	  		
    	  		$value .= '
	      		<td>&#9989;'.$row2["title_name"].'</td>
	    		';

			    	$query_sr1="SELECT * FROM studentresult 
		        	WHERE language_id = '".$language_id."'
		         	AND student_status = 'Completed'
		         	AND level_id = '".$level_id."' 
		         	AND reg_no = '".$reg_no."'
		         	AND FIND_IN_SET (".$row2['title_id'].", title_id)
		         	AND (trial = '' OR trial = 'No')
		         	AND (deleted IS NULL OR deleted = 0)
		         	ORDER BY insert_date ASC";
		         	$result_sr1 = mysqli_query($connect, $query_sr1);
				    while($row_sr1 = mysqli_fetch_array($result_sr1))
		  			{
			  		
			  		// echo $query_sr1; exit();
 		
			  			$newDate = date("d/m/Y", strtotime($row_sr1["insert_date"]));
			  			if ($previous_title == $row2['title_id']) {
						    if ($date == $row_sr1['insert_date']) {
					          $value.='
					          <td></td>
					          <td><b>'.$newDate.'<b></td>
					          <td>'.$row_sr1['teachername'].'</td>
					          ';
					        }else{
					          $value.='
					          <td></td>
					          <td>'.$newDate.'</td>
					          <td>'.$row_sr1['teachername'].'</td>
					          ';
					        }
				    	}else{
				    		if ($date == $row_sr1['insert_date']) {
					          $value.='
					   
					          <td><b>'.$newDate.'<b></td>
					          <td>'.$row_sr1['teachername'].'</td>
					          ';
					        }else{
					          $value.='
					   
					          <td>'.$newDate.'</td>
					          <td>'.$row_sr1['teachername'].'</td>
					          
					          ';
					        }
				    	}
				        $value .='
						</tr>
						';
					$previous_title = $row2['title_id'];
				    }
				

			}else{

		    	$value .= '
            	<tr>
			    <td>'.$row2["title_name"].'</td>
			    <td></td>
			    <td></td>
			    </tr>
			    ';
		    }
		    

		  }  

		}


		return($value);

	}

	function gi_report($status,$age,$fid){
		include '..//dbConfig.php';
		$value = "";
		$query = "
	    SELECT ct.name, st.studentname, st.age, la.language_id, le.level_name, la.language_name, 
	    sr.level_id, sr.reg_no 
	    FROM centre ct
	    LEFT JOIN student st ON (st.fid = ct.fid) 
	    LEFT JOIN studentresult sr ON (sr.reg_no = st.reg_no)
	    LEFT JOIN level le ON (le.level_id = sr.level_id)
	    LEFT JOIN language la ON (la.language_id = sr.language_id) 
	    WHERE ct.fid = '$fid'
	    AND st.age ".$age."
	    AND st.active = 'Yes'
	    AND (sr.deleted IS NULL OR sr.deleted = '0') 
	    AND (sr.trial = 'No' OR sr.trial = '')
	    GROUP BY sr.level_id 
	    ORDER BY sr.level_id ASC
	    ";
	    //echo $query; exit();
	    $result = mysqli_query($connect, $query);
	    if(mysqli_num_rows($result) > 0)
	    {
	     $value .= '
	      <thead>
	      <tr>
	      <th>Center</th>
	      <th>Student Name</th>
	      <th>Age</th>
	      <th>Subject</th>
	      <th>Level</th>
	      <th>Status</th>
	      <th>KPI Class</th>
	      <th>Actual Class</th>
	      </tr>
	      </thead>
	     ';

	     while($row = mysqli_fetch_array($result))
	     { 
	     

	      $query4="SELECT * FROM studentresult sr
	      LEFT JOIN student st ON (st.reg_no = sr.reg_no)
	      WHERE sr.fid = '$fid' 
	      AND sr.level_id = '".$row['level_id']."'
	      AND st.active ='Yes'
	      AND (sr.deleted IS NULL OR sr.deleted = '0') 
	      AND (sr.trial = 'No' OR sr.trial = '')
	      GROUP BY sr.reg_no";

	      if($result4 = mysqli_query($connect, $query4))
	      {
	                   
	          while($row4 = mysqli_fetch_array($result4))
	          {
	            $query2="SELECT COUNT(sr.r_id) AS total,sr.level_id, sr.complete, le.total_class,sr.reg_no
	            FROM studentresult sr 
	            LEFT JOIN level le ON (sr.level_id = le.level_id) 
	            LEFT JOIN language la ON (sr.language_id = la.language_id) 
	            WHERE sr.reg_no='".$row4['reg_no']."' 
	            AND sr.level_id = '".$row4['level_id']."' 
	            AND (sr.deleted IS NULL OR sr.deleted = 0)
	            AND (sr.trial = 'No' OR sr.trial = '')";

	            if($result2 = mysqli_query($connect, $query2))
	            {
	                         
	                while($row2 = mysqli_fetch_array($result2))
	                {
	                  $total_class = $row2['total_class'];
	                  $total = $row2['total'];

	                  $query3 = "
	                    SELECT sr.complete FROM studentresult sr
	                    LEFT JOIN level le ON (le.level_id=sr.level_id)
	                    LEFT JOIN student st ON (st.reg_no = sr.reg_no)  
	                    WHERE sr.level_id = '".$row2['level_id']."'
	                    AND st.active = 'Yes'
	                    AND sr.reg_no = '".$row2['reg_no']."'
	                    AND (sr.deleted IS NULL OR sr.deleted = '0') 
	                    AND (sr.trial = '' OR sr.trial = 'No')
	                    ORDER BY sr.r_id DESC LIMIT 1
	                    ";
	                    if($result3 = mysqli_query($connect, $query3))
	                    {
	                                 
	                        while($row3 = mysqli_fetch_array($result3))
	                        {
	                            
	                          if ($total_class == '0' AND ($status == '0' OR $status == '4')) {
	                                $value .= '
	                                <tbody>
	                                    <tr>
	                                      <td><strong>'.$row["name"].'</strong></td>
	                                      <td>'.$row4['studentname'].'</td>
	                                      <td>'.$row['age'].'</td>
	                                      <td>'.$row['language_name'].'</td>
	                                      <td>'.$row['level_name'].'</td>
	                                      <td style="color:#787878"><strong>No KPI Class</strong></td>
	                                      <td>'.$total_class.'</td>
	                                      <td>'.$row2['total'].'</td> 
	                                ';
	                            }
	                            elseif($row3['complete'] == 'yes' AND $total <= $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '1')){
	                              $value .= '
	                                <tbody>
	                                  <tr>
	                                    <td><strong>'.$row["name"].'</strong></td>
	                                    <td>'.$row4['studentname'].'</td>
	                                    <td>'.$row['age'].'</td>
	                                    <td>'.$row['language_name'].'</td>
	                                    <td>'.$row['level_name'].'</td>
	                                    <td style="color:#31D919"><strong>PASS</strong></td>
	                                    <td>'.$total_class.'</td>
	                                    <td>'.$row2['total'].'</td> 
	                              ';
	                            }
	                            elseif($row3['complete'] == 'yes' AND $total > $total_class AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '2')) {
	                              $value .= '
	                              <tbody>
	                                  <tr>
	                                    <td><strong>'.$row["name"].'</strong></td>
	                                    <td>'.$row4['studentname'].'</td>
	                                    <td>'.$row['age'].'</td>
	                                    <td>'.$row['language_name'].'</td>
	                                    <td>'.$row['level_name'].'</td>
	                                    <td style="color:#F43939"><strong>FAIL</strong></td>
	                                    <td>'.$total_class.'</td>
	                                    <td>'.$row2['total'].'</td> 
	                              ';
	                            }elseif(($row3['complete'] == 'no' OR $row3['complete'] == '') AND !empty($row['level_name']) AND !empty($total_class) AND ($status == '0' OR $status == '3')) {
	                              $value .= '
	                              <tbody>
	                                  <tr>
	                                    <td><strong>'.$row["name"].'</strong></td>
	                                    <td>'.$row4['studentname'].'</td>
	                                    <td>'.$row['age'].'</td>
	                                    <td>'.$row['language_name'].'</td>
	                                    <td>'.$row['level_name'].'</td>
	                                    <td style="color:#2B8BA6"><strong>Not Complete</strong></td>
	                                    <td>'.$total_class.'</td>
	                                    <td>'.$row2['total'].'</td> 
	                              ';
	                            }
	                          }
	                      }
	                }          
	            }
	          }
	      }

	          $value .= '
	          </tbody>
	          </tr>
	          ';
	     }

	    }else{
	      $value.='
	      No Record';
	    }

	    return($value);
	}
	function student_level_complete ($reg_no,$active){
		$position = $_COOKIE['position'];
		include '..//dbConfig.php';
		$value = "";
		$query = "
		SELECT sr.*, lang.language_name, le.level_name, ti.title_name, st.studentname FROM studentresult sr 
		LEFT JOIN language lang ON (sr.language_id = lang.language_id) 
		LEFT JOIN level le ON (sr.level_id = le.level_id) 
		LEFT JOIN level_title ti ON (sr.title_id = ti.title_id) 
		LEFT JOIN student st ON (st.reg_no = sr.reg_no) 
		WHERE sr.reg_no='".$reg_no."'
		AND (sr.deleted IS NULL OR sr.deleted = '0') 
		AND (sr.trial = '' OR sr.trial = 'No') GROUP BY le.level_name ORDER BY le.sorting ASC
		";


		$result = mysqli_query($connect, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 $value .= '
		  <thead>
		    <tr>
		      <th>Level</th>
		      <th>Start date</th>
		      <th>Last date</th>
		      <th>Class</th>
		      <th></th>
		      <th></th>
		      <th></th>
		    </tr>
		  </thead>
		 ';

		 while($row = mysqli_fetch_array($result))
		 {
		  $id= $row['r_id'];
		  $level_id = $row["level_id"];
		  $studentname = $row["studentname"];
		  // echo $row['studentname']; exit();
		  $value .= '
		  <tbody>
		    <tr>
		      <td><strong>'.$row["level_name"].'</strong></td>
		 ';
		  $query_start_date = "SELECT sr.insert_date FROM studentresult sr
		  WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
		  AND sr.level_id='".$row["level_id"]."' 
		  AND sr.reg_no='".$reg_no."'
		  ORDER BY sr.r_id ASC LIMIT 1";
		  $result_start_date = mysqli_query($connect, $query_start_date); 
		  //echo $query_start_date; exit();
		  while($row_start_date = mysqli_fetch_array($result_start_date))
		  {
		  $value .= '
		  <td>'.$row_start_date["insert_date"].'</td>
		  ';
		  }

		  $query_last_date = "SELECT sr.insert_date, sr.complete, sr.r_id FROM studentresult sr
		  WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
		  AND sr.level_id='".$row["level_id"]."' 
		  AND sr.reg_no='".$reg_no."'
		  ORDER BY sr.r_id DESC LIMIT 1";
		  $result_last_date = mysqli_query($connect, $query_last_date); 
		  while($row_last_date = mysqli_fetch_array($result_last_date))
		  {
		    if ($row_last_date['complete'] == 'yes') {
		      $value .= '
		      <td>&#x2705;Completed- '.$row_last_date["insert_date"].'</td>
		      ';
		    }else{
		      $value .= '
		      <td>&#x274C;Not-Completed-  '.$row_last_date["insert_date"].'</td>
		      ';
		    }
		    $query_count = "SELECT count(*) AS total FROM studentresult sr
		    WHERE (sr.deleted IS NULL OR sr.deleted = '0')  
		    AND sr.level_id='".$row["level_id"]."' 
		    AND sr.reg_no='".$reg_no."'";
		    $result_count = mysqli_query($connect, $query_count); 
		    //echo $query_start_date; exit();
		    while($row_count = mysqli_fetch_array($result_count))
		    {
		    $value .= '
		    <td>'.$row_count["total"].'</td>
		    ';
		      if ($row_last_date['complete'] == 'yes' AND $position != 'teacher') {
		        $value .= '
		        <td>
		        <a href="../class/fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('2').'" class="btn btn-info" style="pointer-events: none; opacity: 0.5;">Complete</a>
		        <a href="../class/fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('1').'" class="btn btn-danger">Not completed</a>
		        </td>
		        ';
		      }else{
		        $value .= '
		        <td>
		        <a href="../class/fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('2').'" class="btn btn-info">Complete</a>
		        <a href="../class/fetch_complete_level.php?reg_no='.base64_encode($reg_no).'&studentname='.base64_encode($row['studentname']).'&active='.base64_encode($active).'&id='.base64_encode($row_last_date['r_id']).'&complete='.base64_encode('1').'" class="btn btn-danger" style="pointer-events: none; opacity: 0.5;">Not completed</a>
		        </td>
		        ';
		        
		      }
		    }

		    if ($position == 'cm') {
		    $value .='
		    <td>
		     <select class="form-control" id="select_option_'.$level_id.'" name="select_option">
		          <option value="">Please select reason</option>
		          <option value="select wrong level">Choose the wrong level</option>
		          <option value="others">Others</option>
		        </select>
		        </td>
		        <td>
		        <a class="btn btn-danger" onclick="delelet('.$level_id.')"><i class="glyphicon glyphicon-remove"></i> Delete</a>
		        </td>
		        <script>
		        function delelet($level_id){
		            var box = document.getElementById("select_option_"+$level_id+"").value;
		            if(box == ""){
		              alert("Please select reason");
		            }else{

		              if (box == "others"){
		                var reason = prompt("Please enter your reason");
		                  if(reason.length != 0){
		                      var del=confirm("Are you sure you want to delete this level all Record?");
		                      if (del==true) {
		                        window.location.href = "../delete.php?level_reason="+reason+"&reg_no='.$reg_no.'&level_id="+$level_id+"&fid='.$_COOKIE['fid'].'&username='.$_COOKIE['name'].'&active='.$active.'&studentname='.$studentname.'";
		                        //alert("Pls la");
		                      }
		                  }else if (reason.length == 0){
		                        delelet($level_id);
		                  }
		              }else if (box == "select wrong level"){
		                var del=confirm("Are you sure you want to delete this level all Record?");
		                  if (del==true) {
		                    window.location.href = "../delete.php?level_reason="+box+"&reg_no='.$reg_no.'&level_id="+$level_id+"&fid='.$_COOKIE['fid'].'&username='.$_COOKIE['name'].'&active='.$active.'&studentname='.$studentname.'";
		                  }
		              }

		            }
		          }
		      </script>
		      ';
		      }else{
		        $value .='
		        <td></td>
		        <td></td>
		        ';

		      }
		      $value .='
		      </tr>
		        </tbody>
		      ';  
		  }

		 }

		}
		return($value);
	}
}
?>